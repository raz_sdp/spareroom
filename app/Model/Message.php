<?php
App::uses('AppModel', 'Model');

class Message extends AppModel {

	public $belongsTo = array(
		'Sender' => array(
            'className' => 'User',
            'foreignKey' => 'sender_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Receiver' => array(
            'className' => 'User',
            'foreignKey' => 'receiver_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
	);
}
