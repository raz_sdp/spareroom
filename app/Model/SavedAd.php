<?php
App::uses('AppModel', 'Model');
/**
 * SavedAd Model
 *
 * @property PropertyAd $PropertyAd
 * @property RentAd $RentAd
 * @property WantedAd $WantedAd
 * @property User $User
 */
class SavedAd extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'PropertyAd' => array(
			'className' => 'PropertyAd',
			'foreignKey' => 'property_ad_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'RentAd' => array(
			'className' => 'RentAd',
			'foreignKey' => 'rent_ad_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'WantedAd' => array(
			'className' => 'WantedAd',
			'foreignKey' => 'wanted_ad_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
