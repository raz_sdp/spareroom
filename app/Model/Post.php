<?php
App::uses('AppModel', 'Model');
/**
 * Post Model
 *
 * @property Comment $Comment
 */
class Post extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Comment' => array(
			'className' => 'Comment',
			'foreignKey' => 'post_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

    public $belongsTo = array(
        'BlogCategory' => array(
            'className' => 'BlogCategory',
            'foreignKey' => 'blog_category_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
