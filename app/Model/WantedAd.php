<?php
App::uses('AppModel', 'Model');

class WantedAd extends AppModel {

	public $displayField = 'title';

	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
