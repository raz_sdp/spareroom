<?php
App::uses('AppController', 'Controller');

class CmsUsersController extends AppController {

	public $components = array('Paginator', 'Session');

	public function admin_index() {
        $role = $this->Session->read('role');
        if($role!='admin') {
            return $this->redirect(array('action'=>'login'));
        }
        
        $conditions = array();
        $keyword = $this->request->params['named']['keyword'];
        if (!empty($keyword))
            $conditions = array('OR' =>
                array(
                    'CmsUser.name LIKE' => '%' . $keyword . '%',
                    'CmsUser.username LIKE' => '%' . $keyword . '%',
                    'CmsUser.email LIKE' => '%' . $keyword . '%'
                )
            );
        $this->paginate = array('all',
            'limit' => 20,
            'conditions' => $conditions,
        );
		$this->CmsUser->recursive = 0;
		$this->set('cmsUsers', $this->Paginator->paginate());
	}

	public function admin_add() {
        $role = $this->Session->read('role');
        if($role!='admin') {
            return $this->redirect(array('action'=>'login'));
        }
        
		if ($this->request->is('post')) {
            if(!empty($this->request->data['CmsUser']['simple_pwd'])){
                $this->request->data['CmsUser']['simple_pwd'] = $this->request->data['CmsUser']['simple_pwd'];
                $this->request->data['CmsUser']['password'] = AuthComponent::password($this->request->data['CmsUser']['simple_pwd']);
            }
			$this->CmsUser->create();
			if ($this->CmsUser->save($this->request->data)) {
				$this->Session->setFlash(__('The cms user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The cms user could not be saved. Please, try again.'));
			}
		}
	}

	public function admin_edit($id = null) {
        $role = $this->Session->read('role');
        if($role!='admin') {
            return $this->redirect(array('action'=>'login'));
        }

		if (!$this->CmsUser->exists($id)) {
			throw new NotFoundException(__('Invalid cms user'));
		}
		if ($this->request->is(array('post', 'put'))) {
            if(!empty($this->request->data['CmsUser']['simple_pwd'])){
                $this->request->data['CmsUser']['simple_pwd'] = $this->request->data['CmsUser']['simple_pwd'];
                $this->request->data['CmsUser']['password'] = AuthComponent::password($this->request->data['CmsUser']['simple_pwd']);
            }
            $this->CmsUser->id = $id;
			if ($this->CmsUser->save($this->request->data)) {
				$this->Session->setFlash(__('The cms user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The cms user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('CmsUser.' . $this->CmsUser->primaryKey => $id));
			$this->request->data = $this->CmsUser->find('first', $options);
		}
	}

	public function admin_delete($id = null) {
        $role = $this->Session->read('role');
        if($role!='admin') {
            return $this->redirect(array('action'=>'login'));
        }

		$this->CmsUser->id = $id;
		if (!$this->CmsUser->exists()) {
			throw new NotFoundException(__('Invalid cms user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->CmsUser->delete()) {
			$this->Session->setFlash(__('The cms user has been deleted.'));
		} else {
			$this->Session->setFlash(__('The cms user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

    public function admin_login(){
        $role = $this->Session->read('role');
        if ($this->request->is('post')) {

            $password = AuthComponent::password($this->request->data['CmsUser']['password']);
            #AuthComponent::_setTrace($password);
            $query = array(
                'conditions' => array(
                    'CmsUser.username' => $this->request->data['CmsUser']['username'],
                    'CmsUser.password' => $password,
                ),
            );
            $is_exist = $this->CmsUser->find('first', $query);
            if(!empty($is_exist)){
                $this->Session->write('role',$is_exist['CmsUser']['name']);
                $this->Auth->login($this->request->data['CmsUser']);
                return $this->redirect($this->Auth->redirect());
            } else {
                $this->Auth->logout();
                $this->Session->setFlash(__('Username or password is incorrect'));
            }
        }
    }

    public function admin_logout()
    {
        $this->Session->delete('role');
        $this->Auth->logout();
        return $this->redirect($this->Auth->logout());
    }

    public function admin_dashboard(){
        $this->loadModel('User');
        $this->loadModel('CmsUser');
        $this->loadModel('PropertyAd');
        $this->loadModel('RentAd');
        $this->loadModel('WantedAd');
        $user = $this->User->find('count');
        $cms_user = $this->CmsUser->find('count');
        $ads = $this->PropertyAd->find('count') + $this->RentAd->find('count') + $this->WantedAd->find('count');
        $this->set(compact('user', 'cms_user', 'ads'));
    }

    public function admin_ads_dashboard(){
        $this->loadModel('PropertyAd');
        $this->loadModel('RentAd');
        $this->loadModel('WantedAd');
        $property_ads = $this->PropertyAd->find('count');
        $rent_ads = $this->RentAd->find('count');
        $wanted_ads = $this->WantedAd->find('count');
        $this->set(compact('property_ads', 'rent_ads', 'wanted_ads'));
    }
}
