<?php
App::uses('AppController', 'Controller');

class BlogController extends AppController {

    public $components = array('Paginator', 'Session');

    public function index($category=""){
        //echo $category;die();
        $this->loadModel('Post');
        $this->loadModel('Comment');
        $this->loadModel('BlogCategory');

        $this->Paginator->settings = array(
            'limit' => 10,
            'order' => array(
                'Post.created' => 'desc'
            )
        );
        if($category!=null) {
            $categories_all = $this->BlogCategory->find('list',array(
                    'fields' => 'BlogCategory.id',
                    'conditions'=>array('BlogCategory.parent_id'=>$category)
                )
            );
            array_push($categories_all,$category);
            //AuthComponent::_setTrace($categories_all);
            $this->Paginator->settings['conditions'] = array('Post.blog_category_id' => $categories_all);
        }

        $posts = $this->paginate($this->Post);
        #AuthComponent::_setTrace($posts);

        $categories = $this->BlogCategory->find('all',array('conditions'=>array('BlogCategory.parent_id'=>null)));
        #AuthComponent::_setTrace($categories);

        $this->layout = 'public';
        $general_setting = $this->get_general_setting();
        $this->set(compact('general_setting','posts','categories'));
    }

    public function view($post_id=null){
        $this->loadModel('Post');
        $this->loadModel('Comment');
        $this->loadModel('User');
        $this->loadModel('BlogCategory');

        if (!$this->Post->exists($post_id)) {
            return $this->redirect(array('action'=>'index'));
        }

        $user_id = $this->Session->read('user_id');
        if(!empty($user_id)){
            $this->User->recursive = 0;
            $user = $this->User->findById($user_id);
        } else {
            $user = array();
        }

        $post = $this->Post->findById($post_id);
        foreach($post['Comment'] as $key=>$comment) {
            $this->User->recursive = 0;
            if(!empty($comment['user_id'])){
                $comment_user = $this->User->findById($comment['user_id']);
                if(empty($comment['name']))
                    $post['Comment'][$key]['name'] = $comment_user['User']['first_name'] . " " . $comment_user['User']['last_name'];
                if(empty($comment['email']))
                    $post['Comment'][$key]['email'] =  $comment_user['User']['email'];
            }
        }

        $categories = $this->BlogCategory->find('all',array('conditions'=>array('BlogCategory.parent_id'=>null)));

        $this->layout = 'public';
        $general_setting = $this->get_general_setting();
        $this->set(compact('general_setting','post','user','categories'));
    }

    public function comment($post_id=null) {
        $this->loadModel('Post');
        $this->loadModel('Comment');
        $this->loadModel('User');

        if (!$this->Post->exists($post_id)) {
            return $this->redirect(array('action'=>'index'));
        }

        if($this->request->is('post')) {
            $this->request->data['Comment']['post_id'] = $post_id;
            $this->Comment->save($this->request->data);
            return $this->redirect(array('action'=>'view',$post_id));
        }

        return $this->redirect(array('action'=>'index'));
    }



    public function admin_index($category_id=null) {
        $role = $this->Session->read('role');
        if($role=='manager') {
            return $this->redirect(array('controller'=>'cms_users','action'=>'login','admin'=>true));
        }

        $this->loadModel('Post');
        $this->loadModel('Comment');
        $conditions = array();
        $keyword = $this->request->params['named']['keyword'];
        if (!empty($keyword)) {
            $conditions = array('OR' =>
                array(
                    'Post.author LIKE' => '%' . $keyword . '%',
                    'Post.title LIKE' => '%' . $keyword . '%'
                )
            );
        }
        if($category_id!=null) {
            $conditions = am($conditions, array('Post.blog_category_id'=>$category_id));
        }

        $this->Paginator->settings = array(
            'limit' => 10,
            'conditions' => $conditions,
            'order' => array(
                'Post.created' => 'desc'
            )
        );
        $posts = $this->paginate($this->Post);
        $this->set(compact('posts'));
    }

    public function admin_add() {
        $role = $this->Session->read('role');
        if($role=='manager') {
            return $this->redirect(array('controller'=>'cms_users','action'=>'login','admin'=>true));
        }

        $this->loadModel('Post');
        if ($this->request->is('post')) {
            if(!empty($this->request->data['Post']['feature_img'])){
                $file_name = $this->_upload($this->request->data['Post']['feature_img'], 'blog_images');
                $this->request->data['Post']['feature_img'] = $file_name;
            }
            $this->Post->create();
            if ($this->Post->save($this->request->data)) {
                $this->Session->setFlash(__('The post has been saved.'));
                return $this->redirect(array('action' => 'index','admin'=>true));
            } else {
                $this->Session->setFlash(__('The post could not be saved. Please, try again.'));
            }
        }
        $this->request->data['Post']['author'] = $this->Auth->user('username');
        $blog_categories = $this->Post->BlogCategory->find('list');
        array_unshift($blog_categories, "None");
        $this->set(compact('blog_categories'));
    }

    public function admin_edit($id) {
        $role = $this->Session->read('role');
        if($role=='manager') {
            return $this->redirect(array('controller'=>'cms_users','action'=>'login','admin'=>true));
        }

        $this->loadModel('Post');
        $this->loadModel('Comment');

        if (!$this->Post->exists($id)) {
            return $this->redirect(array('action'=>'index','admin'=>true));
        }

        if($this->request->is(array('post', 'put'))){
            if (!empty($this->request->data['Post']['feature_img'])) {
                if(!empty($this->request->data['Post']['feature_img']['name'])){
                    $file_name = $this->_upload($this->request->data['Post']['feature_img'], 'blog_images');
                    $this->request->data['Post']['feature_img'] = $file_name;
                } else {
                    $options = array('conditions' => array('Post.' . $this->Post->primaryKey => $id));
                    $data = $this->Post->find('first', $options);
                    $this->request->data['Post']['feature_img'] = $data['Post']['feature_img'];
                }
            } else {
                unset($this->request->data['Post']['feature_img']);
            }
            $this->Post->id = $id;
            if ($this->Post->save($this->request->data)) {
                $this->Session->setFlash(__('The post has been saved.'));
            } else {
                $this->Session->setFlash(__('The post could not be saved. Please, try again.'));
            }
        } else {
            $blog_categories = $this->Post->BlogCategory->find('list');
            array_unshift($blog_categories, "None");
            $this->set(compact('blog_categories'));
            $this->request->data = $this->Post->findById($id);
        }
    }

    public function admin_delete($id) {
        $role = $this->Session->read('role');
        if($role=='manager') {
            return $this->redirect(array('controller'=>'cms_users','action'=>'login','admin'=>true));
        }

        $this->loadModel('Post');
        $this->loadModel('Comment');

        $this->Post->id = $id;
        if (!$this->Post->exists()) {
            throw new NotFoundException(__('Invalid Post'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Post->delete()) {
            $this->Session->setFlash(__('The Post has been deleted.'));
        } else {
            $this->Session->setFlash(__('The Post could not be deleted. Please, try again.'));
        }

        return $this->redirect(array('action'=>'index','admin'=>true));
    }

}