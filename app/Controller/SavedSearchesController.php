<?php
App::uses('AppController', 'Controller');

class SavedSearchesController extends AppController {

	public $components = array('Paginator', 'Session');

	public function index() {
		$this->SavedSearch->recursive = 0;
		$this->set('savedSearches', $this->Paginator->paginate());
	}

	public function view($id = null) {
		if (!$this->SavedSearch->exists($id)) {
			throw new NotFoundException(__('Invalid saved search'));
		}
		$options = array('conditions' => array('SavedSearch.' . $this->SavedSearch->primaryKey => $id));
		$this->set('savedSearch', $this->SavedSearch->find('first', $options));
	}

	public function add() {
		if ($this->request->is('post')) {
			$this->SavedSearch->create();
			if ($this->SavedSearch->save($this->request->data)) {
				$this->Session->setFlash(__('The saved search has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The saved search could not be saved. Please, try again.'));
			}
		}
		$users = $this->SavedSearch->User->find('list');
		$this->set(compact('users'));
	}

	public function edit($id = null) {
		if (!$this->SavedSearch->exists($id)) {
			throw new NotFoundException(__('Invalid saved search'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->SavedSearch->save($this->request->data)) {
				$this->Session->setFlash(__('The saved search has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The saved search could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('SavedSearch.' . $this->SavedSearch->primaryKey => $id));
			$this->request->data = $this->SavedSearch->find('first', $options);
		}
		$users = $this->SavedSearch->User->find('list');
		$this->set(compact('users'));
	}

	public function delete($id = null) {
		$this->SavedSearch->id = $id;
		if (!$this->SavedSearch->exists()) {
			throw new NotFoundException(__('Invalid saved search'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SavedSearch->delete()) {
			$this->Session->setFlash(__('The saved search has been deleted.'));
		} else {
			$this->Session->setFlash(__('The saved search could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function admin_index() {
		$this->SavedSearch->recursive = 0;
		$this->set('savedSearches', $this->Paginator->paginate());
	}

	public function admin_view($id = null) {
		if (!$this->SavedSearch->exists($id)) {
			throw new NotFoundException(__('Invalid saved search'));
		}
		$options = array('conditions' => array('SavedSearch.' . $this->SavedSearch->primaryKey => $id));
		$this->set('savedSearch', $this->SavedSearch->find('first', $options));
	}

	public function admin_add() {
		if ($this->request->is('post')) {
			$this->SavedSearch->create();
			if ($this->SavedSearch->save($this->request->data)) {
				$this->Session->setFlash(__('The saved search has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The saved search could not be saved. Please, try again.'));
			}
		}
		$users = $this->SavedSearch->User->find('list');
		$this->set(compact('users'));
	}

	public function admin_edit($id = null) {
		if (!$this->SavedSearch->exists($id)) {
			throw new NotFoundException(__('Invalid saved search'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->SavedSearch->save($this->request->data)) {
				$this->Session->setFlash(__('The saved search has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The saved search could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('SavedSearch.' . $this->SavedSearch->primaryKey => $id));
			$this->request->data = $this->SavedSearch->find('first', $options);
		}
		$users = $this->SavedSearch->User->find('list');
		$this->set(compact('users'));
	}

	public function admin_delete($id = null) {
		$this->SavedSearch->id = $id;
		if (!$this->SavedSearch->exists()) {
			throw new NotFoundException(__('Invalid saved search'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SavedSearch->delete()) {
			$this->Session->setFlash(__('The saved search has been deleted.'));
		} else {
			$this->Session->setFlash(__('The saved search could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
