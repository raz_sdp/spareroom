<?php
App::uses('AppController', 'Controller');

class BlogCategoriesController extends AppController {

	public $components = array('Paginator', 'Session');

	public function admin_index() {
		$role = $this->Session->read('role');
        if($role=='manager') {
            return $this->redirect(array('controller'=>'cms_users','action'=>'login','admin'=>true));
        }
        $conditions = array();
        $keyword = $this->request->params['named']['keyword'];
        if (!empty($keyword)) {
            $conditions = array('OR' =>
                array(
                    'BlogCategory.name LIKE' => '%' . $keyword . '%'
                )
            );
        }

        $this->Paginator->settings = array(
            'limit' => 10,
            'conditions' => $conditions
        );
		$this->BlogCategory->recursive = 0;
		$this->set('blogCategories', $this->Paginator->paginate());
	}

	public function admin_view($id = null) {
		$role = $this->Session->read('role');
        if($role=='manager') {
            return $this->redirect(array('controller'=>'cms_users','action'=>'login','admin'=>true));
        }
		if (!$this->BlogCategory->exists($id)) {
			throw new NotFoundException(__('Invalid blog category'));
		}
		$options = array('conditions' => array('BlogCategory.' . $this->BlogCategory->primaryKey => $id));
		$this->set('blogCategory', $this->BlogCategory->find('first', $options));
	}

	public function admin_add() {
		$role = $this->Session->read('role');
        if($role=='manager') {
            return $this->redirect(array('controller'=>'cms_users','action'=>'login','admin'=>true));
        }
		if ($this->request->is('post')) {
			if($this->request->data['BlogCategory']['parent_id'] == 0) {
				$this->request->data['BlogCategory']['parent_id'] = null;
			}
			$this->BlogCategory->create();
			if ($this->BlogCategory->save($this->request->data)) {
				$this->Session->setFlash(__('The blog category has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The blog category could not be saved. Please, try again.'));
			}
		}
		$parentBlogCategories = $this->BlogCategory->ParentBlogCategory->find('list');
		array_unshift($parentBlogCategories, "None");
		$this->set(compact('parentBlogCategories'));
	}

	public function admin_edit($id = null) {
		$role = $this->Session->read('role');
        if($role=='manager') {
            return $this->redirect(array('controller'=>'cms_users','action'=>'login','admin'=>true));
        }
		if (!$this->BlogCategory->exists($id)) {
			throw new NotFoundException(__('Invalid blog category'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if($this->request->data['BlogCategory']['parent_id'] == 0) {
				$this->request->data['BlogCategory']['parent_id'] = null;
			}
			$this->BlogCategory->id = $id;
			if ($this->BlogCategory->save($this->request->data)) {
				$this->Session->setFlash(__('The blog category has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The blog category could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('BlogCategory.' . $this->BlogCategory->primaryKey => $id));
			$this->request->data = $this->BlogCategory->find('first', $options);
		}
		$parentBlogCategories = $this->BlogCategory->ParentBlogCategory->find('list');
		array_unshift($parentBlogCategories, "None");
		$this->set(compact('parentBlogCategories'));
	}

	public function admin_delete($id = null) {
		$role = $this->Session->read('role');
        if($role=='manager') {
            return $this->redirect(array('controller'=>'cms_users','action'=>'login','admin'=>true));
        }
		$this->BlogCategory->id = $id;
		if (!$this->BlogCategory->exists()) {
			throw new NotFoundException(__('Invalid blog category'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->BlogCategory->delete()) {
			$this->Session->setFlash(__('The blog category has been deleted.'));
		} else {
			$this->Session->setFlash(__('The blog category could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
