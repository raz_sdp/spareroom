<?php

App::uses('Controller', 'Controller');


class AppController extends Controller {

    public $components = array(
        'Session', 'RequestHandler','Cookie',
        'Auth' => array(
            'loginAction' => array('controller' => 'cms_users', 'action' => 'login', 'admin' => true),
            'loginRedirect' => array('controller' => 'cms_users', 'action' => 'dashboard', 'admin' => true),
            'logoutRedirect' => array('controller' => 'cms_users', 'action' => 'login', 'admin' => true),
            'authError' => 'You are not allowed',
            'authenticate' => array(
                'Form' => array(
                    'fields' => array('username' => 'username', 'password' => 'password')
                )
            )
        )
    );
    public $helpers = array('Html');
    public function beforeFilter(){
        if($this->params['admin']){
            $this->layout =  'admin';
        }
        $this->Auth->allow();
        //$this->Auth->allow('display', 'login', 'register');
    }

    //upload image
    function _upload($file, $folder = null, $fileName = null){
        App::import('Vendor', 'phpthumb', array('file' => 'phpthumb' . DS . 'phpthumb.class.php'));
        if(is_uploaded_file($file['tmp_name'])){
            $ext  = strtolower(array_pop(explode('.',$file['name'])));
            if($ext == 'txt') $ext = 'jpg';
            $fileName = time() . rand(1,999) . '.' .$ext;
            if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif'){
                if($folder=="sliders")
                    $folder = $this->slider_img_dir();
                $uploadFile = WWW_ROOT.'files'.DS. $folder . DS .$fileName;
                if(move_uploaded_file($file['tmp_name'],$uploadFile)){
                    $dest_small = WWW_ROOT . 'files' . DS . $folder . DS . 'small' . DS . $fileName;
                    if($folder == 'logos'){
                        $this->_resize($uploadFile, $dest_small);
                    }
                    $this->_orientation($uploadFile, $uploadFile);
                    //@unlink($uploadFile);
                    return $fileName;
                }
            }
        }
    }

    private function _orientation($src, $dest_large){
        $phpThumb = new phpThumb();
        $phpThumb->resetObject();
        $capture_raw_data = false;
        $phpThumb->setSourceFilename($src);
        $phpThumb->setParameter('ar', 'x');     #auto rotate the image, no need to exif()
        $phpThumb->setParameter('f', 'jpeg');   # save output as jpg
        $phpThumb->GenerateThumbnail();
        $phpThumb->RenderToFile($dest_large);
    }
    //image resize
    private function _resize($src, $dest_small){
        $phpThumb = new phpThumb();
        $phpThumb->resetObject();
        $capture_raw_data = false;
        $phpThumb->setSourceFilename($src);
        $phpThumb->setParameter('w', 500);
        $phpThumb->setParameter('h', 400);
        $phpThumb->setParameter('ar', 'x');     #auto rotate the image, no need to exif()
        $phpThumb->setParameter('f', 'jpeg');   # save output as jpg
        //$phpThumb->setParameter('zc', 1);
        $phpThumb->GenerateThumbnail();
        $phpThumb->RenderToFile($dest_small);
    }

    public function lat_lang($address = null)
    {
        header('Content-Type: application/json');
        $this->autoRender = false;
        App::uses('HttpSocket', 'Network/Http');
        $HttpSocket = new HttpSocket();
        if($address!=null){

            $results = $HttpSocket->get('http://maps.googleapis.com/maps/api/geocode/json', array(
                'address' => $address,
                'sensor' => 'false'
            ));
            $lat_lng_obj = json_decode($results->body, true);
            return $lat_lng_obj;
        }
    }

    public function distance($lat1, $lon1, $lat2, $lon2) {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        return $miles;
    }

    public function get_lat_lng($lookup_postcode = null){
        header('Content-Type: application/json');
        $this->autoRender = false;
        // Client's credentials
        $account  = "2747";
        $password = "evxpio81";

        //First Load the Utility Class
        App::uses('Xml', 'Utility');

        // lets grab it by postcode
        $url = 'http://ws1.postcodesoftware.co.uk/lookup.asmx/getGeoData?account=' . $account . "&password=" . $password . "&postcode=" . $lookup_postcode;
        $xml = simplexml_load_file(str_replace(' ','', $url)); // Removes unnecessary spaces
        #AuthComponent::_setTrace($xml);
        if ($xml->ErrorNumber <> 0) // If an error has occurred, show message
        {
            return false;
        }
        else
        {
            $data = Xml::toArray(Xml::build($url));
            $lat_lng['lat'] = $data['GeoData']['Latitude'];
            $lat_lng['lng'] = $data['GeoData']['Longitude'];
            return $lat_lng;
        }

    }

    public function get_general_setting () {
        $this->loadModel("GeneralSetting");
        $this->GeneralSetting->recursive = 1;
        $general_setting = $this->GeneralSetting->find('first');
        #AuthComponent::_setTrace($general_setting);
        return $general_setting;
    }

    public function send_mail($receiver,$subject,$body,$plain_body){
        App::import('Vendor', 'PHPMailer', array('file' => 'PHPMailer' . DS . 'class.phpmailer.php'));

        $mail = new PHPMailer;

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'findmearoomtest@gmail.com';                 // SMTP username
        $mail->Password = 'fmartest';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
        $mail->From = "findmearoomtest@gmail.com";
        $mail->FromName = "findmearoomtest";

        $mail->addAddress($receiver, "Recepient Name");

        $mail->isHTML(true);
        $mail->Subject = $subject;
        $mail->Body = $body;
        $mail->AltBody = $plain_body;

        if(!$mail->send())
        {
            return false;
            //echo "Mailer Error: " . $mail->ErrorInfo;
        }
        else
        {
            return true;
            //echo "Message has been sent successfully";
        }
    }
}
