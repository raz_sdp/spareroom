<?php
App::uses('AppController', 'Controller');

class RentAdsController extends AppController {

	public $components = array('Paginator', 'Session');

	public function index() {
		$this->RentAd->recursive = 0;
		$this->set('rentAds', $this->Paginator->paginate());
	}

	public function view($id = null) {
		if (!$this->RentAd->exists($id)) {
			throw new NotFoundException(__('Invalid rent ad'));
		}
		$options = array('conditions' => array('RentAd.' . $this->RentAd->primaryKey => $id));
		$this->set('rentAd', $this->RentAd->find('first', $options));
	}

	public function add() {
        $this->loadModel("User");
		if ($this->request->is('post')) {
            $password = AuthComponent::password($this->request->data['User']['simple_pwd']);
            $query = array(
                'conditions' => array(
                    'User.email' => $this->request->data['User']['email'],
                    'User.password' => $password,
                ),
            );
            $is_exist = $this->User->find('first', $query);
            if(!empty($is_exist)){
                $this->request->data['RentAd']['user_id'] = $is_exist['User']['id'];
            } else {
                /*$this->Auth->logout();
                $this->Session->setFlash(__('Username or password is incorrect'));*/
            }
            $tmp = "";
            if(!empty($this->request->data['RentAd']['aminities'])) {
                foreach($this->request->data['RentAd']['aminities'] as $item)
                    $tmp .= $item . ", ";
            }
            $this->request->data['RentAd']['aminities'] = $tmp;

            $tmp = "";
            if(!empty($this->request->data['RentAd']['room_aminities'])) {
                foreach($this->request->data['RentAd']['room_aminities'] as $item)
                    $tmp .= $item . ", ";
            }
            $this->request->data['RentAd']['room_aminities'] = $tmp;

            $this->request->data['RentAd']['display_name'] = $this->request->data['RentAd']['display_name_first'] + $this->request->data['RentAd']['display_name_last'];
            //$this->request->data['RentAd']['station_id'] = 100;

            #AuthComponent::_setTrace($this->request->data);

            $this->RentAd->create();
			if ($this->RentAd->save($this->request->data)) {
				$this->Session->setFlash(__('The rent ad has been saved.'));
                return $this->redirect(array('controller'=>'pages', 'action' => 'display'));
			} else {
				$this->Session->setFlash(__('The rent ad could not be saved. Please, try again.'));
                return $this->redirect(array('controller'=>'pages', 'action' => 'display'));
			}
		}
	}

	public function edit($id = null) {
        $this->loadModel('User');
        $user_id = $this->Session->read('user_id');
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $user_id));
        $user = $this->User->find('first', $options);
        #AuthComponent::_setTrace($user);
        $wanted_ads = count($user['WantedAd']);
        $offered_ads = count($user['RentAd']);
        $rent_ads = count($user['RentAd']);
        $saved_searches = count($user['SavedSearch']);
        $saved_ads = count($user['SavedAd']);

        $this->loadModel('Message');
        $messages = $this->Message->find('all', array(
                'conditions' => array(
                    'receiver_id' => $user_id,
                    'has_read' => false
                )
            )
        );
        $messages = count($messages);

		if (!$this->RentAd->exists($id)) {
			throw new NotFoundException(__('Invalid rent ad'));
		}

        $user_id = $this->Session->read('user_id');
        if($id==null || empty($user_id) ) {
            return $this->redirect(array('controller'=>'pages', 'action' => 'display'));
        }

		if ($this->request->is(array('post', 'put'))) {
            #AuthComponent::_setTrace($this->request->data);
            for($i=1; $i<=5; $i++) {
                $no = "photo_" . $i;
                if (!empty($this->request->data['RentAd'][$no])) {
                    if(!empty($this->request->data['RentAd'][$no]['name'])){
                        $file_name = $this->_upload($this->request->data['RentAd'][$no], 'add_images');
                        $this->request->data['RentAd'][$no] = $file_name;
                    } else {
                        $options = array('conditions' => array('RentAd.' . $this->RentAd->primaryKey => $id));
                        $data = $this->RentAd->find('first', $options);
                        $this->request->data['RentAd'][$no] = $data['RentAd'][$no];
                    }
                } else {
                    unset($this->request->data['RentAd'][$no]);
                }
            }

            if(!empty($this->request->data['RentAd']['video_link'])) {
                $target_dir = WWW_ROOT.'files'.DS.'ad_videos';
                $target_file = $target_dir . basename($_FILES["RentAd"]['video_link']["name"]);
                if (move_uploaded_file($_FILES["RentAd"]['video_link']["tmp_name"], $target_file)) {
                    $this->request->data['RentAd']['video_link'] = $_FILES["RentAd"]['video_link']["tmp_name"];
                }
            }

            $tmp = "";
            if(!empty($this->request->data['RentAd']['aminities'])) {
                foreach($this->request->data['RentAd']['aminities'] as $item)
                    $tmp .= $item . ", ";
            }
            $this->request->data['RentAd']['aminities'] = $tmp;

            $tmp = "";
            if(!empty($this->request->data['RentAd']['room_aminities'])) {
                foreach($this->request->data['RentAd']['room_aminities'] as $item)
                    $tmp .= $item . ", ";
            }
            $this->request->data['RentAd']['room_aminities'] = $tmp;

            $this->request->data['RentAd']['display_name'] = $this->request->data['RentAd']['display_name_first'] + $this->request->data['RentAd']['display_name_last'];
            //$this->request->data['RentAd']['station_id'] = 100;

            #AuthComponent::_setTrace($this->request->data);
            $this->RentAd->id = $id;
			if ($this->RentAd->save($this->request->data)) {
				$this->Session->setFlash(__('The rent ad has been saved.'));
				return $this->redirect(array('action' => 'edit', $id));
			} else {
				$this->Session->setFlash(__('The rent ad could not be saved. Please, try again.'));
                return $this->redirect(array('action' => 'edit', $id));
			}
		} else {
            $options = array('conditions' => array('RentAd.' . $this->RentAd->primaryKey => $id));
            $this->request->data = $this->RentAd->find('first', $options);
            if($this->request->data['RentAd']['user_id'] != $user_id)
                return $this->redirect(array('controller'=>'pages', 'action' => 'display'));
        }

        $ad=$this->request->data;
        $table="rent";

        $general_setting = $this->get_general_setting();
        $this->set(compact('general_setting'));

        $this->set(compact('user','rent_ads','wanted_ads','offered_ads','saved_searches','saved_ads','messages'));
        $this->set(compact('ad','table'));
        $this->layout = 'public';
        $this->render('../Pages/edit_ad');
	}

	public function delete($id = null) {
		$this->RentAd->id = $id;
		if (!$this->RentAd->exists()) {
			throw new NotFoundException(__('Invalid rent ad'));
		}
		if ($this->RentAd->delete()) {
			$this->Session->setFlash(__('The rent ad has been deleted.'));
		} else {
			$this->Session->setFlash(__('The rent ad could not be deleted. Please, try again.'));
		}
        return $this->redirect(array('controller'=>'pages', 'action' => 'display'));
	}

	public function admin_index() {
        $role = $this->Session->read('role');
        if($role=='author') {
            return $this->redirect(array('controller'=>'cms_users','action'=>'login','admin'=>true));
        }
        $conditions = array();
        $keyword = $this->request->params['named']['keyword'];
        if (!empty($keyword))
            $conditions = array('OR' =>
                array(
                    'RentAd.area LIKE' => '%' . $keyword . '%',
                    'RentAd.street_name LIKE' => '%' . $keyword . '%',
                    'RentAd.postcode LIKE' => '%' . $keyword . '%',
                    'RentAd.title LIKE' => '%' . $keyword . '%',
                )
            );
        $this->paginate = array('all',
            'limit' => 20,
            'conditions' => $conditions,
        );
		$this->RentAd->recursive = 0;
		$this->set('rentAds', $this->Paginator->paginate());
	}

	public function admin_view($id = null) {
        $role = $this->Session->read('role');
        if($role=='author') {
            return $this->redirect(array('controller'=>'cms_users','action'=>'login','admin'=>true));
        }
		if (!$this->RentAd->exists($id)) {
			throw new NotFoundException(__('Invalid rent ad'));
		}
		return $this->redirect(array('controller'=>'searchEngine', 'action' => 'full_ad', 'rent_ads',$id, 'admin'=>false));
	}

	public function admin_add() {
		if ($this->request->is('post')) {
			$this->RentAd->create();
			if ($this->RentAd->save($this->request->data)) {
				$this->Session->setFlash(__('The rent ad has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The rent ad could not be saved. Please, try again.'));
			}
		}
		$users = $this->RentAd->User->find('list');
		$areas = $this->RentAd->Area->find('list');
		$this->set(compact('users', 'areas'));
	}

	public function admin_edit($id = null) {
		if (!$this->RentAd->exists($id)) {
			throw new NotFoundException(__('Invalid rent ad'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RentAd->save($this->request->data)) {
				$this->Session->setFlash(__('The rent ad has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The rent ad could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RentAd.' . $this->RentAd->primaryKey => $id));
			$this->request->data = $this->RentAd->find('first', $options);
		}
		$users = $this->RentAd->User->find('list');
		$areas = $this->RentAd->Area->find('list');
		$this->set(compact('users', 'areas'));
	}

	public function admin_delete($id = null) {
        $role = $this->Session->read('role');
        if($role=='author') {
            return $this->redirect(array('controller'=>'cms_users','action'=>'login','admin'=>true));
        }
		$this->RentAd->id = $id;
		if (!$this->RentAd->exists()) {
			throw new NotFoundException(__('Invalid rent ad'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RentAd->delete()) {
			$this->Session->setFlash(__('The rent ad has been deleted.'));
		} else {
			$this->Session->setFlash(__('The rent ad could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
