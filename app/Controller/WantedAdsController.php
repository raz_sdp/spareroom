<?php
App::uses('AppController', 'Controller');

class WantedAdsController extends AppController {

	public $components = array('Paginator', 'Session');

	public function index() {
		$this->WantedAd->recursive = 0;
		$this->set('wantedAds', $this->Paginator->paginate());
	}

	public function view($id = null) {
		if (!$this->WantedAd->exists($id)) {
			throw new NotFoundException(__('Invalid wanted ad'));
		}
		$options = array('conditions' => array('WantedAd.' . $this->WantedAd->primaryKey => $id));
		$this->set('wantedAd', $this->WantedAd->find('first', $options));
	}

	public function add() {
        $this->loadModel("User");
		if ($this->request->is('post')) {
            $password = AuthComponent::password($this->request->data['User']['simple_pwd']);
            $query = array(
                'conditions' => array(
                    'User.email' => $this->request->data['User']['email'],
                    'User.password' => $password
                ),
            );
            $is_exist = $this->User->find('first', $query);
            if(!empty($is_exist)){
                $this->request->data['WantedAd']['user_id'] = $is_exist['User']['id'];
            } else {
                /*$this->Auth->logout();
                $this->Session->setFlash(__('Username or password is incorrect'));*/
            }
            $temp = "";
            foreach($this->request->data['WantedAd']['aminities'] as $item)
                $temp .= $item . ", ";
            $this->request->data['WantedAd']['aminities'] = $temp;
            unset($this->request->data['User']);
            #AuthComponent::_setTrace($this->request->data);
			$this->WantedAd->create();
			if ($this->WantedAd->save($this->request->data)) {
				$this->Session->setFlash(__('The wanted ad has been saved.'));
                return $this->redirect(array('controller'=>'pages', 'action' => 'display'));
			} else {
				$this->Session->setFlash(__('The wanted ad could not be saved. Please, try again.'));
                return $this->redirect(array('controller'=>'pages', 'action' => 'display'));
			}
		}
	}

	public function edit($id = null) {
        $this->loadModel('User');
        $user_id = $this->Session->read('user_id');
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $user_id));
        $user = $this->User->find('first', $options);
        #AuthComponent::_setTrace($user);
        $wanted_ads = count($user['WantedAd']);
        $offered_ads = count($user['RentAd']);
        $rent_ads = count($user['RentAd']);
        $saved_searches = count($user['SavedSearch']);
        $saved_ads = count($user['SavedAd']);

        $this->loadModel('Message');
        $messages = $this->Message->find('all', array(
                'conditions' => array(
                    'receiver_id' => $user_id,
                    'has_read' => false
                )
            )
        );
        $messages = count($messages);

		if (!$this->WantedAd->exists($id)) {
			throw new NotFoundException(__('Invalid wanted ad'));
		}

        $user_id = $this->Session->read('user_id');
        if($id==null || empty($user_id) ) {
            return $this->redirect(array('controller'=>'pages', 'action' => 'display'));
        }

		if ($this->request->is(array('post', 'put'))) {

            $password = AuthComponent::password($this->request->data['User']['simple_pwd']);
            $query = array(
                'conditions' => array(
                    'User.email' => $this->request->data['User']['email'],
                    'User.password' => $password
                ),
            );
            $is_exist = $this->User->find('first', $query);
            if(!empty($is_exist)){
                $this->request->data['WantedAd']['user_id'] = $is_exist['User']['id'];
            } else {
                /*$this->Auth->logout();
                $this->Session->setFlash(__('Username or password is incorrect'));*/
            }
            $temp = "";
            foreach($this->request->data['WantedAd']['aminities'] as $item)
                $temp .= $item . ", ";
            $this->request->data['WantedAd']['aminities'] = $temp;
            unset($this->request->data['User']);

            $this->WantedAd->id = $id;
			if ($this->WantedAd->save($this->request->data)) {
				$this->Session->setFlash(__('The wanted ad has been saved.'));
                return $this->redirect(array('action' => 'edit', $id));
			} else {
				$this->Session->setFlash(__('The wanted ad could not be saved. Please, try again.'));
                return $this->redirect(array('action' => 'edit', $id));
			}
        } else {
            $options = array('conditions' => array('WantedAd.' . $this->WantedAd->primaryKey => $id));
            $this->request->data = $this->WantedAd->find('first', $options);
            if($this->request->data['WantedAd']['user_id'] != $user_id)
                return $this->redirect(array('controller'=>'pages', 'action' => 'display'));
        }
        $ad=$this->request->data;
        $table="wanted";

        $general_setting = $this->get_general_setting();
        $this->set(compact('general_setting'));

        $this->set(compact('user','rent_ads','wanted_ads','offered_ads','saved_searches','saved_ads','messages'));
        $this->set(compact('ad','table'));
        $this->layout = 'public';
        $this->render('../Pages/edit_ad');
	}

	public function delete($id = null) {
		$this->WantedAd->id = $id;
		if (!$this->WantedAd->exists()) {
			throw new NotFoundException(__('Invalid wanted ad'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->WantedAd->delete()) {
			$this->Session->setFlash(__('The wanted ad has been deleted.'));
		} else {
			$this->Session->setFlash(__('The wanted ad could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function admin_index() {
		$role = $this->Session->read('role');
        if($role=='author') {
            return $this->redirect(array('controller'=>'cms_users','action'=>'login','admin'=>true));
        }
		$conditions = array();
        $keyword = $this->request->params['named']['keyword'];
        if (!empty($keyword))
            $conditions = array('OR' =>
                array(
                    'WantedAd.genders LIKE' => '%' . $keyword . '%',
                    'WantedAd.looking_for LIKE' => '%' . $keyword . '%',
                    'WantedAd.area_ids LIKE' => '%' . $keyword . '%',
                    'WantedAd.title LIKE' => '%' . $keyword . '%',
                )
            );
        $this->paginate = array('all',
            'limit' => 20,
            'conditions' => $conditions,
        );
		$this->WantedAd->recursive = 0;
		$this->set('wantedAds', $this->Paginator->paginate());
	}

	public function admin_view($id = null) {
		$role = $this->Session->read('role');
        if($role=='author') {
            return $this->redirect(array('controller'=>'cms_users','action'=>'login','admin'=>true));
        }
		if (!$this->WantedAd->exists($id)) {
			throw new NotFoundException(__('Invalid wanted ad'));
		}
		return $this->redirect(array('controller'=>'searchEngine', 'action' => 'full_ad', 'wanted_ads',$id, 'admin'=>false));
	}

	public function admin_add() {
		if ($this->request->is('post')) {
			$this->WantedAd->create();
			if ($this->WantedAd->save($this->request->data)) {
				$this->Session->setFlash(__('The wanted ad has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The wanted ad could not be saved. Please, try again.'));
			}
		}
		$users = $this->WantedAd->User->find('list');
		$this->set(compact('users'));
	}

	public function admin_edit($id = null) {
		if (!$this->WantedAd->exists($id)) {
			throw new NotFoundException(__('Invalid wanted ad'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->WantedAd->save($this->request->data)) {
				$this->Session->setFlash(__('The wanted ad has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The wanted ad could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('WantedAd.' . $this->WantedAd->primaryKey => $id));
			$this->request->data = $this->WantedAd->find('first', $options);
		}
		$users = $this->WantedAd->User->find('list');
		$this->set(compact('users'));
	}

	public function admin_delete($id = null) {
		$role = $this->Session->read('role');
        if($role=='author') {
            return $this->redirect(array('controller'=>'cms_users','action'=>'login','admin'=>true));
        }
		$this->WantedAd->id = $id;
		if (!$this->WantedAd->exists()) {
			throw new NotFoundException(__('Invalid wanted ad'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->WantedAd->delete()) {
			$this->Session->setFlash(__('The wanted ad has been deleted.'));
		} else {
			$this->Session->setFlash(__('The wanted ad could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

    public function add_photo($id = null) {

    }
}
