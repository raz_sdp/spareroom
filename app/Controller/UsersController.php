<?php
App::uses('AppController', 'Controller');

class UsersController extends AppController {

	public $components = array('Paginator', 'Session');

    public function index() {
        $this->User->recursive = 0;
        $this->set('users', $this->Paginator->paginate());
    }

	public function view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

	public function register() {
        $this->autoRender = false;
        $this->autoLayout = false;
		if ($this->request->is('post')) {
            if(!empty($this->request->data['User']['simple_pwd'])){
                $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['simple_pwd']);
            }
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				//$this->Session->setFlash(__('The user has been saved.'));
                $this->Session->write('user_id' , $this->User->id);
				return $this->redirect(array('controller'=>'pages', 'action' => 'display'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		}
	}

    public function login(){
        if ($this->request->is('post')) {
            $password = AuthComponent::password($this->request->data['User']['password']);
            $query = array(
                'conditions' => array(
                    'User.email' => $this->request->data['User']['email'],
                    'User.password' => $password,
                ),
            );
            $is_exist = $this->User->find('first', $query);
            if(!empty($is_exist)){
                $this->Session->write('user_id' , $is_exist['User']['id']);
                return $this->redirect(array('action' => 'my_account'));
            } else {
                /*$this->Auth->logout();
                $this->Session->setFlash(__('Username or password is incorrect'));*/
            }
        }
    }

	public function edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
	}

	public function delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('The user has been deleted.'));
		} else {
			$this->Session->setFlash(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function admin_index() {
        $role = $this->Session->read('role');
        if($role=='author') {
            return $this->redirect(array('controller'=>'cms_users','action'=>'login','admin'=>true));
        }
        $conditions = array();
        $keyword = $this->request->params['named']['keyword'];
        if (!empty($keyword))
            $conditions = array('OR' =>
                array(
                    'User.first_name LIKE' => '%' . $keyword . '%',
                    'User.last_name LIKE' => '%' . $keyword . '%',
                    'User.email LIKE' => '%' . $keyword . '%',
                )
            );
        $this->paginate = array('all',
            'limit' => 20,
            'conditions' => $conditions,
        );
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
	}

	public function admin_view($id = null) {
        $role = $this->Session->read('role');
        if($role=='author') {
            return $this->redirect(array('controller'=>'cms_users','action'=>'login','admin'=>true));
        }
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

	public function admin_delete($id = null) {
        $role = $this->Session->read('role');
        if($role=='author') {
            return $this->redirect(array('controller'=>'cms_users','action'=>'login','admin'=>true));
        }
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('The user has been deleted.'));
		} else {
			$this->Session->setFlash(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

    public function my_account(){
        $user_id = $this->Session->read('user_id');
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $user_id));
        $user = $this->User->find('first', $options);
        if(empty($user)){
            $this->Session->delete('user_id');
            return $this->redirect(array('controller'=>'pages','action'=>'display'));
        }
        #AuthComponent::_setTrace($user);
        $wanted_ads = count($user['WantedAd']);
        $offered_ads = count($user['RentAd']);
        $saved_searches = count($user['SavedSearch']);
        $saved_ads = count($user['SavedAd']);

        $this->loadModel('Message');
        $messages = $this->Message->find('all', array(
                'conditions' => array(
                    'receiver_id' => $user_id,
                    'has_read' => false
                )
            )
        );
        $messages = count($messages);

        $general_setting = $this->get_general_setting();
        $this->set(compact('general_setting'));

        $this->set(compact('user','wanted_ads','offered_ads','saved_searches','saved_ads','messages'));
        $this->layout = 'public';

    }

    public function my_messages(){
        $user_id = $this->Session->read('user_id');
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $user_id));
        $user = $this->User->find('first', $options);
        if(empty($user)){
            $this->Session->delete('user_id');
            return $this->redirect(array('controller'=>'pages','action'=>'display'));
        }
        #AuthComponent::_setTrace($user);
        $wanted_ads = count($user['WantedAd']);
        $offered_ads = count($user['RentAd']);
        $rent_ads = count($user['RentAd']);
        $saved_searches = count($user['SavedSearch']);
        $saved_ads = count($user['SavedAd']);
        $this->loadModel('Message');
        $messages = $this->Message->find('all', array(
                'conditions' => array(
                    'receiver_id' => $user_id,
                    'has_read' => false
                )
            )
        );
        $messages_all = $messages;
        $messages = count($messages);

        $general_setting = $this->get_general_setting();
        $this->set(compact('general_setting'));

        $this->set(compact('user','rent_ads','wanted_ads','offered_ads','saved_searches','saved_ads','messages','messages_all'));
        $this->layout = 'public';
    }

    public function mark_read($id) {
        $this->loadModel('Message');
        $data = $this->Message->findById($id);
        $data['Message']['has_read'] = 1;

        $this->Message->id = $id;
        $this->Message->save($data);
        return $this->redirect(array('action'=>'my_messages'));
    }

    public function my_ads(){
        $user_id = $this->Session->read('user_id');
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $user_id));
        $user = $this->User->find('first', $options);
        if(empty($user)){
            $this->Session->delete('user_id');
            return $this->redirect(array('controller'=>'pages','action'=>'display'));
        }
        #AuthComponent::_setTrace($user);
        $wanted_ads = count($user['WantedAd']);
        $offered_ads = count($user['RentAd']);
        $rent_ads = count($user['RentAd']);
        $saved_searches = count($user['SavedSearch']);
        $saved_ads = count($user['SavedAd']);

        $this->loadModel('Message');
        $messages = $this->Message->find('all', array(
                'conditions' => array(
                    'receiver_id' => $user_id,
                    'has_read' => false
                )
            )
        );
        $messages = count($messages);

        $general_setting = $this->get_general_setting();
        $this->set(compact('general_setting'));

        $this->set(compact('user','rent_ads','wanted_ads','offered_ads','saved_searches','saved_ads','messages'));
        $this->layout = 'public';
    }

    public function saved_searches(){
        $user_id = $this->Session->read('user_id');
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $user_id));
        $user = $this->User->find('first', $options);
        if(empty($user)){
            $this->Session->delete('user_id');
            return $this->redirect(array('controller'=>'pages','action'=>'display'));
        }
        #AuthComponent::_setTrace($user);
        $wanted_ads = count($user['WantedAd']);
        $offered_ads = count($user['RentAd']);
        $rent_ads = count($user['RentAd']);
        $saved_searches = count($user['SavedSearch']);
        $saved_ads = count($user['SavedAd']);
        $this->loadModel('Message');
        $messages = $this->Message->find('all', array(
                'conditions' => array(
                    'receiver_id' => $user_id,
                    'has_read' => false
                )
            )
        );
        $messages = count($messages);

        $general_setting = $this->get_general_setting();
        $this->set(compact('general_setting'));

        $this->set(compact('user','rent_ads','wanted_ads','offered_ads','saved_searches','saved_ads','messages'));
        $this->layout = 'public';
    }

    public function saved_ads(){
        $user_id = $this->Session->read('user_id');
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $user_id));
        $user = $this->User->find('first', $options);
        if(empty($user)){
            $this->Session->delete('user_id');
            return $this->redirect(array('controller'=>'pages','action'=>'display'));
        }
        #AuthComponent::_setTrace($user);
        $wanted_ads = count($user['WantedAd']);
        $offered_ads = count($user['RentAd']);
        $rent_ads = count($user['RentAd']);
        $saved_searches = count($user['SavedSearch']);
        $saved_ads = count($user['SavedAd']);
        $this->loadModel('Message');
        $messages = $this->Message->find('all', array(
                'conditions' => array(
                    'receiver_id' => $user_id,
                    'has_read' => false
                )
            )
        );
        $messages = count($messages);

        $r_key=0;
        $w_key=0;
        $p_key=0;
        $saved_ad_rent=array();
        $saved_ad_wanted=array();
        $saved_ad_prop=array();
        foreach($user['SavedAd'] as $key => $saved_ad) {
            if($saved_ad['add_type'] == 'rent_ad') {
                $this->loadModel('RentAd');
                $saved_ad_rent[$r_key] = $this->RentAd->findById($saved_ad['rent_ad_id']);
                $r_key++;
            } else if($saved_ad['add_type'] == 'wanted_ad') {
                $this->loadModel('WantedAd');
                $saved_ad_wanted[$w_key] = $this->WantedAd->findById($saved_ad['wanted_ad_id']);
                $w_key++;
            } else if($saved_ad['add_type'] == 'property_ad') {
                $this->loadModel('PropertyAd');
                $saved_ad_prop[$p_key] = $this->PropertyAd->findById($saved_ad['property_ad_id']);
                $p_key++;
            }
        }
        #AuthComponent::_setTrace($saved_ad_rent);

        $general_setting = $this->get_general_setting();
        $this->set(compact('general_setting'));

        $this->set(compact('user','rent_ads','wanted_ads','offered_ads','saved_searches','saved_ads','messages','saved_ad_rent','saved_ad_wanted'));
        $this->layout = 'public';
    }

    public function save_ad($type=null,$id=null) {
        $this->loadModel('SavedAd');
        $user_id = $this->Session->read('user_id');
        if(empty($user_id)) {
            $this->redirect(array('controller'=>'pages','action'=>'display','login'));
        }
        if($type==null || $id==null ) {
            $this->redirect(array('controller'=>'pages','action'=>'display'));
        }

        switch($type){
            case "RentAd":
                $type = "rent_ad"; break;
            case "WantedAd":
                $type = "wanted_ad"; break;
            case "PropertyAd":
                $type = "property_ad"; break;
        }

        $is_exist = $this->SavedAd->find('all', array(
                'recursive' => -1,
                'conditions'=> array(
                    'SavedAd.user_id' => $user_id,
                    'SavedAd.'.$type.'_id' => $id
                )
            )
        );
        if(empty($is_exist)) {
            $data['SavedAd']['add_type'] = $type;
            $data['SavedAd'][$type.'_id'] = $id;
            $data['SavedAd']['user_id'] = $user_id;

            $this->SavedAd->save($data);
        }

        $this->redirect(array('controller'=>'searchEngine','action'=>'full_ad', $type."s", $id));
    }

    /*public function edit_ad($table=null, $id=null){
        $this->autoRender = false;
        $user_id = $this->Session->read('user_id');
        if($table==null || $id==null || empty($user_id) ) {
            return $this->redirect(array('controller'=>'pages', 'action' => 'display'));
        }
        if($table == 'rent') {
            $this->loadModel('RentAd');
            $ad = $this->RentAd->findById($id);
            if($user_id != $ad['RentAd']['user_id'])
                return $this->redirect(array('controller'=>'pages', 'action' => 'display'));
        }
        else if($table == 'property') {
            $this->loadModel('PropertyAd');
            $ad = $this->PropertyAd->findById($id);
            if($user_id != $ad['PropertyAd']['user_id'])
                return $this->redirect(array('controller'=>'pages', 'action' => 'display'));
        }
        else if($table == 'wanted') {
            $this->loadModel('WantedAd');
            $ad = $this->WantedAd->findById($id);
            if($user_id != $ad['WantedAd']['user_id'])
                return $this->redirect(array('controller'=>'pages', 'action' => 'display'));
        }

        $this->layout = 'public';
        $this->request->data = $ad;
        $this->set(compact('ad','table'));
        $this->render('../Pages/edit_ad');

    }*/

    public function logout(){
        $this->Session->delete('user_id');
        return $this->redirect(array('controller'=>'pages', 'action' => 'display'));
    }

    public function send_email() {
        #AuthComponent::_setTrace($this->request->data);

        if(!$this->request->is('post')) {
            return $this->redirect(array('controller'=>'pages','action' => 'display'));
        }

        $this->loadModel('User');
        if($this->request->data['ad_table'] == "RentAd") {
            $this->loadModel('RentAd');
            $ad = $this->RentAd->findById($this->request->data['ad_id']);
            $subject = $ad['RentAd']['title'];
            $ad_page = "rent_ads";
        } else if($this->request->data['ad_table'] == "WantedAd") {
            $this->loadModel('WantedAd');
            $ad = $this->WantedAd->findById($this->request->data['ad_id']);
            $subject = $ad['WantedAd']['title'];
            $ad_page = "wanted_ads";
        }

        $sender = $this->User->findById($this->request->data['user_id']);
        $receiver = $ad['User']['email'];
        $body = "By: " . $sender['User']['first_name'] . " " . $sender['User']['last_name'];
        $body .= "(<a href=\"". $sender['User']['email'] ."\">". $sender['User']['email'] ."</a>)";
        $body .= "<br><br>";
        $body .=  $this->request->data['message'];
        $plain_body = $this->request->data['message'];

        $this->loadModel('Message');
        $data['Message']['message'] = $plain_body;
        $data['Message']['sender_id'] = $this->request->data['user_id'];
        $data['Message']['receiver_id'] = $ad['User']['id'];
        $data['Message']['has_read'] = 0;
        $this->Message->save($data);

        if($this->send_mail($receiver,$subject,$body,$plain_body)) {
            $this->Session->setFlash(__('The mail has been sent.'));
            return $this->redirect(array('controller'=>'searchEngine','action' => 'full_ad',$ad_page,$this->request->data['ad_id']));

        } else {
            $this->Session->setFlash(__('The mail can\'t be sent.'));
            return $this->redirect(array('controller'=>'searchEngine','action' => 'full_ad',$ad_page,$this->request->data['ad_id']));
        }
    }

    public function contact() {
        if(!$this->request->is('post')) {
            return $this->redirect(array('controller'=>'pages','action' => 'display'));
        }
        $receiver = 'findmearoomtest@gmail.com';
        $body = "By: " . $this->request->data['name'];
        $body .= "(<a href=\"". $this->request->data['email'] ."\">". $this->request->data['email'] ."</a>)";
        $body .= "<br><br>";
        $body .=  $this->request->data['message'];
        $plain_body = $this->request->data['message'];
        $subject = $this->request->data['subject'];

        if($this->send_mail($receiver,$subject,$body,$plain_body)) {
            $this->Session->setFlash(__('The mail has been sent.'));
            return $this->redirect(array('controller'=>'pages','action' => 'display'));

        } else {
            $this->Session->setFlash(__('The mail can\'t be sent.'));
            return $this->redirect(array('controller'=>'pages','action' => 'display'));
        }
    }

    public function forgot_password() {

        //$this->send_mail($receiver,$subject,$body);
    }
}
