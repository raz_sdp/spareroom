<?php
App::uses('AppController', 'Controller');
/**
 * SavedAds Controller
 *
 * @property SavedAd $SavedAd
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SavedAdsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SavedAd->recursive = 0;
		$this->set('savedAds', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->SavedAd->exists($id)) {
			throw new NotFoundException(__('Invalid saved ad'));
		}
		$options = array('conditions' => array('SavedAd.' . $this->SavedAd->primaryKey => $id));
		$this->set('savedAd', $this->SavedAd->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->SavedAd->create();
			if ($this->SavedAd->save($this->request->data)) {
				$this->Session->setFlash(__('The saved ad has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The saved ad could not be saved. Please, try again.'));
			}
		}
		$propertyAds = $this->SavedAd->PropertyAd->find('list');
		$rentAds = $this->SavedAd->RentAd->find('list');
		$wantedAds = $this->SavedAd->WantedAd->find('list');
		$users = $this->SavedAd->User->find('list');
		$this->set(compact('propertyAds', 'rentAds', 'wantedAds', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->SavedAd->exists($id)) {
			throw new NotFoundException(__('Invalid saved ad'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->SavedAd->save($this->request->data)) {
				$this->Session->setFlash(__('The saved ad has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The saved ad could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('SavedAd.' . $this->SavedAd->primaryKey => $id));
			$this->request->data = $this->SavedAd->find('first', $options);
		}
		$propertyAds = $this->SavedAd->PropertyAd->find('list');
		$rentAds = $this->SavedAd->RentAd->find('list');
		$wantedAds = $this->SavedAd->WantedAd->find('list');
		$users = $this->SavedAd->User->find('list');
		$this->set(compact('propertyAds', 'rentAds', 'wantedAds', 'users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->SavedAd->id = $id;
		if (!$this->SavedAd->exists()) {
			throw new NotFoundException(__('Invalid saved ad'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SavedAd->delete()) {
			$this->Session->setFlash(__('The saved ad has been deleted.'));
		} else {
			$this->Session->setFlash(__('The saved ad could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->SavedAd->recursive = 0;
		$this->set('savedAds', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->SavedAd->exists($id)) {
			throw new NotFoundException(__('Invalid saved ad'));
		}
		$options = array('conditions' => array('SavedAd.' . $this->SavedAd->primaryKey => $id));
		$this->set('savedAd', $this->SavedAd->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->SavedAd->create();
			if ($this->SavedAd->save($this->request->data)) {
				$this->Session->setFlash(__('The saved ad has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The saved ad could not be saved. Please, try again.'));
			}
		}
		$propertyAds = $this->SavedAd->PropertyAd->find('list');
		$rentAds = $this->SavedAd->RentAd->find('list');
		$wantedAds = $this->SavedAd->WantedAd->find('list');
		$users = $this->SavedAd->User->find('list');
		$this->set(compact('propertyAds', 'rentAds', 'wantedAds', 'users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->SavedAd->exists($id)) {
			throw new NotFoundException(__('Invalid saved ad'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->SavedAd->save($this->request->data)) {
				$this->Session->setFlash(__('The saved ad has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The saved ad could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('SavedAd.' . $this->SavedAd->primaryKey => $id));
			$this->request->data = $this->SavedAd->find('first', $options);
		}
		$propertyAds = $this->SavedAd->PropertyAd->find('list');
		$rentAds = $this->SavedAd->RentAd->find('list');
		$wantedAds = $this->SavedAd->WantedAd->find('list');
		$users = $this->SavedAd->User->find('list');
		$this->set(compact('propertyAds', 'rentAds', 'wantedAds', 'users'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->SavedAd->id = $id;
		if (!$this->SavedAd->exists()) {
			throw new NotFoundException(__('Invalid saved ad'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SavedAd->delete()) {
			$this->Session->setFlash(__('The saved ad has been deleted.'));
		} else {
			$this->Session->setFlash(__('The saved ad could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
