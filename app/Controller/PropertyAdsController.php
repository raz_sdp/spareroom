<?php
App::uses('AppController', 'Controller');

class PropertyAdsController extends AppController {

	public $components = array('Paginator', 'Session');

	public function index() {
		$this->PropertyAd->recursive = 0;
		$this->set('propertyAds', $this->Paginator->paginate());
	}

	public function view($id = null) {
		if (!$this->PropertyAd->exists($id)) {
			throw new NotFoundException(__('Invalid property ad'));
		}
		$options = array('conditions' => array('PropertyAd.' . $this->PropertyAd->primaryKey => $id));
		$this->set('propertyAd', $this->PropertyAd->find('first', $options));
	}

	public function add() {
        $this->loadModel("User");
		if ($this->request->is('post')) {
            $password = AuthComponent::password($this->request->data['User']['simple_pwd']);
            $query = array(
                'conditions' => array(
                    'User.email' => $this->request->data['User']['email'],
                    'User.password' => $password
                ),
            );
            $is_exist = $this->User->find('first', $query);
            if(!empty($is_exist)){
                $this->request->data['PropertyAd']['user_id'] = $is_exist['User']['id'];
            } else {
                /*$this->Auth->logout();
                $this->Session->setFlash(__('Username or password is incorrect'));*/
            }
            $tmp = "";
            if(!empty($this->request->data['PropertyAd']['aminities'])) {
                foreach($this->request->data['PropertyAd']['aminities'] as $item)
                    $tmp .= $item . ", ";
            }
            $this->request->data['PropertyAd']['aminities'] = $tmp;

            $this->request->data['PropertyAd']['display_name'] = $this->request->data['PropertyAd']['display_name_first'] + $this->request->data['PropertyAd']['display_name_last'];
            //$this->request->data['RentAd']['station_id'] = 100;

            #AuthComponent::_setTrace($this->request->data);

			$this->PropertyAd->create();
			if ($this->PropertyAd->save($this->request->data)) {
				$this->Session->setFlash(__('The property ad has been saved.'));
                return $this->redirect(array('controller'=>'pages', 'action' => 'display'));
			} else {
				$this->Session->setFlash(__('The property ad could not be saved. Please, try again.'));
                return $this->redirect(array('controller'=>'pages', 'action' => 'display'));
			}
		}
	}

	public function edit($id = null) {
        $this->loadModel('User');
        $user_id = $this->Session->read('user_id');
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $user_id));
        $user = $this->User->find('first', $options);
        #AuthComponent::_setTrace($user);
        $wanted_ads = count($user['WantedAd']);
        $offered_ads = count($user['RentAd']);
        $rent_ads = count($user['RentAd']);
        $saved_searches = count($user['SavedSearch']);
        $saved_ads = count($user['SavedAd']);

        $this->loadModel('Message');
        $messages = $this->Message->find('all', array(
                'conditions' => array(
                    'receiver_id' => $user_id,
                    'has_read' => false
                )
            )
        );
        $messages = count($messages);
		if (!$this->PropertyAd->exists($id)) {
			throw new NotFoundException(__('Invalid property ad'));
		}
        $user_id = $this->Session->read('user_id');
        if($id==null || empty($user_id) ) {
            return $this->redirect(array('controller'=>'pages', 'action' => 'display'));
        }
		if ($this->request->is(array('post', 'put'))) {

            if (!empty($this->request->data['PropertyAd']['photo_1'])) {
                if(!empty($this->request->data['PropertyAd']['photo_1']['name'])){
                    $file_name = $this->_upload($this->request->data['PropertyAd']['photo_1'], 'ad_images');
                    $this->request->data['PropertyAd']['logo'] = $file_name;
                } else {
                    $options = array('conditions' => array('PropertyAd.' . $this->PropertyAd->primaryKey => $id));
                    $data = $this->PropertyAd->find('first', $options);
                    $this->request->data['PropertyAd']['photo_1'] = $data['PropertyAd']['photo_1'];
                }
            } else {
                unset($this->request->data['PropertyAd']['photo_1']);
            }
            $tmp = "";
            if(!empty($this->request->data['PropertyAd']['aminities'])) {
                foreach($this->request->data['PropertyAd']['aminities'] as $item)
                    $tmp .= $item . ", ";
            }
            $this->request->data['PropertyAd']['aminities'] = $tmp;

            $this->request->data['PropertyAd']['display_name'] = $this->request->data['PropertyAd']['display_name_first'] + $this->request->data['PropertyAd']['display_name_last'];

            $this->PropertyAd->id = $id;
			if ($this->PropertyAd->save($this->request->data)) {
				$this->Session->setFlash(__('The property ad has been saved.'));
                return $this->redirect(array('action' => 'edit', $id));
			} else {
				$this->Session->setFlash(__('The property ad could not be saved. Please, try again.'));
                return $this->redirect(array('action' => 'edit', $id));
			}
		} else {
			$options = array('conditions' => array('PropertyAd.' . $this->PropertyAd->primaryKey => $id));
			$this->request->data = $this->PropertyAd->find('first', $options);
            if($this->request->data['PropertyAd']['user_id'] != $user_id)
                return $this->redirect(array('controller'=>'pages', 'action' => 'display'));
		}
        $ad=$this->request->data;
        $table="property";

        $general_setting = $this->get_general_setting();
        $this->set(compact('general_setting'));

        $this->set(compact('user','rent_ads','wanted_ads','offered_ads','saved_searches','saved_ads','messages'));
        $this->set(compact('ad','table'));
        $this->layout = 'public';
        $this->render('../Pages/edit_ad');
	}

	public function delete($id = null) {
		$this->PropertyAd->id = $id;
		if (!$this->PropertyAd->exists()) {
			throw new NotFoundException(__('Invalid property ad'));
		}
		if ($this->PropertyAd->delete()) {
			$this->Session->setFlash(__('The property ad has been deleted.'));
		} else {
			$this->Session->setFlash(__('The property ad could not be deleted. Please, try again.'));
		}
        return $this->redirect(array('controller'=>'pages', 'action' => 'display'));
	}

	public function admin_index() {
		$role = $this->Session->read('role');
        if($role=='author') {
            return $this->redirect(array('controller'=>'cms_users','action'=>'login','admin'=>true));
        }
		$conditions = array();
        $keyword = $this->request->params['named']['keyword'];
        if (!empty($keyword))
            $conditions = array('OR' =>
                array(
                    'PropertyAd.street_name LIKE' => '%' . $keyword . '%',
                    'PropertyAd.postcode LIKE' => '%' . $keyword . '%',
                    'PropertyAd.title LIKE' => '%' . $keyword . '%',
                )
            );
        $this->paginate = array('all',
            'limit' => 20,
            'conditions' => $conditions,
        );
		$this->PropertyAd->recursive = 0;
		$this->set('propertyAds', $this->Paginator->paginate());
	}

	public function admin_view($id = null) {
		$role = $this->Session->read('role');
        if($role=='author') {
            return $this->redirect(array('controller'=>'cms_users','action'=>'login','admin'=>true));
        }
		if (!$this->PropertyAd->exists($id)) {
			throw new NotFoundException(__('Invalid property ad'));
		}
		return $this->redirect(array('controller'=>'searchEngine', 'action' => 'full_ad', 'rent_ads',$id, 'admin'=>false));
	}

	public function admin_add() {
		if ($this->request->is('post')) {
			$this->PropertyAd->create();
			if ($this->PropertyAd->save($this->request->data)) {
				$this->Session->setFlash(__('The property ad has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The property ad could not be saved. Please, try again.'));
			}
		}
		$users = $this->PropertyAd->User->find('list');
		$areas = $this->PropertyAd->Area->find('list');
		$this->set(compact('users', 'areas'));
	}

	public function admin_edit($id = null) {
		if (!$this->PropertyAd->exists($id)) {
			throw new NotFoundException(__('Invalid property ad'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->PropertyAd->save($this->request->data)) {
				$this->Session->setFlash(__('The property ad has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The property ad could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('PropertyAd.' . $this->PropertyAd->primaryKey => $id));
			$this->request->data = $this->PropertyAd->find('first', $options);
		}
		$users = $this->PropertyAd->User->find('list');
		$areas = $this->PropertyAd->Area->find('list');
		$this->set(compact('users', 'areas'));
	}

	public function admin_delete($id = null) {
		$role = $this->Session->read('role');
        if($role=='author') {
            return $this->redirect(array('controller'=>'cms_users','action'=>'login','admin'=>true));
        }
		$this->PropertyAd->id = $id;
		if (!$this->PropertyAd->exists()) {
			throw new NotFoundException(__('Invalid property ad'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->PropertyAd->delete()) {
			$this->Session->setFlash(__('The property ad has been deleted.'));
		} else {
			$this->Session->setFlash(__('The property ad could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
