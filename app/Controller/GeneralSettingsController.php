<?php
App::uses('AppController', 'Controller');

class GeneralSettingsController extends AppController {

	public $components = array('Paginator', 'Session');

	public function admin_edit($id = 1) {
		$role = $this->Session->read('role');
        if($role=='author') {
            return $this->redirect(array('controller'=>'cms_users','action'=>'login','admin'=>true));
        }

		if (!$this->GeneralSetting->exists($id)) {
            $this->GeneralSetting->create();
            $this->GeneralSetting->save(array());
		}
		if ($this->request->is(array('post', 'put'))) {
            if (!empty($this->request->data['GeneralSetting']['logo'])) {
                if(!empty($this->request->data['GeneralSetting']['logo']['name'])){
                    $file_name = $this->_upload($this->request->data['GeneralSetting']['logo'], 'logos');
                    $this->request->data['GeneralSetting']['logo'] = $file_name;
                } else {
                    $options = array('conditions' => array('GeneralSetting.' . $this->GeneralSetting->primaryKey => $id));
                    $data = $this->GeneralSetting->find('first', $options);
                    $this->request->data['GeneralSetting']['logo'] = $data['GeneralSetting']['logo'];
                }
            } else {
                unset($this->request->data['GeneralSetting']['logo']);
            }
            $this->GeneralSetting->id = $id;
			if ($this->GeneralSetting->save($this->request->data)) {
				$this->Session->setFlash(__('The general setting has been saved.'));
				return $this->redirect(array('action' => 'edit'));
			} else {
				$this->Session->setFlash(__('The general setting could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('GeneralSetting.' . $this->GeneralSetting->primaryKey => $id));
			$this->request->data = $this->GeneralSetting->find('first', $options);
		}
	}
}
