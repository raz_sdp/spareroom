<?php
App::uses('AppController', 'Controller');

class SearchEngineController extends AppController {

    public function index(){
        $this->redirect(array('controller'=>'pages', 'action' => 'display', 'search'));
    }

    public function search($paginate=false, $limit=10, $offset=0, $sort_by=null){
        $this->loadModel('AdReference');
        $this->loadModel('WantedAd');
        $this->loadModel('RentAd');
        $this->loadModel('PropertyAd');

        if ($this->request->is('post') || $paginate==true) {
            if($paginate==true){
                $this->request->data = $this->Session->read('request');
                if(empty($this->request->data)){
                    $this->redirect(array('controller'=>'pages', 'action' => 'display', 'search'));
                }
            }
            #AuthComponent::_setTrace($this->request->data);

            $search_for = $this->request->data['search_for'];
            $search_by = $this->request->data['search_by'];
            $conditions = "";
            $is_advanced = $this->request->data['advance_search'];
            if($is_advanced) {
                if(isset($this->request->data['min_rent']) && isset($this->request->data['max_rent'])){
                    $min_rent = $this->request->data['min_rent'];
                    $max_rent = $this->request->data['max_rent'];
                    if($min_rent!=null && $max_rent!=null)
                        $conditions .= " AND rent BETWEEN $min_rent AND $max_rent";
                }
                if(isset($this->request->data['per_week'])){
                    $conditions .= " AND per_week = '" . $this->request->data['per_week'] . "'";
                }
                if(isset($this->request->data['bills_included']) && $search_for=="to_rent"){
                    if($this->request->data['bills_included'] == 1)
                        $conditions .= " AND bills_inc = 'Yes'";
                }
                if(isset($this->request->data['gender'])){
                    $gender = $this->request->data['gender'];
                    if($search_for == "to_rent") {
                        if($gender == 'Couple')
                            $conditions .= " AND new_mate_couples_welcomed = 'Yes'";
                        else
                            $conditions .= " AND new_mate_gender = '$gender' OR new_mate_gender='No Preference'";
                    } else {
                        if($gender == 'Couple')
                            $conditions .= " AND genders = '1 male 1 female'";
                        else if ($gender == 'Male')
                            $conditions .= " AND genders = '1 male' OR genders = '2 males'";
                        else if ($gender == 'Female')
                            $conditions .= " AND genders = '1 female' OR genders = '2 females'";
                    }
                }
                if(isset($this->request->data['room_size'])){
                    $room_size = $this->request->data['room_size'];
                    if($search_for == "to_rent" && $room_size!="dm") {
                        $conditions .= " AND room_size = '$room_size'";
                    }
                }

                if(isset($this->request->data['shared_living_room']) && $this->request->data['shared_living_room']==1){
                    if($search_for == "to_rent") {
                        $conditions .= " AND living_room = 'Yes'";
                    }
                }
                if(isset($this->request->data['smoking_allow']) && $this->request->data['smoking_allow']==1){
                    if($search_for == "to_rent") {
                        $conditions .= " AND new_mate_smoking = 'Yes' OR new_mate_smoking = 'No Preference'";
                    } else {
                        $conditions .= " AND smoker = 'Yes'";
                    }
                }

                if(isset($this->request->data['min_stay']) && $this->request->data['min_stay']!=0){
                    $conditions .= " AND min_stay > '" . $this->request->data['min_stay'] . "'";
                }
                if(isset($this->request->data['max_stay']) && $this->request->data['max_stay']!=0){
                    $conditions .= " AND max_stay < '" . $this->request->data['max_stay'] . "'";
                }
                if(isset($this->request->data['available']) && $this->request->data['available']=="date"){
                    if(isset($this->request->data['available_from'])){
                        $conditions .= " AND available_from < '" . $this->request->data['available_from'] . "'";
                    }
                }
                if(isset($this->request->data['pref_occupation'])){
                    $pref_occupation = $this->request->data['pref_occupation'];
                    if($search_for == "to_rent") {
                        if($pref_occupation != 'dm')
                            $conditions .= " AND new_mate_occupation = '" . $pref_occupation . "'";
                    } else {
                        if($pref_occupation != 'dm')
                            $conditions .= " AND occupation = '" . $pref_occupation . "'";
                    }
                }
                if(isset($this->request->data['pref_gender'])){
                    $pref_gender = $this->request->data['pref_gender'];
                    if($search_for == "to_rent") {
                        if($pref_gender != 'dm' || $pref_gender != 'mixed')
                            $conditions .= " AND new_mate_gender = '" . $pref_gender . "'";
                    }
                }
                if(isset($this->request->data['pref_min_age']) && isset($this->request->data['pref_max_age'])){
                    $pref_min_age = $this->request->data['pref_min_age'];
                    $pref_max_age = $this->request->data['pref_max_age'];
                    if( ($pref_min_age!=null && $pref_min_age!=0) && ($pref_max_age!=null && $pref_max_age!=0) ) {
                        if($search_for != "to_rent") {
                            $conditions .= " AND age BETWEEN $pref_min_age AND $pref_max_age";
                        }
                    }
                }

                if(isset($this->request->data['days_avail'])){
                    if($this->request->data['days_avail']!='dm')
                        $conditions .= " AND days_available = '". $this->request->data['days_avail'] . "'";
                }

                if(isset($this->request->data['advertiser_role'])  && $search_for=="to_rent"){
                    if($this->request->data['advertiser_role']=='agent')
                        $conditions .= " AND advertiser_role = 'Agent'";
                    else if($this->request->data['advertiser_role']=='private')
                        $conditions .= " AND advertiser_role <> 'Agent'";
                }
            }

            if($sort_by != null) {
                if($sort_by == "created")
                    $order = "created DESC";
                elseif($sort_by == "modified")
                    $order = "modified DESC";
                elseif($sort_by == "low_price")
                    $order = "rent ASC";
                elseif($sort_by == "high_price")
                    $order = "rent DESC";
            } else {
                $order = "created DESC";
            }

            $distance = null;
            if($search_for == "to_rent") {
                if($search_by == "location") {
                    $area_postcode = $this->request->data['area_postcode'];
                } else if($search_by == "travel_zone") {
                    $zone_1 = $this->request->data['zone_1'];
                    $zone_2 = $this->request->data['zone_2'];
                } else if($search_by == "tube_line") {
                    $tube_line = $this->request->data['tube_line'];
                } else if($search_by == "commute_duration") {
                    $max_commute_time = $this->request->data['max_commute_time'];
                    $station_name = $this->request->data['station_name'];
                }

                if(isset($this->request->data['just_rooms']))
                    $just_rooms = $this->request->data['just_rooms'];
                if(isset($this->request->data['one_beds']))
                    $one_beds = $this->request->data['one_beds'];
                if(isset($this->request->data['whole_properties']))
                    $whole_properties = $this->request->data['whole_properties'];

                $query = "
                    SELECT *
                    FROM rent_ads
                    WHERE area LIKE '%$area_postcode%' OR postcode LIKE '%$area_postcode%' $conditions
                    ORDER BY $order
                    LIMIT $limit
                    OFFSET $offset
                ";
                $count_query = "
                    SELECT COUNT(id) as total
                    FROM rent_ads
                    WHERE area LIKE '%$area_postcode%' OR postcode LIKE '%$area_postcode%' $conditions
                ";
                $result = $this->RentAd->query($query);
                $count = $this->RentAd->query($count_query);
                $flag = "rent_ads";
                foreach($result as $key => $item) {
                    if($this->get_lat_lng($item['rent_ads']['postcode']))
                        $tmp_loc = $this->get_lat_lng($item['rent_ads']['postcode']);
                    else
                        $tmp_loc = array('lat'=> 51.50998, 'lng'=> -0.1337);

                    $result[$key]['rent_ads']['geo_loc'] = $tmp_loc;

                    if(isset($this->request->data['distance'])){
                        $distance = $this->request->data['distance'];
                        if (!empty($area_postcode))
                            $lat_lng = $this->get_lat_lng($area_postcode);
                        else {
                            $lat_lng['lat'] = "51.50998";
                            $lat_lng['lng'] = "-0.1337";
                        }
                        $lat = $lat_lng['lat'];
                        $lng = $lat_lng['lng'];

                        if($distance!=0) {
                            if(!($this->distance($lat, $lng, $tmp_loc['lat'], $tmp_loc['lng'])<=$distance)) {
                                unset($result['key']);
                            }
                        }
                    }
                }

            } else {
                if(!empty($this->request->data['area_postcode2']))
                    $area_postcode = $this->request->data['area_postcode2'];
                else if(!empty($this->request->data['area_postcode']))
                    $area_postcode = $this->request->data['area_postcode'];

                $query = "
                    SELECT *
                    FROM wanted_ads
                    WHERE area LIKE '%$area_postcode%' $conditions
                    ORDER BY $order
                    LIMIT $limit
                    OFFSET $offset
                ";
                $count_query = "
                    SELECT COUNT(id) as total
                    FROM wanted_ads
                    WHERE area LIKE '%$area_postcode%' $conditions
                ";
                $result = $this->WantedAd->query($query);
                $count = $this->WantedAd->query($count_query);
                $flag = "wanted_ads";
                foreach($result as $key => $item) {
                    if($this->get_lat_lng($item['wanted_ads']['area']))
                        $tmp_loc = $this->get_lat_lng($item['wanted_ads']['area']);
                    else
                        $tmp_loc = array('lat'=> 51.50998, 'lng'=>-0.1337);

                    $result[$key]['wanted_ads']['geo_loc'] = $tmp_loc;

                    if(isset($this->request->data['distance'])){
                        $distance = $this->request->data['distance'];
                        if (!empty($area_postcode))
                            $lat_lng = $this->get_lat_lng($area_postcode);
                        else {
                            $lat_lng['lat'] = "51.50998";
                            $lat_lng['lng'] = "-0.1337";
                        }
                        $lat = $lat_lng['lat'];
                        $lng = $lat_lng['lng'];

                        if($distance!=0) {
                            if(!($this->distance($lat, $lng, $tmp_loc['lat'], $tmp_loc['lng'])<=$distance)) {
                                unset($result[$key]);
                            }
                        }
                    }
                }
            }

            $request = $this->request->data;
            $this->Session->write('request', $request);
            $count = $count[0][0]['total'];

            $general_setting = $this->get_general_setting();
            $this->set(compact('general_setting'));

            $this->set(compact('result','request','flag','count','limit','offset','sort_by'));
            $this->layout = 'public';
            $this->render('../Pages/search_result');

        } else {
            $this->redirect(array('controller'=>'pages', 'action' => 'display', 'search'));
        }
    }

    public function search_api($paginate=false, $limit=10, $offset=0, $sort_by=null){
        header('Content-Type: application/json');
        $this->autoLayout = false;
        $this->autoRender = false;

        $this->loadModel('AdReference');
        $this->loadModel('WantedAd');
        $this->loadModel('RentAd');
        $this->loadModel('PropertyAd');

        if ($this->request->is('post') || $paginate==true) {
            if($paginate==true){
                $this->request->data = $this->Session->read('request');
                if(empty($this->request->data)){
                    die(json_encode(array('success' => false, 'msg' => 'Invalid request.')));
                }
            }
            #AuthComponent::_setTrace($this->request->data);

            $search_for = $this->request->data['search_for'];
            $search_by = $this->request->data['search_by'];
            $conditions = "";
            $is_advanced = $this->request->data['advance_search'];
            if($is_advanced) {
                if(isset($this->request->data['min_rent']) && isset($this->request->data['max_rent'])){
                    $min_rent = $this->request->data['min_rent'];
                    $max_rent = $this->request->data['max_rent'];
                    if($min_rent!=null && $max_rent!=null)
                        $conditions .= " AND rent BETWEEN $min_rent AND $max_rent";
                }
                if(isset($this->request->data['per_week'])){
                    $conditions .= " AND per_week = '" . $this->request->data['per_week'] . "'";
                }
                if(isset($this->request->data['bills_included']) && $search_for=="to_rent"){
                    if($this->request->data['bills_included'] == 1)
                        $conditions .= " AND bills_inc = 'Yes'";
                }
                if(isset($this->request->data['gender'])){
                    $gender = $this->request->data['gender'];
                    if($search_for == "to_rent") {
                        if($gender == 'Couple')
                            $conditions .= " AND new_mate_couples_welcomed = 'Yes'";
                        else
                            $conditions .= " AND new_mate_gender = '$gender' OR new_mate_gender='No Preference'";
                    } else {
                        if($gender == 'Couple')
                            $conditions .= " AND genders = '1 male 1 female'";
                        else if ($gender == 'Male')
                            $conditions .= " AND genders = '1 male' OR genders = '2 males'";
                        else if ($gender == 'Female')
                            $conditions .= " AND genders = '1 female' OR genders = '2 females'";
                    }
                }
                if(isset($this->request->data['room_size'])){
                    $room_size = $this->request->data['room_size'];
                    if($search_for == "to_rent" && $room_size!="dm") {
                        $conditions .= " AND room_size = '$room_size'";
                    }
                }

                if(isset($this->request->data['shared_living_room']) && $this->request->data['shared_living_room']==1){
                    if($search_for == "to_rent") {
                        $conditions .= " AND living_room = 'Yes'";
                    }
                }
                if(isset($this->request->data['smoking_allow']) && $this->request->data['smoking_allow']==1){
                    if($search_for == "to_rent") {
                        $conditions .= " AND new_mate_smoking = 'Yes' OR new_mate_smoking = 'No Preference'";
                    } else {
                        $conditions .= " AND smoker = 'Yes'";
                    }
                }

                if(isset($this->request->data['min_stay']) && $this->request->data['min_stay']!=0){
                    $conditions .= " AND min_stay > '" . $this->request->data['min_stay'] . "'";
                }
                if(isset($this->request->data['max_stay']) && $this->request->data['max_stay']!=0){
                    $conditions .= " AND max_stay < '" . $this->request->data['max_stay'] . "'";
                }
                if(isset($this->request->data['available']) && $this->request->data['available']=="date"){
                    if(isset($this->request->data['available_from'])){
                        $conditions .= " AND available_from < '" . $this->request->data['available_from'] . "'";
                    }
                }
                if(isset($this->request->data['pref_occupation'])){
                    $pref_occupation = $this->request->data['pref_occupation'];
                    if($search_for == "to_rent") {
                        if($pref_occupation != 'dm')
                            $conditions .= " AND new_mate_occupation = '" . $pref_occupation . "'";
                    } else {
                        if($pref_occupation != 'dm')
                            $conditions .= " AND occupation = '" . $pref_occupation . "'";
                    }
                }
                if(isset($this->request->data['pref_gender'])){
                    $pref_gender = $this->request->data['pref_gender'];
                    if($search_for == "to_rent") {
                        if($pref_gender != 'dm' || $pref_gender != 'mixed')
                            $conditions .= " AND new_mate_gender = '" . $pref_gender . "'";
                    }
                }
                if(isset($this->request->data['pref_min_age']) && isset($this->request->data['pref_max_age'])){
                    $pref_min_age = $this->request->data['pref_min_age'];
                    $pref_max_age = $this->request->data['pref_max_age'];
                    if( ($pref_min_age!=null && $pref_min_age!=0) && ($pref_max_age!=null && $pref_max_age!=0) ) {
                        if($search_for != "to_rent") {
                            $conditions .= " AND age BETWEEN $pref_min_age AND $pref_max_age";
                        }
                    }
                }

                if(isset($this->request->data['days_avail'])){
                    if($this->request->data['days_avail']!='dm')
                        $conditions .= " AND days_available = '". $this->request->data['days_avail'] . "'";
                }

                if(isset($this->request->data['advertiser_role'])  && $search_for=="to_rent"){
                    if($this->request->data['advertiser_role']=='agent')
                        $conditions .= " AND advertiser_role = 'Agent'";
                    else if($this->request->data['advertiser_role']=='private')
                        $conditions .= " AND advertiser_role <> 'Agent'";
                }
            }

            if($sort_by != null) {
                if($sort_by == "created")
                    $order = "created DESC";
                elseif($sort_by == "modified")
                    $order = "modified DESC";
                elseif($sort_by == "low_price")
                    $order = "rent ASC";
                elseif($sort_by == "high_price")
                    $order = "rent DESC";
            } else {
                $order = "created DESC";
            }

            $distance = null;
            if($search_for == "to_rent") {
                if($search_by == "location") {
                    $area_postcode = $this->request->data['area_postcode'];
                } else if($search_by == "travel_zone") {
                    $zone_1 = $this->request->data['zone_1'];
                    $zone_2 = $this->request->data['zone_2'];
                } else if($search_by == "tube_line") {
                    $tube_line = $this->request->data['tube_line'];
                } else if($search_by == "commute_duration") {
                    $max_commute_time = $this->request->data['max_commute_time'];
                    $station_name = $this->request->data['station_name'];
                }

                if(isset($this->request->data['just_rooms']))
                    $just_rooms = $this->request->data['just_rooms'];
                if(isset($this->request->data['one_beds']))
                    $one_beds = $this->request->data['one_beds'];
                if(isset($this->request->data['whole_properties']))
                    $whole_properties = $this->request->data['whole_properties'];

                $query = "
                    SELECT *
                    FROM rent_ads
                    WHERE area LIKE '%$area_postcode%' OR postcode LIKE '%$area_postcode%' $conditions
                    ORDER BY $order
                    LIMIT $limit
                    OFFSET $offset
                ";
                $count_query = "
                    SELECT COUNT(id) as total
                    FROM rent_ads
                    WHERE area LIKE '%$area_postcode%' OR postcode LIKE '%$area_postcode%' $conditions
                ";
                $result = $this->RentAd->query($query);
                $count = $this->RentAd->query($count_query);
                $flag = "rent_ads";
                foreach($result as $key => $item) {
                    if($this->get_lat_lng($item['rent_ads']['postcode']))
                        $tmp_loc = $this->get_lat_lng($item['rent_ads']['postcode']);
                    else
                        $tmp_loc = array('lat'=> 51.50998, 'lng'=> -0.1337);

                    $result[$key]['rent_ads']['geo_loc'] = $tmp_loc;

                    if(isset($this->request->data['distance'])){
                        $distance = $this->request->data['distance'];
                        if (!empty($area_postcode))
                            $lat_lng = $this->get_lat_lng($area_postcode);
                        else {
                            $lat_lng['lat'] = "51.50998";
                            $lat_lng['lng'] = "-0.1337";
                        }
                        $lat = $lat_lng['lat'];
                        $lng = $lat_lng['lng'];

                        if($distance!=0) {
                            if(!($this->distance($lat, $lng, $tmp_loc['lat'], $tmp_loc['lng'])<=$distance)) {
                                unset($result['key']);
                            }
                        }
                    }
                }

            } else {
                if(!empty($this->request->data['area_postcode2']))
                    $area_postcode = $this->request->data['area_postcode2'];
                else if(!empty($this->request->data['area_postcode']))
                    $area_postcode = $this->request->data['area_postcode'];

                $query = "
                    SELECT *
                    FROM wanted_ads
                    WHERE area LIKE '%$area_postcode%' $conditions
                    ORDER BY $order
                    LIMIT $limit
                    OFFSET $offset
                ";
                $count_query = "
                    SELECT COUNT(id) as total
                    FROM wanted_ads
                    WHERE area LIKE '%$area_postcode%' $conditions
                ";
                $result = $this->WantedAd->query($query);
                $count = $this->WantedAd->query($count_query);
                $flag = "wanted_ads";
                foreach($result as $key => $item) {
                    if($this->get_lat_lng($item['wanted_ads']['area']))
                        $tmp_loc = $this->get_lat_lng($item['wanted_ads']['area']);
                    else
                        $tmp_loc = array('lat'=> 51.50998, 'lng'=>-0.1337);

                    $result[$key]['wanted_ads']['geo_loc'] = $tmp_loc;

                    if(isset($this->request->data['distance'])){
                        $distance = $this->request->data['distance'];
                        if (!empty($area_postcode))
                            $lat_lng = $this->get_lat_lng($area_postcode);
                        else {
                            $lat_lng['lat'] = "51.50998";
                            $lat_lng['lng'] = "-0.1337";
                        }
                        $lat = $lat_lng['lat'];
                        $lng = $lat_lng['lng'];

                        if($distance!=0) {
                            if(!($this->distance($lat, $lng, $tmp_loc['lat'], $tmp_loc['lng'])<=$distance)) {
                                unset($result[$key]);
                            }
                        }
                    }
                }
            }

            $request = $this->request->data;
            $this->Session->write('request', $request);
            $count = $count[0][0]['total'];


            /*$test = array(1=>'this',10=>'that','a key'=>'the other', 'we dont need no stinkin key');
            $a = json_encode($test);
            AuthComponent::_setTrace($a);*/

            /*foreach ($result as $r){
                AuthComponent::_setTrace($r['rent_ads'],false);
            }*/

            AuthComponent::_setTrace($result);
            echo json_decode(json_encode($result[0]['rent_ads']['id'])); die();

            $data['result'] = json_encode($result);
            $data['request'] = $request;
            $data['flag'] = $flag;
            $data['count'] = $count;
            $data['limit'] = $limit;
            $data['offset'] = $offset;
            $data['sort_by'] = $sort_by;
            die(json_encode(array('success' => true, 'data' => $data)));
        } else die(json_encode(array('success' => false, 'msg' => 'Invalid request.')));
    }

    public function full_ad($table=null, $id=null){
        if($table==null || $id==null) {
            $this->redirect(array('controller'=>'pages', 'action' => 'display', 'search'));
        }
        $this->loadModel('AdReference');
        $this->loadModel('WantedAd');
        $this->loadModel('RentAd');
        $this->loadModel('PropertyAd');
        $this->loadModel('SavedAd');

        $user_id = $this->Session->read('user_id');
        $this->loadModel('User');
        $user = $this->User->findById($user_id);
        #AuthComponent::_setTrace($user);

        if($table=='rent_ads') {
            $options = array('conditions' => array('RentAd.' . $this->RentAd->primaryKey => $id));
            $result = $this->RentAd->find('first', $options);

            if($this->get_lat_lng($result['RentAd']['postcode']))
                $result['RentAd']['geo_loc'] = $this->get_lat_lng($result['RentAd']['postcode']);
            else
                $result['RentAd']['geo_loc'] = array('lat'=> 51.50998, 'lng'=>-0.1337);

            $flag = "RentAd";
            $is_saved = $this->SavedAd->find('all', array(
                    'recursive' => -1,
                    'conditions'=> array(
                        'SavedAd.user_id' => $user_id,
                        'SavedAd.rent_ad_id' => $id
                    )
                )
            );

        } else if($table=='wanted_ads') {
            $options = array('conditions' => array('WantedAd.' . $this->WantedAd->primaryKey => $id));
            $result = $this->WantedAd->find('first', $options);

            if($this->get_lat_lng($result['WantedAd']['area']))
                $result['WantedAd']['geo_loc'] = $this->get_lat_lng($result['WantedAd']['area']);
            else
                $result['WantedAd']['geo_loc'] = array('lat'=> 51.50998, 'lng'=>-0.1337);

            $flag = "WantedAd";
            $is_saved = $this->SavedAd->find('all', array(
                    'recursive' => -1,
                    'conditions'=> array(
                        'SavedAd.user_id' => $user_id,
                        'SavedAd.wanted_ad_id' => $id
                    )
                )
            );
        }
        #AuthComponent::_setTrace($result);


        if(!empty($is_saved))
            $is_saved = true;
        else
            $is_saved = false;

        $general_setting = $this->get_general_setting();
        $this->set(compact('general_setting'));

        $this->set(compact('result','flag','user','is_saved'));
        $this->layout = 'public';
        $this->render('../Pages/ad_full');
    }

    public function saved_search($id) {
        $this->loadModel('SavedSearch');

        $search = $this->SavedSearch->findById($id);

        $params = explode('|',$search['SavedSearch']['parameters']);
        array_pop($params);
        foreach($params as $para) {
            $para_ex = explode(':',$para);
            $field = $para_ex[0];
            $value = $para_ex[1];
            $request[$field] = $value;
        }
        $this->Session->write('request', $request);
        return $this->redirect(array('action' => 'search', 1));
    }

    public function save_search(){
        $this->loadModel('SavedSearch');
        $user_id = $this->Session->read('user_id');
        $data = array();
        if(!empty($user_id))
            $data['user_id'] = $user_id;
        else
            return $this->redirect(array('controller'=>'pages', 'action' => 'display','login'));

        $request = $this->Session->read('request');

        $data['parameters'] = "";
        foreach($request as $key=>$item) {
            $data['parameters'] .= $key . ":" . $item . "|";
        }
        $this->SavedSearch->save($data);
        return $this->redirect(array('action' => 'search', 1));
    }

    public function find_by_ref(){
        if(!$this->request->is('post')) {
            $this->redirect(array('controller'=>'pages', 'action' => 'display', 'search'));
        }
        $ref = $this->request->data['ref'];
        $ref_ex = explode('-',$ref);

        if($ref_ex[0]=="RentAd")
            $table = 'rent_ads';
        else if($ref_ex[0]=="WantedAd")
            $table = 'wanted_ads';
        else if($ref_ex[0]=="PropertyAd")
            $table = 'property_ads';
        else
            $this->redirect(array('controller'=>'pages', 'action' => 'display', 'search'));

        $id = $ref_ex[1];

        if(isset($table) && isset($id)) {
            $this->full_ad($table,$id);
        } else {
            $this->redirect(array('controller'=>'pages', 'action' => 'display', 'search'));
        }
    }

     public function show_room_map(){
          $general_setting = $this->get_general_setting();
          $this->set(compact('general_setting'));
          $this->layout = 'public';
          $this->render('../Pages/search_result_map');
     }

}