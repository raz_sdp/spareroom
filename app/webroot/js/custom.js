var user_lat;
var user_lng;

/*function showPositionOnMap(locations) {
    // avg lat lng as center. temporary. may be changed.
    var marker, i;
    var centerLat = 0, centerLng = 0;
    for (i = 0; i < locations.length; i++) {
        centerLat += Number(locations[i][1]);
        centerLng += Number(locations[i][2]);
    }
    centerLat /= locations.length;
    centerLng /= locations.length;
    //returnLatLng();
    var mapOptions = {
        zoom: 8,
        center: new google.maps.LatLng( centerLat, centerLng )
    };

    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    var image;
    image = '../img/pin.png';

    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            map: map,
            animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng( locations[i][0], locations[i][1] ),
            icon: image
        });
        //alert("lat: "+locations[i][0]+" \n lng: "+locations[i][1]);
        google.maps.event.addListener(window, 'load', (function(marker, i) {})(marker, i));
    }
}*/

function returnLatLng() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, locationError, {
            timeout: 1000,
            maximumAge: Infinity,
            enableHighAccuracy: false
        });
    } else {
        alert("Geolocation is not supported by this browser.");
    }

    function showPosition(position) {
        user_lat = position.coords.latitude;
        user_lng = position.coords.longitude;
    }

    function locationError(e) {
        if (window.confirm("Do you want to share your location?")){
            window.alert('Please turn on your GPS.');
        } else {
            user_lat = 51.50998;
            user_lng = -0.1337;
        }
    }
}