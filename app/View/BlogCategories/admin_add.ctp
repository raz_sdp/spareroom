<?php echo $this->element('admin_menu');?>
<div class="index col-md-10 col-sm-10">
    <div class="white">
	<?php echo $this->Form->create('BlogCategory'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Blog Category'); ?></legend>
		<div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Name</label>
            <div class="col-sm-9">
                <?php echo $this->Form->input('name',array('label' => false,'class'=>'form-control')); ?>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Parent</label>
            <div class="col-sm-9">
                <?php echo $this->Form->input('parent_id',array('label' => false,'class'=>'form-control', 'options'=>$parentBlogCategories)); ?>
            </div>
        </div>
	</fieldset>
	<?php echo $this->Form->end(array('Submit', 'class'=>'btn submit-green s-c')); ?>
</div>
