<div class="blogCategories view">
<h2><?php echo __('Blog Category'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($blogCategory['BlogCategory']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($blogCategory['BlogCategory']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Parent Blog Category'); ?></dt>
		<dd>
			<?php echo $this->Html->link($blogCategory['ParentBlogCategory']['name'], array('controller' => 'blog_categories', 'action' => 'view', $blogCategory['ParentBlogCategory']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($blogCategory['BlogCategory']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($blogCategory['BlogCategory']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Blog Category'), array('action' => 'edit', $blogCategory['BlogCategory']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Blog Category'), array('action' => 'delete', $blogCategory['BlogCategory']['id']), array(), __('Are you sure you want to delete # %s?', $blogCategory['BlogCategory']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Blog Categories'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Blog Category'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Blog Categories'), array('controller' => 'blog_categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Parent Blog Category'), array('controller' => 'blog_categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Posts'), array('controller' => 'posts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Post'), array('controller' => 'posts', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Blog Categories'); ?></h3>
	<?php if (!empty($blogCategory['ChildBlogCategory'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Parent Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($blogCategory['ChildBlogCategory'] as $childBlogCategory): ?>
		<tr>
			<td><?php echo $childBlogCategory['id']; ?></td>
			<td><?php echo $childBlogCategory['name']; ?></td>
			<td><?php echo $childBlogCategory['parent_id']; ?></td>
			<td><?php echo $childBlogCategory['created']; ?></td>
			<td><?php echo $childBlogCategory['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'blog_categories', 'action' => 'view', $childBlogCategory['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'blog_categories', 'action' => 'edit', $childBlogCategory['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'blog_categories', 'action' => 'delete', $childBlogCategory['id']), array(), __('Are you sure you want to delete # %s?', $childBlogCategory['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Child Blog Category'), array('controller' => 'blog_categories', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Posts'); ?></h3>
	<?php if (!empty($blogCategory['Post'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Title'); ?></th>
		<th><?php echo __('Author'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Feature Img'); ?></th>
		<th><?php echo __('Blog Category Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($blogCategory['Post'] as $post): ?>
		<tr>
			<td><?php echo $post['id']; ?></td>
			<td><?php echo $post['title']; ?></td>
			<td><?php echo $post['author']; ?></td>
			<td><?php echo $post['description']; ?></td>
			<td><?php echo $post['feature_img']; ?></td>
			<td><?php echo $post['blog_category_id']; ?></td>
			<td><?php echo $post['created']; ?></td>
			<td><?php echo $post['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'posts', 'action' => 'view', $post['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'posts', 'action' => 'edit', $post['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'posts', 'action' => 'delete', $post['id']), array(), __('Are you sure you want to delete # %s?', $post['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Post'), array('controller' => 'posts', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
