<div class="savedSearches form">
<?php echo $this->Form->create('SavedSearch'); ?>
	<fieldset>
		<legend><?php echo __('Edit Saved Search'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('user_id');
		echo $this->Form->input('parameters');
		echo $this->Form->input('daily_alert');
		echo $this->Form->input('instant_alert');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('SavedSearch.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('SavedSearch.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Saved Searches'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
