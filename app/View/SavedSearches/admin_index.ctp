<div class="savedSearches index">
	<h2><?php echo __('Saved Searches'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('parameters'); ?></th>
			<th><?php echo $this->Paginator->sort('daily_alert'); ?></th>
			<th><?php echo $this->Paginator->sort('instant_alert'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($savedSearches as $savedSearch): ?>
	<tr>
		<td><?php echo h($savedSearch['SavedSearch']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($savedSearch['User']['id'], array('controller' => 'users', 'action' => 'view', $savedSearch['User']['id'])); ?>
		</td>
		<td><?php echo h($savedSearch['SavedSearch']['parameters']); ?>&nbsp;</td>
		<td><?php echo h($savedSearch['SavedSearch']['daily_alert']); ?>&nbsp;</td>
		<td><?php echo h($savedSearch['SavedSearch']['instant_alert']); ?>&nbsp;</td>
		<td><?php echo h($savedSearch['SavedSearch']['created']); ?>&nbsp;</td>
		<td><?php echo h($savedSearch['SavedSearch']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $savedSearch['SavedSearch']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $savedSearch['SavedSearch']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $savedSearch['SavedSearch']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $savedSearch['SavedSearch']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Saved Search'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
