<div class="savedSearches view">
<h2><?php echo __('Saved Search'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($savedSearch['SavedSearch']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($savedSearch['User']['id'], array('controller' => 'users', 'action' => 'view', $savedSearch['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Parameters'); ?></dt>
		<dd>
			<?php echo h($savedSearch['SavedSearch']['parameters']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Daily Alert'); ?></dt>
		<dd>
			<?php echo h($savedSearch['SavedSearch']['daily_alert']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Instant Alert'); ?></dt>
		<dd>
			<?php echo h($savedSearch['SavedSearch']['instant_alert']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($savedSearch['SavedSearch']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($savedSearch['SavedSearch']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Saved Search'), array('action' => 'edit', $savedSearch['SavedSearch']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Saved Search'), array('action' => 'delete', $savedSearch['SavedSearch']['id']), array(), __('Are you sure you want to delete # %s?', $savedSearch['SavedSearch']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Saved Searches'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Saved Search'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
