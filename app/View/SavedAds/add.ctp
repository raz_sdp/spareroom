<div class="savedAds form">
<?php echo $this->Form->create('SavedAd'); ?>
	<fieldset>
		<legend><?php echo __('Add Saved Ad'); ?></legend>
	<?php
		echo $this->Form->input('add_type');
		echo $this->Form->input('property_ad_id');
		echo $this->Form->input('rent_ad_id');
		echo $this->Form->input('wanted_ad_id');
		echo $this->Form->input('user_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Saved Ads'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Property Ads'), array('controller' => 'property_ads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Ad'), array('controller' => 'property_ads', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Rent Ads'), array('controller' => 'rent_ads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Rent Ad'), array('controller' => 'rent_ads', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Wanted Ads'), array('controller' => 'wanted_ads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Wanted Ad'), array('controller' => 'wanted_ads', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
