<div class="savedAds view">
<h2><?php echo __('Saved Ad'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($savedAd['SavedAd']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Add Type'); ?></dt>
		<dd>
			<?php echo h($savedAd['SavedAd']['add_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Property Ad'); ?></dt>
		<dd>
			<?php echo $this->Html->link($savedAd['PropertyAd']['title'], array('controller' => 'property_ads', 'action' => 'view', $savedAd['PropertyAd']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Rent Ad'); ?></dt>
		<dd>
			<?php echo $this->Html->link($savedAd['RentAd']['id'], array('controller' => 'rent_ads', 'action' => 'view', $savedAd['RentAd']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Wanted Ad'); ?></dt>
		<dd>
			<?php echo $this->Html->link($savedAd['WantedAd']['title'], array('controller' => 'wanted_ads', 'action' => 'view', $savedAd['WantedAd']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($savedAd['User']['id'], array('controller' => 'users', 'action' => 'view', $savedAd['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($savedAd['SavedAd']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($savedAd['SavedAd']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Saved Ad'), array('action' => 'edit', $savedAd['SavedAd']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Saved Ad'), array('action' => 'delete', $savedAd['SavedAd']['id']), array(), __('Are you sure you want to delete # %s?', $savedAd['SavedAd']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Saved Ads'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Saved Ad'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Ads'), array('controller' => 'property_ads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Ad'), array('controller' => 'property_ads', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Rent Ads'), array('controller' => 'rent_ads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Rent Ad'), array('controller' => 'rent_ads', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Wanted Ads'), array('controller' => 'wanted_ads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Wanted Ad'), array('controller' => 'wanted_ads', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
