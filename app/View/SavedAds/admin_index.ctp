<div class="savedAds index">
	<h2><?php echo __('Saved Ads'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('add_type'); ?></th>
			<th><?php echo $this->Paginator->sort('property_ad_id'); ?></th>
			<th><?php echo $this->Paginator->sort('rent_ad_id'); ?></th>
			<th><?php echo $this->Paginator->sort('wanted_ad_id'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($savedAds as $savedAd): ?>
	<tr>
		<td><?php echo h($savedAd['SavedAd']['id']); ?>&nbsp;</td>
		<td><?php echo h($savedAd['SavedAd']['add_type']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($savedAd['PropertyAd']['title'], array('controller' => 'property_ads', 'action' => 'view', $savedAd['PropertyAd']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($savedAd['RentAd']['id'], array('controller' => 'rent_ads', 'action' => 'view', $savedAd['RentAd']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($savedAd['WantedAd']['title'], array('controller' => 'wanted_ads', 'action' => 'view', $savedAd['WantedAd']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($savedAd['User']['id'], array('controller' => 'users', 'action' => 'view', $savedAd['User']['id'])); ?>
		</td>
		<td><?php echo h($savedAd['SavedAd']['created']); ?>&nbsp;</td>
		<td><?php echo h($savedAd['SavedAd']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $savedAd['SavedAd']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $savedAd['SavedAd']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $savedAd['SavedAd']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $savedAd['SavedAd']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Saved Ad'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Property Ads'), array('controller' => 'property_ads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Ad'), array('controller' => 'property_ads', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Rent Ads'), array('controller' => 'rent_ads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Rent Ad'), array('controller' => 'rent_ads', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Wanted Ads'), array('controller' => 'wanted_ads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Wanted Ad'), array('controller' => 'wanted_ads', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
