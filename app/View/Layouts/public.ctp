<?php //AuthComponent::_setTrace($this->params); ?>
<!DOCTYPE html>
<html  lang="en-US">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title>FindmeaRoom</title>

    <link rel="shortcut icon" href="images/favicon.html" type="image/x-icon">
    <link rel="icon" href="images/favicon.html" type="image/x-icon">  
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script type="text/javascript" src="../src/jquery.floating-social-share.js"></script>

    <?php
        echo $this->Html->meta('icon');
        //echo $this->Html->css('cake.generic');
        echo $this->Html->css(array('bootstrap', 'http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700','jquery.bxslider', 'style_public', 'responsive','font-awesome.min','jquery.floating-social-share'));
        echo $this->Html->script(array('jquery-1.11.1', 'bootstrap', 'jquery.bxslider', 'custom'));

        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
    ?>
    <!--[if lt IE 9]-->
    <?php echo $this->Html->script(array('html5shiv', 'respond.min')); ?>
    <!--[endif]-->
    <script>
        $( document ).ready(function() {
            $('.bxslider').bxSlider({
                minSlides: 1,
                maxSlides: 4,
                slideWidth:220,
                slideMargin: 0
            });

        });
    </script>
</head>

<body>
<header class="pg-header">
    <div class="upper_header">
        <div class="container">
            <div class="row"><!-- logopart -->
                <div class="col-sm-3 col-xs-4 logo-part">
                    <?php echo $this->Html->link( $this->Html->image('logo.png'), '/', array('escape' => false));?></a>
                </div>
                <!--end of logopart -->
                <!-- logo-right-part -->
                <div class="col-sm-9 col-xs-8">
                     <div class="row signup-part">
                        <div class="col-md-2 col-sm-3 col-xs-4">
                               <a href="#"><?php echo $this->Html->image('dwonload-btn.png');?></a>               
                        </div><!-- top nav part -->   
                        <div class="col-md-6 col-md-offset-4 col-sm-offset-1 col-sm-8 col-xs-8"><!-- sign in part -->
                            <div class="row sign-up-part">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="socila-part">               
                                        <a href="#"><?php echo $this->Html->image('face-bbok.png');?></a>
                                        <a href="#"><?php echo $this->Html->image('twitter.png');?></a>
                                        <a href="#"><?php echo $this->Html->image('linkdin.png');?></a>
                                    </div> 
                                </div>
                                <?php
                                $user_id = $this->Session->read('user_id');
                                #AuthComponent::_setTrace($user_id);
                                ?>
                                <div class="col-sm-8 col-xs-12">
                                    <div class="sign_in">
                                         <ul>
                                             <?php if(!isset($user_id)) { ?>
                                             <li><a href="<?php echo $this->webroot;?>login">Login</a></li>
                                             <li><a href="<?php echo $this->webroot;?>register">Register</a></li>
                                             <?php } else { ?>
                                             <li><a href="<?php echo $this->webroot;?>my_account">My Account</a></li>
                                             <?php } ?>
                                         </ul>
                                    </div>  
                                </div>
                                
                            </div>                                              
                        </div><!--end of sign in part -->                       
                     </div>                  
                     <div class="row navigation-part"><!-- serch- cart part -->
                        <div class="col-sm-12 col-xs-12">
                             <button type="button" class="navbar-toggle main-nav-collapse" data-toggle="collapse" data-target="#top_nav"> 
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span> 
                                <span class="icon-bar"></span> 
                            </button> 
                            <div class="collapse navbar-collapse  navigation-part" id="top_nav">
                                <ul class="nav navbar-nav">  <!-- nav-justified -->
                                    <li>
                                    <?php
                                    $controller = $this->params['controller'];
                                    $action = $this->params['action'];
                                    //$method = $this->params['pass'][0];

                                    if(empty($page)){
                                        if(!empty($this->params['pass']))
                                            $page = $this->params['pass'][0];
                                        else
                                            $page = null;
                                    }

                                    ?>
                                    <?php
                                        if($page=="home")
                                            echo $this->Html->link('Home','/',array('class'=>'active_menu'));
                                        else
                                            echo $this->Html->link('Home','/');
                                    ?>
                                    </li>
                                    <li>
                                        <?php
                                        if($page=="about")
                                            echo $this->Html->link('About us','/about',array('class'=>'active_menu'));
                                        else
                                            echo $this->Html->link('About us','/about');
                                        ?>
                                    </li>
                                    <li>
                                        <?php
                                        if($page=="search" || $page=="advanced_search" || $controller=="searchEngine")
                                            echo $this->Html->link('Search','/search',array('class'=>'active_menu'));
                                        else
                                            echo $this->Html->link('Search','/search');
                                        ?>
                                    </li>
                                    <li>
                                        <?php
                                        if($page=="browse")
                                            echo $this->Html->link('Browse','/browse',array('class'=>'active_menu'));
                                        else
                                            echo $this->Html->link('Browse','/browse');
                                        ?>
                                    </li><li>
                                        <?php
                                        if($page=="place_ad" || $page=="add_option" || $page=="room_to_rent" || $page=="room_wanted" || $page=="whole_property")
                                            echo $this->Html->link('Place Add','/place_ad',array('class'=>'active_menu'));
                                        else
                                            echo $this->Html->link('Place Add','/place_ad');
                                        ?>
                                    </li>
                                    <li>
                                        <?php
                                        if($page=="info")
                                            echo $this->Html->link('Info & advice','/info',array('class'=>'active_menu'));
                                        else
                                            echo $this->Html->link('Info & advice','/info');
                                        ?>
                                    </li><li>
                                        <?php
                                        if($page=="blog" || $controller=="blog")
                                            echo $this->Html->link('BLOG','/blog',array('class'=>'active_menu','target'=>'_blank'));
                                        else
                                            echo $this->Html->link('BLOG','/blog',array('target'=>'_blank'));
                                        ?>
                                    </li>
                                    <li>
                                        <?php
                                        if($page=="contact")
                                            echo $this->Html->link('Contact Us','/contact',array('class'=>'active_menu'));
                                        else
                                            echo $this->Html->link('Contact Us','/contact');
                                        ?>
                                    </li>
                                     <!-- <li class="dropdown">  
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Inmate initiatives<span class="caret"></span></a>  
                                        <ul class="dropdown-menu"> 
                                            <li><a href="#" title="fall back">fall back</a></li> 
                                            <li><a href="#" title="men s.t.o.p">men S.t.o.p</a></li>
                                            <li><a href="#" title="the mentoring project">the mentoring Project</a></li>
                                            <li class="last"><a href="#" title="letter to the streets">letter to the streets</a></li>  
                                        </ul>  
                                    </li> -->
                                </ul>
                             </div>  
                        </div>                      
                    </div>
                 </div>
             </div>          
        </div>
    </div>
</header>
 <!-- header -->

<div id="content">
    <?php echo $this->Session->flash(); ?>
    <?php echo $this->fetch('content'); ?>
</div>

<footer class="foooter-part">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-sm-4">
                    <h4>All Links</h4>
                    <ul class="footer-link">
                        <li><?php echo $this->Html->link('Home','/');?></li>
                        <li><?php echo $this->Html->link('About us','/about');?></li>
                        <li><?php echo $this->Html->link('Search','/search');?></li>
                        <li><?php echo $this->Html->link('Browse','/browse');?></li>
                        <li><?php echo $this->Html->link('Place Ad','/place_ad');?></li>
                        <li><?php echo $this->Html->link('Info & advice','/info');?></li>
                        <li><?php echo $this->Html->link('BLOG','/blog', array('target'=>'_blank'));?></li>
                        <li><?php echo $this->Html->link('Contact Us','/contact');?></li>
                    </ul>
                </div>

                <div class="col-md-3 col-sm-4">
                    <h4>Address Details</h4>
                    <address class="address-text">
                        <?php
                            $address = $general_setting['GeneralSetting']['address_1'] . ", " . $general_setting['GeneralSetting']['address_2'] . ", " . $general_setting['GeneralSetting']['city'] . ", " . $general_setting['GeneralSetting']['country'] . ", " . $general_setting['GeneralSetting']['postcode'];
                        ?>
                        <?php echo $address;?>
                    </address>
                    <div class="contact-details">
                        Contact No: 	<?php echo $general_setting['GeneralSetting']['phone'];?>
                    </div>
                    <div class="contact-details">
                        Email: 	<?php echo $general_setting['GeneralSetting']['email'];?>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4">
                    <h4>Client Over view</h4>
                    <div class="client-view">
                        <p>
                            <sup>“</sup>
                            I can't believe the response to my advert! Just one day and the room has gone to someone I'm sure will be perfect.
                            <sub>”</sub>
                        </p>
                    </div>
                    <div class="client-view view-2">
                        <p>
                            <sup>“</sup>
                            Thank you very much, I was over-run with over 20 applicants within the first 2 days, so much better than other sites.
                            <sub>”</sub>
                        </p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <h4 class="mar-left">Need any help?</h4>
                    <div class="help-para">
                        <?php echo "blah blah blah";?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="copy-text">
                <?php echo $general_setting['GeneralSetting']['copyright_text'];?>
            </div>
        </div>
    </div>

</footer>

<?php echo $this->Html->script(array('jquery.validate')); ?>

</body>
</html>