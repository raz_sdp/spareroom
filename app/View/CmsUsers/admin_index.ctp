<script type="text/javascript">
    //fn for from(calender)-to(calender) to show transaction records for a specified records
    $(function () {
        $("#go").on('click', function () {
            var keyword = $.trim($('#keyword').val());
            location.href = ROOT+ 'admin/cms_users/index/keyword:' + keyword;
        })
    });
</script>
<?php echo $this->element('admin_menu'); ?>
<div class="index col-md-10 col-sm-10">
<div class="white">
    <div class="paging pull-right">
        <ul class="pagination pull-right">
            <?php
            $this->Paginator->options(array(
                'url' => array(
                    'keyword' => $keyword
                )
            ));
            echo $this->Paginator->prev(' < ', array('tag' => 'li', 'disabledTag' => 'li', 'escape' => false), '<a href="#"> < </a>', array('class' => 'prev disabled', 'tag' => 'li', 'disabledTag' => 'li', 'escape' => false));
            echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a'));
            echo $this->Paginator->next(' > ', array('tag' => 'li', 'disabledTag' => 'li', 'escape' => false), '<a href="#"> > </a>', array('class' => 'next disabled', 'tag' => 'li', 'disabledTag' => 'li', 'escape' => false));
            ?>
        </ul>
        <p class="text-right">
            <?php
            echo $this->Paginator->counter(array(
                'format' => __('Showing {:start} to {:end} of {:count} entries')
            ));
            ?>
        </p>
    </div>
	<h2><?php echo __('Cms Users'); ?></h2>
    <div class="form-horizontal">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-1 control-label">Keyword</label>
            <div class="col-sm-3">
                <?php echo $this->Form->input('titlex', array('id' => 'keyword', 'label' => false,'class'=>'form-control')); ?>
            </div>
            <div class="col-sm-1">
                <?php
                echo $this->Form->input('Go', array('id' => 'go','class'=>'btn btn-info', 'type' => 'button', 'label' => false));
                ?>
            </div>
        </div>
    </div>
	<table cellpadding="0" cellspacing="0" class="table">
	<thead>
	<tr>
			<th><a href="/spareroom/admin/cms_users/index/sort:name/direction:asc">Role</a></th>
			<th><?php echo $this->Paginator->sort('email'); ?></th>
			<th><?php echo $this->Paginator->sort('username'); ?></th>
			<th><?php echo $this->Paginator->sort('simple_pwd'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($cmsUsers as $cmsUser): ?>
	<tr>
		<td><?php echo h($cmsUser['CmsUser']['name']); ?>&nbsp;</td>
		<td><?php echo h($cmsUser['CmsUser']['email']); ?>&nbsp;</td>
		<td><?php echo h($cmsUser['CmsUser']['username']); ?>&nbsp;</td>
		<td><?php echo h($cmsUser['CmsUser']['simple_pwd']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $cmsUser['CmsUser']['id']), array( 'class' => 'btn btn-info')); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $cmsUser['CmsUser']['id']), array('class' => 'btn btn-info', 'confirm' => __('Are you sure you want to delete # %s?', $cmsUser['CmsUser']['id']))); ?>
		</td>
	</tr>
	<?php endforeach; ?>
	</tbody>
	</table>
</div>
</div>
