<?php echo $this->element('admin_menu'); ?>

<div class="index col-md-10 col-sm-10">
    <div class="white">
        <div class="row dashbrd">
            <div class="col-md-3 col-sm-3">
                <a href="<?php echo $this->Html->url(array('controller' => 'property_ads', 'action' => 'index', 'admin' => true)) ?>">
                    <div class="realtime t-color">
                        <!-- <div class="staff t-color"> -->

                        <span>Whole Property Ads</span>

                        <div class="all-desc">
                            <div class="pull-right">
                                <?php echo $this->Html->image('board_whole_properties.png') ?>
                            </div>
                            <p class="count-num"><?php echo $property_ads; ?></p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a href="<?php echo $this->Html->url(array('controller' => 'rent_ads', 'action' => 'index', 'admin' => true)) ?>">
                    <div class="d-feedback t-color">
                        <!-- <div class="realtime t-color"> -->
                        <span>Room for Rent Ads</span>

                        <div class="all-desc">
                            <div class="pull-right">
                                <?php echo $this->Html->image('board_rooms_offered.png') ?>
                            </div>
                            <p class="count-num"><?php echo $rent_ads; ?></p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a href="<?php echo $this->Html->url(array('controller' => 'wanted_ads', 'action' => 'index', 'admin' => true)) ?>">
                    <div class="realtime t-color">
                        <span>Room Wanted Ads</span>

                        <div class="all-desc">
                            <div class="pull-right">
                                <?php echo $this->Html->image('board_rooms_wanted.png') ?>
                            </div>
                            <p class="count-num"><?php echo $wanted_ads; ?></p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>