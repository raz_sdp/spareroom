<?php echo $this->element('admin_menu');?>
<div class="index col-md-10 col-sm-10">
    <div class="white">
	<?php echo $this->Form->create('CmsUser'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Cms User'); ?></legend>
		<div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Role</label>
            <div class="col-sm-9">
                <?php echo $this->Form->input('name',array('label' => false,'class'=>'form-control','options'=>array('admin'=>'Admin','manager'=>'Manager','author'=>'Author'))); ?>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Email</label>
            <div class="col-sm-9">
                <?php echo $this->Form->input('email',array('label' => false,'class'=>'form-control')); ?>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Username</label>
            <div class="col-sm-9">
                <?php echo $this->Form->input('username',array('label' => false,'class'=>'form-control')); ?>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Simple Password</label>
            <div class="col-sm-9">
                <?php echo $this->Form->input('simple_pwd',array('label' => false,'class'=>'form-control')); ?>
            </div>
        </div>
	</fieldset>
	<?php echo $this->Form->end(array('Submit', 'class'=>'btn submit-green s-c')); ?>
</div>
