<?php echo $this->element('admin_menu'); ?>
<?php $role = $this->Session->read('role');?>
<div class="index col-md-10 col-sm-10">
    <div class="white">
        <div class="row dashbrd">
        <?php if($role!='author') { ?>
            <div class="col-md-3 col-sm-3">
                <a href="<?php echo $this->Html->url(array('controller' => 'general_settings', 'action' => 'edit', 'admin' => true)) ?>">
                    <div class="realtime t-color fill">
                        <span>General Settings</span>

                        <div class="all-desc">
                            <div class="pull-right">
                                <?php echo $this->Html->image('admin-settings-icon.png') ?>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'index', 'admin'=>true)) ?>">
                    <div class="realtime t-color">
                        <!-- <div class="staff t-color"> -->

                        <span>Users</span>

                        <div class="all-desc">
                            <div class="pull-right">
                                <?php echo $this->Html->image('users.png') ?>
                            </div>
                            <p class="count-num"><?php echo $user; ?></p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a href="<?php echo $this->Html->url(array('controller' => 'cms_users', 'action' => 'ads_dashboard', 'admin'=>true)) ?>">
                    <div class="d-feedback t-color">
                        <!-- <div class="realtime t-color"> -->
                        <span>Ads</span>

                        <div class="all-desc">
                            <div class="pull-right">
                                <?php echo $this->Html->image('ad.png', array('height'=>'50px', 'width'=>'50px')) ?>
                            </div>
                            <p class="count-num"><?php echo $ads; ?></p>
                        </div>
                    </div>
                </a>
            </div>
        <?php } ?>

        <?php if($role!='manager') { ?>
            <div class="col-md-3 col-sm-3">
                <a href="<?php echo $this->Html->url(array('controller' => 'blog', 'action' => 'index', 'admin' => true)) ?>">
                    <div class="realtime t-color">
                        <span>Blog</span>
                        <div class="all-desc">
                            <div class="pull-right">
                                <?php echo $this->Html->image('blog.png', array('height'=>'50px', 'width'=>'50px')) ?>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        <?php } ?>

        <?php if($role=='admin') { ?>
            <div class="col-md-3 col-sm-3">
                <a href="<?php echo $this->Html->url(array('controller' => 'cms_users', 'action' => 'index', 'admin' => true, 'settings')) ?>">
                    <div class="realtime t-color fill">
                        <span>cms users</span>

                        <div class="all-desc">
                            <div class="pull-right">
                                <?php echo $this->Html->image('administrator.png', array('height'=>'50px', 'width'=>'50px')) ?>
                            </div>
                            <p class="count-num"><?php echo $cms_user; ?></p>
                        </div>
                    </div>
                </a>
            </div>
        <?php } ?>

        </div>
    </div>
</div>