<div class="areas view">
<h2><?php echo __('Area'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($area['Area']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Area Name'); ?></dt>
		<dd>
			<?php echo h($area['Area']['area_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Parent Area'); ?></dt>
		<dd>
			<?php echo $this->Html->link($area['ParentArea']['id'], array('controller' => 'areas', 'action' => 'view', $area['ParentArea']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($area['Area']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($area['Area']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Area'), array('action' => 'edit', $area['Area']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Area'), array('action' => 'delete', $area['Area']['id']), array(), __('Are you sure you want to delete # %s?', $area['Area']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Areas'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Area'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Areas'), array('controller' => 'areas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Parent Area'), array('controller' => 'areas', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Ads'), array('controller' => 'property_ads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Ad'), array('controller' => 'property_ads', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Rent Ads'), array('controller' => 'rent_ads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Rent Ad'), array('controller' => 'rent_ads', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Areas'); ?></h3>
	<?php if (!empty($area['ChildArea'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Area Name'); ?></th>
		<th><?php echo __('Parent Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($area['ChildArea'] as $childArea): ?>
		<tr>
			<td><?php echo $childArea['id']; ?></td>
			<td><?php echo $childArea['area_name']; ?></td>
			<td><?php echo $childArea['parent_id']; ?></td>
			<td><?php echo $childArea['created']; ?></td>
			<td><?php echo $childArea['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'areas', 'action' => 'view', $childArea['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'areas', 'action' => 'edit', $childArea['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'areas', 'action' => 'delete', $childArea['id']), array(), __('Are you sure you want to delete # %s?', $childArea['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Child Area'), array('controller' => 'areas', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Property Ads'); ?></h3>
	<?php if (!empty($area['PropertyAd'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('No Rooms'); ?></th>
		<th><?php echo __('Type'); ?></th>
		<th><?php echo __('Rent'); ?></th>
		<th><?php echo __('Per Week'); ?></th>
		<th><?php echo __('Advertiser Type'); ?></th>
		<th><?php echo __('Area Id'); ?></th>
		<th><?php echo __('Street Name'); ?></th>
		<th><?php echo __('No Of Mins'); ?></th>
		<th><?php echo __('No Of Mins By'); ?></th>
		<th><?php echo __('Station Id'); ?></th>
		<th><?php echo __('Security Deposit'); ?></th>
		<th><?php echo __('Furnishing'); ?></th>
		<th><?php echo __('Available From'); ?></th>
		<th><?php echo __('Min Stay'); ?></th>
		<th><?php echo __('Max Stay'); ?></th>
		<th><?php echo __('Short Term Considered'); ?></th>
		<th><?php echo __('References Needed'); ?></th>
		<th><?php echo __('Fees Apply'); ?></th>
		<th><?php echo __('Aminities'); ?></th>
		<th><?php echo __('Tenant Smoking'); ?></th>
		<th><?php echo __('Tenant Occupation'); ?></th>
		<th><?php echo __('Tenant Pets'); ?></th>
		<th><?php echo __('Tenant Dss Welcomed'); ?></th>
		<th><?php echo __('Title'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Company Name'); ?></th>
		<th><?php echo __('Display Name'); ?></th>
		<th><?php echo __('Telephone'); ?></th>
		<th><?php echo __('Display Telephone'); ?></th>
		<th><?php echo __('Daily Mail'); ?></th>
		<th><?php echo __('Instant Mail'); ?></th>
		<th><?php echo __('Max Mail'); ?></th>
		<th><?php echo __('Photo 1'); ?></th>
		<th><?php echo __('Photo 1 Desc'); ?></th>
		<th><?php echo __('Photo 2'); ?></th>
		<th><?php echo __('Photo 2 Desc'); ?></th>
		<th><?php echo __('Photo 3'); ?></th>
		<th><?php echo __('Photo 3 Desc'); ?></th>
		<th><?php echo __('Photo 4'); ?></th>
		<th><?php echo __('Photo 4 Desc'); ?></th>
		<th><?php echo __('Photo 5'); ?></th>
		<th><?php echo __('Photo 5 Desc'); ?></th>
		<th><?php echo __('Video Link'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($area['PropertyAd'] as $propertyAd): ?>
		<tr>
			<td><?php echo $propertyAd['id']; ?></td>
			<td><?php echo $propertyAd['user_id']; ?></td>
			<td><?php echo $propertyAd['no_rooms']; ?></td>
			<td><?php echo $propertyAd['type']; ?></td>
			<td><?php echo $propertyAd['rent']; ?></td>
			<td><?php echo $propertyAd['per_week']; ?></td>
			<td><?php echo $propertyAd['advertiser_type']; ?></td>
			<td><?php echo $propertyAd['area_id']; ?></td>
			<td><?php echo $propertyAd['street_name']; ?></td>
			<td><?php echo $propertyAd['no_of_mins']; ?></td>
			<td><?php echo $propertyAd['no_of_mins_by']; ?></td>
			<td><?php echo $propertyAd['station_id']; ?></td>
			<td><?php echo $propertyAd['security_deposit']; ?></td>
			<td><?php echo $propertyAd['furnishing']; ?></td>
			<td><?php echo $propertyAd['available_from']; ?></td>
			<td><?php echo $propertyAd['min_stay']; ?></td>
			<td><?php echo $propertyAd['max_stay']; ?></td>
			<td><?php echo $propertyAd['short_term_considered']; ?></td>
			<td><?php echo $propertyAd['references_needed']; ?></td>
			<td><?php echo $propertyAd['fees_apply']; ?></td>
			<td><?php echo $propertyAd['aminities']; ?></td>
			<td><?php echo $propertyAd['tenant_smoking']; ?></td>
			<td><?php echo $propertyAd['tenant_occupation']; ?></td>
			<td><?php echo $propertyAd['tenant_pets']; ?></td>
			<td><?php echo $propertyAd['tenant_dss_welcomed']; ?></td>
			<td><?php echo $propertyAd['title']; ?></td>
			<td><?php echo $propertyAd['description']; ?></td>
			<td><?php echo $propertyAd['company_name']; ?></td>
			<td><?php echo $propertyAd['display_name']; ?></td>
			<td><?php echo $propertyAd['telephone']; ?></td>
			<td><?php echo $propertyAd['display_telephone']; ?></td>
			<td><?php echo $propertyAd['daily_mail']; ?></td>
			<td><?php echo $propertyAd['instant_mail']; ?></td>
			<td><?php echo $propertyAd['max_mail']; ?></td>
			<td><?php echo $propertyAd['photo_1']; ?></td>
			<td><?php echo $propertyAd['photo_1_desc']; ?></td>
			<td><?php echo $propertyAd['photo_2']; ?></td>
			<td><?php echo $propertyAd['photo_2_desc']; ?></td>
			<td><?php echo $propertyAd['photo_3']; ?></td>
			<td><?php echo $propertyAd['photo_3_desc']; ?></td>
			<td><?php echo $propertyAd['photo_4']; ?></td>
			<td><?php echo $propertyAd['photo_4_desc']; ?></td>
			<td><?php echo $propertyAd['photo_5']; ?></td>
			<td><?php echo $propertyAd['photo_5_desc']; ?></td>
			<td><?php echo $propertyAd['video_link']; ?></td>
			<td><?php echo $propertyAd['created']; ?></td>
			<td><?php echo $propertyAd['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'property_ads', 'action' => 'view', $propertyAd['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'property_ads', 'action' => 'edit', $propertyAd['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'property_ads', 'action' => 'delete', $propertyAd['id']), array(), __('Are you sure you want to delete # %s?', $propertyAd['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Property Ad'), array('controller' => 'property_ads', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Rent Ads'); ?></h3>
	<?php if (!empty($area['RentAd'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Rooms For Rent'); ?></th>
		<th><?php echo __('Rooms In Property'); ?></th>
		<th><?php echo __('Property Type'); ?></th>
		<th><?php echo __('Current Occupants'); ?></th>
		<th><?php echo __('Postcode'); ?></th>
		<th><?php echo __('Advertiser Role'); ?></th>
		<th><?php echo __('Email Id'); ?></th>
		<th><?php echo __('Area Id'); ?></th>
		<th><?php echo __('Street Name'); ?></th>
		<th><?php echo __('No Of Mins'); ?></th>
		<th><?php echo __('No Of Mins By'); ?></th>
		<th><?php echo __('Station Id'); ?></th>
		<th><?php echo __('Living Room'); ?></th>
		<th><?php echo __('Aminities'); ?></th>
		<th><?php echo __('Cost Of Room'); ?></th>
		<th><?php echo __('Per Week'); ?></th>
		<th><?php echo __('Room Size'); ?></th>
		<th><?php echo __('Room Aminities'); ?></th>
		<th><?php echo __('Security Deposit'); ?></th>
		<th><?php echo __('Available From'); ?></th>
		<th><?php echo __('Min Stay'); ?></th>
		<th><?php echo __('Max Stay'); ?></th>
		<th><?php echo __('Short Term Considered'); ?></th>
		<th><?php echo __('Days Available'); ?></th>
		<th><?php echo __('References Needed'); ?></th>
		<th><?php echo __('Fees Apply'); ?></th>
		<th><?php echo __('Bills Inc'); ?></th>
		<th><?php echo __('Broadband'); ?></th>
		<th><?php echo __('Exist Mate Smoking'); ?></th>
		<th><?php echo __('Exist Mate Gender'); ?></th>
		<th><?php echo __('Exist Mate Occupation'); ?></th>
		<th><?php echo __('Exist Mate Pets'); ?></th>
		<th><?php echo __('Exist Mate Age'); ?></th>
		<th><?php echo __('Exist Mate Language'); ?></th>
		<th><?php echo __('Exist Mate Nationality'); ?></th>
		<th><?php echo __('Exist Mate Orientation'); ?></th>
		<th><?php echo __('Exist Mate Interests'); ?></th>
		<th><?php echo __('Is Orientation Part'); ?></th>
		<th><?php echo __('New Mate Smoking'); ?></th>
		<th><?php echo __('New Mate Gender'); ?></th>
		<th><?php echo __('New Mate Occupation'); ?></th>
		<th><?php echo __('New Mate Pets'); ?></th>
		<th><?php echo __('New Mate Min Age'); ?></th>
		<th><?php echo __('New Mate Max Age'); ?></th>
		<th><?php echo __('New Mate Language'); ?></th>
		<th><?php echo __('New Mate Orientation'); ?></th>
		<th><?php echo __('New Mate Couples Welcomed'); ?></th>
		<th><?php echo __('New Mate Dss Welcomed'); ?></th>
		<th><?php echo __('New Mate Veg Pref'); ?></th>
		<th><?php echo __('Short Description'); ?></th>
		<th><?php echo __('Long Description'); ?></th>
		<th><?php echo __('Company Name'); ?></th>
		<th><?php echo __('Display Name'); ?></th>
		<th><?php echo __('Telephone'); ?></th>
		<th><?php echo __('Show Telephone'); ?></th>
		<th><?php echo __('Daily Mail'); ?></th>
		<th><?php echo __('Instant Mail'); ?></th>
		<th><?php echo __('Max Mail'); ?></th>
		<th><?php echo __('Photo 1'); ?></th>
		<th><?php echo __('Photo 1 Desc'); ?></th>
		<th><?php echo __('Photo 2'); ?></th>
		<th><?php echo __('Photo 2 Desc'); ?></th>
		<th><?php echo __('Photo 3'); ?></th>
		<th><?php echo __('Photo 3 Desc'); ?></th>
		<th><?php echo __('Photo 4'); ?></th>
		<th><?php echo __('Photo 4 Desc'); ?></th>
		<th><?php echo __('Photo 5'); ?></th>
		<th><?php echo __('Photo 5 Desc'); ?></th>
		<th><?php echo __('Video Link'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($area['RentAd'] as $rentAd): ?>
		<tr>
			<td><?php echo $rentAd['id']; ?></td>
			<td><?php echo $rentAd['user_id']; ?></td>
			<td><?php echo $rentAd['rooms_for_rent']; ?></td>
			<td><?php echo $rentAd['rooms_in_property']; ?></td>
			<td><?php echo $rentAd['property_type']; ?></td>
			<td><?php echo $rentAd['current_occupants']; ?></td>
			<td><?php echo $rentAd['postcode']; ?></td>
			<td><?php echo $rentAd['advertiser_role']; ?></td>
			<td><?php echo $rentAd['email_id']; ?></td>
			<td><?php echo $rentAd['area_id']; ?></td>
			<td><?php echo $rentAd['street_name']; ?></td>
			<td><?php echo $rentAd['no_of_mins']; ?></td>
			<td><?php echo $rentAd['no_of_mins_by']; ?></td>
			<td><?php echo $rentAd['station_id']; ?></td>
			<td><?php echo $rentAd['living_room']; ?></td>
			<td><?php echo $rentAd['aminities']; ?></td>
			<td><?php echo $rentAd['cost_of_room']; ?></td>
			<td><?php echo $rentAd['per_week']; ?></td>
			<td><?php echo $rentAd['room_size']; ?></td>
			<td><?php echo $rentAd['room_aminities']; ?></td>
			<td><?php echo $rentAd['security_deposit']; ?></td>
			<td><?php echo $rentAd['available_from']; ?></td>
			<td><?php echo $rentAd['min_stay']; ?></td>
			<td><?php echo $rentAd['max_stay']; ?></td>
			<td><?php echo $rentAd['short_term_considered']; ?></td>
			<td><?php echo $rentAd['days_available']; ?></td>
			<td><?php echo $rentAd['references_needed']; ?></td>
			<td><?php echo $rentAd['fees_apply']; ?></td>
			<td><?php echo $rentAd['bills_inc']; ?></td>
			<td><?php echo $rentAd['broadband']; ?></td>
			<td><?php echo $rentAd['exist_mate_smoking']; ?></td>
			<td><?php echo $rentAd['exist_mate_gender']; ?></td>
			<td><?php echo $rentAd['exist_mate_occupation']; ?></td>
			<td><?php echo $rentAd['exist_mate_pets']; ?></td>
			<td><?php echo $rentAd['exist_mate_age']; ?></td>
			<td><?php echo $rentAd['exist_mate_language']; ?></td>
			<td><?php echo $rentAd['exist_mate_nationality']; ?></td>
			<td><?php echo $rentAd['exist_mate_orientation']; ?></td>
			<td><?php echo $rentAd['exist_mate_interests']; ?></td>
			<td><?php echo $rentAd['is_orientation_part']; ?></td>
			<td><?php echo $rentAd['new_mate_smoking']; ?></td>
			<td><?php echo $rentAd['new_mate_gender']; ?></td>
			<td><?php echo $rentAd['new_mate_occupation']; ?></td>
			<td><?php echo $rentAd['new_mate_pets']; ?></td>
			<td><?php echo $rentAd['new_mate_min_age']; ?></td>
			<td><?php echo $rentAd['new_mate_max_age']; ?></td>
			<td><?php echo $rentAd['new_mate_language']; ?></td>
			<td><?php echo $rentAd['new_mate_orientation']; ?></td>
			<td><?php echo $rentAd['new_mate_couples_welcomed']; ?></td>
			<td><?php echo $rentAd['new_mate_dss_welcomed']; ?></td>
			<td><?php echo $rentAd['new_mate_veg_pref']; ?></td>
			<td><?php echo $rentAd['short_description']; ?></td>
			<td><?php echo $rentAd['long_description']; ?></td>
			<td><?php echo $rentAd['company_name']; ?></td>
			<td><?php echo $rentAd['display_name']; ?></td>
			<td><?php echo $rentAd['telephone']; ?></td>
			<td><?php echo $rentAd['show_telephone']; ?></td>
			<td><?php echo $rentAd['daily_mail']; ?></td>
			<td><?php echo $rentAd['instant_mail']; ?></td>
			<td><?php echo $rentAd['max_mail']; ?></td>
			<td><?php echo $rentAd['photo_1']; ?></td>
			<td><?php echo $rentAd['photo_1_desc']; ?></td>
			<td><?php echo $rentAd['photo_2']; ?></td>
			<td><?php echo $rentAd['photo_2_desc']; ?></td>
			<td><?php echo $rentAd['photo_3']; ?></td>
			<td><?php echo $rentAd['photo_3_desc']; ?></td>
			<td><?php echo $rentAd['photo_4']; ?></td>
			<td><?php echo $rentAd['photo_4_desc']; ?></td>
			<td><?php echo $rentAd['photo_5']; ?></td>
			<td><?php echo $rentAd['photo_5_desc']; ?></td>
			<td><?php echo $rentAd['video_link']; ?></td>
			<td><?php echo $rentAd['created']; ?></td>
			<td><?php echo $rentAd['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'rent_ads', 'action' => 'view', $rentAd['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'rent_ads', 'action' => 'edit', $rentAd['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'rent_ads', 'action' => 'delete', $rentAd['id']), array(), __('Are you sure you want to delete # %s?', $rentAd['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Rent Ad'), array('controller' => 'rent_ads', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
