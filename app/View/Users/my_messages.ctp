<div class="container">
<?php echo $this->element('my_account_menu'); ?>

<?php if(empty($user)) { ?>
    <p>You have to <a href="<?php echo $this->webroot;?>login">login</a> or, <a href="<?php echo $this->webroot;?>register">register</a>.</p>
<?php } else { ?>

<main id="spareroom" class="wrap">
<div class="grid-4-8-4" id="mainheader">
    <div>&nbsp;</div>
    <div>
        <h1>Welcome <?php echo $user['User']['first_name']." ".$user['User']['last_name'];?></h1>
        <h5>Your Messages</h5>
    </div>
    <div>&nbsp;</div>
</div>
<div class="grid-4-8-4">
<aside>
<?php echo $this->element('search_aside'); ?>
</aside>

<div id="maincontent">
    <?php if($messages!=0) { ?>
        <?php foreach($messages_all as $message) { ?>
            <div class="single_ad">
                <div class="ad_header">
                    <?php
                    $mark_link = $this->webroot . "users/mark_read/" . $message['Message']['id'];
                    ?>
                    <h4>From: <?php echo $message['Sender']['first_name']." ".$message['Sender']['last_name']." (".$message['Sender']['email'].")";?></h4>
                    <a href="<?php echo $mark_link;?>">Mark as read</a>
                </div>
                <div class="ad_body">
                    <p><?php echo $message['Message']['message'];?></p>
                    <p><?php echo $message['Message']['created'];?></p>
                </div>
            </div>
        <?php } } else { ?>
        <div class="single_ad">
            <div class="ad_header"></div>
            <div class="ad_body">
                <p>You don't have any message to show.</p>
            </div>
        </div>
    <?php } ?>
</div>

</main>
<?php } ?>
</div>