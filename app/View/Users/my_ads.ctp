<div class="container">
<?php echo $this->element('my_account_menu'); ?>

<?php if(empty($user)) { ?>
    <p>You have to <a href="<?php echo $this->webroot;?>login">login</a> or, <a href="<?php echo $this->webroot;?>register">register</a>.</p>
<?php } else { ?>

<main id="spareroom" class="wrap ">
<div class="grid-4-8-4" id="mainheader">
    <div>&nbsp;</div>
    <div>
        <h1>  Welcome <?php echo $user['User']['first_name']." ".$user['User']['last_name'];?>  </h1>
        <h5>Your Placed Ads</h5>
    </div>
    <div>&nbsp;</div>
</div>
<div class="grid-4-8-4">
<aside>
<?php echo $this->element('search_aside'); ?>
</aside>

<div id="maincontent">
    <h2>To Rent Ads Placed By You</h2>
    <?php if($rent_ads!=0) { ?>
    <?php foreach($user['RentAd'] as $rent_ad) { ?>
    <div class="single_ad">
        <div class="ad_header">
            <?php
            if(strlen($rent_ad['title'])==0) $title="No title available.";
            else $title=$rent_ad['title'];

            if(strlen($rent_ad['description'])==0) $description="No description available.";
            else $description=$rent_ad['description'];

            $ad_link = $this->webroot . "searchEngine/full_ad/rent_ads/" . $rent_ad['id'];
            $manage_link = $this->webroot . "rent_ads/edit/" . $rent_ad['id'];
            ?>
            <h4><a href="<?php echo $ad_link;?>"><?php echo $title;?></a></h4>
            <a href="<?php echo $manage_link;?>">Manage your ad</a>
        </div>
        <div class="ad_body">
            <p><?php echo $description;?></p>
            <p><?php echo $rent_ad['created'];?></p>
        </div>
    </div>
    <?php } } else { ?>
        <div class="single_ad">
            <div class="ad_header"></div>
            <div class="ad_body">
                <p>You haven't placed a room to rent ad.</p>
            </div>
        </div>
    <?php } ?>


    <h2>Wanted Ads Placed By You</h2>
    <?php if($wanted_ads!=0) { ?>
    <?php foreach($user['WantedAd'] as $wanted_ad) { ?>
    <div class="single_ad">
        <div class="ad_header">
            <?php
            if(strlen($wanted_ads['title'])==0) $title="No title available.";
            else $title=$wanted_ads['title'];

            if(strlen($wanted_ads['description'])==0) $description="No description available.";
            else $description=$wanted_ads['description'];

            $ad_link = $this->webroot . "searchEngine/full_ad/wanted_ads/" . $wanted_ad['id'];
            $manage_link = $this->webroot . "wanted_ads/edit/" . $wanted_ad['id'];
            ?>
            <h4><a href="<?php echo $ad_link;?>"><?php echo $title;?></a></h4>
            <a href="<?php echo $manage_link;?>">Manage your ad</a>
        </div>
        <div class="ad_body">
            <p><?php echo $description;?></p>
            <p><?php echo $wanted_ad['created'];?></p>
        </div>
    </div>
    <?php } } else { ?>
        <div class="single_ad">
            <div class="ad_header"></div>
            <div class="ad_body">
                <p>You haven't placed a room wanted ad.</p>
            </div>
        </div>
    <?php } ?>
</div>

</div>
</main>
<?php } ?>
</div>