<div class="container">
<?php echo $this->element('my_account_menu'); ?>

<?php if(empty($user)) { ?>
    <p>You have to <a href="<?php echo $this->webroot;?>login">login</a> or, <a href="<?php echo $this->webroot;?>register">register</a>.</p>
<?php } else { ?>

<main id="spareroom" class="wrap">
<div class="grid-4-8-4" id="mainheader">
    <div>&nbsp;</div>
    <div>
        <h1>  Welcome <?php echo $user['User']['first_name']." ".$user['User']['last_name'];?>  </h1>
        <h5>Your Saved searches</h5>
    </div>
    <div>&nbsp;</div>
</div>
<div class="grid-4-8-4">
<aside>
<?php echo $this->element('search_aside'); ?>
</aside>

<div id="maincontent">
    <?php if($saved_searches!=0) { ?>
        <?php foreach ($user['SavedSearch'] as $search) { ?>
            <div class="single_ad">
                <div class="ad_header">
                    <?php
                        $for = '';
                        $loc = '';
                        $params = explode('|',$search['parameters']);
                        array_pop($params);
                        foreach($params as $para) {
                            $para_ex = explode(':',$para);
                            $field = $para_ex[0];
                            $value = $para_ex[1];

                            if($field=='search_for')
                                $for = $value;

                            if($field=='area_postcode')
                                $loc = $value;
                        }
                    ?>
                    <h4><a href="<?php echo $this->webroot."searchEngine/saved_search/".$search['id'];?>"><?php echo $for.", ".$loc;?></a></h4>
                </div>
                <div class="ad_body">
                    <p><?php echo $search['created'];?></p>
                </div>
            </div>
        <?php } ?>
    <?php } else { ?>
        <div class="single_ad">
            <div class="ad_header"></div>
            <div class="ad_body">
                <p>You don't have any saved searches.</p>
            </div>
        </div>
    <?php } ?>
</div>

</main>
<?php } ?>
</div>