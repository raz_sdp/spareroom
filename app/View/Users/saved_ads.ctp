<div class="container">
<?php echo $this->element('my_account_menu'); ?>

<?php if(empty($user)) { ?>
    <p>You have to <a href="<?php echo $this->webroot;?>login">login</a> or, <a href="<?php echo $this->webroot;?>register">register</a>.</p>
<?php } else { ?>

<main id="spareroom" class="wrap">
<div class="grid-4-8-4" id="mainheader">
    <div>&nbsp;</div>
    <div>
        <h1>  Welcome <?php echo $user['User']['first_name']." ".$user['User']['last_name'];?>  </h1>
        <h5>Your Saved Ads</h5>
    </div>
    <div>&nbsp;</div>
</div>
<div class="grid-4-8-4">
<aside>
<?php echo $this->element('search_aside'); ?>
</aside>

<div id="maincontent">
    <h2>To Rent Ads Saved By You</h2>
    <?php if(count($saved_ad_rent)!=0) { ?>
        <?php foreach($saved_ad_rent as $rent_saved_ad) { ?>
            <div class="single_ad">
                <div class="ad_header">
                    <?php
                    if(strlen($rent_saved_ad['RentAd']['title'])==0) $title="No title available.";
                    else $title=$rent_saved_ad['RentAd']['title'];

                    if(strlen($rent_saved_ad['RentAd']['description'])==0) $description="No description available.";
                    else $description=$rent_saved_ad['RentAd']['description'];

                    $ad_link = $this->webroot . "searchEngine/full_ad/rent_ads/" . $rent_saved_ad['RentAd']['id'];
                    ?>
                    <h4><a href="<?php echo $ad_link;?>"><?php echo $title;?></a></h4>
                </div>
                <div class="ad_body">
                    <p><?php echo $description;?></p>
                    <p><?php echo $rent_saved_ad['RentAd']['created'];?></p>
                </div>
            </div>
        <?php } } else { ?>
        <div class="single_ad">
            <div class="ad_header"></div>
            <div class="ad_body">
                <p>You don't have any room to rent ad saved.</p>
            </div>
        </div>
    <?php } ?>


    <h2>Wanted Ads Saved By You</h2>
    <?php if(count($saved_ad_wanted)!=0) { ?>
        <?php foreach($saved_ad_wanted as $wanted_saved_ad) { ?>
            <div class="single_ad">
                <div class="ad_header">
                    <?php
                    if(strlen($wanted_saved_ad['WantedAd']['title'])==0) $title="No title available.";
                    else $title=$wanted_saved_ad['WantedAd']['title'];

                    if(strlen($wanted_saved_ad['WantedAd']['description'])==0) $description="No description available.";
                    else $description=$wanted_saved_ad['WantedAd']['description'];

                    $ad_link = $this->webroot . "searchEngine/full_ad/wanted_ads/" . $wanted_saved_ad['WantedAd']['id'];
                    ?>
                    <h4><a href="<?php echo $ad_link;?>"><?php echo $title;?></a></h4>
                </div>
                <div class="ad_body">
                    <p><?php echo $description;?></p>
                    <p><?php echo $wanted_saved_ad['WantedAd']['created'];?></p>
                </div>
            </div>
        <?php } } else { ?>
        <div class="single_ad">
            <div class="ad_header"></div>
            <div class="ad_body">
                <p>You don't have any room wanted ad saved.</p>
            </div>
        </div>
    <?php } ?>
</div>

</main>
<?php } ?>
</div>