<div class="container">
<?php echo $this->element('my_account_menu'); ?>

<?php if(empty($user)) { ?>
    <p>You have to <a href="<?php echo $this->webroot;?>login">login</a> or, <a href="<?php echo $this->webroot;?>register">register</a>.</p>
<?php } else { ?>

<main id="spareroom" class="wrap">
<div class="grid-4-8-4" id="mainheader">
    <div>&nbsp;</div>
    <div><h1>  Welcome <?php echo $user['User']['first_name']." ".$user['User']['last_name'];?>  </h1></div>
    <div>&nbsp;</div>
</div>
<div class="grid-4-8-4">
<aside>
<?php echo $this->element('search_aside'); ?>
</aside>

<div id="maincontent">
    <div class="grid-4">
        <div>
          <div class="block block_standard asidecontent">
            <div class="block_header">
              <h3>My Account</h3>
            </div>
            <div class="block_content">
              <h5><a href="Users/my_messages">My messages</a> (<?php echo $messages?>)</h5>
              <p>Check your latest messages.</p>
              <h5><a href="Users/my_ads">My ads</a> (<?php echo $offered_ads+$wanted_ads?>)</h5>
              <p>
                Manage Room Offered or Room Wanted adverts you have placed.
                You have <?php echo $offered_ads;?> offered ads and <?php echo $wanted_ads;?> wanted ads
              </p>
            </div>
          </div>
          
        </div>

        <div>
            <!-- NOTE: The inverse of access_expired (!access_expired) implies the user HAS access.
            However this is not the case. It literally means the users access has expired -->
            <div class="block block_prominent upgrade_status upgrade_status_normal asidecontent">
                <div class="block_content">
                  <h5>You are not Upgraded.</h5>
                  <p>
                    Upgrade now for the following benefits...
                  </p>
                  <ol class="benefits">
                    <li>
                      <strong>Early Bird Access</strong>
                      <img src="http://images2.spareroom.co.uk/img/spareroom/v4/icons/early_bird_large.png">
                      <br>
                      13,565 more new ads currently available to you with Early Bird Access
                      <br>
                      <a href="/about">Learn more »</a>
                    </li>
                    <li>
                      <strong>A bold ad</strong>
                      <img src="http://images2.spareroom.co.uk/img/spareroom/v4/icons/bold_listing.png">
                      <br>
                      On average Bold Ads get double the enquiries - find your room or flatmate faster
                      <br>
                      <a href="/about">Learn more »</a>
                    </li>
                  </ol>
                  <p class="upgrade_now_links">
                    <a class="btn btn-primary" title="" href="Users/upgrade">Upgrade now</a>
                  </p>
                </div>
            </div>
            &nbsp; <!-- just in case any situations where this side is blank! -->
        </div> <!-- /.colmidright -->
    </div>
</div>

</div>
</main>
<?php } ?>
</div>