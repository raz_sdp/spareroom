<?php echo $this->element('admin_menu'); ?>
<div class="index col-md-10 col-sm-10">
<div class="white">
<div class="users view">
<h2><?php echo __('User'); ?></h2>
	<dl>
		<dt class="bg-info"><?php echo __('Id'); ?></dt>
		<dd class="bg-warning">
			<?php echo h($user['User']['id']); ?>
			&nbsp;
		</dd>
		<dt class="bg-info"><?php echo __('Email'); ?></dt>
		<dd class="bg-warning">
			<?php echo h($user['User']['email']); ?>
			&nbsp;
		</dd>
		<dt class="bg-info"><?php echo __('First Name'); ?></dt>
		<dd class="bg-warning">
			<?php echo h($user['User']['first_name']); ?>
			&nbsp;
		</dd>
		<dt class="bg-info"><?php echo __('Last Name'); ?></dt>
		<dd class="bg-warning">
			<?php echo h($user['User']['last_name']); ?>
			&nbsp;
		</dd>
		<dt class="bg-info"><?php echo __('Gender'); ?></dt>
		<dd class="bg-warning">
			<?php echo h($user['User']['gender']); ?>
			&nbsp;
		</dd>
		<dt class="bg-info"><?php echo __('Looking For Flat'); ?></dt>
		<dd class="bg-warning">
			<?php echo h($user['User']['looking_for_flat']); ?>
			&nbsp;
		</dd>
		<dt class="bg-info"><?php echo __('Have Flat'); ?></dt>
		<dd class="bg-warning">
			<?php echo h($user['User']['have_flat']); ?>
			&nbsp;
		</dd>
		<dt class="bg-info"><?php echo __('New Share'); ?></dt>
		<dd class="bg-warning">
			<?php echo h($user['User']['new_share']); ?>
			&nbsp;
		</dd>
		<dt class="bg-info"><?php echo __('Adverts'); ?></dt>
		<dd class="bg-warning">
			<?php echo h($user['User']['adverts']); ?>
			&nbsp;
		</dd>
		<dt class="bg-info"><?php echo __('Newsletter'); ?></dt>
		<dd class="bg-warning">
			<?php echo h($user['User']['newsletter']); ?>
			&nbsp;
		</dd>
		<dt class="bg-info"><?php echo __('Heard Where'); ?></dt>
		<dd class="bg-warning">
			<?php echo h($user['User']['heard_where']); ?>
			&nbsp;
		</dd>
		<dt class="bg-info"><?php echo __('Created'); ?></dt>
		<dd class="bg-warning">
			<?php echo h($user['User']['created']); ?>
			&nbsp;
		</dd>
		<dt class="bg-info"><?php echo __('Modified'); ?></dt>
		<dd class="bg-warning">
			<?php echo h($user['User']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="related">
	<h3><?php echo __('Related Property Ads'); ?></h3>
	<?php if (!empty($user['PropertyAd'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="table">
	<tr>
		<th><?php echo __('Id'); ?></th>
        <th><?php echo __('Title'); ?></th>
		<th><?php echo __('Advertiser Type'); ?></th>
		<th><?php echo __('Area Id'); ?></th>
		<th><?php echo __('Street Name'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($user['PropertyAd'] as $propertyAd): ?>
		<tr>
			<td><?php echo $propertyAd['id']; ?></td>
            <td><?php echo $propertyAd['title']; ?></td>
			<td><?php echo $propertyAd['advertiser_type']; ?></td>
			<td><?php echo $propertyAd['area_id']; ?></td>
			<td><?php echo $propertyAd['street_name']; ?></td>
			<td><?php echo $propertyAd['created']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'property_ads', 'action' => 'view', $propertyAd['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'property_ads', 'action' => 'delete', $propertyAd['id']), array(), __('Are you sure you want to delete # %s?', $propertyAd['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

</div>
<div class="related">
	<h3><?php echo __('Related Rent Ads'); ?></h3>
	<?php if (!empty($user['RentAd'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="table">
	<tr>
		<th><?php echo __('Id'); ?></th>
        <th><?php echo __('Title'); ?></th>
		<th><?php echo __('Postcode'); ?></th>
		<th><?php echo __('Advertiser Role'); ?></th>
		<th><?php echo __('Email Id'); ?></th>
		<th><?php echo __('Area Id'); ?></th>
		<th><?php echo __('Street Name'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($user['RentAd'] as $rentAd): ?>
		<tr>
			<td><?php echo $rentAd['id']; ?></td>
            <td><?php echo $rentAd['title']; ?></td>
			<td><?php echo $rentAd['postcode']; ?></td>
			<td><?php echo $rentAd['advertiser_role']; ?></td>
			<td><?php echo $rentAd['email_id']; ?></td>
			<td><?php echo $rentAd['area_id']; ?></td>
			<td><?php echo $rentAd['street_name']; ?></td>
			<td><?php echo $rentAd['created']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'rent_ads', 'action' => 'view', $rentAd['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'rent_ads', 'action' => 'delete', $rentAd['id']), array(), __('Are you sure you want to delete # %s?', $rentAd['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

</div>
<div class="related">
	<h3><?php echo __('Related Saved Ads'); ?></h3>
	<?php if (!empty($user['SavedAd'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="table">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Add Type'); ?></th>
		<th><?php echo __('Property Ad Id'); ?></th>
		<th><?php echo __('Rent Ad Id'); ?></th>
		<th><?php echo __('Wanted Ad Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($user['SavedAd'] as $savedAd): ?>
		<tr>
			<td><?php echo $savedAd['id']; ?></td>
			<td><?php echo $savedAd['add_type']; ?></td>
			<td><?php echo $savedAd['property_ad_id']; ?></td>
			<td><?php echo $savedAd['rent_ad_id']; ?></td>
			<td><?php echo $savedAd['wanted_ad_id']; ?></td>
			<td><?php echo $savedAd['created']; ?></td>
			<td class="actions">
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'saved_ads', 'action' => 'delete', $savedAd['id']), array(), __('Are you sure you want to delete # %s?', $savedAd['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

</div>
<div class="related">
	<h3><?php echo __('Related Saved Searches'); ?></h3>
	<?php if (!empty($user['SavedSearch'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="table">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Parameters'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($user['SavedSearch'] as $savedSearch): ?>
		<tr>
			<td><?php echo $savedSearch['id']; ?></td>
			<td><?php echo $savedSearch['parameters']; ?></td>
			<td><?php echo $savedSearch['created']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'saved_searches', 'action' => 'view', $savedSearch['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'saved_searches', 'action' => 'edit', $savedSearch['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'saved_searches', 'action' => 'delete', $savedSearch['id']), array(), __('Are you sure you want to delete # %s?', $savedSearch['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

</div>
<div class="related">
	<h3><?php echo __('Related Testimonials'); ?></h3>
	<?php if (!empty($user['Testimonial'])){ ?>
	<table cellpadding = "0" cellspacing = "0" class="table">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Comment'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($user['Testimonial'] as $testimonial): ?>
		<tr>
			<td><?php echo $testimonial['id']; ?></td>
			<td><?php echo $testimonial['user_id']; ?></td>
			<td><?php echo $testimonial['comment']; ?></td>
			<td><?php echo $testimonial['created']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'testimonials', 'action' => 'view', $testimonial['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'testimonials', 'action' => 'delete', $testimonial['id']), array(), __('Are you sure you want to delete # %s?', $testimonial['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
    <?php } else { echo "<h4>No testimonial available from this user.</h4>"; }  ?>
</div>
<div class="related">
	<h3><?php echo __('Related Wanted Ads'); ?></h3>
	<?php if (!empty($user['WantedAd'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="table">
	<tr>
		<th><?php echo __('Id'); ?></th>
        <th><?php echo __('Genders'); ?></th>
		<th><?php echo __('Looking For'); ?></th>
		<th><?php echo __('Area Ids'); ?></th>
		<th><?php echo __('Title'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($user['WantedAd'] as $wantedAd): ?>
		<tr>
			<td><?php echo $wantedAd['id']; ?></td>
			<td><?php echo $wantedAd['genders']; ?></td>
			<td><?php echo $wantedAd['looking_for']; ?></td>
			<td><?php echo $wantedAd['area_ids']; ?></td>
			<td><?php echo $wantedAd['title']; ?></td>
			<td><?php echo $wantedAd['created']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'wanted_ads', 'action' => 'view', $wantedAd['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'wanted_ads', 'action' => 'delete', $wantedAd['id']), array(), __('Are you sure you want to delete # %s?', $wantedAd['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

</div>

</div>
</div>
