<div class="users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend><?php echo __('Edit User'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('email');
		echo $this->Form->input('password');
		echo $this->Form->input('simple_pwd');
		echo $this->Form->input('first_name');
		echo $this->Form->input('last_name');
		echo $this->Form->input('gender');
		echo $this->Form->input('looking_for_flat');
		echo $this->Form->input('have_flat');
		echo $this->Form->input('new_share');
		echo $this->Form->input('adverts');
		echo $this->Form->input('newsletter');
		echo $this->Form->input('heard_where');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('User.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('User.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Property Ads'), array('controller' => 'property_ads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Ad'), array('controller' => 'property_ads', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Rent Ads'), array('controller' => 'rent_ads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Rent Ad'), array('controller' => 'rent_ads', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Saved Ads'), array('controller' => 'saved_ads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Saved Ad'), array('controller' => 'saved_ads', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Saved Searches'), array('controller' => 'saved_searches', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Saved Search'), array('controller' => 'saved_searches', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Testimonials'), array('controller' => 'testimonials', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Testimonial'), array('controller' => 'testimonials', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Wanted Ads'), array('controller' => 'wanted_ads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Wanted Ad'), array('controller' => 'wanted_ads', 'action' => 'add')); ?> </li>
	</ul>
</div>
