<div class="users view">
<h2><?php echo __('User'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($user['User']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($user['User']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Password'); ?></dt>
		<dd>
			<?php echo h($user['User']['password']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Simple Pwd'); ?></dt>
		<dd>
			<?php echo h($user['User']['simple_pwd']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('First Name'); ?></dt>
		<dd>
			<?php echo h($user['User']['first_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Last Name'); ?></dt>
		<dd>
			<?php echo h($user['User']['last_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Gender'); ?></dt>
		<dd>
			<?php echo h($user['User']['gender']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Looking For Flat'); ?></dt>
		<dd>
			<?php echo h($user['User']['looking_for_flat']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Have Flat'); ?></dt>
		<dd>
			<?php echo h($user['User']['have_flat']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('New Share'); ?></dt>
		<dd>
			<?php echo h($user['User']['new_share']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Adverts'); ?></dt>
		<dd>
			<?php echo h($user['User']['adverts']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Newsletter'); ?></dt>
		<dd>
			<?php echo h($user['User']['newsletter']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Heard Where'); ?></dt>
		<dd>
			<?php echo h($user['User']['heard_where']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($user['User']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($user['User']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit User'), array('action' => 'edit', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete User'), array('action' => 'delete', $user['User']['id']), array(), __('Are you sure you want to delete # %s?', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Ads'), array('controller' => 'property_ads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Ad'), array('controller' => 'property_ads', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Rent Ads'), array('controller' => 'rent_ads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Rent Ad'), array('controller' => 'rent_ads', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Saved Ads'), array('controller' => 'saved_ads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Saved Ad'), array('controller' => 'saved_ads', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Saved Searches'), array('controller' => 'saved_searches', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Saved Search'), array('controller' => 'saved_searches', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Testimonials'), array('controller' => 'testimonials', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Testimonial'), array('controller' => 'testimonials', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Wanted Ads'), array('controller' => 'wanted_ads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Wanted Ad'), array('controller' => 'wanted_ads', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Property Ads'); ?></h3>
	<?php if (!empty($user['PropertyAd'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('No Rooms'); ?></th>
		<th><?php echo __('Type'); ?></th>
		<th><?php echo __('Rent'); ?></th>
		<th><?php echo __('Per Week'); ?></th>
		<th><?php echo __('Advertiser Type'); ?></th>
		<th><?php echo __('Area Id'); ?></th>
		<th><?php echo __('Street Name'); ?></th>
		<th><?php echo __('No Of Mins'); ?></th>
		<th><?php echo __('No Of Mins By'); ?></th>
		<th><?php echo __('Station Id'); ?></th>
		<th><?php echo __('Security Deposit'); ?></th>
		<th><?php echo __('Furnishing'); ?></th>
		<th><?php echo __('Available From'); ?></th>
		<th><?php echo __('Min Stay'); ?></th>
		<th><?php echo __('Max Stay'); ?></th>
		<th><?php echo __('Short Term Considered'); ?></th>
		<th><?php echo __('References Needed'); ?></th>
		<th><?php echo __('Fees Apply'); ?></th>
		<th><?php echo __('Aminities'); ?></th>
		<th><?php echo __('Tenant Smoking'); ?></th>
		<th><?php echo __('Tenant Occupation'); ?></th>
		<th><?php echo __('Tenant Pets'); ?></th>
		<th><?php echo __('Tenant Dss Welcomed'); ?></th>
		<th><?php echo __('Title'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Company Name'); ?></th>
		<th><?php echo __('Display Name'); ?></th>
		<th><?php echo __('Telephone'); ?></th>
		<th><?php echo __('Display Telephone'); ?></th>
		<th><?php echo __('Daily Mail'); ?></th>
		<th><?php echo __('Instant Mail'); ?></th>
		<th><?php echo __('Max Mail'); ?></th>
		<th><?php echo __('Photo 1'); ?></th>
		<th><?php echo __('Photo 1 Desc'); ?></th>
		<th><?php echo __('Photo 2'); ?></th>
		<th><?php echo __('Photo 2 Desc'); ?></th>
		<th><?php echo __('Photo 3'); ?></th>
		<th><?php echo __('Photo 3 Desc'); ?></th>
		<th><?php echo __('Photo 4'); ?></th>
		<th><?php echo __('Photo 4 Desc'); ?></th>
		<th><?php echo __('Photo 5'); ?></th>
		<th><?php echo __('Photo 5 Desc'); ?></th>
		<th><?php echo __('Video Link'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($user['PropertyAd'] as $propertyAd): ?>
		<tr>
			<td><?php echo $propertyAd['id']; ?></td>
			<td><?php echo $propertyAd['user_id']; ?></td>
			<td><?php echo $propertyAd['no_rooms']; ?></td>
			<td><?php echo $propertyAd['type']; ?></td>
			<td><?php echo $propertyAd['rent']; ?></td>
			<td><?php echo $propertyAd['per_week']; ?></td>
			<td><?php echo $propertyAd['advertiser_type']; ?></td>
			<td><?php echo $propertyAd['area_id']; ?></td>
			<td><?php echo $propertyAd['street_name']; ?></td>
			<td><?php echo $propertyAd['no_of_mins']; ?></td>
			<td><?php echo $propertyAd['no_of_mins_by']; ?></td>
			<td><?php echo $propertyAd['station_id']; ?></td>
			<td><?php echo $propertyAd['security_deposit']; ?></td>
			<td><?php echo $propertyAd['furnishing']; ?></td>
			<td><?php echo $propertyAd['available_from']; ?></td>
			<td><?php echo $propertyAd['min_stay']; ?></td>
			<td><?php echo $propertyAd['max_stay']; ?></td>
			<td><?php echo $propertyAd['short_term_considered']; ?></td>
			<td><?php echo $propertyAd['references_needed']; ?></td>
			<td><?php echo $propertyAd['fees_apply']; ?></td>
			<td><?php echo $propertyAd['aminities']; ?></td>
			<td><?php echo $propertyAd['tenant_smoking']; ?></td>
			<td><?php echo $propertyAd['tenant_occupation']; ?></td>
			<td><?php echo $propertyAd['tenant_pets']; ?></td>
			<td><?php echo $propertyAd['tenant_dss_welcomed']; ?></td>
			<td><?php echo $propertyAd['title']; ?></td>
			<td><?php echo $propertyAd['description']; ?></td>
			<td><?php echo $propertyAd['company_name']; ?></td>
			<td><?php echo $propertyAd['display_name']; ?></td>
			<td><?php echo $propertyAd['telephone']; ?></td>
			<td><?php echo $propertyAd['display_telephone']; ?></td>
			<td><?php echo $propertyAd['daily_mail']; ?></td>
			<td><?php echo $propertyAd['instant_mail']; ?></td>
			<td><?php echo $propertyAd['max_mail']; ?></td>
			<td><?php echo $propertyAd['photo_1']; ?></td>
			<td><?php echo $propertyAd['photo_1_desc']; ?></td>
			<td><?php echo $propertyAd['photo_2']; ?></td>
			<td><?php echo $propertyAd['photo_2_desc']; ?></td>
			<td><?php echo $propertyAd['photo_3']; ?></td>
			<td><?php echo $propertyAd['photo_3_desc']; ?></td>
			<td><?php echo $propertyAd['photo_4']; ?></td>
			<td><?php echo $propertyAd['photo_4_desc']; ?></td>
			<td><?php echo $propertyAd['photo_5']; ?></td>
			<td><?php echo $propertyAd['photo_5_desc']; ?></td>
			<td><?php echo $propertyAd['video_link']; ?></td>
			<td><?php echo $propertyAd['created']; ?></td>
			<td><?php echo $propertyAd['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'property_ads', 'action' => 'view', $propertyAd['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'property_ads', 'action' => 'edit', $propertyAd['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'property_ads', 'action' => 'delete', $propertyAd['id']), array(), __('Are you sure you want to delete # %s?', $propertyAd['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Property Ad'), array('controller' => 'property_ads', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Rent Ads'); ?></h3>
	<?php if (!empty($user['RentAd'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Rooms For Rent'); ?></th>
		<th><?php echo __('Rooms In Property'); ?></th>
		<th><?php echo __('Property Type'); ?></th>
		<th><?php echo __('Current Occupants'); ?></th>
		<th><?php echo __('Postcode'); ?></th>
		<th><?php echo __('Advertiser Role'); ?></th>
		<th><?php echo __('Email Id'); ?></th>
		<th><?php echo __('Area Id'); ?></th>
		<th><?php echo __('Street Name'); ?></th>
		<th><?php echo __('No Of Mins'); ?></th>
		<th><?php echo __('No Of Mins By'); ?></th>
		<th><?php echo __('Station Id'); ?></th>
		<th><?php echo __('Living Room'); ?></th>
		<th><?php echo __('Aminities'); ?></th>
		<th><?php echo __('Cost Of Room'); ?></th>
		<th><?php echo __('Per Week'); ?></th>
		<th><?php echo __('Room Size'); ?></th>
		<th><?php echo __('Room Aminities'); ?></th>
		<th><?php echo __('Security Deposit'); ?></th>
		<th><?php echo __('Available From'); ?></th>
		<th><?php echo __('Min Stay'); ?></th>
		<th><?php echo __('Max Stay'); ?></th>
		<th><?php echo __('Short Term Considered'); ?></th>
		<th><?php echo __('Days Available'); ?></th>
		<th><?php echo __('References Needed'); ?></th>
		<th><?php echo __('Fees Apply'); ?></th>
		<th><?php echo __('Bills Inc'); ?></th>
		<th><?php echo __('Broadband'); ?></th>
		<th><?php echo __('Exist Mate Smoking'); ?></th>
		<th><?php echo __('Exist Mate Gender'); ?></th>
		<th><?php echo __('Exist Mate Occupation'); ?></th>
		<th><?php echo __('Exist Mate Pets'); ?></th>
		<th><?php echo __('Exist Mate Age'); ?></th>
		<th><?php echo __('Exist Mate Language'); ?></th>
		<th><?php echo __('Exist Mate Nationality'); ?></th>
		<th><?php echo __('Exist Mate Orientation'); ?></th>
		<th><?php echo __('Exist Mate Interests'); ?></th>
		<th><?php echo __('Is Orientation Part'); ?></th>
		<th><?php echo __('New Mate Smoking'); ?></th>
		<th><?php echo __('New Mate Gender'); ?></th>
		<th><?php echo __('New Mate Occupation'); ?></th>
		<th><?php echo __('New Mate Pets'); ?></th>
		<th><?php echo __('New Mate Min Age'); ?></th>
		<th><?php echo __('New Mate Max Age'); ?></th>
		<th><?php echo __('New Mate Language'); ?></th>
		<th><?php echo __('New Mate Orientation'); ?></th>
		<th><?php echo __('New Mate Couples Welcomed'); ?></th>
		<th><?php echo __('New Mate Dss Welcomed'); ?></th>
		<th><?php echo __('New Mate Veg Pref'); ?></th>
		<th><?php echo __('Short Description'); ?></th>
		<th><?php echo __('Long Description'); ?></th>
		<th><?php echo __('Company Name'); ?></th>
		<th><?php echo __('Display Name'); ?></th>
		<th><?php echo __('Telephone'); ?></th>
		<th><?php echo __('Show Telephone'); ?></th>
		<th><?php echo __('Daily Mail'); ?></th>
		<th><?php echo __('Instant Mail'); ?></th>
		<th><?php echo __('Max Mail'); ?></th>
		<th><?php echo __('Photo 1'); ?></th>
		<th><?php echo __('Photo 1 Desc'); ?></th>
		<th><?php echo __('Photo 2'); ?></th>
		<th><?php echo __('Photo 2 Desc'); ?></th>
		<th><?php echo __('Photo 3'); ?></th>
		<th><?php echo __('Photo 3 Desc'); ?></th>
		<th><?php echo __('Photo 4'); ?></th>
		<th><?php echo __('Photo 4 Desc'); ?></th>
		<th><?php echo __('Photo 5'); ?></th>
		<th><?php echo __('Photo 5 Desc'); ?></th>
		<th><?php echo __('Video Link'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($user['RentAd'] as $rentAd): ?>
		<tr>
			<td><?php echo $rentAd['id']; ?></td>
			<td><?php echo $rentAd['user_id']; ?></td>
			<td><?php echo $rentAd['rooms_for_rent']; ?></td>
			<td><?php echo $rentAd['rooms_in_property']; ?></td>
			<td><?php echo $rentAd['property_type']; ?></td>
			<td><?php echo $rentAd['current_occupants']; ?></td>
			<td><?php echo $rentAd['postcode']; ?></td>
			<td><?php echo $rentAd['advertiser_role']; ?></td>
			<td><?php echo $rentAd['email_id']; ?></td>
			<td><?php echo $rentAd['area_id']; ?></td>
			<td><?php echo $rentAd['street_name']; ?></td>
			<td><?php echo $rentAd['no_of_mins']; ?></td>
			<td><?php echo $rentAd['no_of_mins_by']; ?></td>
			<td><?php echo $rentAd['station_id']; ?></td>
			<td><?php echo $rentAd['living_room']; ?></td>
			<td><?php echo $rentAd['aminities']; ?></td>
			<td><?php echo $rentAd['cost_of_room']; ?></td>
			<td><?php echo $rentAd['per_week']; ?></td>
			<td><?php echo $rentAd['room_size']; ?></td>
			<td><?php echo $rentAd['room_aminities']; ?></td>
			<td><?php echo $rentAd['security_deposit']; ?></td>
			<td><?php echo $rentAd['available_from']; ?></td>
			<td><?php echo $rentAd['min_stay']; ?></td>
			<td><?php echo $rentAd['max_stay']; ?></td>
			<td><?php echo $rentAd['short_term_considered']; ?></td>
			<td><?php echo $rentAd['days_available']; ?></td>
			<td><?php echo $rentAd['references_needed']; ?></td>
			<td><?php echo $rentAd['fees_apply']; ?></td>
			<td><?php echo $rentAd['bills_inc']; ?></td>
			<td><?php echo $rentAd['broadband']; ?></td>
			<td><?php echo $rentAd['exist_mate_smoking']; ?></td>
			<td><?php echo $rentAd['exist_mate_gender']; ?></td>
			<td><?php echo $rentAd['exist_mate_occupation']; ?></td>
			<td><?php echo $rentAd['exist_mate_pets']; ?></td>
			<td><?php echo $rentAd['exist_mate_age']; ?></td>
			<td><?php echo $rentAd['exist_mate_language']; ?></td>
			<td><?php echo $rentAd['exist_mate_nationality']; ?></td>
			<td><?php echo $rentAd['exist_mate_orientation']; ?></td>
			<td><?php echo $rentAd['exist_mate_interests']; ?></td>
			<td><?php echo $rentAd['is_orientation_part']; ?></td>
			<td><?php echo $rentAd['new_mate_smoking']; ?></td>
			<td><?php echo $rentAd['new_mate_gender']; ?></td>
			<td><?php echo $rentAd['new_mate_occupation']; ?></td>
			<td><?php echo $rentAd['new_mate_pets']; ?></td>
			<td><?php echo $rentAd['new_mate_min_age']; ?></td>
			<td><?php echo $rentAd['new_mate_max_age']; ?></td>
			<td><?php echo $rentAd['new_mate_language']; ?></td>
			<td><?php echo $rentAd['new_mate_orientation']; ?></td>
			<td><?php echo $rentAd['new_mate_couples_welcomed']; ?></td>
			<td><?php echo $rentAd['new_mate_dss_welcomed']; ?></td>
			<td><?php echo $rentAd['new_mate_veg_pref']; ?></td>
			<td><?php echo $rentAd['short_description']; ?></td>
			<td><?php echo $rentAd['long_description']; ?></td>
			<td><?php echo $rentAd['company_name']; ?></td>
			<td><?php echo $rentAd['display_name']; ?></td>
			<td><?php echo $rentAd['telephone']; ?></td>
			<td><?php echo $rentAd['show_telephone']; ?></td>
			<td><?php echo $rentAd['daily_mail']; ?></td>
			<td><?php echo $rentAd['instant_mail']; ?></td>
			<td><?php echo $rentAd['max_mail']; ?></td>
			<td><?php echo $rentAd['photo_1']; ?></td>
			<td><?php echo $rentAd['photo_1_desc']; ?></td>
			<td><?php echo $rentAd['photo_2']; ?></td>
			<td><?php echo $rentAd['photo_2_desc']; ?></td>
			<td><?php echo $rentAd['photo_3']; ?></td>
			<td><?php echo $rentAd['photo_3_desc']; ?></td>
			<td><?php echo $rentAd['photo_4']; ?></td>
			<td><?php echo $rentAd['photo_4_desc']; ?></td>
			<td><?php echo $rentAd['photo_5']; ?></td>
			<td><?php echo $rentAd['photo_5_desc']; ?></td>
			<td><?php echo $rentAd['video_link']; ?></td>
			<td><?php echo $rentAd['created']; ?></td>
			<td><?php echo $rentAd['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'rent_ads', 'action' => 'view', $rentAd['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'rent_ads', 'action' => 'edit', $rentAd['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'rent_ads', 'action' => 'delete', $rentAd['id']), array(), __('Are you sure you want to delete # %s?', $rentAd['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Rent Ad'), array('controller' => 'rent_ads', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Saved Ads'); ?></h3>
	<?php if (!empty($user['SavedAd'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Add Type'); ?></th>
		<th><?php echo __('Property Ad Id'); ?></th>
		<th><?php echo __('Rent Ad Id'); ?></th>
		<th><?php echo __('Wanted Ad Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($user['SavedAd'] as $savedAd): ?>
		<tr>
			<td><?php echo $savedAd['id']; ?></td>
			<td><?php echo $savedAd['add_type']; ?></td>
			<td><?php echo $savedAd['property_ad_id']; ?></td>
			<td><?php echo $savedAd['rent_ad_id']; ?></td>
			<td><?php echo $savedAd['wanted_ad_id']; ?></td>
			<td><?php echo $savedAd['user_id']; ?></td>
			<td><?php echo $savedAd['created']; ?></td>
			<td><?php echo $savedAd['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'saved_ads', 'action' => 'view', $savedAd['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'saved_ads', 'action' => 'edit', $savedAd['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'saved_ads', 'action' => 'delete', $savedAd['id']), array(), __('Are you sure you want to delete # %s?', $savedAd['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Saved Ad'), array('controller' => 'saved_ads', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Saved Searches'); ?></h3>
	<?php if (!empty($user['SavedSearch'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Parameters'); ?></th>
		<th><?php echo __('Daily Alert'); ?></th>
		<th><?php echo __('Instant Alert'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($user['SavedSearch'] as $savedSearch): ?>
		<tr>
			<td><?php echo $savedSearch['id']; ?></td>
			<td><?php echo $savedSearch['user_id']; ?></td>
			<td><?php echo $savedSearch['parameters']; ?></td>
			<td><?php echo $savedSearch['daily_alert']; ?></td>
			<td><?php echo $savedSearch['instant_alert']; ?></td>
			<td><?php echo $savedSearch['created']; ?></td>
			<td><?php echo $savedSearch['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'saved_searches', 'action' => 'view', $savedSearch['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'saved_searches', 'action' => 'edit', $savedSearch['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'saved_searches', 'action' => 'delete', $savedSearch['id']), array(), __('Are you sure you want to delete # %s?', $savedSearch['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Saved Search'), array('controller' => 'saved_searches', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Testimonials'); ?></h3>
	<?php if (!empty($user['Testimonial'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Comment'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($user['Testimonial'] as $testimonial): ?>
		<tr>
			<td><?php echo $testimonial['id']; ?></td>
			<td><?php echo $testimonial['user_id']; ?></td>
			<td><?php echo $testimonial['comment']; ?></td>
			<td><?php echo $testimonial['created']; ?></td>
			<td><?php echo $testimonial['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'testimonials', 'action' => 'view', $testimonial['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'testimonials', 'action' => 'edit', $testimonial['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'testimonials', 'action' => 'delete', $testimonial['id']), array(), __('Are you sure you want to delete # %s?', $testimonial['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Testimonial'), array('controller' => 'testimonials', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Wanted Ads'); ?></h3>
	<?php if (!empty($user['WantedAd'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Genders'); ?></th>
		<th><?php echo __('Looking For'); ?></th>
		<th><?php echo __('Buddy Ups'); ?></th>
		<th><?php echo __('Area Ids'); ?></th>
		<th><?php echo __('Budget'); ?></th>
		<th><?php echo __('Per Week'); ?></th>
		<th><?php echo __('Available From'); ?></th>
		<th><?php echo __('Min Stay'); ?></th>
		<th><?php echo __('Max Stay'); ?></th>
		<th><?php echo __('Weekly Stay'); ?></th>
		<th><?php echo __('Amenities'); ?></th>
		<th><?php echo __('Age'); ?></th>
		<th><?php echo __('Occupation'); ?></th>
		<th><?php echo __('Dss'); ?></th>
		<th><?php echo __('Smoker'); ?></th>
		<th><?php echo __('Pets'); ?></th>
		<th><?php echo __('Orientation'); ?></th>
		<th><?php echo __('Is Orientation Part'); ?></th>
		<th><?php echo __('Language'); ?></th>
		<th><?php echo __('Nationality'); ?></th>
		<th><?php echo __('Interests'); ?></th>
		<th><?php echo __('Display Name'); ?></th>
		<th><?php echo __('Prefered Gender'); ?></th>
		<th><?php echo __('Prefered Age Min'); ?></th>
		<th><?php echo __('Prefered Age Max'); ?></th>
		<th><?php echo __('Prefered Occupation'); ?></th>
		<th><?php echo __('Prefered Smoking'); ?></th>
		<th><?php echo __('Prefered Pets'); ?></th>
		<th><?php echo __('Prefered Orientation'); ?></th>
		<th><?php echo __('Title'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Telephone'); ?></th>
		<th><?php echo __('Show Telephone'); ?></th>
		<th><?php echo __('Daily Mail'); ?></th>
		<th><?php echo __('Instant Mail'); ?></th>
		<th><?php echo __('Max Mail'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($user['WantedAd'] as $wantedAd): ?>
		<tr>
			<td><?php echo $wantedAd['id']; ?></td>
			<td><?php echo $wantedAd['user_id']; ?></td>
			<td><?php echo $wantedAd['genders']; ?></td>
			<td><?php echo $wantedAd['looking_for']; ?></td>
			<td><?php echo $wantedAd['buddy_ups']; ?></td>
			<td><?php echo $wantedAd['area_ids']; ?></td>
			<td><?php echo $wantedAd['budget']; ?></td>
			<td><?php echo $wantedAd['per_week']; ?></td>
			<td><?php echo $wantedAd['available_from']; ?></td>
			<td><?php echo $wantedAd['min_stay']; ?></td>
			<td><?php echo $wantedAd['max_stay']; ?></td>
			<td><?php echo $wantedAd['weekly_stay']; ?></td>
			<td><?php echo $wantedAd['amenities']; ?></td>
			<td><?php echo $wantedAd['age']; ?></td>
			<td><?php echo $wantedAd['occupation']; ?></td>
			<td><?php echo $wantedAd['dss']; ?></td>
			<td><?php echo $wantedAd['smoker']; ?></td>
			<td><?php echo $wantedAd['pets']; ?></td>
			<td><?php echo $wantedAd['orientation']; ?></td>
			<td><?php echo $wantedAd['is_orientation_part']; ?></td>
			<td><?php echo $wantedAd['language']; ?></td>
			<td><?php echo $wantedAd['nationality']; ?></td>
			<td><?php echo $wantedAd['interests']; ?></td>
			<td><?php echo $wantedAd['display_name']; ?></td>
			<td><?php echo $wantedAd['prefered_gender']; ?></td>
			<td><?php echo $wantedAd['prefered_age_min']; ?></td>
			<td><?php echo $wantedAd['prefered_age_max']; ?></td>
			<td><?php echo $wantedAd['prefered_occupation']; ?></td>
			<td><?php echo $wantedAd['prefered_smoking']; ?></td>
			<td><?php echo $wantedAd['prefered_pets']; ?></td>
			<td><?php echo $wantedAd['prefered_orientation']; ?></td>
			<td><?php echo $wantedAd['title']; ?></td>
			<td><?php echo $wantedAd['description']; ?></td>
			<td><?php echo $wantedAd['telephone']; ?></td>
			<td><?php echo $wantedAd['show_telephone']; ?></td>
			<td><?php echo $wantedAd['daily_mail']; ?></td>
			<td><?php echo $wantedAd['instant_mail']; ?></td>
			<td><?php echo $wantedAd['max_mail']; ?></td>
			<td><?php echo $wantedAd['created']; ?></td>
			<td><?php echo $wantedAd['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'wanted_ads', 'action' => 'view', $wantedAd['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'wanted_ads', 'action' => 'edit', $wantedAd['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'wanted_ads', 'action' => 'delete', $wantedAd['id']), array(), __('Are you sure you want to delete # %s?', $wantedAd['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Wanted Ad'), array('controller' => 'wanted_ads', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
