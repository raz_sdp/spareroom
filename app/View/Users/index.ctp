<?php
    //$session_user_id = $this->Session->read('user_id');
    //AuthComponent::_setTrace($session_user_id);
?>
<div class="users index">
	<h2><?php echo __('Users'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('email'); ?></th>
			<th><?php echo $this->Paginator->sort('password'); ?></th>
			<th><?php echo $this->Paginator->sort('simple_pwd'); ?></th>
			<th><?php echo $this->Paginator->sort('first_name'); ?></th>
			<th><?php echo $this->Paginator->sort('last_name'); ?></th>
			<th><?php echo $this->Paginator->sort('gender'); ?></th>
			<th><?php echo $this->Paginator->sort('looking_for_flat'); ?></th>
			<th><?php echo $this->Paginator->sort('have_flat'); ?></th>
			<th><?php echo $this->Paginator->sort('new_share'); ?></th>
			<th><?php echo $this->Paginator->sort('adverts'); ?></th>
			<th><?php echo $this->Paginator->sort('newsletter'); ?></th>
			<th><?php echo $this->Paginator->sort('heard_where'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($users as $user): ?>
	<tr>
		<td><?php echo h($user['User']['email']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['password']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['simple_pwd']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['first_name']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['last_name']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['gender']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['looking_for_flat']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['have_flat']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['new_share']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['adverts']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['newsletter']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['heard_where']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $user['User']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $user['User']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $user['User']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $user['User']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New User'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Property Ads'), array('controller' => 'property_ads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Ad'), array('controller' => 'property_ads', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Rent Ads'), array('controller' => 'rent_ads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Rent Ad'), array('controller' => 'rent_ads', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Saved Ads'), array('controller' => 'saved_ads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Saved Ad'), array('controller' => 'saved_ads', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Saved Searches'), array('controller' => 'saved_searches', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Saved Search'), array('controller' => 'saved_searches', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Testimonials'), array('controller' => 'testimonials', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Testimonial'), array('controller' => 'testimonials', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Wanted Ads'), array('controller' => 'wanted_ads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Wanted Ad'), array('controller' => 'wanted_ads', 'action' => 'add')); ?> </li>
	</ul>
</div>
