<div class="rentAds index">
	<h2><?php echo __('Rent Ads'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('rooms_for_rent'); ?></th>
			<th><?php echo $this->Paginator->sort('rooms_in_property'); ?></th>
			<th><?php echo $this->Paginator->sort('property_type'); ?></th>
			<th><?php echo $this->Paginator->sort('current_occupants'); ?></th>
			<th><?php echo $this->Paginator->sort('postcode'); ?></th>
			<th><?php echo $this->Paginator->sort('advertiser_role'); ?></th>
			<th><?php echo $this->Paginator->sort('email_id'); ?></th>
			<th><?php echo $this->Paginator->sort('area_id'); ?></th>
			<th><?php echo $this->Paginator->sort('street_name'); ?></th>
			<th><?php echo $this->Paginator->sort('no_of_mins'); ?></th>
			<th><?php echo $this->Paginator->sort('no_of_mins_by'); ?></th>
			<th><?php echo $this->Paginator->sort('station_id'); ?></th>
			<th><?php echo $this->Paginator->sort('living_room'); ?></th>
			<th><?php echo $this->Paginator->sort('aminities'); ?></th>
			<th><?php echo $this->Paginator->sort('cost_of_room'); ?></th>
			<th><?php echo $this->Paginator->sort('per_week'); ?></th>
			<th><?php echo $this->Paginator->sort('room_size'); ?></th>
			<th><?php echo $this->Paginator->sort('room_aminities'); ?></th>
			<th><?php echo $this->Paginator->sort('security_deposit'); ?></th>
			<th><?php echo $this->Paginator->sort('available_from'); ?></th>
			<th><?php echo $this->Paginator->sort('min_stay'); ?></th>
			<th><?php echo $this->Paginator->sort('max_stay'); ?></th>
			<th><?php echo $this->Paginator->sort('short_term_considered'); ?></th>
			<th><?php echo $this->Paginator->sort('days_available'); ?></th>
			<th><?php echo $this->Paginator->sort('references_needed'); ?></th>
			<th><?php echo $this->Paginator->sort('fees_apply'); ?></th>
			<th><?php echo $this->Paginator->sort('bills_inc'); ?></th>
			<th><?php echo $this->Paginator->sort('broadband'); ?></th>
			<th><?php echo $this->Paginator->sort('exist_mate_smoking'); ?></th>
			<th><?php echo $this->Paginator->sort('exist_mate_gender'); ?></th>
			<th><?php echo $this->Paginator->sort('exist_mate_occupation'); ?></th>
			<th><?php echo $this->Paginator->sort('exist_mate_pets'); ?></th>
			<th><?php echo $this->Paginator->sort('exist_mate_age'); ?></th>
			<th><?php echo $this->Paginator->sort('exist_mate_language'); ?></th>
			<th><?php echo $this->Paginator->sort('exist_mate_nationality'); ?></th>
			<th><?php echo $this->Paginator->sort('exist_mate_orientation'); ?></th>
			<th><?php echo $this->Paginator->sort('exist_mate_interests'); ?></th>
			<th><?php echo $this->Paginator->sort('is_orientation_part'); ?></th>
			<th><?php echo $this->Paginator->sort('new_mate_smoking'); ?></th>
			<th><?php echo $this->Paginator->sort('new_mate_gender'); ?></th>
			<th><?php echo $this->Paginator->sort('new_mate_occupation'); ?></th>
			<th><?php echo $this->Paginator->sort('new_mate_pets'); ?></th>
			<th><?php echo $this->Paginator->sort('new_mate_min_age'); ?></th>
			<th><?php echo $this->Paginator->sort('new_mate_max_age'); ?></th>
			<th><?php echo $this->Paginator->sort('new_mate_language'); ?></th>
			<th><?php echo $this->Paginator->sort('new_mate_orientation'); ?></th>
			<th><?php echo $this->Paginator->sort('new_mate_couples_welcomed'); ?></th>
			<th><?php echo $this->Paginator->sort('new_mate_dss_welcomed'); ?></th>
			<th><?php echo $this->Paginator->sort('new_mate_veg_pref'); ?></th>
			<th><?php echo $this->Paginator->sort('short_description'); ?></th>
			<th><?php echo $this->Paginator->sort('long_description'); ?></th>
			<th><?php echo $this->Paginator->sort('company_name'); ?></th>
			<th><?php echo $this->Paginator->sort('display_name'); ?></th>
			<th><?php echo $this->Paginator->sort('telephone'); ?></th>
			<th><?php echo $this->Paginator->sort('show_telephone'); ?></th>
			<th><?php echo $this->Paginator->sort('daily_mail'); ?></th>
			<th><?php echo $this->Paginator->sort('instant_mail'); ?></th>
			<th><?php echo $this->Paginator->sort('max_mail'); ?></th>
			<th><?php echo $this->Paginator->sort('photo_1'); ?></th>
			<th><?php echo $this->Paginator->sort('photo_1_desc'); ?></th>
			<th><?php echo $this->Paginator->sort('photo_2'); ?></th>
			<th><?php echo $this->Paginator->sort('photo_2_desc'); ?></th>
			<th><?php echo $this->Paginator->sort('photo_3'); ?></th>
			<th><?php echo $this->Paginator->sort('photo_3_desc'); ?></th>
			<th><?php echo $this->Paginator->sort('photo_4'); ?></th>
			<th><?php echo $this->Paginator->sort('photo_4_desc'); ?></th>
			<th><?php echo $this->Paginator->sort('photo_5'); ?></th>
			<th><?php echo $this->Paginator->sort('photo_5_desc'); ?></th>
			<th><?php echo $this->Paginator->sort('video_link'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($rentAds as $rentAd): ?>
	<tr>
		<td><?php echo h($rentAd['RentAd']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($rentAd['User']['id'], array('controller' => 'users', 'action' => 'view', $rentAd['User']['id'])); ?>
		</td>
		<td><?php echo h($rentAd['RentAd']['rooms_for_rent']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['rooms_in_property']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['property_type']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['current_occupants']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['postcode']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['advertiser_role']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['email_id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($rentAd['Area']['id'], array('controller' => 'areas', 'action' => 'view', $rentAd['Area']['id'])); ?>
		</td>
		<td><?php echo h($rentAd['RentAd']['street_name']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['no_of_mins']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['no_of_mins_by']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['station_id']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['living_room']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['aminities']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['cost_of_room']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['per_week']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['room_size']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['room_aminities']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['security_deposit']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['available_from']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['min_stay']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['max_stay']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['short_term_considered']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['days_available']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['references_needed']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['fees_apply']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['bills_inc']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['broadband']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['exist_mate_smoking']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['exist_mate_gender']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['exist_mate_occupation']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['exist_mate_pets']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['exist_mate_age']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['exist_mate_language']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['exist_mate_nationality']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['exist_mate_orientation']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['exist_mate_interests']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['is_orientation_part']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['new_mate_smoking']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['new_mate_gender']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['new_mate_occupation']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['new_mate_pets']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['new_mate_min_age']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['new_mate_max_age']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['new_mate_language']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['new_mate_orientation']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['new_mate_couples_welcomed']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['new_mate_dss_welcomed']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['new_mate_veg_pref']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['short_description']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['long_description']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['company_name']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['display_name']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['telephone']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['show_telephone']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['daily_mail']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['instant_mail']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['max_mail']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['photo_1']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['photo_1_desc']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['photo_2']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['photo_2_desc']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['photo_3']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['photo_3_desc']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['photo_4']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['photo_4_desc']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['photo_5']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['photo_5_desc']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['video_link']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['created']); ?>&nbsp;</td>
		<td><?php echo h($rentAd['RentAd']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $rentAd['RentAd']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $rentAd['RentAd']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $rentAd['RentAd']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $rentAd['RentAd']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Rent Ad'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Areas'), array('controller' => 'areas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Area'), array('controller' => 'areas', 'action' => 'add')); ?> </li>
	</ul>
</div>
