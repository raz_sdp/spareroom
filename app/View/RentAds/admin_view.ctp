<div class="rentAds view">
<h2><?php echo __('Rent Ad'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($rentAd['User']['id'], array('controller' => 'users', 'action' => 'view', $rentAd['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Rooms For Rent'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['rooms_for_rent']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Rooms In Property'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['rooms_in_property']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Property Type'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['property_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Current Occupants'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['current_occupants']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Postcode'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['postcode']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Advertiser Role'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['advertiser_role']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email Id'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['email_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Area'); ?></dt>
		<dd>
			<?php echo $this->Html->link($rentAd['Area']['id'], array('controller' => 'areas', 'action' => 'view', $rentAd['Area']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Street Name'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['street_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('No Of Mins'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['no_of_mins']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('No Of Mins By'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['no_of_mins_by']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Station Id'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['station_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Living Room'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['living_room']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Aminities'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['aminities']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cost Of Room'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['cost_of_room']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Per Week'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['per_week']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Room Size'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['room_size']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Room Aminities'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['room_aminities']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Security Deposit'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['security_deposit']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Available From'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['available_from']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Min Stay'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['min_stay']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Max Stay'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['max_stay']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Short Term Considered'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['short_term_considered']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Days Available'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['days_available']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('References Needed'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['references_needed']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fees Apply'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['fees_apply']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bills Inc'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['bills_inc']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Broadband'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['broadband']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Exist Mate Smoking'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['exist_mate_smoking']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Exist Mate Gender'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['exist_mate_gender']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Exist Mate Occupation'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['exist_mate_occupation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Exist Mate Pets'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['exist_mate_pets']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Exist Mate Age'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['exist_mate_age']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Exist Mate Language'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['exist_mate_language']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Exist Mate Nationality'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['exist_mate_nationality']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Exist Mate Orientation'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['exist_mate_orientation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Exist Mate Interests'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['exist_mate_interests']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Orientation Part'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['is_orientation_part']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('New Mate Smoking'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['new_mate_smoking']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('New Mate Gender'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['new_mate_gender']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('New Mate Occupation'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['new_mate_occupation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('New Mate Pets'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['new_mate_pets']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('New Mate Min Age'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['new_mate_min_age']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('New Mate Max Age'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['new_mate_max_age']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('New Mate Language'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['new_mate_language']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('New Mate Orientation'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['new_mate_orientation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('New Mate Couples Welcomed'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['new_mate_couples_welcomed']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('New Mate Dss Welcomed'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['new_mate_dss_welcomed']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('New Mate Veg Pref'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['new_mate_veg_pref']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Short Description'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['short_description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Long Description'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['long_description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Company Name'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['company_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Display Name'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['display_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Telephone'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['telephone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Show Telephone'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['show_telephone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Daily Mail'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['daily_mail']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Instant Mail'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['instant_mail']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Max Mail'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['max_mail']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Photo 1'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['photo_1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Photo 1 Desc'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['photo_1_desc']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Photo 2'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['photo_2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Photo 2 Desc'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['photo_2_desc']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Photo 3'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['photo_3']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Photo 3 Desc'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['photo_3_desc']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Photo 4'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['photo_4']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Photo 4 Desc'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['photo_4_desc']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Photo 5'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['photo_5']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Photo 5 Desc'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['photo_5_desc']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Video Link'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['video_link']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($rentAd['RentAd']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Rent Ad'), array('action' => 'edit', $rentAd['RentAd']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Rent Ad'), array('action' => 'delete', $rentAd['RentAd']['id']), array(), __('Are you sure you want to delete # %s?', $rentAd['RentAd']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Rent Ads'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Rent Ad'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Areas'), array('controller' => 'areas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Area'), array('controller' => 'areas', 'action' => 'add')); ?> </li>
	</ul>
</div>
