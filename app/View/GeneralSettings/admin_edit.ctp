<?php echo $this->element('admin_menu');?>
<div class="index col-md-10 col-sm-10">
    <div class="white">
    <?php echo $this->Form->create('GeneralSetting', array('type'=>'file')); ?>
        <fieldset>
            <legend><?php echo __('Admin Edit General Setting'); ?></legend>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Name</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('name',array('label' => false,'class'=>'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Image</label>
                <div class="col-sm-9">
                    <?php if(!empty($this->Form->data['GeneralSetting']['logo'])){
                    echo "<div class=\"thumbnail-item\">";
                        echo $this->Html->image('/files/logos/' . $this->request->data['GeneralSetting']['logo'], array('alt'=>'picture', 'width'=> '150px','height' => 'auto'));
                        echo "</div>";
                    } ?>
                    <?php echo $this->Form->input('logo',array('label' => false,'class'=>'form-control' ,'type' => 'file')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('email',array('label' => false,'class'=>'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Phone</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('Phone',array('label' => false,'class'=>'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Facebook Link</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('facebook_link',array('label' => false,'class'=>'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Twitter Link</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('twitter_link',array('label' => false,'class'=>'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Copyright Text</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('copyright_text',array('label' => false,'class'=>'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Address 1</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('address_1',array('label' => false,'class'=>'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Address 2</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('address_2',array('label' => false,'class'=>'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">City</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('city',array('label' => false,'class'=>'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Country</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('country',array('label' => false,'class'=>'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Postcode</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('postcode',array('label' => false,'class'=>'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Latitude</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('lat',array('label' => false,'class'=>'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Longitude</label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('lng',array('label' => false,'class'=>'form-control')); ?>
                </div>
            </div>
        <?php
            /*echo $this->Form->input('name');
            if(!empty($this->Form->data['GeneralSetting']['logo'])){
                echo "<div class=\"thumbnail-item\">";
                echo $this->Html->image('/files/logos/' . $this->request->data['GeneralSetting']['logo'], array('alt'=>'picture', 'width'=> '150px','height' => 'auto'));
                echo "</div>";
            }
            echo $this->Form->input('logo', array('type'=>'file'));
            echo $this->Form->input('about');
            echo $this->Form->input('email');
            echo $this->Form->input('phone');
            echo $this->Form->input('facebook_link');
            echo $this->Form->input('twitter_link');
            echo $this->Form->input('copyright_text');
            echo $this->Form->input('address_1');
            echo $this->Form->input('address_2');
            echo $this->Form->input('city');
            echo $this->Form->input('country');
            echo $this->Form->input('postcode');
            echo $this->Form->input('lat');
            echo $this->Form->input('lng');*/
        ?>
        </fieldset>
    <?php echo $this->Form->end(array('Submit', 'class'=>'btn submit-green s-c')); ?>
    </div>
</div>
