<div class="propertyAds index">
	<h2><?php echo __('Property Ads'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('no_rooms'); ?></th>
			<th><?php echo $this->Paginator->sort('type'); ?></th>
			<th><?php echo $this->Paginator->sort('rent'); ?></th>
			<th><?php echo $this->Paginator->sort('per_week'); ?></th>
			<th><?php echo $this->Paginator->sort('advertiser_type'); ?></th>
			<th><?php echo $this->Paginator->sort('area_id'); ?></th>
			<th><?php echo $this->Paginator->sort('street_name'); ?></th>
			<th><?php echo $this->Paginator->sort('no_of_mins'); ?></th>
			<th><?php echo $this->Paginator->sort('no_of_mins_by'); ?></th>
			<th><?php echo $this->Paginator->sort('station_id'); ?></th>
			<th><?php echo $this->Paginator->sort('security_deposit'); ?></th>
			<th><?php echo $this->Paginator->sort('furnishing'); ?></th>
			<th><?php echo $this->Paginator->sort('available_from'); ?></th>
			<th><?php echo $this->Paginator->sort('min_stay'); ?></th>
			<th><?php echo $this->Paginator->sort('max_stay'); ?></th>
			<th><?php echo $this->Paginator->sort('short_term_considered'); ?></th>
			<th><?php echo $this->Paginator->sort('references_needed'); ?></th>
			<th><?php echo $this->Paginator->sort('fees_apply'); ?></th>
			<th><?php echo $this->Paginator->sort('aminities'); ?></th>
			<th><?php echo $this->Paginator->sort('tenant_smoking'); ?></th>
			<th><?php echo $this->Paginator->sort('tenant_occupation'); ?></th>
			<th><?php echo $this->Paginator->sort('tenant_pets'); ?></th>
			<th><?php echo $this->Paginator->sort('tenant_dss_welcomed'); ?></th>
			<th><?php echo $this->Paginator->sort('title'); ?></th>
			<th><?php echo $this->Paginator->sort('description'); ?></th>
			<th><?php echo $this->Paginator->sort('company_name'); ?></th>
			<th><?php echo $this->Paginator->sort('display_name'); ?></th>
			<th><?php echo $this->Paginator->sort('telephone'); ?></th>
			<th><?php echo $this->Paginator->sort('display_telephone'); ?></th>
			<th><?php echo $this->Paginator->sort('daily_mail'); ?></th>
			<th><?php echo $this->Paginator->sort('instant_mail'); ?></th>
			<th><?php echo $this->Paginator->sort('max_mail'); ?></th>
			<th><?php echo $this->Paginator->sort('photo_1'); ?></th>
			<th><?php echo $this->Paginator->sort('photo_1_desc'); ?></th>
			<th><?php echo $this->Paginator->sort('photo_2'); ?></th>
			<th><?php echo $this->Paginator->sort('photo_2_desc'); ?></th>
			<th><?php echo $this->Paginator->sort('photo_3'); ?></th>
			<th><?php echo $this->Paginator->sort('photo_3_desc'); ?></th>
			<th><?php echo $this->Paginator->sort('photo_4'); ?></th>
			<th><?php echo $this->Paginator->sort('photo_4_desc'); ?></th>
			<th><?php echo $this->Paginator->sort('photo_5'); ?></th>
			<th><?php echo $this->Paginator->sort('photo_5_desc'); ?></th>
			<th><?php echo $this->Paginator->sort('video_link'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($propertyAds as $propertyAd): ?>
	<tr>
		<td><?php echo h($propertyAd['PropertyAd']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($propertyAd['User']['id'], array('controller' => 'users', 'action' => 'view', $propertyAd['User']['id'])); ?>
		</td>
		<td><?php echo h($propertyAd['PropertyAd']['no_rooms']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['type']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['rent']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['per_week']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['advertiser_type']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($propertyAd['Area']['id'], array('controller' => 'areas', 'action' => 'view', $propertyAd['Area']['id'])); ?>
		</td>
		<td><?php echo h($propertyAd['PropertyAd']['street_name']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['no_of_mins']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['no_of_mins_by']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['station_id']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['security_deposit']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['furnishing']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['available_from']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['min_stay']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['max_stay']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['short_term_considered']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['references_needed']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['fees_apply']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['aminities']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['tenant_smoking']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['tenant_occupation']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['tenant_pets']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['tenant_dss_welcomed']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['title']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['description']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['company_name']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['display_name']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['telephone']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['display_telephone']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['daily_mail']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['instant_mail']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['max_mail']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['photo_1']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['photo_1_desc']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['photo_2']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['photo_2_desc']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['photo_3']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['photo_3_desc']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['photo_4']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['photo_4_desc']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['photo_5']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['photo_5_desc']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['video_link']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['created']); ?>&nbsp;</td>
		<td><?php echo h($propertyAd['PropertyAd']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $propertyAd['PropertyAd']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $propertyAd['PropertyAd']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $propertyAd['PropertyAd']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $propertyAd['PropertyAd']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Property Ad'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Areas'), array('controller' => 'areas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Area'), array('controller' => 'areas', 'action' => 'add')); ?> </li>
	</ul>
</div>
