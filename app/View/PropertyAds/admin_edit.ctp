<div class="propertyAds form">
<?php echo $this->Form->create('PropertyAd'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Property Ad'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('user_id');
		echo $this->Form->input('no_rooms');
		echo $this->Form->input('type');
		echo $this->Form->input('rent');
		echo $this->Form->input('per_week');
		echo $this->Form->input('advertiser_type');
		echo $this->Form->input('area_id');
		echo $this->Form->input('street_name');
		echo $this->Form->input('no_of_mins');
		echo $this->Form->input('no_of_mins_by');
		echo $this->Form->input('station_id');
		echo $this->Form->input('security_deposit');
		echo $this->Form->input('furnishing');
		echo $this->Form->input('available_from');
		echo $this->Form->input('min_stay');
		echo $this->Form->input('max_stay');
		echo $this->Form->input('short_term_considered');
		echo $this->Form->input('references_needed');
		echo $this->Form->input('fees_apply');
		echo $this->Form->input('aminities');
		echo $this->Form->input('tenant_smoking');
		echo $this->Form->input('tenant_occupation');
		echo $this->Form->input('tenant_pets');
		echo $this->Form->input('tenant_dss_welcomed');
		echo $this->Form->input('title');
		echo $this->Form->input('description');
		echo $this->Form->input('company_name');
		echo $this->Form->input('display_name');
		echo $this->Form->input('telephone');
		echo $this->Form->input('display_telephone');
		echo $this->Form->input('daily_mail');
		echo $this->Form->input('instant_mail');
		echo $this->Form->input('max_mail');
		echo $this->Form->input('photo_1');
		echo $this->Form->input('photo_1_desc');
		echo $this->Form->input('photo_2');
		echo $this->Form->input('photo_2_desc');
		echo $this->Form->input('photo_3');
		echo $this->Form->input('photo_3_desc');
		echo $this->Form->input('photo_4');
		echo $this->Form->input('photo_4_desc');
		echo $this->Form->input('photo_5');
		echo $this->Form->input('photo_5_desc');
		echo $this->Form->input('video_link');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('PropertyAd.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('PropertyAd.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Property Ads'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Areas'), array('controller' => 'areas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Area'), array('controller' => 'areas', 'action' => 'add')); ?> </li>
	</ul>
</div>
