<div class="propertyAds view">
<h2><?php echo __('Property Ad'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($propertyAd['User']['id'], array('controller' => 'users', 'action' => 'view', $propertyAd['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('No Rooms'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['no_rooms']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Rent'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['rent']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Per Week'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['per_week']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Advertiser Type'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['advertiser_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Area'); ?></dt>
		<dd>
			<?php echo $this->Html->link($propertyAd['Area']['id'], array('controller' => 'areas', 'action' => 'view', $propertyAd['Area']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Street Name'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['street_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('No Of Mins'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['no_of_mins']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('No Of Mins By'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['no_of_mins_by']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Station Id'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['station_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Security Deposit'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['security_deposit']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Furnishing'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['furnishing']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Available From'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['available_from']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Min Stay'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['min_stay']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Max Stay'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['max_stay']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Short Term Considered'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['short_term_considered']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('References Needed'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['references_needed']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fees Apply'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['fees_apply']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Aminities'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['aminities']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tenant Smoking'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['tenant_smoking']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tenant Occupation'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['tenant_occupation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tenant Pets'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['tenant_pets']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tenant Dss Welcomed'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['tenant_dss_welcomed']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Company Name'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['company_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Display Name'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['display_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Telephone'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['telephone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Display Telephone'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['display_telephone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Daily Mail'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['daily_mail']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Instant Mail'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['instant_mail']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Max Mail'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['max_mail']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Photo 1'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['photo_1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Photo 1 Desc'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['photo_1_desc']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Photo 2'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['photo_2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Photo 2 Desc'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['photo_2_desc']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Photo 3'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['photo_3']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Photo 3 Desc'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['photo_3_desc']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Photo 4'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['photo_4']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Photo 4 Desc'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['photo_4_desc']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Photo 5'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['photo_5']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Photo 5 Desc'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['photo_5_desc']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Video Link'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['video_link']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($propertyAd['PropertyAd']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Property Ad'), array('action' => 'edit', $propertyAd['PropertyAd']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Property Ad'), array('action' => 'delete', $propertyAd['PropertyAd']['id']), array(), __('Are you sure you want to delete # %s?', $propertyAd['PropertyAd']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Ads'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Ad'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Areas'), array('controller' => 'areas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Area'), array('controller' => 'areas', 'action' => 'add')); ?> </li>
	</ul>
</div>
