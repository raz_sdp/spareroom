<div class="block_tabs">
    <a class="block_tab tab_mng_edit" id="edit_btn" href="#">Edit</a>
    <a class="block_tab tab_mng_photos" id="photo_btn" href="#">Photos</a>
    <a class="block_tab tab_mng_video" id="video_btn" href="#">Video</a>
    <a class="block_tab tab_mng_delete" id="delete_btn" href="#">Delete</a>
</div>

<script>
    $('#edit_btn').click(function(e){
        e.preventDefault();
        $('#main_panel').show();
        $('#photo_panel').hide();
        $('#video_panel').hide();
        $('#delete_panel').hide();
    });
    $('#photo_btn').click(function(e){
        e.preventDefault();
        $('#main_panel').hide();
        $('#photo_panel').show();
        $('#video_panel').hide();
        $('#delete_panel').hide();
    });
    $('#video_btn').click(function(e){
        e.preventDefault();
        $('#main_panel').hide();
        $('#photo_panel').hide();
        $('#video_panel').show();
        $('#delete_panel').hide();
    });
    $('#delete_btn').click(function(e){
        e.preventDefault();
        $('#main_panel').hide();
        $('#photo_panel').hide();
        $('#video_panel').hide();
        $('#delete_panel').show();
    });
</script>