<div id="ad_details_panel" class="block block_simple detail_page_tab_content">
<div class=" detail_page_tab_content_inner">
<div id="listing_header">
    <ul id="listing_tools">
        <!--<li class="email"><a href="/flatshare/tellafriend.pl?advert_id=3527022&amp;search_id=265832575&amp;flatshare_type=offered&amp;search_results=%2Fflatshare%2F%3Fsearch_id%3D265832575%26&amp;featured=&amp;nav_context=search" title="Tell a friend by emailing details of this ad to them" rel="nofollow">Send to a friend</a></li>
        <li class="print"><a href="/flatshare/flatshare_detail.pl?flatshare_id=3527022&amp;mode=print&amp;flatshare_type=offered&amp;search_id=265832575&amp;search_results=%2Fflatshare%2F%3Fsearch_id%3D265832575%26&amp;city_id=&amp;featured=&amp;alert_id=&amp;alert_type=&amp;" title="Print friendly version of this page" rel="nofollow" target="_blank"><span>Print version</span></a></li>
        <li class="unsuitable">
            <a href="/flatshare/shortlist.pl?function=unsuitable&amp;flatshare_id=3527022&amp;flatshare_type=offered&amp;search_id=265832575&amp;search_results=%2Fflatshare%2F%3Fsearch_id%3D265832575%26&amp;city_id=&amp;featured=" title="To save time later, mark this ad as unsuitable" rel="nofollow">Mark as unsuitable</a>
        </li>-->
        <li class="save">
            <?php
            if($is_saved)
                echo "This ad is saved for you.";
            else { ?>
                <a href="<?php echo $this->webroot."users/save_ad/".$flag."/".$result[$flag]['id'];?>" rel="nofollow">Save</a>
            <?php } ?>
        </li>
    </ul>
    <p id="listing_ref">
							<span class="new_listing">
							    Ad ref# <?php echo $flag."-".$result[$flag]['id'];?>
                                <span class="listing_extra_info listing_mon_fri"><?php echo $result[$flag]['days_available'];?></span>
							</span>
    </p>
</div>

<div class="grid-8-4-4">
<div class="advert-intro">
    <div class="photos landscape floatbox" data-fb-options="group:1 doSlideshow:true">
        <dl class="landscape">
            <dt class="mainImage">
                <?php
                if(!empty($result[$flag]['photo_1'])){
                    $img_url = $this->Html->url('/files/add_images/'.$result[$flag]['photo_1'], true);
                }else{
                    $img_url = $this->Html->url('/files/add_images/default_search_img.jpg', true);
                }
                ?>
                <a href="<?php echo $img_url;?>" class="floatbox">
                <img src="<?php echo $img_url;?>" width="400px" style="margin-bottom:10px;">
                </a>
                <br/>

                <?php if(!empty($result[$flag]['photo_2'])) {
                    $img2_url = $this->Html->url('/files/add_images/'.$result[$flag]['photo_2'], true);
                ?>
                    <a href="<?php echo $img2_url;?>" class="floatbox">
                    <img src="<?php echo $img2_url;?>" width="80px" height="80px" style="margin-right:5px;">
                    </a>
                <?php } ?>
                <?php if(!empty($result[$flag]['photo_3'])) {
                    $img3_url = $this->Html->url('/files/add_images/'.$result[$flag]['photo_3'], true);
                    ?>
                    <a href="<?php echo $img3_url;?>" class="floatbox">
                    <img src="<?php echo $img3_url;?>" width="80px" height="80px" style="margin-right:5px;">
                    </a>
                <?php } ?>
                <?php if(!empty($result[$flag]['photo_4'])) {
                    $img4_url = $this->Html->url('/files/add_images/'.$result[$flag]['photo_4'], true);
                    ?>
                    <a href="<?php echo $img4_url;?>" class="floatbox">
                    <img src="<?php echo $img4_url;?>" width="80px" height="80px" style="margin-right:5px;">
                    </a>
                <?php } ?>
                <?php if(!empty($result[$flag]['photo_5'])) {
                    $img5_url = $this->Html->url('/files/add_images/'.$result[$flag]['photo_5'], true);
                    ?>
                    <a href="<?php echo $img5_url;?>" class="floatbox">
                    <img src="<?php echo $img5_url;?>" width="80px" height="80px" style="margin-right:5px;">
                    </a>
                <?php } ?>

                <!--<span class="zoom">
                    <span class="hidden">Click to </span> zoom
                </span>-->
            </dt>
            <dd class="caption"></dd>
        </dl>
    </div>
    <p class="detaildesc"><?php echo utf8_encode($result[$flag]['description']);?></p>
</div>
<div class="ad_details">
<ul class="key_features">
    <?php if($flag=="RentAd") { ?>
        <li><?php echo $result[$flag]['street_name'];?></li>
        <li><?php echo $result[$flag]['room_size'];?></li>
        <li><?php echo $result[$flag]['postcode'];?> <small></small></li>
    <?php } else { ?>
        <li><?php echo $result[$flag]['genders'];?></li>
        <li><?php echo $result[$flag]['looking_for'];?></li>
        <li><?php echo $result[$flag]['area'];?> <small></small></li>
    <?php } ?>
</ul>
<hr class="hrz_featurelist">
<ul class="key_features">
    <li><?php echo $result[$flag]['rent'];?><abbr title="<?php echo $result[$flag]['per_week'];?>"><?php echo $result[$flag]['per_week'];?></abbr><small>(<?php if($flag=="RentAd") echo $result[$flag]['room_size']; else echo $result[$flag]['looking_for'];?>)</small></li>
</ul>
<a href="#" id="show_map" class="ir view_on_map_v4 map_link"></a>
<table class="featurestable">
    <caption>Availability</caption>
    <tbody>
    <tr>
        <th>Available</th>
        <td><?php
            if(date("Y-m-d")>=$result[$flag]['available_from'])
                echo "<em>Now</em>";
            else
                echo "<em>". $result[$flag]['available_from'] ."</em>";
            ?>
        </td>
    </tr>
    <tr>
        <th>Minimum term</th>
        <td><?php
            if($result[$flag]['min_stay'] == 0)
                echo "none";
            else
                echo $result[$flag]['min_stay']." months";
            ?>
        </td>
    </tr>
    <tr>
        <th>Maximum term</th>
        <td><?php
            if($result[$flag]['max_stay'] == 0)
                echo "none";
            else
                echo $result[$flag]['max_stay']." months";
            ?>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="colspan2">
            <ul class="bulletlist">
                <li class="listing_mon_fri">Room available <?php echo $result[$flag]['days_available'];?></li>
                <?php if(!empty($result[$flag]['short_term_considered'])) { ?>
                <li><?php if($flag=="RentAd") echo $result[$flag]['short_term_considered'];?></li>
                <?php } ?>
            </ul>
        </td>
    </tr>
    </tbody>
</table>
<?php if($flag=="RentAd") { ?>
    <hr class="hrz_featurelist">
    <table class="featurestable">
        <caption>Extra costs</caption>
        <tbody>
        <tr>
            <th>Deposit</th>
            <td><?php echo $result[$flag]['security_deposit'];?></td>
        </tr>
        <tr>
            <th>Bills included?</th>
            <td><?php echo $result[$flag]['bills_inc'];?></td>
        </tr>
        </tbody>
    </table>
<?php } ?>
<hr class="hrz_featurelist">
<?php $aminities = explode(',',$result[$flag]['aminities']);?>
<table class="featurestable">
    <caption>Amenities</caption>
    <tbody>
    <tr>
        <th>Furnishings</th>
        <td><?php if(in_array('furnished',$aminities)){ echo "Yes"; } else { echo "No"; }?></td>
    </tr>
    <tr>
        <th>Parking</th>
        <td> <?php if(in_array('parking',$aminities)){ echo "Yes"; } else { echo "No"; }?></td>
    </tr>
    <tr>
        <th>Garden/terrace</th>
        <td> <?php if(in_array('garden/roof terrace',$aminities)){ echo "Yes"; } else { echo "No"; }?></td>
    </tr>
    <tr>
        <th>Disabled access</th>
        <td><?php if(in_array('disabled access',$aminities)){ echo "Yes"; } else { echo "No"; }?></td>
    </tr>
    <tr>
        <th>Shared Living room</th>
        <?php if($flag=="RentAd") { ?>
            <td><span class="tick"><?php echo $result[$flag]['living_room'];?></span></td>
        <?php } else { ?>
            <td> <?php if(in_array('shared living room',$aminities)){ echo "Yes"; } else { echo "No"; }?> </td>
        <?php } ?>
    </tr>
    <tr>
        <th>Broadband included</th>
        <?php if($flag=="RentAd") { ?>
            <td><span class="tick"><?php echo $result[$flag]['broadband'];?></span></td>
        <?php } else { ?>
            <td> <?php if(in_array('broadband',$aminities)){ echo "Yes"; } else { echo "No"; }?> </td>
        <?php } ?>
    </tr>
    </tbody>
</table>
<hr class="hrz_featurelist">
<?php if($flag=="RentAd") { ?>
    <table class="featurestable">
        <caption>Current Household</caption>
        <tbody>
        <tr>
            <th>Age</th>
            <td><?php echo $result[$flag]['exist_mate_age'];?></td>
        </tr>
        <tr>
            <th>Smoker?</th>
            <td><?php echo $result[$flag]['exist_mate_smoking'];?></td>
        </tr>
        <tr>
            <th>Any pets?</th>
            <td><?php echo $result[$flag]['exist_mate_pets'];?></td>
        </tr>
        <tr>
            <th>Language</th>
            <td><?php echo $result[$flag]['exist_mate_language'];?></td>
        </tr>
        <tr>
            <th>Nationality</th>
            <td><?php echo $result[$flag]['exist_mate_nationality'];?></td>
        </tr>
        <tr>
            <th>Occupation</th>
            <td><?php echo $result[$flag]['exist_mate_occupation'];?></td>
        </tr>
        <tr>
            <th>Interests</th>
            <td><?php echo $result[$flag]['exist_mate_interests'];?></td>
        </tr>
        <tr>
            <th>Gender</th>
            <td><?php echo $result[$flag]['exist_mate_gender'];?></td>
        </tr>
        </tbody>
    </table>
<?php } ?>
<hr class="hrz_featurelist">
<table class="featurestable">
    <caption>New Housemate preferences</caption>
    <tbody>
    <?php if($flag=="RentAd") { ?>
        <tr>
            <th>Couples OK?</th>
            <td><?php echo $result[$flag]['new_mate_couples_welcomed'];?></td>
        </tr>
    <?php } ?>
    <tr>
        <th>Smoking OK?</th>
        <?php if($flag=="RentAd") { ?>
            <td><?php echo $result[$flag]['new_mate_smoking'];?></td>
        <?php } else { ?>
            <td><?php echo $result[$flag]['prefered_smoking'];?></td>
        <?php } ?>
    </tr>
    <tr>
        <th>Pets OK?</th>
        <?php if($flag=="RentAd") { ?>
            <td><?php echo $result[$flag]['new_mate_pets'];?></td>
        <?php } else { ?>
            <td><?php echo $result[$flag]['prefered_pets'];?></td>
        <?php } ?>
    </tr>
    <tr>
        <th>Occupation</th>
        <?php if($flag=="RentAd") { ?>
            <td><?php echo $result[$flag]['new_mate_occupation'];?></td>
        <?php } else { ?>
            <td><?php echo $result[$flag]['prefered_occupation'];?></td>
        <?php } ?>
    </tr>
    <?php if($flag=="RentAd") { ?>
        <tr>
            <th><!-- Need -->References?</th>
            <td><?php echo $result[$flag]['references_needed'];?></td>
        </tr>
    <?php } ?>
    <tr>
        <th>Min age</th>
        <?php if($flag=="RentAd") { ?>
            <td><?php echo $result[$flag]['new_mate_min_age'];?></td>
        <?php } else { ?>
            <td><?php echo $result[$flag]['prefered_age_min'];?></td>
        <?php } ?>
    </tr>
    <tr>
        <th>Max age</th>
        <?php if($flag=="RentAd") { ?>
            <td><?php echo $result[$flag]['new_mate_max_age'];?></td>
        <?php } else { ?>
            <td><?php echo $result[$flag]['prefered_age_max'];?></td>
        <?php } ?>
    </tr>
    <tr>
        <th>Gender</th>
        <?php if($flag=="RentAd") { ?>
            <td><?php echo $result[$flag]['new_mate_gender'];?></td>
        <?php } else { ?>
            <td><?php echo $result[$flag]['prefered_gender'];?></td>
        <?php } ?>
    </tr>
    </tbody>
</table>
</div>
<div class="asidecontent">
    <div class="block block_bubble block_contact">
        <div class="block_header">
            <h3>Upgrade to <span>contact</span></h3>
        </div>
        <div class="block_content block_areas">
            <div class="block_area block_area_first">
                <dl>
                    <dt><?php echo $result[$flag]['display_name'];?></dt>
                    <dd><em><?php echo $result[$flag]['advertiser_role']?></em></dd>
                </dl>
                <p>
                    This is a newly-listed Free Ad (less than 7 days old).
                    To contact the advertiser you'll need to upgrade to get Early Bird Access
                        <span class="tooltip">
                            <span class="tooltip_item help">
                               (<a href="/content/infowebsitehelp/how-the-site-works">?</a>)
                            </span>
                            <span class="tooltip_box right">
                                <small class="tooltip_text">
                                    Early Bird Access is just one benefit of upgrading.
                                    With it you can contact all ads on the site as soon as they appear -
                                    otherwise you'll have to wait 7 days before you can contact any ads with the Early Bird icon.
                                    <a href="/content/infowebsitehelp/how-the-site-works">Find out more »</a>
                                </small>
                                <span class="tooltip_arrow">&nbsp;</span>
                            </span>
                        </span>
                    <br>
                    <a href="/flatshare/get_early_bird.pl?M_context=90">Upgrade</a>
                    from 86p per day.
                </p>
                <ul class="contact_methods">
                    <li class="emailadvertiser"><a class="btn btn-primary btn-lg btn-block" id="mail_btn2" href="#" title="Email the advertiser" ><span>Email advertiser</span></a></li>
                    <li class="phoneadvertiser"><a class="btn btn-primary btn-lg btn-block" id="phone_btn2" href="#" title="Phone the advertiser"><span>Phone advertiser</span></a></li>
                </ul>
                <script>
                    $("#mail_btn2").click(function(e){
                        e.preventDefault();
                        $("#ad_details_panel").hide();
                        $("#ad_details_mail_panel").show();
                        $("#ad_details_phone_panel").hide();
                        $("#detail_btn").removeClass('block_tab_active');
                        $("#mail_btn").addClass('block_tab_active');
                        $("#phone_btn").removeClass('block_tab_active');
                    });
                    $("#phone_btn2").click(function(e){
                        e.preventDefault();
                        $("#ad_details_panel").hide();
                        $("#ad_details_mail_panel").hide();
                        $("#ad_details_phone_panel").show();
                        $("#detail_btn").removeClass('block_tab_active');
                        $("#mail_btn").removeClass('block_tab_active');
                        $("#phone_btn").addClass('block_tab_active');
                    });
                </script>
            </div> <!-- end block_area -->
            <div class="block_area">
                <p>In a hurry? <a href="/flatshare/shortlist.pl?function=add&amp;context=interest&amp;flatshare_id=3527022&amp;flatshare_type=offered&amp;search_id=265832575&amp;search_results=%2Fflatshare%2F%3Fsearch_id%3D265832575%26&amp;city_id=&amp;featured=" class="bluebuttontextlink" title="Show interest - you'll appear in the advertiser's list of Who's Interested in their ad" rel="nofollow">Show interest for Free</a> and we will send the advertiser your profile</p>
            </div>
            <div class="block_area block_area_last">
                <p>Read our <a href="/content/info-tenants/safety-tips-for-room-seekers/">safety tips</a>.</p>
            </div>
        </div><!-- end block_areas -->
    </div><!-- end block_contact-->
    <!--doesnt work for wanted ads -->
</div><!-- end col_last -->
</div><!-- end cols4 -->
</div><!-- end /.block_content from main tabs -->
</div><!-- end /.detail_page_tab_content -->

<div class="map-overlay" style="display:none">
    <div class="map-close-btn" id="map-close-btn"><h2>(X) Close</h2></div>
    <div id="map-canvas"></div>
</div>
<script>

    google.maps.event.addDomListener(window, 'load', initialize);

    $(document).ready(function(){
        $('#map-close-btn').click(function(e){
            e.preventDefault();
            $('.map-overlay').hide();
        });
        $('#show_map').click(function(e){
            e.preventDefault();
            $('.map-overlay').show();
        });
    });

    function initialize() {
        var mapCanvas = document.getElementById('map-canvas');

        var lat = "<?php echo $result[$flag]['geo_loc']['lat'];?>";
        var lng = "<?php echo $result[$flag]['geo_loc']['lng'];?>";
        lat = Number(lat);
        lng = Number(lng);

        var mapOptions = {
            center: new google.maps.LatLng(lat, lng),
            zoom: 8,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(mapCanvas, mapOptions);
    }
</script>