<?php
if($flag == "RentAd") $page_name = "rent_ads";
else if($flag="WantedAd") $page_name = "wanted_ads";
$fb_link = "https://www.facebook.com/sharer/sharer.php?u=" . $_SERVER['HTTP_HOST'] . $this->webroot. "searchEngine/full_ad/" . $page_name . "/" . $result[$flag]['id'] . ";title=" . $result[$flag]['title'];
$tweet_link = "https://twitter.com/intent/tweet?status=". $result[$flag]['title'] . "+" .  $_SERVER['HTTP_HOST'] . $this->webroot . "searchEngine/full_ad/" . $page_name . "/" . $result[$flag]['id'];
?>

<div class="block_tabs detail_page_tabs">
    <a id="detail_btn" class="block_tab block_tab_active" href="#">Add details</a>
    <a id="mail_btn" class="block_tab" href="#" title="Email the advertiser" rel="nofollow"><span>Email the advertiser</span></a>
    <a id="phone_btn" class="block_tab phone_tab" href="#" title="Phone the advertiser" rel="nofollow"><span>Phone the advertiser</span></a>
    <div id="social_buttons_share">
        <a id="facebookButtonShare" class="facebookButtonShare" name="link" href="<?php echo $fb_link;?>" target="_blank">Share</a>
        <a id="twitterButtonShare" class="twitterButtonShare" name="link" href="<?php echo $tweet_link;?>" target="_blank">Tweet</a>
    </div>

    <script>
        $("#detail_btn").click(function(e){
            e.preventDefault();
            $("#ad_details_panel").show();
            $("#ad_details_mail_panel").hide();
            $("#ad_details_phone_panel").hide();
            $("#detail_btn").addClass('block_tab_active');
            $("#mail_btn").removeClass('block_tab_active');
            $("#phone_btn").removeClass('block_tab_active');
        });
        $("#mail_btn").click(function(e){
            e.preventDefault();
            $("#ad_details_panel").hide();
            $("#ad_details_mail_panel").show();
            $("#ad_details_phone_panel").hide();
            $("#detail_btn").removeClass('block_tab_active');
            $("#mail_btn").addClass('block_tab_active');
            $("#phone_btn").removeClass('block_tab_active');
        });
        $("#phone_btn").click(function(e){
            e.preventDefault();
            $("#ad_details_panel").hide();
            $("#ad_details_mail_panel").hide();
            $("#ad_details_phone_panel").show();
            $("#detail_btn").removeClass('block_tab_active');
            $("#mail_btn").removeClass('block_tab_active');
            $("#phone_btn").addClass('block_tab_active');
        });
    </script>
</div>