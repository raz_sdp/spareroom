<div style="display:none; border: 1px solid lightblue;" id="ad_details_phone_panel" class="block_content detail_page_tab_content_inner">
    <div class="grid-12-4">
        <p style="font-size: 20px; margin-left: 10px; max-width: 100%">
        <?php if(empty($user)) { ?>
            You must <a href="<?php echo $this->webroot;?>login">Log In</a> or <a href="<?php echo $this->webroot;?>register">Register</a> in order to achieve the contact no. of the adviser.
        <?php } else { ?>
            The telephone of this advertiser is: <br> <a href="tel:<?php echo $result[$flag]['telephone'] ?>"><?php echo $result[$flag]['telephone'] ?></a>
        <?php } ?>
        </p>
    </div>
</div>