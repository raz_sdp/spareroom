<div class="block_content">
<?php $table1 = ucfirst($table)."Ad";?>
<form name="basicform" id="basicform" method="post" action="<?php echo $this->webroot . $table;?>_ads/edit/<?php echo $ad[$table1]['id']?>">

<div id="sf1" class="frm">
    <fieldset>
        <legend>Step 1 of 6</legend>
        <h3>Get started with your free advert</h3>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">I have </label>
            <div class="col-lg-6">
                <select class="form-control" id="step1_have" name="data[RentAd][rooms_for_rent]">
                    <?php
                    for($i=1; $i<=12; $i++)
                        echo "<option value=\"$i room for rent\">$i room for rent</option>";
                    ?>
                </select>

            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Size & type of property </label>
            <div class="col-lg-3">
                <select class="form-control" id="step1_size" name="data[RentAd][rooms_in_property]">
                    <?php
                    for($i=2; $i<=12; $i++)
                        echo "<option value=\"$i bed\">$i bed</option>";
                    ?>
                </select>
            </div>

            <div class="col-lg-3">
                <select class="form-control" id="step1_size1" name="data[RentAd][property_type]">
                    <option value="Flat/Apartment">Flat/Apartment</option>
                    <option value="House">House</option>
                    <option value="Property">Property</option>
                </select>
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>


        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">There are already </label>
            <div class="col-lg-6">
                <select class="form-control" id="step1_already" name="data[RentAd][current_occupants]">
                    <?php
                    for($i=0; $i<=10; $i++)
                        echo "<option value=\"$i\">$i occupants</option>";
                    ?>
                </select>
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Postcode of property</label>
            <div class="col-lg-6">
                <input type="text" class="form-control" placeholder="(e.g. SE15 8PD)" name="data[RentAd][postcode]" id="step1_postcode">
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">I am a </label>
            <div class="col-lg-6">
                <input type="radio" name="data[RentAd][advertiser_role]" value="Live in landlord"> <b>Live in landlord</b> (I own the property and live there)<br>
                <input type="radio" name="data[RentAd][advertiser_role]" value="Live out landlord"> <b>Live out landlord</b> (I own the property but don't live there)<br>
                <input type="radio" name="data[RentAd][advertiser_role]" value="Current tenant/flatmate"> <b>Current tenant/flatmate</b> (I am living in the property) <br>
                <input type="radio" name="data[RentAd][advertiser_role]" value="Agent"> <b>Agent</b> (I am advertising on a landlord's behalf) <br>
                <input type="radio" name="data[RentAd][advertiser_role]" value="Former flatmate"> <b>Former flatmate</b> (I am moving out and need someone to replace me)
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>


        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">My email is </label>
            <div class="col-lg-6">
                <input type="email" class="form-control" placeholder="(We will keep this safe and not display publicly)" name="data[RentAd][email]" id="step1_email">
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <button class="btn btn-primary open1" type="button" name="step1_complete">Next <span class="fa fa-arrow-right"></span></button>
                <br><br>
            </div>
        </div>

    </fieldset>
</div>



<div id="sf2" class="frm" style="display: none;">
    <fieldset>
        <legend>Step 2 of 6</legend>
        <h3>Get started with your free advert</h3>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Area </label>
            <div class="col-lg-6">
                <select class="form-control" id="step2_area" name="data[RentAd][area_id]">
                    <option value="" SELECTED>Select area...
                    <OPTION VALUE="22345">London SE15
                    <OPTION VALUE="22341">Nunhead
                    <OPTION VALUE="22343">Peckham
                    <OPTION VALUE="22342">Peckham Rye
                    <OPTION VALUE="22344">Rye Lane
                </select>
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Street name </label>
            <div class="col-lg-6">
                <input type="text" class="form-control" name="data[RentAd][street_name]">
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Post Code </label>
            <div class="col-lg-6">
                <label id="postcode_label"></label>
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Transport </label>
            <div class="col-lg-3">
                <select class="form-control" id="step2_transport" name="data[RentAd][no_of_mins]">
                    <option value="5">0-5</option>
                    <option value="10">5-10</option>
                    <option value="15">10-15</option>
                    <option value="20">15-20</option>
                    <option value="25">20-25</option>
                    <option value="30">25+</option>
                </select>minutes
            </div>

            <div class="col-lg-3">
                <select class="form-control" id="step2_transport1" name="data[RentAd][no_of_mins_by]">
                    <option value="walk">walk</option>
                    <option value="by car">by car</option>
                    <option value="by bus">by bus</option>
                    <option value="by tram">by tram</option>
                </select>
                from
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname"> </label>
            <div class="col-lg-6">
                <select class="form-control" id="step2_transport2" name="data[RentAd][station_name]">
                    <option value="EASTCROYDON">East Croydon</option>
                    <option value="NUNHEAD">Nunhead</option>
                    <option value="PECKHAMRYE">Peckham Rye</option>
                    <option value="QUEENSROADPECKHAM">Queens Road Peckham</option>
                    <option value="BERMONDSEY">Bermondsey</option>
                    <option value="BOROUGH">Borough</option>
                    <option value="BROCKLEY">Brockley</option>
                    <option value="CANADAWATER">Canada Water</option>
                    <option value="CROFTONPARK">Crofton Park</option>
                    <option value="DENMARKHILL">Denmark Hill</option>
                    <option value="DEPTFORD">Deptford</option>
                    <option value="EASTDULWICH">East Dulwich</option>
                    <option value="ELEPHANTCASTLE">Elephant & Castlev</option>
                    <option value="FORESTHILL">Forest Hill</option>
                    <option value="HONOROAKPARK">Honor Oak Parkv</option>
                    <option value="LADYWELL">Ladywell</option>
                    <option value="LAMBETHNORTH">Lambeth North</option>
                    <option value="LONDONBRIDGE">London Bridge</option>
                    <option value="NEWCROSS">New Cross</option>
                    <option value="NEWCROSSGATE">New Cross Gate</option>
                    <option value="ROTHERHITHE">Rotherhithe</option>
                    <option value="SOUTHBERMONDSEY">South Bermondsey</option>
                    <option value="SOUTHWARK">Southwark</option>
                    <option value="STJOHNS">St. Johns</option>
                    <option value="SURREYQUAYS">Surrey Quays</option>
                    <option value="WATERLOO">Waterloo</option>
                    <option value="WATERLOOEAST">Waterloo East</option>
                </select>
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>


        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Living Room? </label>
            <div class="col-lg-6">
                <input type="radio" name="data[RentAd][living_room]" checked value="Yes" id="step2_living_room"> Yes, there is a shared living room &nbsp;
                <input type="radio" name="data[RentAd][living_room]" value="No" id="step2_living_room"> No
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Amenities </label>
            <div class="col-lg-3">
                <input type="checkbox" name="data[RentAd][aminities][]" value="parking" id="step2_amenities"> Parking
            </div>
            <div class="col-lg-3">
                <input type="checkbox" name="data[RentAd][aminities][]" value="balcony/patio" id="step2_amenities"> Balcony/patio
            </div>
            <div class="col-lg-3">
                <input type="checkbox" name="data[RentAd][aminities][]" value="garden/roof terrace" id="step2_amenities"> Garden/roof terrace
            </div>
            <div class="col-lg-3">
                <input type="checkbox" name="data[RentAd][aminities][]" value="disabled access" id="step2_amenities"> Disabled access
            </div>
            <div class="col-lg-3">
                <input type="checkbox" name="data[RentAd][aminities][]" value="garage" id="step2_amenities"> Garage
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <button class="btn btn-warning back1" type="button"><span class="fa fa-arrow-left"></span> Back</button>
                <button class="btn btn-primary open2" type="button">Next <span class="fa fa-arrow-right"></span></button>
                <br><br>
            </div>
        </div>
    </fieldset>
</div>

<div id="sf3" class="frm" style="display: none;">
    <fieldset>
        <legend>Step 3 of 6</legend>
        <h3>The room</h3>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Cost of room </label>
            <div class="col-lg-4">
                <input type="text" placeholder="Your Cost for Room" id="step3_cost_of_room" name="data[RentAd][rent]" class="form-control">
                <input type="radio" id="step3_cost" name="data[RentAd][per_week]" value="per week"> per week
                <input type="radio" id="step3_cost" name="data[RentAd][per_week]" value="per month"> per calendar month
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Size of room </label>
            <div class="col-lg-6">
                <input type="radio" id="step3_size_of_room" name="data[RentAd][room_size]" value="Single"> Single
                <input type="radio" id="step3_size_of_room" name="data[RentAd][room_size]" value="Double"> Double
            </div>

        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Amenities </label>
            <div class="col-lg-6">
                <input type="checkbox" id="step3_amenities" name="data[RentAd][room_aminities][]" value="En suite"> En suite<br>
                <input type="checkbox" id="step3_amenities1" name="data[RentAd][room_aminities][]" value="Furnished"> Furnished
            </div>

        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Security deposit </label>
            <div class="col-lg-6">
                <label>£</label>
                <input type="number" id="step3_security" name="data[RentAd][security_deposit]" class="form-control" style="width: 63%;">
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Available from </label>
            <div class="col-lg-2">
                <input type="date" class="form-control" name="data[RentAd][available_from]">
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>


        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Minimum stay </label>
            <div class="col-lg-6">
                <select class="form-control" name="data[RentAd][min_stay]" id="step3_minimum_stay">
                    <option value="0">No minimum</option>
                    <option value="1">1 Month</option>
                    <option value="2">2 Months</option>
                    <option value="3">3 Months</option>
                    <option value="4">4 Months</option>
                    <option value="5">5 Months</option>
                    <option value="6">6 Months</option>
                    <option value="7">7 Months</option>
                    <option value="8">8 Months</option>
                    <option value="9">9 Months</option>
                    <option value="10">10 Months</option>
                    <option value="11">11 Months</option>
                    <option value="12">1 Year</option>
                    <option value="15">1 Year 3 Months</option>
                    <option value="18">1 Year 6 Months</option>
                    <option value="21">1 Year 9 Months</option>
                    <option value="24">2 Years</option>
                    <option value="36">3 Years</option>
                </select>
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Maximum stay </label>
            <div class="col-lg-6">
                <select class="form-control" name="data[RentAd][max_stay]" id="step3_maximum_stay">
                    <option value="0">No maximum</option>
                    <option value="1">1 Month</option>
                    <option value="2">2 Months</option>
                    <option value="3">3 Months</option>
                    <option value="4">4 Months</option>
                    <option value="5">5 Months</option>
                    <option value="6">6 Months</option>
                    <option value="7">7 Months</option>
                    <option value="8">8 Months</option>
                    <option value="9">9 Months</option>
                    <option value="10">10 Months</option>
                    <option value="11">11 Months</option>
                    <option value="12">1 Year</option>
                    <option value="15">1 Year 3 Months</option>
                    <option value="18">1 Year 6 Months</option>
                    <option value="21">1 Year 9 Months</option>
                    <option value="24">2 Years</option>
                    <option value="36">3 Years</option>
                </select>
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Short Term Lets Considered? </label>
            <div class="col-lg-6">
                <input type="checkbox" name="data[RentAd][short_term_considered]" value="1" id="step3_tick_for_yes"> Tick for Yes
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Days available </label>
            <div class="col-lg-6">
                <select class="form-control" name="data[RentAd][days_available]" id="step3_days_available">
                    <option value="7 days a week" selected>7 days a week</option>
                    <option value="Mon to Fri only">Mon to Fri only</option>
                    <option value="Weekends only">Weekends only</option>
                </select>
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">References required? </label>
            <div class="col-lg-6">
                <input type="radio" name="data[RentAd][references_needed]" value="Yes" id="step3_references_required"> Yes
                <input type="radio" name="data[RentAd][references_needed]" value="No" id="step3_references_required"> No
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>


        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Bills included </label>
            <div class="col-lg-6">
                <input type="radio" name="data[RentAd][bills_inc]" value="Yes" id="step3_bills_included"> Yes
                <input type="radio" name="data[RentAd][bills_inc]" value="No" id="step3_bills_included"> No
                <input type="radio" name="data[RentAd][bills_inc]" value="Some" id="step3_bills_included"> Some
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Broadband available? </label>
            <div class="col-lg-6">
                <input type="radio" name="data[RentAd][broadband]" value="Yes" id="step3_broadband_available"> Yes
                <input type="radio" name="data[RentAd][broadband]" value="No" id="step3_broadband_available"> No
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <button class="btn btn-warning back2" type="button"><span class="fa fa-arrow-left"></span> Back</button>
                <button class="btn btn-primary open3" type="button">Next <span class="fa fa-arrow-right"></span></button>
                <br><br>
            </div>
        </div>
    </fieldset>
</div>

<div id="sf4" class="frm" style="display: none;">
<fieldset>
<legend>Step 4 of 6</legend>
<h3>The Existing Flatmate</h3>

<div class="form-group">
    <label class="col-lg-2 control-label" for="uname">Smoking </label>
    <div class="col-lg-6">
        <select class="form-control" name="data[RentAd][exist_mate_smoking]" id="step4_smoking">
            <option value="No">No</option>
            <option value="Yes">Yes</option>
        </select>
    </div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="uname">Gender </label>
    <div class="col-lg-6">
        <select class="form-control" name="data[RentAd][exist_mate_gender]" id="step4_gender">
            <option value="Male">Male</option>
            <option value="Female">Female</option>
        </select>
    </div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="uname">Occupation </label>
    <div class="col-lg-6">
        <select class="form-control" name="data[RentAd][exist_mate_occupation]" id="step4_occupation">
            <option value="Not disclosed" selected>Not disclosed</option>
            <option value="Student">Student</option>
            <option value="Professional">Professional</option>
            <option value="Other">Other</option>
            <option value="Mixed">Mixed</option>
        </select>
    </div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="uname">Pets </label>
    <div class="col-lg-6">
        <select class="form-control" name="data[RentAd][exist_mate_pets]" id="step4_pets">
            <option value="No">No</option>
            <option value="Yes">Yes</option>
        </select>
    </div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="uname">Age </label>
    <div class="col-lg-6">
        <select class="form-control" name="data[RentAd][exist_mate_age]" id="step4_age">
            <option value="null" selected="">-</option>
            <?php
            for($i=16; $i<=99; $i++)
                echo "<option value=\"$i\">$i</option>";
            ?>
        </select>
    </div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="uname">Language </label>
    <div class="col-lg-6">
        <select class="form-control" name="data[RentAd][exist_mate_language]" id="step4_language">
            <option value="26">English</option>
            <option value="27">Mixed</option>
            <option value="4">Arabic</option>
            <option value="11">Bengali</option>
            <option value="17">Cantonese</option>
            <option value="37">French</option>
            <option value="39">German</option>
            <option value="44">Hindi</option>
            <option value="48">Indonesian</option>
            <option value="50">Japanese</option>
            <option value="62">Malay</option>
            <option value="63">Mandarin</option>
            <option value="73">Portuguese</option>
            <option value="74">Punjabi</option>
            <option value="79">Russian</option>
            <option value="88">Spanish</option>
            <option value="1">Afrikaans</option>
            <option value="2">Albanian</option>
            <option value="3">Amharic</option>
            <option value="5">Armenian</option>
            <option value="6">Aymara</option>
            <option value="7">Baluchi</option>
            <option value="8">Bambara</option>
            <option value="9">Basque</option>
            <option value="10">Belarussian</option>
            <option value="12">Berber</option>
            <option value="13">Bislama</option>
            <option value="14">Bosnian</option>
            <option value="15">Bulgarian</option>
            <option value="16">Burmese</option>
            <option value="18">Catalan</option>
            <option value="19">Ciluba</option>
            <option value="20">Creole</option>
            <option value="21">Croatian</option>
            <option value="22">Czech</option>
            <option value="23">Danish</option>
            <option value="24">Dari</option>
            <option value="25">Dutch</option>
            <option value="28">Eskimo</option>
            <option value="29">Estonian</option>
            <option value="30">Ewe</option>
            <option value="31">Fang</option>
            <option value="32">Faroese</option>
            <option value="33">Farsi (Persian)</option>
            <option value="34">Filipino</option>
            <option value="35">Finnish</option>
            <option value="36">Flemish</option>
            <option value="38">Galician</option>
            <option value="40">Greek</option>
            <option value="41">Gujarati</option>
            <option value="42">Hausa</option>
            <option value="43">Hebrew</option>
            <option value="45">Hungarian</option>
            <option value="46">Ibo</option>
            <option value="47">Icelandic</option>
            <option value="49">Italian</option>
            <option value="51">Kabi�</option>
            <option value="52">Kashmiri</option>
            <option value="53">Kirundi</option>
            <option value="54">Kiswahili</option>
            <option value="55">Korean</option>
            <option value="56">Latvian</option>
            <option value="57">Lingala</option>
            <option value="58">Lithuanian</option>
            <option value="59">Luxembourgish</option>
            <option value="60">Macedonian</option>
            <option value="61">Malagasy</option>
            <option value="64">Mayan</option>
            <option value="65">Motu</option>
            <option value="66">Nepali</option>
            <option value="67">Norwegian</option>
            <option value="68">Noub</option>
            <option value="69">Pashto</option>
            <option value="70">Peul</option>
            <option value="71">Pidgin</option>
            <option value="72">Polish</option>
            <option value="75">Pushtu</option>
            <option value="76">Quechua</option>
            <option value="77">Romanian</option>
            <option value="78">Romansch</option>
            <option value="80">Sango</option>
            <option value="81">Serbian</option>
            <option value="82">Setswana</option>
            <option value="83">Sindhi</option>
            <option value="84">Sinhala</option>
            <option value="85">Slovak</option>
            <option value="86">Slovene</option>
            <option value="87">Somali</option>
            <option value="89">Swahili</option>
            <option value="90">Swedish</option>
            <option value="91">Tamil</option>
            <option value="92">Thai</option>
            <option value="93">Turkish</option>
            <option value="94">Urdu</option>
            <option value="95">Vietnamese</option>
            <option value="99">Welsh</option>
            <option value="96">Xhosa</option>
            <option value="97">Yoruba</option>
            <option value="98">Zulu</option>
        </select>
    </div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<div class="form-group">
<label class="col-lg-2 control-label" for="uname">Nationality </label>
<div class="col-lg-6">
    <select class="form-control" name="data[RentAd][exist_mate_nationality]" id="step4_nationality">
        <option value="Not disclosed" selected>Not disclosed</option>
        <option value="Afghan">Afghan</option>
        <option>Albanian</option>
        <option>Algerian</option>
        <option>American</option>
        <option>Andorran</option>
        <option>Angolan</option>
        <option>Antiguans</option>
        <option>Argentinean</option>
        <option>Armenian</option>
        <option>Australian</option>
        <option>Austrian</option>
        <option>Azerbaijani</option>
        <option>Bahamian</option>
        <option>Bahraini</option>
        <option>Bangladeshi</option>
        <option>Barbadian</option>
        <option>Barbudans</option>
        <option>Batswana</option>
        <option>Belarusian</option>
        <option>Belgian</option>
        <option>Belizean</option>
        <option>Beninese</option>
        <option>Bhutanese</option>
        <option>Bolivian</option>
        <option>Bosnian</option>
        <option>Brazilian</option>
        <option>British</option>
        <option>Bruneian</option>
        <option>Bulgarian</option>
        <option>Burkinabe</option>
        <option>Burmese</option>
        <option>Burundian</option>
        <option>Cambodian</option>
        <option>Cameroonian</option>
        <option>Canadian</option>
        <option>Cape Verdean</option>
        <option>Central African</option>
        <option>Chadian</option>
        <option>Chilean</option>
        <option>Chinese</option>
        <option>Colombian</option>
        <option>Comoran</option>
        <option>Congolese</option>
        <option>Costa Rican</option>
        <option>Croatian</option>
        <option>Cuban</option>
        <option>Cypriot</option>
        <option>Czech</option>
        <option>Danish</option>
        <option>Djibouti</option>
        <option>Dominican</option>
        <option>Dutch</option>
        <option>East Timorese</option>
        <option>Ecuadorean</option>
        <option>Egyptian</option>
        <option>Emirian</option>
        <option>English</option>
        <option>Equatorial Guinean</option>
        <option>Eritrean</option>
        <option>Estonian</option>
        <option>Ethiopian</option>
        <option>Fijian</option>
        <option>Filipino</option>
        <option>Finnish</option>
        <option>French</option>
        <option>Gabonese</option>
        <option>Gambian</option>
        <option>Georgian</option>
        <option>German</option>
        <option>Ghanaian</option>
        <option>Greek</option>
        <option>Grenadian</option>
        <option>Guatemalan</option>
        <option>Guinea-Bissauan</option>
        <option>Guinean</option>
        <option>Guyanese</option>
        <option>Haitian</option>
        <option>Herzegovinian</option>
        <option>Honduran</option>
        <option>Hungarian</option>
        <option>I-Kiribati</option>
        <option>Icelander</option>
        <option>Indian</option>
        <option>Indonesian</option>
        <option>Iranian</option>
        <option>Iraqi</option>
        <option>Irish</option>
        <option>Israeli</option>
        <option>Italian</option>
        <option>Ivorian</option>
        <option>Jamaican</option>
        <option>Japanese</option>
        <option>Jordanian</option>
        <option>Kazakhstani</option>
        <option>Kenyan</option>
        <option>Kittian and Nevisian</option>
        <option>Kuwaiti</option>
        <option>Kyrgyz</option>
        <option>Laotian</option>
        <option>Latvian</option>
        <option>Lebanese</option>
        <option>Liberian</option>
        <option>Libyan</option>
        <option>Liechtensteiner</option>
        <option>Lithuanian</option>
        <option>Luxembourger</option>
        <option>Macedonian</option>
        <option>Malagasy</option>
        <option>Malawian</option>
        <option>Malaysian</option>
        <option>Maldivan</option>
        <option>Malian</option>
        <option>Maltese</option>
        <option>Marshallese</option>
        <option>Mauritanian</option>
        <option>Mauritian</option>
        <option>Mexican</option>
        <option>Micronesian</option>
        <option>Moldovan</option>
        <option>Monacan</option>
        <option>Mongolian</option>
        <option>Moroccan</option>
        <option>Mosotho</option>
        <option>Motswana</option>
        <option>Mozambican</option>
        <option>Namibian</option>
        <option>Nauruan</option>
        <option>Nepalese</option>
        <option>Netherlander</option>
        <option>New Zealander</option>
        <option>Ni-Vanuatu</option>
        <option>Nicaraguan</option>
        <option>Nigerian</option>
        <option>Nigerien</option>
        <option>North Korean</option>
        <option>Northern Irish</option>
        <option>Norwegian</option>
        <option>Omani</option>
        <option>Pakistani</option>
        <option>Palauan</option>
        <option>Panamanian</option>
        <option>Papua New Guinean</option>
        <option>Paraguayan</option>
        <option>Peruvian</option>
        <option>Polish</option>
        <option>Portuguese</option>
        <option>Qatari</option>
        <option>Romanian</option>
        <option>Russian</option>
        <option>Rwandan</option>
        <option>Saint Lucian</option>
        <option>Salvadoran</option>
        <option>Samoan</option>
        <option>San Marinese</option>
        <option>Sao Tomean</option>
        <option>Saudi</option>
        <option>Scottish</option>
        <option>Senegalese</option>
        <option>Serbian</option>
        <option>Seychellois</option>
        <option>Sierra Leonean</option>
        <option>Singaporean</option>
        <option>Slovakian</option>
        <option>Slovenian</option>
        <option>Solomon Islander</option>
        <option>Somali</option>
        <option>South African</option>
        <option>South Korean</option>
        <option>Spanish</option>
        <option>Sri Lankan</option>
        <option>Sudanese</option>
        <option>Surinamer</option>
        <option>Swazi</option>
        <option>Swedish</option>
        <option>Swiss</option>
        <option>Syrian</option>
        <option>Taiwanese</option>
        <option>Tajik</option>
        <option>Tanzanian</option>
        <option>Thai</option>
        <option>Togolese</option>
        <option>Tongan</option>
        <option>Trinidadian or Tobagonian</option>
        <option>Tunisian</option>
        <option>Turkish</option>
        <option>Tuvaluan</option>
        <option>Ugandan</option>
        <option>Ukrainian</option>
        <option>Uruguayan</option>
        <option>Uzbekistani</option>
        <option>Venezuelan</option>
        <option>Vietnamese</option>
        <option>Welsh</option>
        <option>Yemenite</option>
        <option>Zambian</option>
        <option>Zimbabwean</option>
    </select>
</div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="uname">Sexual orientation </label>
    <div class="col-lg-6">
        <select class="form-control" name="data[RentAd][exist_mate_orientation]" id="step4_sexual_orientation">
            <option value="Not Disclosed">Not Disclosed</option>
            <option value="Straight">Straight</option>
            <option value="Mixed">Mixed</option>
            <option value="Gay/Lesbian">Gay/Lesbian</option>
            <option value="Bi-sexual">Bi-sexual</option>
        </select>
        <input type="checkbox" name="data[RentAd][is_orientation_part]" value="1" id="step4_sexual_orientation_yes">Yes, I would like my orientation to form part of my advert, search criteria and allow others to search on this field
    </div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="uname">Interests </label>
    <div class="col-lg-6">
        <input type="text" class="form-control" name="data[RentAd][exist_mate_interests]" id="step4_interests">
    </div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<h3>Preferences For New Flatmate</h3>

<div class="form-group">
    <label class="col-lg-2 control-label" for="uname">Smoking </label>
    <div class="col-lg-6">
        <select class="form-control" name="data[RentAd][new_mate_smoking]" id="step4_smoking1">
            <option value="No Preference">No Preference</option>
            <option value="Yes">Yes</option>
            <option value="No">No</option>
        </select>
    </div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="uname">Gender </label>
    <div class="col-lg-6">
        <select class="form-control" name="data[RentAd][new_mate_gender]" id="step4_gender1">
            <option value="No Preference">No Preference</option>
            <option value="Male">Male</option>
            <option value="Female">Female</option>
        </select>
    </div>

</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="uname">Occupation </label>
    <div class="col-lg-6">
        <select class="form-control" name="data[RentAd][new_mate_occupation]" id="step4_occupation1">
            <option value="No Preference">No Preference</option>
            <option value="Not disclosed" selected>Not disclosed</option>
            <option value="Student">Student</option>
            <option value="Professional">Professional</option>
            <option value="Other">Other</option>
            <option value="Mixed">Mixed</option>
        </select>
    </div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="uname">Pets </label>
    <div class="col-lg-6">
        <select class="form-control" name="data[RentAd][new_mate_pets]" id="step4_pets1">
            <option value="No Preference">No Preference</option>
            <option value="No">No</option>
            <option value="Yes">Yes</option>
        </select>
    </div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="uname">Minimum age </label>
    <div class="col-lg-6">
        <select class="form-control" name="data[RentAd][new_mate_min_age]" id="step4_minimum1">
            <option value="null" selected="">-</option>
            <?php
            for($i=16; $i<=99; $i++)
                echo "<option value=\"$i\">$i</option>";
            ?>
        </select>
    </div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="uname">Maximum age </label>
    <div class="col-lg-6">
        <select class="form-control" name="data[RentAd][new_mate_max_age]" id="step4_maximum1">
            <option value="null" selected="">-</option>
            <?php
            for($i=16; $i<=99; $i++)
                echo "<option value=\"$i\">$i</option>";
            ?>
        </select>
    </div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="uname">Language </label>
    <div class="col-lg-6">
        <select class="form-control" name="data[RentAd][new_mate_language]" id="step4_language1">
            <option value="26">English</option>
            <option value="27">Mixed</option>
            <option value="4">Arabic</option>
            <option value="11">Bengali</option>
            <option value="17">Cantonese</option>
            <option value="37">French</option>
            <option value="39">German</option>
            <option value="44">Hindi</option>
            <option value="48">Indonesian</option>
            <option value="50">Japanese</option>
            <option value="62">Malay</option>
            <option value="63">Mandarin</option>
            <option value="73">Portuguese</option>
            <option value="74">Punjabi</option>
            <option value="79">Russian</option>
            <option value="88">Spanish</option>
            <option value="1">Afrikaans</option>
            <option value="2">Albanian</option>
            <option value="3">Amharic</option>
            <option value="5">Armenian</option>
            <option value="6">Aymara</option>
            <option value="7">Baluchi</option>
            <option value="8">Bambara</option>
            <option value="9">Basque</option>
            <option value="10">Belarussian</option>
            <option value="12">Berber</option>
            <option value="13">Bislama</option>
            <option value="14">Bosnian</option>
            <option value="15">Bulgarian</option>
            <option value="16">Burmese</option>
            <option value="18">Catalan</option>
            <option value="19">Ciluba</option>
            <option value="20">Creole</option>
            <option value="21">Croatian</option>
            <option value="22">Czech</option>
            <option value="23">Danish</option>
            <option value="24">Dari</option>
            <option value="25">Dutch</option>
            <option value="28">Eskimo</option>
            <option value="29">Estonian</option>
            <option value="30">Ewe</option>
            <option value="31">Fang</option>
            <option value="32">Faroese</option>
            <option value="33">Farsi (Persian)</option>
            <option value="34">Filipino</option>
            <option value="35">Finnish</option>
            <option value="36">Flemish</option>
            <option value="38">Galician</option>
            <option value="40">Greek</option>
            <option value="41">Gujarati</option>
            <option value="42">Hausa</option>
            <option value="43">Hebrew</option>
            <option value="45">Hungarian</option>
            <option value="46">Ibo</option>
            <option value="47">Icelandic</option>
            <option value="49">Italian</option>
            <option value="51">Kabi�</option>
            <option value="52">Kashmiri</option>
            <option value="53">Kirundi</option>
            <option value="54">Kiswahili</option>
            <option value="55">Korean</option>
            <option value="56">Latvian</option>
            <option value="57">Lingala</option>
            <option value="58">Lithuanian</option>
            <option value="59">Luxembourgish</option>
            <option value="60">Macedonian</option>
            <option value="61">Malagasy</option>
            <option value="64">Mayan</option>
            <option value="65">Motu</option>
            <option value="66">Nepali</option>
            <option value="67">Norwegian</option>
            <option value="68">Noub</option>
            <option value="69">Pashto</option>
            <option value="70">Peul</option>
            <option value="71">Pidgin</option>
            <option value="72">Polish</option>
            <option value="75">Pushtu</option>
            <option value="76">Quechua</option>
            <option value="77">Romanian</option>
            <option value="78">Romansch</option>
            <option value="80">Sango</option>
            <option value="81">Serbian</option>
            <option value="82">Setswana</option>
            <option value="83">Sindhi</option>
            <option value="84">Sinhala</option>
            <option value="85">Slovak</option>
            <option value="86">Slovene</option>
            <option value="87">Somali</option>
            <option value="89">Swahili</option>
            <option value="90">Swedish</option>
            <option value="91">Tamil</option>
            <option value="92">Thai</option>
            <option value="93">Turkish</option>
            <option value="94">Urdu</option>
            <option value="95">Vietnamese</option>
            <option value="99">Welsh</option>
            <option value="96">Xhosa</option>
            <option value="97">Yoruba</option>
            <option value="98">Zulu</option>
        </select>
    </div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="uname">Orientation </label>
    <div class="col-lg-6">
        <select class="form-control" name="data[RentAd][new_mate_orientation]" id="step4_orientation1">
            <option value="Not Disclosed">Not Disclosed</option>
            <option value="Straight">Straight</option>
            <option value="Mixed">Mixed</option>
            <option value="Gay/Lesbian">Gay/Lesbian</option>
            <option value="Bi-sexual">Bi-sexual</option>
        </select>
    </div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="uname">Couples welcome? </label>
    <div class="col-lg-6">
        <input type="radio" name="data[RentAd][new_mate_couples_welcomed]" value="No" id="step4_couples_welcome">No
        <input type="radio" name="data[RentAd][new_mate_couples_welcomed]" value="Yes" id="step4_couples_welcome"> Yes
    </div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="uname">Misc </label>
    <div class="col-lg-6">
        <input type="checkbox" name="data[RentAd][new_mate_dss_welcomed]" value="Yes" nam="step4_misc" id="step4_misc"> DSS welcome <br>
        <input type="checkbox" name="data[RentAd][new_mate_veg_pref]" value="Yes" name="step4_misc1" id="step4_misc"> Vegetarian preferred<br>
        * Please specify rent adjustments in your advert description below.
    </div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<div class="form-group">
    <div class="col-lg-10 col-lg-offset-2">
        <button class="btn btn-warning back3" type="button"><span class="fa fa-arrow-left"></span> Back</button>
        <button class="btn btn-primary open4" type="button">Next <span class="fa fa-arrow-right"></span></button>
        <br><br>
    </div>
</div>
</fieldset>
</div>

<div id="sf5" class="frm" style="display: none;">
    <fieldset>
        <legend>Step 5 of 6</legend>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Title<br>(short description - max 50 characters) </label>
            <div class="col-lg-6">
                <input type="text" placeholder="Title" id="title" name="data[RentAd][title]" class="form-control" autocomplete="off">
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Description </label>
            <div class="col-lg-6">
                <input type="text" placeholder="" id="description" name="data[RentAd][description]" class="form-control" autocomplete="off"style="height: 300px;"><br>
                <b> Tips: Give more detail about the accommodation, who you are looking for and what a potential flatmate should expect living with you. You must write at least 25 words and can write as much as you like within reason.</b>
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Photos </label>
            <div class="col-lg-6">
                <b>You will have the opportunity to add photos to your advert after step 6</b>
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Your Name</label>
            <div class="col-lg-6">
                <input type="text" placeholder="First Name" id="fname" name="data[RentAd][display_name_first]" class="form-control" autocomplete="off" style="width: 40%;float: left;margin-right: 3%;">
                <input type="text" placeholder="Last Name" id="lname" name="data[RentAd][display_name_last]" class="form-control" autocomplete="off" style="width: 40%;">
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Telephone</label>
            <div class="col-lg-6">
                <input type="text" placeholder="Telephone" id="telephone" name="data[RentAd][telephone]" class="form-control" autocomplete="off">
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Telephone</label>
            <div class="col-lg-6">
                <b> As per your login details provided on the next step (NOTE We never reveal your email address - users email you through our messaging system which we then forward to your email, thus protecting your privacy)</b>
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <button class="btn btn-warning back4" type="button"><span class="fa fa-arrow-left"></span> Back</button>
                <button class="btn btn-primary open5" type="button">Next <span class="fa fa-arrow-right"></span></button>
                <br><br>
            </div>
        </div>
    </fieldset>
</div>

<div id="sf6" class="frm" style="display: none;">
    <fieldset>
        <legend>Step 6 of 6</legend>

        <div class="form-group">
            <label class="control-label col-sm-2" for="email">Email:</label>
            <div class="col-sm-10">
                <input type="email" class="form-control" id="email" name="data[User][email]" placeholder="Enter email">
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>


        <div class="form-group">
            <label class="control-label col-sm-2" for="pwd">Password:</label>
            <div class="col-sm-10">
                <input type="password" class="form-control" id="password" name="data[User][simple_pwd]" placeholder="Enter password">
                <a href="/forget_password" style="float:right;">Forgot Your Password</a>
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <button class="btn btn-warning back5" type="button"><span class="fa fa-arrow-left"></span> Back</button>
                <input class="btn btn-primary open6" type="submit" id="login" value="Submit">
                <img src="spinner.html" alt="" id="loader" style="display: none">
            </div>
        </div>

        <hr>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <h3>Not registered? Join us FREE</h3>
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <a href="/register" class="btn btn-primary">Register with your email</a>
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <hr>
    </fieldset>
</div>
</form>
</div>

<script type="text/javascript">

    jQuery().ready(function() {

        // validate form on keyup and submit
        var v = jQuery("#basicform").validate({
            rules: {
                step1_postcode: {
                    required: true,
                    minlength: 4
                },
                step1_email: {
                    required: true,
                    minlength: 4,
                    email: true,
                    maxlength: 30
                },

                upass2: {
                    required: true,
                    minlength: 6,
                    equalTo: "#upass1"
                }

            },
            errorElement: "span",
            errorClass: "help-inline-error"
        });

        function change_postcode_label () {
            var postcode = $("input[name='data[RentAd][postcode]']").val();
            if(postcode==null || postcode.length == 0 || postcode==" " )
                postcode = "Not set.";
            //console.log(postcode);
            $("#postcode_label").html(postcode);
        }

        $(".open1").click(function() {
            if (v.form()) {
                $(".frm").hide("fast");
                $("#sf2").show("slow");
            }
            change_postcode_label();
        });

        $(".open2").click(function() {
            if (v.form()) {
                $(".frm").hide("fast");
                $("#sf3").show("slow");
            }
        });



        $(".open3").click(function() {
            if (v.form()) {
                $(".frm").hide("fast");
                $("#sf4").show("slow");
            }
        });


        $(".open4").click(function() {
            if (v.form()) {
                $(".frm").hide("fast");
                $("#sf5").show("slow");
            }
        });


        $(".open5").click(function() {
            if (v.form()) {
                $(".frm").hide("fast");
                $("#sf6").show("slow");
            }
        });

        /*   $(".open6").click(function() {
         if (v.form()) {
         $("#loader").show();
         setTimeout(function(){
         $("#basicform").html('<h2>Thanks for your time.</h2>');
         }, 1000);
         return false;
         }
         });

         */

        $(".back1").click(function() {
            $(".frm").hide("fast");
            $("#sf1").show("slow");
        });

        $(".back2").click(function() {
            $(".frm").hide("fast");
            $("#sf2").show("slow");
            change_postcode_label();
        });

        $(".back3").click(function() {
            $(".frm").hide("fast");
            $("#sf3").show("slow");
        });

        $(".back4").click(function() {
            $(".frm").hide("fast");
            $("#sf4").show("slow");
        });

        $(".back5").click(function() {
            $(".frm").hide("fast");
            $("#sf5").show("slow");
        });

    });
</script>