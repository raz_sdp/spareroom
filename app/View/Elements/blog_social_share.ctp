<?php echo $this->Html->script(array('jquery.floating-social-share')); ?>
<script>
    $("body").floatingSocialShare({
        buttons: ["facebook", "twitter", "google-plus", "linkedin","pinterest"],
        text: "share with: ",
        counter: false,
        title: document.title,
        url: window.location.href
    });
</script>