<div class="col-lg-2">
    <div class="search">
        <!--<h1>Search</h1>
              <form id="form1" name="form1" method="get" action="blog.php">
                  <table class="job-search">
                      <tbody>
                          <tr>
                              <td>
                                <input type="text" name="keyword" id="textfield" class="search-job">
                              </td>
                          </tr>
                          <tr>
                               <td>&nbsp;</td>
                          </tr>
                          <tr>
                                <td><input type="submit" name="Search" value="Search" class="search-button"></td>
                          </tr>
                      </tbody>
                  </table>
              </form> -->


        <style>
            .linkcat{color: #363636;  font-family: Corbell; font-size: 20px; font-weight:400px; text-decoration:none}
            .linkcat:hover{ color:#666;}
            .linkcatul{ display:none; list-style:none}
            .linkcatul li{ list-style-type:none}
            .linkcatul li a{ font-size:16px !important}
        </style>
        <script>
            $(document).ready(function(e) {
                //alert("ok");
                $(".linkcatli").mouseover(function(e) {
                    $(this).children("ul").show().slideDown("slow");
                });
                $(".linkcatli").mouseout(function(e) {
                    $(this).children("ul").hide().slideUp("slow");
                });
            });
        </script>

        <ul style="margin: 0px; padding: 0px; list-style:none">
            <h1>Categories</h1>
            <?php foreach ($categories as $category){ ?>
                <li class="linkcatli">
                    <a class="linkcat" href="<?php echo $this->webroot."blog/index/".$category['BlogCategory']['id'];?>">
                        <?php echo $category['BlogCategory']['name'];?>
                    </a>
                    <?php if(!empty($category['ChildBlogCategory'])){ ?>
                        <ul class="linkcatul" style="font-size: 16px; font-family: Corbell; display: none;">
                            <?php foreach ($category['ChildBlogCategory'] as $sub_category){ ?>
                                <li>
                                    <a class="linkcat" href="<?php echo $this->webroot."blog/index/".$sub_category['id'];?>">
                                        <?php echo $sub_category['name'];?>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>