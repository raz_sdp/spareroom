<div class="actions col-md-2 col-sm-2">
	<h5  id="menu"><?php echo $this->Html->link(__('Dashboard'), array('controller' => 'cms_users' ,'action' => 'dashboard', 'admin' => true)); ?></h5>
	<?php 
		$controller = $this->params['controller'];
		//print_r($controller);
		$action = $this->params['action'];
		//print_r($action);
		$method = $this->params['pass'][0];
		//print_r($method);

		$role = $this->Session->read('role');
        
	?>
	<ul class="nav-menu">
		<?php if($role!='author') { ?>
		<li  class="node<?php if($controller == 'general_settings') echo ' selected';?>"><?php echo $this->Html->link(__('General Settings'), array('controller' => 'general_settings' ,'action' => 'edit', 'admin' => true)); ?></li>

		<li class="document<?php if($controller == 'users') echo ' selected';?>"><?php echo $this->Html->link(__('Users'), array('controller' => 'users', 'action' => 'index', 'admin' => true)); ?></li>

        <li class="node<?php if($controller == 'property_ads' || $controller == 'rent_ads' || $controller == 'wanted_ads' || ($controller == 'cms_users' && $action == 'admin_ads_dashboard')) echo ' selected';?>"><?php echo $this->Html->link(__('Ads'), array('controller' => 'cms_users', 'action' => 'ads_dashboard','admin' => true)); ?></li>
        <?php if($controller == 'property_ads' || $controller == 'rent_ads' || $controller == 'wanted_ads' || ($controller == 'cms_users' && $action == 'admin_ads_dashboard')){ ?>
            <li class="add a-s<?php if($controller == 'property_ads' && $action == 'admin_index') echo ' selected';?>"><?php echo $this->Html->link(__('Property Ads'), array('controller' => 'property_ads', 'action' => 'index', 'admin' => true)); ?></li>
            <li class="add a-s<?php if($controller == 'rent_ads' && $action == 'admin_index') echo ' selected';?>"><?php echo $this->Html->link(__('Rent Ads'), array('controller' => 'rent_ads', 'action' => 'index', 'admin' => true)); ?></li>
            <li class="add a-s<?php if($controller == 'wanted_ads' && $action == 'admin_index') echo ' selected';?>"><?php echo $this->Html->link(__('Wanted Ads'), array('controller' => 'wanted_ads', 'action' => 'index', 'admin' => true)); ?></li>
        <?php } ?>
        <?php } ?>

        <?php if($role!='manager') { ?>
        <li class="document<?php if($controller == 'blog' || $controller == 'blog_categories') echo ' selected';?>"><?php echo $this->Html->link(__('Blog'), array('controller' => 'blog', 'action' => 'index', 'admin' => true)); ?></li>
        <?php if($controller == 'blog' || $controller == 'blog_categories'){ ?>
            <li class="add a-s<?php if($controller == 'blog' && $action == 'admin_add') echo ' selected';?>"><?php echo $this->Html->link(__('New Post'), array('controller' => 'blog', 'action' => 'add', 'admin' => true)); ?></li>
            <li class="add a-s<?php if($controller == 'blog_categories' && $action == 'admin_index') echo ' selected';?>"><?php echo $this->Html->link(__('Categories'), array('controller' => 'blog_categories', 'action' => 'index', 'admin' => true)); ?></li>
            <li class="add a-s<?php if($controller == 'blog_categories' && $action == 'admin_add') echo ' selected';?>"><?php echo $this->Html->link(__('Add Category'), array('controller' => 'blog_categories', 'action' => 'add', 'admin' => true)); ?></li>
        <?php } ?>
        <?php } ?>

        <?php if($role=='admin') { ?>
	    <li class="node<?php if($controller =='cms_users' && ($action == 'admin_index' || $action == 'admin_add' || $action == 'admin_view')) echo ' selected';?>"><?php echo $this->Html->link(__('Cms Users'), array('controller' => 'cms_users', 'action' => 'index', 'admin' => true)); ?></li>
	    <?php if($controller =='cms_users' && ($action == 'admin_index' || $action == 'admin_add' || $action == 'admin_view')){ ?>
            <li class="add a-s<?php if($controller == 'cms_users' && $action == 'admin_add') echo ' selected';?>"><?php echo $this->Html->link(__('New Cms User'), array('controller' => 'cms_users', 'action' => 'add', 'admin' => true)); ?></li>
        <?php } ?>
        <?php } ?>

        <li class="logout"><?php echo $this->Html->link(__('Logout'), array('controller' => 'cms_users', 'action' => 'logout')); ?></li>
    </ul>
</div>