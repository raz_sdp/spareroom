<div class="block_content">
    <?php if($table!="wanted") { ?>
    <?php $table1 = ucfirst($table)."Ad";?>

    <form action="<?php echo $this->webroot . $table;?>_ads/edit/<?php echo $ad[$table1]['id']?>" method="post" id="photos_upload" enctype="multipart/form-data">
        <?php
        if(!empty($this->Form->data[$table1]['photo_1'])){
        echo "<div class=\"thumbnail-item\">";
            echo $this->Html->image('/files/add_images/' . $this->request->data[$table1]['photo_1'], array('alt'=>'picture', 'width'=> '150px','height' => 'auto'));
            echo "</div>";
        }
        ?>
        Photo 1 : <input type="file" name="data[<?php echo $table1;?>][photo_1]">
        <br>Photo 1 Description : <input type="text" name="data[<?php echo $table1;?>][photo_1_desc]" value="<?php echo $ad[$table1]['photo_1_desc'];?>">
        <br><br>
        <?php
        if(!empty($this->Form->data[$table1]['photo_2'])){
            echo "<div class=\"thumbnail-item\">";
            echo $this->Html->image('/files/add_images/' . $this->request->data[$table1]['photo_2'], array('alt'=>'picture', 'width'=> '150px','height' => 'auto'));
            echo "</div>";
        }
        ?>
        Photo 2 : <input type="file" name="data[<?php echo $table1;?>][photo_2]">
        <br>Photo 2 Description : <input type="text" name="data[<?php echo $table1;?>][photo_2_desc]" value="<?php echo $ad[$table1]['photo_2_desc'];?>">
        <br><br>
        <?php
        if(!empty($this->Form->data[$table1]['photo_3'])){
            echo "<div class=\"thumbnail-item\">";
            echo $this->Html->image('/files/add_images/' . $this->request->data[$table1]['photo_3'], array('alt'=>'picture', 'width'=> '150px','height' => 'auto'));
            echo "</div>";
        }
        ?>
        Photo 3 : <input type="file" name="data[<?php echo $table1;?>][photo_3]">
        <br>Photo 3 Description : <input type="text" name="data[<?php echo $table1;?>][photo3_desc]" value="<?php echo $ad[$table1]['photo_3_desc'];?>">
        <br><br>
        <?php
        if(!empty($this->Form->data[$table1]['photo_4'])){
            echo "<div class=\"thumbnail-item\">";
            echo $this->Html->image('/files/add_images/' . $this->request->data[$table1]['photo_4'], array('alt'=>'picture', 'width'=> '150px','height' => 'auto'));
            echo "</div>";
        }
        ?>
        Photo 4 : <input type="file" name="data[<?php echo $table1;?>][photo_4]">
        <br>Photo 4 Description : <input type="text" name="data[<?php echo $table1;?>][photo_4_desc]" value="<?php echo $ad[$table1]['photo_4_desc'];?>">
        <br><br>
        <?php
        if(!empty($this->Form->data[$table1]['photo_5'])){
            echo "<div class=\"thumbnail-item\">";
            echo $this->Html->image('/files/add_images/' . $this->request->data[$table1]['photo_5'], array('alt'=>'picture', 'width'=> '150px','height' => 'auto'));
            echo "</div>";
        }
        ?>
        Photo 5 : <input type="file" name="data[<?php echo $table1;?>][photo_5]">
        <br>Photo 5 Description : <input type="text" name="data[<?php echo $table1;?>][photo_5_desc]" value="<?php echo $ad[$table1]['photo_5_desc'];?>">
        <br><br><br><input type="submit">
    </form>
    <?php } ?>

    <?php if($table=="wanted") { ?>
        <h2>Your current photos</h2>
        <p>
            No photos available for wanted ads.
        </p>
    <?php } else { ?>
        <h2>Your current photos</h2>
        <p>
            You have no photos yet.<br>
            Adverts with photos have much greater chance of responses.
        </p>
    <?php } ?>

</div>