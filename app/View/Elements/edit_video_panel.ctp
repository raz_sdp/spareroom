<div class="block_content">
    <?php if($table!="wanted") { ?>
    <?php $table1 = ucfirst($table)."Ad";?>
    <form action="<?php echo $this->webroot . $table;?>_ads/edit/<?php echo $ad[$table1]['id']?>" method="post" enctype="multipart/form-data">
        <fieldset>
            <legend>Upload video</legend>
            <p>
                <label for="caption">Title</label><br>
                <input type="text" size="30" value="" name="data[<?php echo $table1;?>][video_caption]">
            </p>
            <p>
                <input type="hidden" name="MAX_FILE_SIZE" value="209715200">
                <input type="file" name="data[<?php echo $table1;?>][video_link]">
            </p>
            <p>
                <button type="submit">Upload</button>
            </p>
        </fieldset>
    </form>
    <?php } ?>

    <?php if($table=="wanted") { ?>
        <h2>Your current photos</h2>
        <p>
            No photos available for wanted ads.
        </p>
    <?php } else { ?>
        <h2>Your current photos</h2>
        <p>
            You have no photos yet.<br>
            Adverts with photos have much greater chance of responses.
        </p>
    <?php } ?>
</div> <!-- /.block_content -->