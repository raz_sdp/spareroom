<div style="display:none; border: 1px solid lightblue;" id="ad_details_mail_panel" class="block_content detail_page_tab_content_inner">
    <div class="grid-12-4">
        <?php if(empty($user)) { ?>
            <p style="font-size: 20px; margin-left: 10px">You must <a href="<?php echo $this->webroot;?>login">Log In</a> or <a href="<?php echo $this->webroot;?>register">Register</a> in order to achieve the contact no. of the adviser.</p>
        <?php } else { ?>
            <div class="contact-form">
                <h2 id="contact_the_advertiser_heading" class="byemail">
                    Contact the advertiser of this by email
                </h2>

                <form action="<?php echo $this->webroot;?>users/send_email" method="post">
                    <input type="hidden" name="ad_table" value="<?php echo $flag;?>">
                    <input type="hidden" name="ad_id" value="<?php echo $result[$flag]['id'];?>">
                    <input type="hidden" name="user_id" value="<?php echo $user['User']['id'];?>">

                    <div id="contact_advertiser_form">
                        <div class="formrow">
                            <div class="formlabel">To:</div>
                            <div><?php echo $result['User']['first_name']." ". $result['User']['last_name'];?></div>
                        </div>
                        <div class="formrow">
                            <div class="formlabel">Subject:</div>
                            <div><?php echo $result[$flag]['title'];?></div>
                        </div>
                        <div class="formrow" id="messagefield">
                            <div class="formlabel" id="messagefieldlabel">Message:</div>
                            <div id="messagefieldinput">
                                &nbsp;<textarea name="message" rows="7" cols="35" wrap="virtual"></textarea>
                            </div>
                        </div>
                        <div class="formrow" id="fromfield">
                            <div class="formlabel">From:</div>
                            <div id="fromfieldinput">
                                <?php echo $user['User']['first_name']." ". $user['User']['last_name'] . "(" . $user['User']['email'] . ")" ;?>
                                <span class="lightercaption">
                                            (not you? <a href="<?php echo $this->webroot;?>users/logout" tabindex="-1">log
                                        in as other user</a>)
                                        </span>
                            </div>
                        </div>
                    </div>

                    <!-- end new or existing user bit  FROM form input div-->
                    <div style="clear:both">&nbsp;<!-- filler div - otherwise on spareroom, the white background of the containing div  stops just after the from: label as the new/existing user forms are floating divs --></div>
                    <input class="primary-standard" type="submit" value="Send message">
                </form>
            </div><!-- end /.col_first -->
        <?php } ?>
        <div class="asidecontent">
        </div><!-- end col_last -->
    </div><!-- end cols4 -->
</div>
