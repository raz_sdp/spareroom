<div class="block_content">
<?php $table1 = ucfirst($table)."Ad";?>
<form name="basicform" id="basicform" method="post" action="<?php echo $this->webroot . $table;?>_ads/edit/<?php echo $ad[$table1]['id']?>">

<div id="sf1" class="frm">
    <fieldset>
        <legend>Step 1 of 5</legend>
        <h3>Get started with your free advert</h3>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">I have </label>
            <div class="col-lg-3">
                <select class="form-control" id="step1_size" name="data[PropertyAd][no_rooms]">
                    <?php
                    for($i=2; $i<=12; $i++)
                        echo "<option value=\"$i bed\">$i bed</option>";
                    ?>
                </select>
            </div>

            <div class="col-lg-3">
                <select class="form-control" id="step1_size1" name="data[PropertyAd][type]">
                    <option value="Flat">Flat/Apartment</option>
                    <option>House</option>
                    <option>Property</option>
                </select>
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Rent </label>
            <div class="col-lg-4">
                <input type="text" placeholder="Your Cost for Room" id="step3_cost_of_room" name="data[PropertyAd][rent]" class="form-control">
                <input type="radio" id="step3_cost" name="data[PropertyAd][per_week]" value="per week"> per week
                <input type="radio" id="step3_cost" name="data[PropertyAd][per_week]" value="per month"> per calendar month
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Postcode of property</label>
            <div class="col-lg-6">
                <input type="text" class="form-control" placeholder="(e.g. SE15 8PD)" name="data[PropertyAd][postcode]" id="step1_postcode">
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">I am a </label>
            <div class="col-lg-6">
                <input type="radio" name="data[PropertyAd][advertiser_type]" value="Live out landlord"> <b>Live out landlord</b> (I own the property but don't live there)<br>
                <input type="radio" name="data[PropertyAd][advertiser_type]" value="Agent"> <b>Agent</b> (I am advertising on a landlord's behalf) <br>
                <input type="radio" name="data[PropertyAd][advertiser_type]" value="Current tenant/flatmate"> <b>Current tenant/flatmate</b> (I am living in the property)
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">My email is </label>
            <div class="col-lg-6">
                <input type="email" class="form-control" placeholder="(We will keep this safe and not display publicly)" name="data[PropertyAd][email]" id="step1_email">

            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <button class="btn btn-primary open1" type="button" name="step1_complete">Next <span class="fa fa-arrow-right"></span></button>
                <br><br>
            </div>
        </div>
    </fieldset>
</div>



<div id="sf2" class="frm" style="display: none;">
<fieldset>
<legend>Step 2 of 5</legend>
<h3>Get started with your free advert</h3>

<div class="form-group">
    <label class="col-lg-2 control-label" for="uname">Area </label>
    <div class="col-lg-6">
        <select class="form-control" id="step2_area" name="data[PropertyAd][area_id]">
            <option value="" SELECTED>Select area...
            <OPTION VALUE="22345">London SE15
            <OPTION VALUE="22341">Nunhead
            <OPTION VALUE="22343">Peckham
            <OPTION VALUE="22342">Peckham Rye
            <OPTION VALUE="22344">Rye Lane
        </select>
    </div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="uname">Street name </label>
    <div class="col-lg-6">
        <input type="text" class="form-control" name="data[PropertyAd][street_name]">
    </div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="uname">Post Code </label>
    <div class="col-lg-6">
        <label id="postcode_label"></label>
    </div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="uname">Transport </label>
    <div class="col-lg-3">
        <select class="form-control" id="step2_transport" name="data[PropertyAd][no_of_mins]">
            <option value="5">0-5</option>
            <option value="10">5-10</option>
            <option value="15">10-15</option>
            <option value="20">15-20</option>
            <option value="25">20-25</option>
            <option value="30">25+</option>
        </select>minutes
    </div>

    <div class="col-lg-3">
        <select class="form-control" id="step2_transport1" name="data[PropertyAd][no_of_mins_by]">
            <option value="walk">walk</option>
            <option value="by car">by car</option>
            <option value="by bus">by bus</option>
            <option value="by tram">by tram</option>
        </select>
        from
    </div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="uname"> </label>
    <div class="col-lg-6">
        <select class="form-control" id="step2_transport2" name="data[PropertyAd][station_name]">
            <option value="EASTCROYDON">East Croydon</option>
            <option value="NUNHEAD">Nunhead</option>
            <option value="PECKHAMRYE">Peckham Rye</option>
            <option value="QUEENSROADPECKHAM">Queens Road Peckham</option>
            <option value="BERMONDSEY">Bermondsey</option>
            <option value="BOROUGH">Borough</option>
            <option value="BROCKLEY">Brockley</option>
            <option value="CANADAWATER">Canada Water</option>
            <option value="CROFTONPARK">Crofton Park</option>
            <option value="DENMARKHILL">Denmark Hill</option>
            <option value="DEPTFORD">Deptford</option>
            <option value="EASTDULWICH">East Dulwich</option>
            <option value="ELEPHANTCASTLE">Elephant & Castlev</option>
            <option value="FORESTHILL">Forest Hill</option>
            <option value="HONOROAKPARK">Honor Oak Parkv</option>
            <option value="LADYWELL">Ladywell</option>
            <option value="LAMBETHNORTH">Lambeth North</option>
            <option value="LONDONBRIDGE">London Bridge</option>
            <option value="NEWCROSS">New Cross</option>
            <option value="NEWCROSSGATE">New Cross Gate</option>
            <option value="ROTHERHITHE">Rotherhithe</option>
            <option value="SOUTHBERMONDSEY">South Bermondsey</option>
            <option value="SOUTHWARK">Southwark</option>
            <option value="STJOHNS">St. Johns</option>
            <option value="SURREYQUAYS">Surrey Quays</option>
            <option value="WATERLOO">Waterloo</option>
            <option value="WATERLOOEAST">Waterloo East</option>
        </select>
    </div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="uname">Security deposit </label>
    <div class="col-lg-6">
        <label>£</label>
        <input type="number" id="step3_security" name="data[PropertyAd][security_deposit]" class="form-control" style="width: 63%;">
    </div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="uname">Furnishing </label>
    <div class="col-lg-6">
        <label>£</label>
        <input type="number" id="step3_security" name="data[PropertyAd][furnishing]" class="form-control" style="width: 63%;">
    </div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="uname">Available from </label>
    <div class="col-lg-2">
        <input type="date" class="form-control" name="data[PropertyAd][available_from]">
    </div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>


<div class="form-group">
    <label class="col-lg-2 control-label" for="uname">Minimum stay </label>
    <div class="col-lg-6">
        <select class="form-control" name="data[PropertyAd][min_stay]" id="step3_minimum_stay">
            <option value="0">No minimum</option>
            <option value="1">1 Month</option>
            <option value="2">2 Months</option>
            <option value="3">3 Months</option>
            <option value="4">4 Months</option>
            <option value="5">5 Months</option>
            <option value="6">6 Months</option>
            <option value="7">7 Months</option>
            <option value="8">8 Months</option>
            <option value="9">9 Months</option>
            <option value="10">10 Months</option>
            <option value="11">11 Months</option>
            <option value="12">1 Year</option>
            <option value="15">1 Year 3 Months</option>
            <option value="18">1 Year 6 Months</option>
            <option value="21">1 Year 9 Months</option>
            <option value="24">2 Years</option>
            <option value="36">3 Years</option>
        </select>
    </div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="uname">Maximum stay </label>
    <div class="col-lg-6">
        <select class="form-control" name="data[PropertyAd][max_stay]" id="step3_maximum_stay">
            <option value="0">No maximum</option>
            <option value="1">1 Month</option>
            <option value="2">2 Months</option>
            <option value="3">3 Months</option>
            <option value="4">4 Months</option>
            <option value="5">5 Months</option>
            <option value="6">6 Months</option>
            <option value="7">7 Months</option>
            <option value="8">8 Months</option>
            <option value="9">9 Months</option>
            <option value="10">10 Months</option>
            <option value="11">11 Months</option>
            <option value="12">1 Year</option>
            <option value="15">1 Year 3 Months</option>
            <option value="18">1 Year 6 Months</option>
            <option value="21">1 Year 9 Months</option>
            <option value="24">2 Years</option>
            <option value="36">3 Years</option>
        </select>
    </div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="uname">Short Term Lets Considered? </label>
    <div class="col-lg-6">
        <input type="checkbox" name="data[PropertyAd][short_term_considered]" value="1" id="step3_tick_for_yes"> Tick for Yes
    </div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="uname">References required? </label>
    <div class="col-lg-6">
        <input type="radio" name="data[PropertyAd][references_needed]" value="Yes" id="step3_references_required"> Yes
        <input type="radio" name="data[PropertyAd][references_needed]" value="No" id="step3_references_required"> No
    </div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="uname">Fees apply? </label>
    <div class="col-lg-6">
        <input type="checkbox" name="data[PropertyAd][fees_apply]" value="1" id="step3_tick_for_yes"> Tick for Yes
    </div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="uname">Amenities </label>
    <div class="col-lg-3">
        <input type="checkbox" name="data[PropertyAd][aminities][]" value="Parking" id="step2_amenities"> Parking
    </div>
    <div class="col-lg-3">
        <input type="checkbox" name="data[PropertyAd][aminities][]" value="Balcony/patio" id="step2_amenities"> Balcony/patio
    </div>
    <div class="col-lg-3">
        <input type="checkbox" name="data[PropertyAd][aminities][]" value="Garden/roof terrace" id="step2_amenities"> Garden/roof terrace
    </div>
    <div class="col-lg-3">
        <input type="checkbox" name="data[PropertyAd][aminities][]" value="Disabled access" id="step2_amenities"> Disabled access
    </div>
    <div class="col-lg-3">
        <input type="checkbox" name="data[PropertyAd][aminities][]" value="Garage" id="step2_amenities"> Garage
    </div>
</div>
<div class="clearfix" style="height: 10px;clear: both;"></div>

<div class="form-group">
    <div class="col-lg-10 col-lg-offset-2">
        <button class="btn btn-primary back1" type="button"><span class="fa fa-arrow-left"></span> Back</button>
        <button class="btn btn-primary open2" type="button">Next <span class="fa fa-arrow-right"></span></button>
        <br><br>
    </div>
</div>
</fieldset>
</div>

<div id="sf3" class="frm" style="display: none;">
    <fieldset>
        <legend>Step 3 of 5</legend>
        <h3>Preferences For New Flatmate</h3>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Smoking </label>
            <div class="col-lg-6">
                <select class="form-control" name="data[PropertyAd][tenant_smoking]" id="step4_smoking">
                    <option value="No">No</option>
                    <option value="Yes">Yes</option>
                </select>
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Occupation </label>
            <div class="col-lg-6">
                <select class="form-control" name="data[PropertyAd][tenant_occupation]" id="step4_occupation">
                    <option value="Not disclosed" selected>Not disclosed</option>
                    <option value="Student">Student</option>
                    <option value="Professional">Professional</option>
                    <option value="Other">Other</option>
                    <option value="Mixed">Mixed</option>
                </select>
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Pets </label>
            <div class="col-lg-6">
                <select class="form-control" name="data[PropertyAd][tenant_mate_pets]" id="step4_pets">
                    <option value="No">No</option>
                    <option value="Yes">Yes</option>
                </select>
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Misc </label>
            <div class="col-lg-6">
                <input type="checkbox" name="data[PropertyAd][tenant_dss_welcomed]" value="Yes" id="step4_misc"> DSS welcome
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <button class="btn btn-warning back2" type="button"><span class="fa fa-arrow-left"></span> Back</button>
                <button class="btn btn-primary open4" type="button">Next <span class="fa fa-arrow-right"></span></button>
                <br><br>
            </div>
        </div>
    </fieldset>
</div>


<div id="sf5" class="frm" style="display: none;">
    <fieldset>
        <legend>Step 4 of 5</legend>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Title<br>(short description - max 50 characters) </label>
            <div class="col-lg-6">
                <input type="text" placeholder="Title" id="title" name="data[PropertyAd][title]" class="form-control" autocomplete="off">
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Description </label>
            <div class="col-lg-6">
                <input type="text" placeholder="" id="description" name="data[PropertyAd][description]" class="form-control" autocomplete="off"style="height: 300px;"><br>
                <b> Tips: Give more detail about the accommodation, who you are looking for and what a potential flatmate should expect living with you. You must write at least 25 words and can write as much as you like within reason.</b>
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Photos </label>
            <div class="col-lg-6">
                <b>You will have the opportunity to add photos to your advert after step 6</b>
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Your Name</label>
            <div class="col-lg-6">
                <input type="text" placeholder="First Name" id="fname" name="data[PropertyAd][display_name_first]" class="form-control" autocomplete="off" style="width: 40%;float: left;margin-right: 3%;">
                <input type="text" placeholder="Last Name" id="lname" name="data[PropertyAd][display_name_last]" class="form-control" autocomplete="off" style="width: 40%;">
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Telephone</label>
            <div class="col-lg-6">
                <input type="text" placeholder="Telephone" id="telephone" name="data[PropertyAd][telephone]" class="form-control" autocomplete="off">
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Telephone</label>
            <div class="col-lg-6">
                <b> As per your login details provided on the next step (NOTE We never reveal your email address - users email you through our messaging system which we then forward to your email, thus protecting your privacy)</b>
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <button class="btn btn-warning back3" type="button"><span class="fa fa-arrow-left"></span> Back</button>
                <button class="btn btn-primary open5" type="button">Next <span class="fa fa-arrow-right"></span></button>
                <br><br>
            </div>
        </div>
    </fieldset>
</div>

<div id="sf6" class="frm" style="display: none;">
    <fieldset>
        <legend>Step 5 of 5</legend>

        <div class="form-group">
            <label class="control-label col-sm-2" for="email">Email:</label>
            <div class="col-sm-10">
                <input type="email" class="form-control" id="email" name="data[User][email]" placeholder="Enter email">
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <label class="control-label col-sm-2" for="pwd">Password:</label>
            <div class="col-sm-10">
                <input type="password" class="form-control" id="password" name="data[User][simple_pwd]" placeholder="Enter password">
                <a href="/forget_password" style="float:right;">Forgot Your Password</a>
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <button class="btn btn-warning back5" type="button"><span class="fa fa-arrow-left"></span> Back</button>
                <input class="btn btn-primary open6" type="submit" id="login" value="Submit">
                <img src="spinner.html" alt="" id="loader" style="display: none">
            </div>
        </div>

        <hr>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <h3>Not registered? Join us FREE</h3>
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <a href="/register" class="btn btn-primary">Register with your email</a>
            </div>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>

        <hr>

    </fieldset>
</div>
</form>

<script type="text/javascript">


    jQuery().ready(function() {

        // validate form on keyup and submit
        var v = jQuery("#basicform").validate({
            rules: {
                step1_postcode: {
                    required: true,
                    minlength: 4
                },
                step1_email: {
                    required: true,
                    minlength: 4,
                    email: true,
                    maxlength: 30
                },

                upass2: {
                    required: true,
                    minlength: 6,
                    equalTo: "#upass1"
                }

            },
            errorElement: "span",
            errorClass: "help-inline-error"
        });

        function change_postcode_label () {
            var postcode = $("input[name='data[RentAd][postcode]']").val();
            if(postcode==null || postcode.length == 0 || postcode==" " )
                postcode = "Not set.";
            //console.log(postcode);
            $("#postcode_label").html(postcode);
        }

        $(".open1").click(function() {
            if (v.form()) {
                $(".frm").hide("fast");
                $("#sf2").show("slow");
            }
            change_postcode_label();
        });

        $(".open2").click(function() {
            if (v.form()) {
                $(".frm").hide("fast");
                $("#sf3").show("slow");
            }
        });



        $(".open3").click(function() {
            if (v.form()) {
                $(".frm").hide("fast");
                $("#sf4").show("slow");
            }
        });


        $(".open4").click(function() {
            if (v.form()) {
                $(".frm").hide("fast");
                $("#sf5").show("slow");
            }
        });


        $(".open5").click(function() {
            if (v.form()) {
                $(".frm").hide("fast");
                $("#sf6").show("slow");
            }
        });

        /*   $(".open6").click(function() {
         if (v.form()) {
         $("#loader").show();
         setTimeout(function(){
         $("#basicform").html('<h2>Thanks for your time.</h2>');
         }, 1000);
         return false;
         }
         });

         */

        $(".back1").click(function() {
            $(".frm").hide("fast");
            $("#sf1").show("slow");
        });

        $(".back2").click(function() {
            $(".frm").hide("fast");
            $("#sf2").show("slow");
            change_postcode_label();
        });

        $(".back3").click(function() {
            $(".frm").hide("fast");
            $("#sf3").show("slow");
        });

        $(".back4").click(function() {
            $(".frm").hide("fast");
            $("#sf4").show("slow");
        });

        $(".back5").click(function() {
            $(".frm").hide("fast");
            $("#sf5").show("slow");
        });

    });
</script>

</div>