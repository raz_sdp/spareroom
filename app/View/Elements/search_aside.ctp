        <?php //AuthComponent::_setTrace($request);?>
        <div class="block block_panel search_filter" id="panel" style="width:220px">
            <div class="block_header">
                <h3>Search</h3>
            </div>
            <form action="<?php echo $this->webroot;?>searchEngine/search" method="post" style="font-size: 12px" class="block_content block_areas">
                <input name="nmsq_mode" type="hidden" value="normal">
                <input type="hidden" name="action" value="search">
                <input type="hidden" name="advance_search" value="0">

                <?php
                if(!empty($request['search_for']))
                    $s = $request['search_for'];
                else
                    $s = null;
                ?>

                <div class="block_area block_area_first search_type">
                    <ul>
                        <li>
                            <label title="Rooms for Rent in existing flat and house shares">
                                <input type="radio" name="search_for" <?php if( $s=='to_rent' || $s==null ) echo "checked"; ?> value="to_rent"> Rooms for rent
                            </label>
                        </li>
                        <li>
                            <label title="Potential flatmates and tenants seeking accommodation">
                                <input type="radio" name="search_for" <?php if($s=='wanted') echo "checked"; ?> value="wanted" class="radiobuttons"> Rooms Wanted
                            </label>
                        </li>
                        <li>
                            <label title="Find others seeking accommodation to form a new house / flatshare with">
                                <input type="radio" name="search_for" <?php if($s=='buddy_up') echo "checked"; ?> value="buddy_up" class="radiobuttons"> Buddy ups
                            </label>
                        <span class="tooltip">
                            <span class="tooltip_box">
                                <small class="tooltip_text">
                                    Find potential buddy ups to share this property with
                                </small>
                                <span class="tooltip_arrow">&nbsp;</span>
                            </span>
                            <span class="help tooltip_item">
                                (<a href="/content/info-faq/buddyups/" rel="nofollow">?</a>)
                            </span>
                        </span>
                        </li>
                    </ul>
                </div>

                <div class="block_area search_where">
                <div class="form-group">
                    <label for="search_field">Where</label>
                    <input type="hidden" name="search_by" value="location">
                    <input type="text" name="area_postcode" value="<?php if(!empty($request['area_postcode'])) echo $request['area_postcode']; ?>" onfocus="if(this.value=='(e.g.Greenwich)'){this.value='';}" class="textbox form-control" id="search_field">
                </div>
                    <?php
                        if(!empty($request['distance']))
                            $d = $request['distance'];
                        else
                            $d = null;
                    ?>
                <div class="form-group">
                    <select name="distance" id="radius" class="form-control">
                            <option value="0" <?php if($d==0 || $d==null) echo "selected";?>>This area only</option>
                            <option value="1" <?php if($d==1) echo "selected";?>>1 mile radius</option>
                            <option value="2" <?php if($d==2) echo "selected";?>>2 mile radius</option>
                            <option value="3" <?php if($d==3) echo "selected";?>>3 mile radius</option>
                            <option value="4" <?php if($d==4) echo "selected";?>>4 mile radius</option>
                            <option value="5" <?php if($d==5) echo "selected";?>>5 mile radius</option>
                            <option value="10" <?php if($d==10) echo "selected";?>>10 mile radius</option>
                            <option value="15" <?php if($d==15) echo "selected";?>>15 mile radius</option>
                            <option value="20" <?php if($d==20) echo "selected";?>>20 mile radius</option>
                            <option value="30" <?php if($d==30) echo "selected";?>>30 mile radius</option>
                            <option value="40" <?php if($d==40) echo "selected";?>>40 mile radius</option>
                        </select>
                </div>
                </div>

                <div class="search_filtered_by">

                </div>

                <div class="block_area block_area_last search_go">
                    <p class="search_advanced_link">
                        <a href="<?php echo $this->webroot;?>advanced_search" class="advanced_search">Advanced search</a>
                    </p>
                    <p class="buttons">
                        <input type="submit" value="Search again" class="btn btn-default">
                    </p>
                </div>
            </form>
        </div>
        <div id="narrow_by_search_options">
            <p>
                <a class="btn btn-primary" title="" href="<?php echo $this->webroot;?>advanced_search" rel="nofollow">Narrow search</a>                
            </p>            
        </div>

        <div class="block block_standard" id="findbox" style="width:220px;">
            <div class="block_header">
                <h3>Find an ad</h3>
            </div>
            <form action="<?php echo $this->webroot;?>searchEngine/find_by_ref" method="post" class="block_content form-inline">
                <div class="form-group left">
                    <label for="findid">Advert ref#</label>
                    <input type="text" class="form-control" name="ref" id="findid">
                    <input type="Submit" value="Go" class="btn btn-default left">   
                </div>                         
            </form>
        </div>