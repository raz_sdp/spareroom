<div class="container">
    <h2>Search</h2>
    <ul class="nav nav-tabs">
        <li><?php echo $this->Html->link('Basic Search','/search');?></li>
        <li class="active"><?php echo $this->Html->link('Advanced Search','/advanced_search');?></li>
    </ul>

    <form name="basicform" id="basicform" method="post" action="searchEngine/search">
        <input type="hidden" name="advance_search" value="1">
        <div class="tab-content">
            <div id="home" class="tab-pane fade in active">
                <h3>Search & location details</h3>

                <div class="form-group">
                    <label class="col-lg-2 control-label" for="search_for">Search For </label>
                    <div class="col-lg-2">
                        <input type="radio" name="search_for" value="to_rent" checked> Rooms for rent
                    </div>
                    <div class="col-lg-2">
                        <input type="radio" name="search_for" value="wanted"> Rooms Wanted
                    </div>
                    <div class="col-lg-2">
                        <input type="radio" name="search_for" value="buddy_up"> Buddy ups (?)
                    </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>

                <div id="room_to_rent_fields">
                    <div class="form-group">
                        <label class="col-lg-2 control-label" for="search_by">Search by </label>
                        <div class="col-md-3">
                            <input type="radio" name="search_by" value="location" checked> Location
                        </div>
                        <div class="col-md-3">
                            <input type="radio" name="search_by" value="travel_zone"> London travel zones
                        </div>
                        <div class="col-md-3">
                            <input type="radio" name="search_by" value="tube_line"> London tube line
                        </div>
                        <div class="col-md-3">
                            <input type="radio" name="search_by" value="commute_duration"> Commute duration
                        </div>
                    </div>
                    <br><br><br>
                    <div class="col-md-2"></div>
                    <div class="col-md-6" id="location_fields">
                    <div class="form-group">
                  <input type="text" class="form-control" name="area_postcode" placeholder="Area or postcode">
               </div>
               <div class="form-group">
                  <select class="form-control" name="distance">
                        <option value="0">This area only</option>
                        <option value="1">1 mile radius</option>
                        <option value="2">2 mile radius</option>
                        <option value="3">3 mile radius</option>
                        <option value="4">4 mile radius</option>
                        <option value="5">5 mile radius</option>
                        <option value="10">10 mile radius</option>
                        <option value="15">15 mile radius</option>
                        <option value="20">20 mile radius</option>
                        <option value="30">30 mile radius</option>
                        <option value="40">40 mile radius</option>
                    </select>
              </div>                          
                    </div>
                    <div class="col-md-6" id="travel_zone_fields" style="display:none;">
                        <label class="col-lg-2 control-label" for="uname"> </label>
                        <div class="col-md-10">
                            Between zone
                            <select class="form-control" name="zone_1">
                                <option value="1" selected>1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                            </select>
                            and
                            <select class="form-control" name="zone_2">
                                <option value="1">1</option>
                                <option value="2" selected>2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6" id="tube_line_fields" style="display:none;">
                        <select name="tube_line" class="form-control">
                            <option value="" selected>Choose Tube Line</option>
                            <option value="Bakerloo Line">Bakerloo Line</option>
                            <option value="Central Line">Central Line</option>
                            <option value="Circle Line">Circle Line</option>
                            <option value="District Line">District Line</option>
                            <option value="Docklands Light Railway">Docklands Light Railway</option>
                            <option value="Hammersmith and City Line">Hammersmith and City Line</option>
                            <option value="Jubilee Line">Jubilee Line</option>
                            <option value="London Overground">London Overground</option>
                            <option value="Metropolitan Line">Metropolitan Line</option>
                            <option value="Northern Line">Northern Line</option>
                            <option value="Piccadilly Line">Piccadilly Line</option>
                            <option value="Victoria Line">Victoria Line</option>
                        </select>
                    </div>
                    <div class="col-md-6" id="commute_duration_fields" style="display:none;">
                        <div class="form_inputs">
                            within
                            <span class="form_input form_select">
                                <select name="max_commute_time">
                                    <option value="10">10</option>
                                    <option value="20" selected="">20</option>
                                    <option value="30">30</option>
                                    <option value="40">40</option>
                                    <option value="50">50</option>
                                    <option value="60">60</option>
                                </select>
                            </span>
                            mins commute from
                            <span class="form_input form_select">
                                <select name="station_name">
                                <option value="" selected="">Choose station</option>
                                <option value="ABBEYROAD">Abbey Road</option>
                                <option value="ACTONCENTRAL">Acton Central</option>
                                <option value="ACTONTOWN">Acton Town</option>
                                <option value="ALDGATE">Aldgate</option>
                                <option value="ALDGATEEAST">Aldgate East</option>
                                <option value="ALLSAINTS">All Saints</option>
                                <option value="ALPERTON">Alperton</option>
                                <option value="AMERSHAM">Amersham</option>
                                <option value="ANERLEY">Anerley</option>
                                <option value="ANGEL">Angel</option>
                                <option value="ARCHWAY">Archway</option>
                                <option value="ARNOSGROVE">Arnos Grove</option>
                                <option value="BAKERSTREET">Baker Street</option>
                                <option value="BALHAM">Balham</option>
                                <option value="BANKMONUMENT">Bank / Monument</option>
                                <option value="BARBICAN">Barbican</option>
                                <option value="BARKING">Barking</option>
                                <option value="BARKINGSIDE">Barkingside</option>
                                <option value="BARONSCOURT">Barons Court</option>
                                <option value="BAYSWATER">Bayswater</option>
                                <option value="BECKTON">Beckton</option>
                                <option value="BECKTONPARK">Beckton Park</option>
                                <option value="BECONTREE">Becontree</option>
                                <option value="BELSIZEPARK">Belsize Park</option>
                                <option value="BERMONDSEY">Bermondsey</option>
                                <option value="BETHNALGREEN2">Bethnal Green</option>
                                <option value="BLACKFRIARS">Blackfriars</option>
                                <option value="BLACKHORSEROAD">Blackhorse Road</option>
                                <option value="BLACKWALL">Blackwall</option>
                                <option value="BONDSTREET">Bond Street</option>
                                <option value="BOROUGH">Borough</option>
                                <option value="BOSTONMANOR">Boston Manor</option>
                                <option value="BOUNDSGREEN">Bounds Green</option>
                                <option value="BOWCHURCH">Bow Church</option>
                                <option value="BOWROAD">Bow Road</option>
                                <option value="BRENTCROSS">Brent Cross</option>
                                <option value="BRIXTONSTATION">Brixton</option>
                                <option value="BROCKLEY">Brockley</option>
                                <option value="BROMLEYBYBOW">Bromley-by-Bow</option>
                                <option value="BRONDESBURY">Brondesbury</option>
                                <option value="BRONDESBURYPARK">Brondesbury Park</option>
                                <option value="BUCKHURSTHILL">Buckhurst Hill</option>
                                <option value="BURNTOAK">Burnt Oak</option>
                                <option value="BUSHEY">Bushey</option>
                                <option value="CALEDONIANROAD">Caledonian Road</option>
                                <option value="CALEDONIANROADBARNSBURY">Caledonian Road &amp; Barnsbury</option>
                                <option value="CAMDENROAD">Camden Road</option>
                                <option value="CAMDENTOWN">Camden Town</option>
                                <option value="CANADAWATER">Canada Water</option>
                                <option value="CANARYWHARF">Canary Wharf</option>
                                <option value="CANNINGTOWN">Canning Town</option>
                                <option value="CANNONSTREET">Cannon Street</option>
                                <option value="CANONBURY">Canonbury</option>
                                <option value="CANONSPARK">Canons Park</option>
                                <option value="CARPENDERSPARK">Carpenders Park</option>
                                <option value="CHALFONTLATIMER">Chalfont &amp; Latimer</option>
                                <option value="CHALKFARM">Chalk Farm</option>
                                <option value="CHANCERYLANE">Chancery Lane</option>
                                <option value="CHARINGCROSS">Charing Cross</option>
                                <option value="CHESHAM">Chesham</option>
                                <option value="CHIGWELL">Chigwell</option>
                                <option value="CHISWICKPARK">Chiswick Park</option>
                                <option value="CHORLEYWOOD">Chorleywood</option>
                                <option value="CLAPHAMCOMMON">Clapham Common</option>
                                <option value="CLAPHAMHIGHSTREET">Clapham High Street</option>
                                <option value="CLAPHAMJUNCTION">Clapham Junction</option>
                                <option value="CLAPHAMNORTH">Clapham North</option>
                                <option value="CLAPHAMSOUTH">Clapham South</option>
                                <option value="COCKFOSTERS">Cockfosters</option>
                                <option value="COLINDALE">Colindale</option>
                                <option value="COLLIERSWOOD">Colliers Wood</option>
                                <option value="COVENTGARDEN">Covent Garden</option>
                                <option value="CROSSHARBOUR">Crossharbour</option>
                                <option value="CROUCHHILL">Crouch Hill</option>
                                <option value="CROXLEY">Croxley</option>
                                <option value="CRYSTALPALACE">Crystal Palace</option>
                                <option value="CUSTOMHOUSE">Custom House</option>
                                <option value="CUTTYSARK">Cutty Sark</option>
                                <option value="CYPRUS">Cyprus</option>
                                <option value="DAGENHAMEAST">Dagenham East</option>
                                <option value="DAGENHAMHEATHWAY">Dagenham Heathway</option>
                                <option value="DALSTONJUNCTION">Dalston Junction</option>
                                <option value="DALSTONKINGSLAND">Dalston Kingsland</option>
                                <option value="DEBDEN">Debden</option>
                                <option value="DENMARKHILL">Denmark Hill</option>
                                <option value="DEPTFORDBRIDGE">Deptford Bridge</option>
                                <option value="DEVONSROAD">Devons Road</option>
                                <option value="DOLLISHILL">Dollis Hill</option>
                                <option value="EALINGBROADWAY">Ealing Broadway</option>
                                <option value="EALINGCOMMON">Ealing Common</option>
                                <option value="EARLSCOURT">Earl's Court</option>
                                <option value="EASTACTON">East Acton</option>
                                <option value="EASTFINCHLEY">East Finchley</option>
                                <option value="EASTHAM">East Ham</option>
                                <option value="EASTINDIA">East India</option>
                                <option value="EASTPUTNEY">East Putney</option>
                                <option value="EASTCOTE">Eastcote</option>
                                <option value="EDGWARE">Edgware</option>
                                <option value="EDGWAREROAD">Edgware Road</option>
                                <option value="ELEPHANTCASTLE">Elephant &amp; Castle</option>
                                <option value="ELMPARK">Elm Park</option>
                                <option value="ELVERSONROAD">Elverson Road</option>
                                <option value="EMBANKMENT">Embankment</option>
                                <option value="EPPING">Epping</option>
                                <option value="EUSTON">Euston</option>
                                <option value="EUSTONSQUARE">Euston Square</option>
                                <option value="FAIRLOP">Fairlop</option>
                                <option value="FARRINGDON">Farringdon</option>
                                <option value="FINCHLEYCENTRAL">Finchley Central</option>
                                <option value="FINCHLEYROAD">Finchley Road</option>
                                <option value="FINCHLEYROADFROGNAL">Finchley Road &amp; Frognal</option>
                                <option value="FINSBURYPARK">Finsbury Park</option>
                                <option value="FORESTHILL">Forest Hill</option>
                                <option value="FULHAMBROADWAY">Fulham Broadway</option>
                                <option value="GALLIONSREACH">Gallions Reach</option>
                                <option value="GANTSHILL">Gants Hill</option>
                                <option value="GLOUCESTERROAD">Gloucester Road</option>
                                <option value="GOLDERSGREEN">Golders Green</option>
                                <option value="GOLDHAWKROAD">Goldhawk Road</option>
                                <option value="GOODGESTREET">Goodge Street</option>
                                <option value="GOSPELOAK">Gospel Oak</option>
                                <option value="GRANGEHILL">Grange Hill</option>
                                <option value="GREATPORTLANDSTREET">Great Portland Street</option>
                                <option value="GREENPARK">Green Park</option>
                                <option value="GREENFORD">Greenford</option>
                                <option value="GUNNERSBURY">Gunnersbury</option>
                                <option value="HACKNEYCENTRAL">Hackney Central</option>
                                <option value="HACKNEYWICK">Hackney Wick</option>
                                <option value="HAGGERSTON">Haggerston</option>
                                <option value="HAINAULT">Hainault</option>
                                <option value="HAMMERSMITH">Hammersmith</option>
                                <option value="HAMPSTEAD">Hampstead</option>
                                <option value="HAMPSTEADHEATH">Hampstead Heath</option>
                                <option value="HANGERLANE">Hanger Lane</option>
                                <option value="HARLESDEN">Harlesden</option>
                                <option value="HARRINGAYGREENLANES">Harringay Green Lanes</option>
                                <option value="HARROWANDWEALDSTONE">Harrow and Wealdstone</option>
                                <option value="HARROWONTHEHILL">Harrow-on-the-Hill</option>
                                <option value="HATCHEND">Hatch End</option>
                                <option value="HATTONCROSS">Hatton Cross</option>
                                <option value="HEADSTONELANE">Headstone Lane</option>
                                <option value="HEATHROWTERMINAL4">Heathrow Terminal 4</option>
                                <option value="HEATHROWTERMINALS123">Heathrow Terminals 1,2,3</option>
                                <option value="HENDONCENTRAL">Hendon Central</option>
                                <option value="HERONQUAYS">Heron Quays</option>
                                <option value="HIGHBARNET">High Barnet</option>
                                <option value="HIGHSTREETKENSINGTON">High Street Kensington</option>
                                <option value="HIGHBURYISLINGTON">Highbury &amp; Islington</option>
                                <option value="HIGHGATE">Highgate</option>
                                <option value="HILLINGDON">Hillingdon</option>
                                <option value="HOLBORN">Holborn</option>
                                <option value="HOLLANDPARK">Holland Park</option>
                                <option value="HOLLOWAYROAD">Holloway Road</option>
                                <option value="HOMERTON">Homerton</option>
                                <option value="HONOROAKPARK">Honor Oak Park</option>
                                <option value="HORNCHURCH">Hornchurch</option>
                                <option value="HOUNSLOWCENTRAL">Hounslow Central</option>
                                <option value="HOUNSLOWEAST">Hounslow East</option>
                                <option value="HOUNSLOWWEST">Hounslow West</option>
                                <option value="HOXTON">Hoxton</option>
                                <option value="HYDEPARKCORNER">Hyde Park Corner</option>
                                <option value="ICKENHAM">Ickenham</option>
                                <option value="IMPERIALWHARF">Imperial Wharf</option>
                                <option value="ISLANDGARDENS">Island Gardens</option>
                                <option value="KENNINGTON">Kennington</option>
                                <option value="KENSALGREEN">Kensal Green</option>
                                <option value="KENSALRISE">Kensal Rise</option>
                                <option value="KENSINGTONOLYMPIA">Kensington (Olympia)</option>
                                <option value="KENTISHTOWN">Kentish Town</option>
                                <option value="KENTISHTOWNWEST">Kentish Town West</option>
                                <option value="KENTON">Kenton</option>
                                <option value="KEWGARDENS">Kew Gardens</option>
                                <option value="KILBURN">Kilburn</option>
                                <option value="KILBURNHIGHROAD">Kilburn High Road</option>
                                <option value="KILBURNPARK">Kilburn Park</option>
                                <option value="KINGGEORGEV">King George V</option>
                                <option value="KINGSCROSSSTPANCRAS">King's Cross St. Pancras</option>
                                <option value="KINGSBURY">Kingsbury</option>
                                <option value="KNIGHTSBRIDGE">Knightsbridge</option>
                                <option value="LADBROKEGROVE">Ladbroke Grove</option>
                                <option value="LAMBETHNORTH">Lambeth North</option>
                                <option value="LANCASTERGATE">Lancaster Gate</option>
                                <option value="LANGDONPARK">Langdon Park</option>
                                <option value="LATIMERROAD">Latimer Road</option>
                                <option value="LEICESTERSQUARE">Leicester Square</option>
                                <option value="LEWISHAM">Lewisham</option>
                                <option value="LEYTON">Leyton</option>
                                <option value="LEYTONMIDLANDROAD">Leyton Midland Road</option>
                                <option value="LEYTONSTONE">Leytonstone</option>
                                <option value="LEYTONSTONEHIGHROAD">Leytonstone High Road</option>
                                <option value="LIMEHOUSE">Limehouse</option>
                                <option value="LIVERPOOLSTREET">Liverpool Street</option>
                                <option value="LONDONBRIDGE">London Bridge</option>
                                <option value="LONDONCITYAIRPORT">London City Airport</option>
                                <option value="LOUGHTON">Loughton</option>
                                <option value="MAIDAVALE">Maida Vale</option>
                                <option value="MANORHOUSE">Manor House</option>
                                <option value="MANSIONHOUSE">Mansion House</option>
                                <option value="MARBLEARCH">Marble Arch</option>
                                <option value="MARYLEBONE">Marylebone</option>
                                <option value="MILEEND">Mile End</option>
                                <option value="MILLHILLEAST">Mill Hill East</option>
                                <option value="MOORPARK">Moor Park</option>
                                <option value="MOORGATE">Moorgate</option>
                                <option value="MORDEN">Morden</option>
                                <option value="MORNINGTONCRESCENT">Mornington Crescent</option>
                                <option value="MUDCHUTE">Mudchute</option>
                                <option value="NEASDEN">Neasden</option>
                                <option value="NEWCROSS">New Cross</option>
                                <option value="NEWCROSSGATE">New Cross Gate</option>
                                <option value="NEWBURYPARK">Newbury Park</option>
                                <option value="NORTHACTON">North Acton</option>
                                <option value="NORTHEALING">North Ealing</option>
                                <option value="NORTHGREENWICH">North Greenwich</option>
                                <option value="NORTHHARROW">North Harrow</option>
                                <option value="NORTHWEMBLEY">North Wembley</option>
                                <option value="NORTHFIELDS">Northfields</option>
                                <option value="NORTHOLT">Northolt</option>
                                <option value="NORTHWICKPARK">Northwick Park</option>
                                <option value="NORTHWOOD">Northwood</option>
                                <option value="NORTHWOODHILLS">Northwood Hills</option>
                                <option value="NORWOODJUNCTION">Norwood Junction</option>
                                <option value="NOTTINGHILLGATE">Notting Hill Gate</option>
                                <option value="OAKWOOD">Oakwood</option>
                                <option value="OLDSTREET">Old Street</option>
                                <option value="OSTERLEY">Osterley</option>
                                <option value="OVAL">Oval</option>
                                <option value="OXFORDCIRCUS">Oxford Circus</option>
                                <option value="PADDINGTON">Paddington</option>
                                <option value="PARKROYAL">Park Royal</option>
                                <option value="PARSONSGREEN">Parsons Green</option>
                                <option value="PECKHAMRYE">Peckham Rye</option>
                                <option value="PENGEWEST">Penge West</option>
                                <option value="PERIVALE">Perivale</option>
                                <option value="PICCADILLYCIRCUS">Piccadilly Circus</option>
                                <option value="PIMLICO">Pimlico</option>
                                <option value="PINNER">Pinner</option>
                                <option value="PLAISTOW">Plaistow</option>
                                <option value="PONTOONDOCK">Pontoon Dock</option>
                                <option value="POPLAR">Poplar</option>
                                <option value="PRESTONROAD">Preston Road</option>
                                <option value="PRINCEREGENT">Prince Regent</option>
                                <option value="PUDDINGMILLANE">Pudding Mill Lane</option>
                                <option value="PUTNEYBRIDGE">Putney Bridge</option>
                                <option value="QUEENSPARK">Queen's Park</option>
                                <option value="QUEENSROADPECKHAM">Queens Road Peckham</option>
                                <option value="QUEENSBURY">Queensbury</option>
                                <option value="QUEENSWAY">Queensway</option>
                                <option value="RAVENSCOURTPARK">Ravenscourt Park</option>
                                <option value="RAYNERSLANE">Rayners Lane</option>
                                <option value="REDBRIDGE">Redbridge</option>
                                <option value="REGENTSPARK">Regent's Park</option>
                                <option value="RICHMOND">Richmond</option>
                                <option value="RICKMANSWORTH">Rickmansworth</option>
                                <option value="RODINGVALLEY">Roding Valley</option>
                                <option value="ROTHERHITHE">Rotherhithe</option>
                                <option value="ROYALALBERT">Royal Albert</option>
                                <option value="ROYALOAK">Royal Oak</option>
                                <option value="ROYALVICTORIA">Royal Victoria</option>
                                <option value="RUISLIP">Ruislip</option>
                                <option value="RUISLIPGARDENS">Ruislip Gardens</option>
                                <option value="RUISLIPMANOR">Ruislip Manor</option>
                                <option value="RUSSELLSQUARE">Russell Square</option>
                                <option value="SEVENSISTERS">Seven Sisters</option>
                                <option value="SHADWELL">Shadwell</option>
                                <option value="SHEPHERDSBUSH">Shepherd's Bush</option>
                                <option value="SHEPHERDSBUSH2">Shepherd's Bush Market</option>
                                <option value="SHOREDITCH">Shoreditch</option>
                                <option value="SLOANESQUARE">Sloane Square</option>
                                <option value="SNARESBROOK">Snaresbrook</option>
                                <option value="SOUTHACTON">South Acton</option>
                                <option value="SOUTHEALING">South Ealing</option>
                                <option value="SOUTHHAMPSTEAD">South Hampstead</option>
                                <option value="SOUTHHARROW">South Harrow</option>
                                <option value="SOUTHKENSINGTON">South Kensington</option>
                                <option value="SOUTHKENTON">South Kenton</option>
                                <option value="SOUTHQUAY">South Quay</option>
                                <option value="SOUTHRUISLIP">South Ruislip</option>
                                <option value="SOUTHTOTTENHAM">South Tottenham</option>
                                <option value="SOUTHWIMBLEDON">South Wimbledon</option>
                                <option value="SOUTHWOODFORD">South Woodford</option>
                                <option value="SOUTHFIELDS">Southfields</option>
                                <option value="SOUTHGATESTATION">Southgate</option>
                                <option value="SOUTHWARK">Southwark</option>
                                <option value="STJAMESSPARK">St. James's Park</option>
                                <option value="STJOHNSWOOD">St. John's Wood</option>
                                <option value="STPAULS">St. Paul's</option>
                                <option value="STAMFORDBROOK">Stamford Brook</option>
                                <option value="STANMORE">Stanmore</option>
                                <option value="STARLANE">Star Lane</option>
                                <option value="STEPNEYGREEN">Stepney Green</option>
                                <option value="STOCKWELL">Stockwell</option>
                                <option value="STONEBRIDGEPARK">Stonebridge Park</option>
                                <option value="STRATFORD">Stratford</option>
                                <option value="STRATFORDHIGHSTREET">Stratford High Street</option>
                                <option value="STRATFORDINTERNATIONAL">Stratford International</option>
                                <option value="SUDBURYHILL">Sudbury Hill</option>
                                <option value="SUDBURYTOWN">Sudbury Town</option>
                                <option value="SURREYQUAYS">Surrey Quays</option>
                                <option value="SWISSCOTTAGE">Swiss Cottage</option>
                                <option value="SYDENHAM">Sydenham</option>
                                <option value="TEMPLE">Temple</option>
                                <option value="THEYDONBOIS">Theydon Bois</option>
                                <option value="TOOTINGBEC">Tooting Bec</option>
                                <option value="TOOTINGBROADWAY">Tooting Broadway</option>
                                <option value="TOTTENHAMCOURTROAD">Tottenham Court Road</option>
                                <option value="TOTTENHAMHALE">Tottenham Hale</option>
                                <option value="TOTTERIDGEWHETSTONE">Totteridge &amp; Whetstone</option>
                                <option value="TOWERGATEWAY">Tower Gateway</option>
                                <option value="TOWERHILL">Tower Hill</option>
                                <option value="TUFNELLPARK">Tufnell Park</option>
                                <option value="TURNHAMGREEN">Turnham Green</option>
                                <option value="TURNPIKELANE">Turnpike Lane</option>
                                <option value="UPMINSTER">Upminster</option>
                                <option value="UPMINSTERBRIDGE">Upminster Bridge</option>
                                <option value="UPNEY">Upney</option>
                                <option value="UPPERHOLLOWAY">Upper olloway</option>
                                <option value="UPTONPARK">Upton Park</option>
                                <option value="UXBRIDGE">Uxbridge</option>
                                <option value="VAUXHALL">Vauxhall</option>
                                <option value="VICTORIA">Victoria</option>
                                <option value="WALTHAMSTOWCENTRAL">Walthamstow Central</option>
                                <option value="WALTHAMSTOWQUEENSROAD">Walthamstow Queen's Road</option>
                                <option value="WANDSWORTHROAD">Wandsworth Road</option>
                                <option value="WANSTEAD">Wanstead</option>
                                <option value="WANSTEADPARK">Wanstead Park</option>
                                <option value="WAPPING">Wapping</option>
                                <option value="WARRENSTREET">Warren Street</option>
                                <option value="WARWICKAVENUE">Warwick Avenue</option>
                                <option value="WATERLOO">Waterloo</option>
                                <option value="WATFORD">Watford</option>
                                <option value="WATFORDHIGHSTREET">Watford High Street</option>
                                <option value="WATFORDJUNCTION">Watford Junction</option>
                                <option value="WEMBLEYCENTRAL">Wembley Central</option>
                                <option value="WEMBLEYPARK">Wembley Park</option>
                                <option value="WESTACTON">West Acton</option>
                                <option value="WESTBROMPTON">West Brompton</option>
                                <option value="WESTCROYDON">West Croydon</option>
                                <option value="WESTFINCHLEY">West Finchley</option>
                                <option value="WESTHAM">West Ham</option>
                                <option value="WESTHAMPSTEAD">West Hampstead</option>
                                <option value="WESTHARROW">West Harrow</option>
                                <option value="WESTINDIAQUAY">West India Quay</option>
                                <option value="WESTKENSINGTON">West Kensington</option>
                                <option value="WESTRUISLIP">West Ruislip</option>
                                <option value="WESTSILVERTOWN">West Silvertown</option>
                                <option value="WESTBOURNEPARK">Westbourne Park</option>
                                <option value="WESTFERRY">Westferry</option>
                                <option value="WESTMINSTER">Westminster</option>
                                <option value="WHITECITY">White City</option>
                                <option value="WHITECHAPEL">Whitechapel</option>
                                <option value="WILLESDENGREEN">Willesden Green</option>
                                <option value="WILLESDENJUNCTION">Willesden Junction</option>
                                <option value="WIMBLEDONMAIN">Wimbledon</option>
                                <option value="WIMBLEDONPARK">Wimbledon Park</option>
                                <option value="WOODGREEN">Wood Green</option>
                                <option value="WOODFORD">Woodford</option>
                                <option value="WOODGRANGEPARK">Woodgrange Park</option>
                                <option value="WOODSIDEPARK">Woodside Park</option>
                                <option value="WOOLWICHARSENAL">Woolwich Arsenal</option>
                                </select>
                            </span>
                            station
                        </div>
                    </div>

                    <div class="clearfix" style="height: 30px;clear: both;"></div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label" for="uname">Include </label>
                        <div class="col-lg-4">
                            <input type="checkbox" name="just_rooms" value="1" id="buddyup"> Rooms in existing flat/house shares
                        </div>
                        <div class="col-lg-4">
                            <input type="checkbox" name="one_beds" value="1" id="buddyup"> Studio flats and 1 bed flats
                        </div>
                        <div class="col-lg-4">
                            <input type="checkbox" name="whole_properties" value="1" id="buddyup"> Whole properties (suitable for buddying up)
                        </div>
                    </div>
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                </div>
                <div id="other_search_fields" style="display: none">
                    <div class="col-md-2"></div>
                    <div class="col-md-6" id="location_fields">
                        <input type="text" class="form-control" name="area_postcode2" placeholder="Area or postcode">
                    </div>
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                </div>

                <h3>Property preferences</h3>

                <div class="form-group">
                    <label class="col-lg-2 control-label" for="uname">Rent &pound; </label>
                    <div class="col-lg-10">
                        <input type="number" class="" name="min_rent" placeholder="no min"> to
                        <input type="number" name="max_rent" placeholder="no max">
                        <input type="radio" name="per_week" value="per week"> pw
                        <input type="radio" name="per_week" checked value="per month"> pcm
                    </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>

                <div class="form-group">
                    <label class="col-lg-2 control-label" for="uname">Bills included </label>
                    <div class="col-lg-10">
                        <input type="checkbox" name="bills_included" value="1"> Only show rooms with all bills included in the rent
                    </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>

                <div class="form-group">
                    <label class="col-lg-2 control-label" for="uname">Rooms for </label>
                    <div class="col-lg-2">
                        <input type="radio" name="gender" value="Male"> Males
                    </div>
                    <div class="col-lg-2">
                        <input type="radio" name="gender" value="Female"> Females
                    </div>
                    <div class="col-lg-2">
                        <input type="radio" name="gender" value="Couple"> Couples
                    </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>

                <div class="form-group">
                    <label class="col-lg-2 control-label" for="uname">Rooms Sizes </label>
                    <div class="col-lg-2">
                        <input type="radio" name="room_size" value="Double"> Double
                    </div>
                    <div class="col-lg-2">
                        <input type="radio" name="room_size" value="Single"> Single
                    </div>
                    <div class="col-lg-2">
                        <input type="radio" name="room_size" value="dm"> Don't mind
                    </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>

                <div class="form-group">
                    <label class="col-lg-2 control-label" for="uname">En suite </label>
                    <div class="col-lg-2">
                        <input type="radio" name="en_suite" value="yes"> Yes
                    </div>
                    <div class="col-lg-2">
                        <input type="radio" name="en_suite" value="dm"> Don't mind
                    </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>

                <div class="form-group">
                    <label class="col-lg-12 control-label" for="uname">Number of rooms i.e. More than one of you need a room? </label>
                    <div class="col-lg-2"></div>
                    <div class="col-lg-2">
                        <select name="no_of_rooms" class="form-control">
                            <option value="1" SELECTED >1 room for rent</option>
                            <option value="2">2+ rooms for rent</option>
                            <option value="3">3+ rooms for rent</option>
                        </select>
                    </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>

                <div class="form-group">
                    <label class="col-lg-2 control-label" for="uname">Shared living room? </label>
                    <div class="col-lg-10">
                        i.e. communal living space (rather than converted to another bedroom for instance)
                    </div>
                    <div class="col-lg-6">
                        <input type="checkbox" name="shared_living_room" value="1"> Shared living room required
                    </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>

                <div class="form-group">
                    <label class="col-lg-2 control-label" for="uname">Smoking allowed </label>
                    <div class="col-lg-2">
                        <input type="radio" name="smoking_allow" value="1"> Yes
                    </div>
                    <div class="col-lg-2">
                        <input type="radio" name="smoking_allow" value="0"> No
                    </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>

                <div class="form-group">
                    <label class="col-lg-2 control-label" for="uname">Length of stay </label>
                    <div class="col-lg-4">
                        Minimum:
                        <select name="min_stay">
                            <option value="0">No minimum</option>
                            <option value="1">1 Month</option>
                            <option value="2">2 Months</option>
                            <option value="3">3 Months</option>
                            <option value="4">4 Months</option>
                            <option value="5">5 Months</option>
                            <option value="6">6 Months</option>
                            <option value="7">7 Months</option>
                            <option value="8">8 Months</option>
                            <option value="9">9 Months</option>
                            <option value="10">10 Months</option>
                            <option value="11">11 Months</option>
                            <option value="12">1 Year</option>
                            <option value="15">1 Year 3 Months</option>
                            <option value="18">1 Year 6 Months</option>
                            <option value="21">1 Year 9 Months</option>
                            <option value="24">2 Years</option>
                        </select>
                        <br><br>
                        Maximum:
                        <select name="max_stay">
                            <option value="0">No maximum</option>
                            <option value="1">1 Month</option>
                            <option value="2">2 Months</option>
                            <option value="3">3 Months</option>
                            <option value="4">4 Months</option>
                            <option value="5">5 Months</option>
                            <option value="6">6 Months</option>
                            <option value="7">7 Months</option>
                            <option value="8">8 Months</option>
                            <option value="9">9 Months</option>
                            <option value="10">10 Months</option>
                            <option value="11">11 Months</option>
                            <option value="12">1 Year</option>
                            <option value="15">1 Year 3 Months</option>
                            <option value="18">1 Year 6 Months</option>
                            <option value="21">1 Year 9 Months</option>
                            <option value="24">2 Years</option>
                            <option value="36">3 Years</option>
                        </select>
                    </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>

                <div class="form-group">
                    <label class="col-lg-2 control-label" for="uname">To move in </label>
                    <div class="col-lg-4">
                        <input type="radio" name="available" value="anytime"> Anytime
                        <br>
                        <input type="radio" name="available" value="date">
                        <input type="date" name="available_from">
                        <br>
                        (We will search 21 days either side of this date)
                    </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>

                <h3>Sharing preferences</h3>

                <div class="clearfix" style="height: 10px;clear: both;"></div>

                <div class="form-group">
                    <label class="col-lg-2 control-label" for="uname">Occupation </label>
                    <div class="col-lg-2">
                        <input type="radio" name="pref_occupation" value="dm"> Don't mind
                    </div>
                    <div class="col-lg-2">
                        <input type="radio" name="pref_occupation" value="student"> Student
                    </div>
                    <div class="col-lg-2">
                        <input type="radio" name="pref_occupation" value="professional"> Professional
                    </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>

                <div class="form-group">
                    <label class="col-lg-2 control-label" for="uname">Gender </label>
                    <div class="col-lg-4">
                        <input type="radio" name="pref_gender" value="dm"> Don't mind
                        &nbsp;&nbsp;&nbsp;
                        <input type="radio" name="pref_gender" value="mixed"> Mixed
                        <br>
                        <input type="radio" name="pref_gender" value="Male"> Males only
                        &nbsp;&nbsp;&nbsp;
                        <input type="radio" name="pref_gender" value="Female"> Females only
                    </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>

                <div class="form-group">
                    <label class="col-lg-2 control-label" for="uname">Age range </label>
                    <div class="col-lg-10">
                        <input type="text" name="pref_min_age" placeholder="min"> to
                        &nbsp;
                        <input type="text" name="pref_max_age" placeholder="max">
                    </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>

                <div class="form-group">
                    <label class="col-lg-2 control-label" for="uname">Size of household
                    </label>
                    <div class="col-lg-10">
                        i.e. Total number of flatmates (inc you)<br>
                        Between
                        <select name="min_beds">
                            <option value="" SELECTED >-</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                        </select>
                        and
                        <select name="max_beds">
                            <option value="" SELECTED >-</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6+">6+</option>
                        </select>
                    </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>

                <div class="form-group">
                    <label class="col-lg-2 control-label" for="uname">Live with landlord? </label>
                    <div class="col-lg-10">
                        <input type="radio" name="pref_live_landlord" value="yes"> Yes
                        &nbsp;
                        <input type="radio" name="pref_live_landlord" value="no"> No
                        &nbsp;
                        <input type="radio" name="pref_live_landlord" value="dm"> Don't mind
                    </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>

                <div class="form-group">
                    <label class="col-lg-2 control-label" for="uname">Your preferences
                    </label>
                    <div class="col-lg-10">
                        <input type="checkbox" name="pref_orientation" value="1" placeholder="min">  Gay/lesbian shares only
                    </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>

                <h3>Other search options</h3>

                <div class="form-group">
                    <label class="col-lg-2 control-label" for="uname">Keywords </label>
                    <div class="col-lg-10">
                        <input type="text" name="keywords" placeholder="">
                    </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>

                <div class="form-group">
                    <label class="col-lg-2 control-label" for="uname">Miscellaneous </label>
                    <div class="col-lg-10">
                        <input type="checkbox" name="misc_parking" value="1"> Parking available<br>
                        <input type="checkbox" name="misc_pets" value="1"> Pets allowed<br>
                        <input type="checkbox" name="misc_photo" value="1"> Photo ads only<br>
                        <input type="checkbox" name="misc_veg" value="1"> Vegetarians preferred<br>
                        <input type="checkbox" name="misc_short_cons" value="1"> Short term lets<br>
                        <input type="checkbox" name="misc_dss" value="1"> DSS OK<br>
                        <input type="checkbox" name="misc_dis_access" value="1"> Disabled access
                    </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>

                <div class="form-group">
                    <label class="col-lg-2 control-label" for="uname">Days available </label>
                    <div class="col-lg-10">
                        <input type="radio" name="days_avail" value="dm" > Don't mind  <br>
                        <input type="radio" name="days_avail" value="Mon to Fri only"> Monday to Friday lets only<br>
                        <input type="radio" name="days_avail" value="7 days a week"> 7 days per week only (i.e. regular full time lets)<br>
                        <input type="radio" name="days_avail" value="Weekends only"> Weekends only
                    </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>

                <div class="form-group">
                    <label class="col-lg-2 control-label" for="uname">Ads posted by </label>
                    <div class="col-lg-10">
                        <input type="radio" name="advertiser_role" value="agent"> Agents only   <br>
                        <input type="radio" name="advertiser_role" value="private"> Private ads only (i.e. no agents) <br>
                        <input type="radio" name="advertiser_role" value="everyone"> Everyone
                    </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>
            </div>

            <div class="form-group">
                <div class="col-lg-10 col-lg-offset-2">
                    <input type="submit" class="btn btn-primary" value="Search" >
                    <br><br>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    jQuery().ready(function() {
        // validate form on keyup and submit
        /*var v = jQuery("#basicform").validate({
         rules: {
         step1_postcode: {
         required: true,
         minlength: 4
         },
         step1_email: {
         required: true,
         minlength: 4,
         email: true,
         maxlength: 30
         },

         upass2: {
         required: true,
         minlength: 6,
         equalTo: "#upass1"
         }

         },
         errorElement: "span",
         errorClass: "help-inline-error"
         });*/
        show_search_by_fields();
        function show_search_by_fields(){
            var field_ids = ['location_fields', 'travel_zone_fields', 'tube_line_fields', 'commute_duration_fields'];
            var val = $(this).val();
            var show_field_id = val + "_fields";

            for(var i=0; i<field_ids.length; i++){
                if(field_ids[i]==show_field_id){
                    $("#"+show_field_id).show();
                } else {
                    $("#"+field_ids[i]).hide();
                }
            }
        }

        show_search_for_fields();
        function show_search_for_fields(){
            if($(this).val()=="to_rent") {
                $("#room_to_rent_fields").show();
                $("#other_search_fields").hide();
            } else {
                $("#other_search_fields").show();
                $("#room_to_rent_fields").hide();
            }
        }

        $('input[type=radio][name=search_by]').change(show_search_by_fields);
        $('input[type=radio][name=search_for]').change(show_search_for_fields);

    });
</script>
