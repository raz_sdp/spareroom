<div id="container">

    <?php $ad_type="rent"; ?>
	<main id="spareroom" class="wrap" style="width: 75%;">
        <?php echo $this->element('my_account_menu'); ?>
		<div class="grid-12-4" id="mainheader">
			<div>
				<h1>Manage your ad</h1>
			</div>
			<div>&nbsp;</div>
		</div>      
		<div class="grid-12-4">
        	<div id="maincontent" style="max-width: 100%">
	            <div class="block_tabbed block_simple_tabbed block_manage_listing" id="block_tabbed">
                    <?php echo $this->element('edit_menu');?>

                    <div class="block block_simple" id="main_panel">
                        <?php
                            if($table=="rent") echo $this->element('edit_main_panel_rent');
                            elseif($table=="wanted") echo $this->element('edit_main_panel_wanted');
                            elseif($table=="property") echo $this->element('edit_main_panel_property');
                        ?>
                    </div>
                    <div class="block block_simple" id="photo_panel" style="display:none;">
                        <?php echo $this->element('edit_photo_panel');?>
	            	</div>
	            	<div class="block block_simple" id="video_panel" style="display:none">
                        <?php echo $this->element('edit_video_panel');?>
				    </div>
				    <div class="block block_simple" id="delete_panel" style="display:none;">
                        <?php echo $this->element('edit_delete_panel');?>
                    </div>
          		</div>
        	</div>   
    	</div>
	</main>

</div>

