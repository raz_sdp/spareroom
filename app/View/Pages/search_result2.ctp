<?php #AuthComponent::_setTrace($result); ?>
<div class="container">
<div class="serach_aside">
<aside style="width:21%">
    <div class="block block_panel search_filter">
        <div class="block_header">
            <h3>Search</h3>
        </div>  
        <form action="/searchEngine/search" method="post" class="block_content block_areas">
            <input name="nmsq_mode" type="hidden" value="normal">
            <input type="hidden" name="action" value="search">
    
            <div class="block_area block_area_first search_type">
                <ul>
                    <li>
                        <label title="Rooms for Rent in existing flat and house shares">
                            <input type="radio" name="flatshare_type" checked="" value="offered"> Rooms for rent
                        </label>
                    </li>
                    <li>
                        <label title="Potential flatmates and tenants seeking accommodation">
                            <input type="radio" name="flatshare_type" value="wanted" class="radiobuttons"> Rooms Wanted
                        </label>
                    </li>
                    <li>
                        <label title="Find others seeking accommodation to form a new house / flatshare with">
                            <input type="radio" name="flatshare_type" value="buddyup" class="radiobuttons"> Buddy ups
                        </label>        
                        <span class="tooltip">
                            <span class="tooltip_box">
                                <small class="tooltip_text">
                                    Find potential buddy ups to share this property with
                                </small>
                                <span class="tooltip_arrow">&nbsp;</span>
                            </span>
                            <span class="help tooltip_item">
                                (<a href="/content/info-faq/buddyups/" rel="nofollow">?</a>)
                            </span>
                        </span>
                    </li>
                </ul>
            </div>
        
            <div class="block_area search_where">
                <p class="block_area_label">
                    <label for="search_field">Where</label><br>
                    <input type="text" name="search" value="ng1" onfocus="if(this.value=='(e.g.Greenwich)'){this.value='';}" class="textbox" id="search_field">
                </p>
                <p>
                    <select name="miles_from_max" id="radius">
                        <option value="0">This area only</option>
                        <option value="1">1 mile radius</option>
                        <option value="2">2 mile radius</option>
                        <option value="3">3 mile radius</option>
                        <option value="4">4 mile radius</option>
                        <option value="5">5 mile radius</option>
                        <option value="10">10 mile radius</option>
                        <option value="15">15 mile radius</option>
                        <option value="20">20 mile radius</option>
                        <option value="30">30 mile radius</option>
                        <option value="40">40 mile radius</option>
                    </select>
                </p>
            </div>
        
            <div class="search_filtered_by">

            </div>
        
            <div class="block_area block_area_last search_go">
                <p class="search_advanced_link">
                    <a href="advanced_search" class="advanced_search">Advanced search</a>
                </p>
                <p class="buttons">
                    <input type="submit" value="Search again">
                </p>
            </div>
        </form>
    </div>

    <div id="narrow_by_search_options">
         <p>
             <a class="primary-standard" title="" href="advanced_search" rel="nofollow">Narrow search</a>
         </p>
    </div>
            
    <div class="block block_standard" id="findbox">
        <div class="block_header">
            <h3>Find an ad</h3>
        </div>
        <form action="/flatshare/propertyref.pl" method="GET" class="block_content">
            <p>
                <label for="findid">Advert ref#</label><br>
                <input type="text" class="textbox" name="advert_id" id="findid">
                <input type="Submit" value="Go">
            </p>
        </form>
    </div>
</aside>
</div>

<div class="search_body"style="width:77%; position:relative;top:-489px;margin-left:217px;">
    <div class="above_search_results">
        <div class="searching_options">
            <p id="results_header">
                Showing <strong> 1-10 </strong> of <strong>31</strong> results
            </p>
            <p class="sort_by">
                <label for="sort_by">Sort by : </label>
                <select name="sort_by" id="sort_by">
                    <option value="">Default sort order</option>
                    <option value="days_since_placed">Newest Ads</option>
                    <option value="last_updated">Last updated</option>
                    <option value="price_low_to_high">Price (lowest first)</option>
                    <option value="price_high_to_low">Price (highest first)</option>
                </select>

                <script type="text/javascript">
                    var sb = document.getElementById('sort_by');
                    sb.onchange = function() {
                        var o = this.options[this.selectedIndex].value;
                        document.location.href = '/flatshare/index.cgi?&search_id=265815730&offset=0&sort_by='+o;
                    };
                </script>
            </p>
        </div>

        <div class="searching_options_btn">
            <p class="serching_opt">
                <a href="/flatshare/index.cgi?&amp;search_id=265815730&amp;show_results=as+a+map" class="icon map findAflatHIDE"><span></span>Show results on a map</a>
                <a href="/flatshare/savesearch_confirm.pl?search_id=265815730" class="icon email" rel="nofollow"><span></span>Save search for alerts</a>
                <a href="advanced_search" class="icon magnify"><span></span>Refine search</a>
            </p>
        </div>
    </div>

    <ul class="listing-results">
        <li><article class="listing-bold">
            <header>
                <a href="/flatshare/flatshare_detail.pl?flatshare_id=380640&amp;search_id=265815730&amp;city_id=&amp;flatshare_type=offered&amp;search_results=%2Fflatshare%2F%3Fsearch_id%3D265815730%26&amp;">
                <strong>£50 <abbr title="per week">pw</abbr><!-- each --><!-- 50 --></strong>
                    <h1> 3 doubles , Nottingham (NG1)</h1>
                </a>
                <p class="details">
                    <em>Available Now</em>
                    <em class="listing-price"></em>
                </p>
            </header>
            <figure>
                 <a href="/flatshare/flatshare_detail.pl?flatshare_id=380640&amp;search_id=265815730&amp;city_id=&amp;flatshare_type=offered&amp;search_results=%2Fflatshare%2F%3Fsearch_id%3D265815730%26&amp;">
                    <img src="//photos2.spareroom.co.uk/images/flatshare/listings/square/21/58/2158882.jpg" alt="Main photo">
                   <p class="media-details">
                       <span><i class="fa fa-camera"></i> 2 </span>
                     <span class="no-video"><i class="fa fa-video-camera"></i> 0 </span>
                    </p> 
                 </a>
            </figure>
            <div class="listing-results-content">
               <h2>
                  <a href="/flatshare/flatshare_detail.pl?flatshare_id=380640&amp;search_id=265815730&amp;city_id=&amp;flatshare_type=offered&amp;search_results=%2Fflatshare%2F%3Fsearch_id%3D265815730%26&amp;" title="Advert details"> Rooms Available </a>
               </h2>
                 <p class="description ">
                   <a href="/flatshare/flatshare_detail.pl?flatshare_id=380640&amp;search_id=265815730&amp;city_id=&amp;flatshare_type=offered&amp;search_results=%2Fflatshare%2F%3Fsearch_id%3D265815730%26&amp;" title="Advert details">               Pls call rather than email...

                        Please read full advert...thanks in advance
          
                        ******SINGLE 35PW*****
                    ...</a>
                 </p>
            <footer>
               <span class="status"> Free to Contact </span>  
                  <span class="tooltip">
                    <span class="tooltip_item">
                       <a href="/flatshare/shortlist.pl?function=add&amp;flatshare_id=380640&amp;flatshare_type=offered&amp;search_results=%2Fflatshare%2F%3Fsearch_id%3D265815730%26"><i class="fa fa-star-o"></i> Save</a>
                  </span>
               <span class="tooltip_box">
                  <small class="tooltip_text">Add this to your 'saved ads' list for quick reference in future. You'll find your saved ad lists under the 'search' tab in the main menu    </small>
                  <span class="tooltip_arrow">&nbsp;</span>
                  </span>
                </span>
                    <a href="/flatshare/flatshare_detail.pl?flatshare_id=380640&amp;search_id=265815730&amp;city_id=&amp;flatshare_type=offered&amp;search_results=%2Fflatshare%2F%3Fsearch_id%3D265815730%26&amp;" title="Advert details" class="more"> More info</a>
            </footer>
        </div>
        <mark class="new">NEW</mark></article>
        </li>
    </ul>
</div>
</div>
