<div class="container">
	<h1>About Find me a Room</h1>
	<div class="row">
	<div class="col-md-8">
		<p class="text-justify">
			We're SpareRoom, the UK's busiest flatshare site. Every three minutes, someone finds a flatmate with us. That's a lot of people, meaning more choice for you - and more chance of finding flatmates you really click with.
		</p>
	</div>
	<div class="col-mod-4">
		<?php echo $this->Html->image('about-spareroom-minutes.png')?>  
	</div>
	</div>
	<div class="row">
		<div class="col-md-6">
		    <?php echo $this->Html->image('about-spareroom-sofa.png')?>  	      
		</div>
		<div class="col-md-6 about-life-share">
	        <h2>Life's better when you share it</h2>
	        <p>
	        Living with the right people beats living on your own any day. When you live with the right people, you don't just share your space - you share your life. 
	        </p>
	        <p>
	        They become more than your flatmates, more than the people ahead of you in the queue for the bathroom. They're your friends.  
	        </p>
	        <p>
	        SpareRoom is all about helping you find those people. The people who make sharing amazing.
	        </p>
		</div>
	</div>
</div>