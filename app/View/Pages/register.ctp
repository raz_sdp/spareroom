<div class="container">
    <h2>Register now for free</h2>

    <?php //echo $this->Form->create('User'); ?>
    <form class="form-horizontal" role="form" method="post" action="<?php echo $this->html->url('/users/register');?>" name="basicform" id="basicform">

        <div class="form-group">
            <label class="control-label col-sm-2" for="email">First Name</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="fname" name="data[User][first_name]" placeholder="Enter First Name">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-2" for="email">Last Name</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="lname" name="data[User][last_name]" placeholder="Enter Last Name">
            </div>
        </div>


        <div class="form-group">
            <label class="control-label col-sm-2" for="email">Email:</label>
            <div class="col-sm-10">
                <input type="email" class="form-control" id="email" name="data[User][email]" placeholder="Enter email">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-2" for="email">Confirm Email:</label>
            <div class="col-sm-10">
                <input type="email" class="form-control" id="cemail" name="data[User][email]"placeholder="Enter email Again">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-2" for="pwd">Password:</label>
            <div class="col-sm-10">
                <input type="password" class="form-control" id="password" name="data[User][simple_pwd]" placeholder="Enter password">
            </div>
        </div>


        <div class="form-group">
            <label class="control-label col-sm-2" for="pwd">Confirm Password:</label>
            <div class="col-sm-10">
                <input type="password" class="form-control" id="cpassword" name="data[User][simple_pwd]" placeholder="Enter password Again">
            </div>
        </div>


        <div class="form-group">
            <label class="control-label col-sm-2" for="email">Gender:</label>
            <div class="col-sm-10">
                <input type="radio"  id="gender" name="data[User][gender]" value="Male"> Male
                <input type="radio" id="gender" name="data[User][gender]" value="Female"> Female
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-2" for="tick">Tick those which apply to you:</label>
            <div class="col-sm-10">
                <input type="checkbox"  id="tick1" name="data[User][looking_for_flat]" value="I am looking for a flat or house share"> I am looking for a flat or house share<br>
                <input type="checkbox" id="tick2" name="data[User][have_flat]" value="I have a flat or house share"> I have a flat or house share<br>
                <input type="checkbox" id="tick3" name="data[User][new_share]" value="I'd like to find people to form a new share"> I'd like to find people to form a new share
            </div>
        </div>


        <div class="form-group">
            <label class="control-label col-sm-2" for="tick">Email options:</label>
            <div class="col-sm-10">
                <input type="checkbox"  id="email1" name="data[User][adverts]" value="Send me matching adverts"> Send me matching adverts<br>
                <input type="checkbox" id="email2" name="data[User][newsletter]" value="Send me occasional newsletters"> Send me occasional newsletters<br>

            </div>
        </div>


        <div class="form-group">
            <label class="control-label col-sm-2" for="email">Where did you hear about us?</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="hear" name="data[User][heard_where]" placeholder="Where did you hear about us?">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-2" for="email"></label>
            <div class="col-sm-10">
                By clicking the button below, I agree to the SpareRoom.co.uk <a href="#">terms</a> and <a href="privacy_policy.html">privacy policy</a>.
            </div>
        </div>


        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <input type="submit" class="btn btn-primary" value="Register" name="register" />
            </div>
        </div>
    </form>
</div>




<script type="text/javascript">
    jQuery().ready(function() {
        // validate form on keyup and submit
        var v = jQuery("#basicform").validate({
            rules: {
                email: {
                    required: true,
                    minlength: 5,
                    maxlength: 30
                },
                cemail: {
                    required: true,
                    minlength: 5,
                    email: true,
                    maxlength: 100,
                    equalTo: "#email"
                },
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 15
                },
                cpassword: {
                    required: true,
                    minlength: 6,
                    equalTo: "#password"
                }

            },
            errorElement: "span",
            errorClass: "help-inline-error"
        });
    });
</script>
