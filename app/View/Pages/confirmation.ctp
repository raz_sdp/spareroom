<div id="container">
	<main id="spareroom" class="wrap">
<div class="grid-12-4" id="mainheader"><div><h1>Advertise your room</h1></div><div>&nbsp;</div></div>	<div class="grid-12-4">
		<div>
			
						
							<h3 id="confirmationheader">Confirmation</h3>
						
						



	
	
		<p class="msg success">
			Thank you, your ad has been placed. Your ad ref is <strong class="listing_ref">4568312</strong>
			<br>
			You will receive email confirmation shortly.
			Adverts will usually be approved and appear in the search results within 90 minutes.
		</p>
		
		<!-- If they've clicked bold ad, auto redirect them to the bold upgrade page -->
		
			
		
		
		
			<!-- NOT JUST REGISTERED -->
		
	



	<script src="http://cdn-static.formisimo.com/tracking/js/tracking.js"></script>


	<script src="http://cdn-static.formisimo.com/tracking/js/conversion.js"></script>


						
							
								
									<div id="next_steps" class="asidecontent">
										<h3>Next steps...</h3>				
										<div class="grid-8">
											<div class="block block_prominent">
												<div class="block_content">
													<h2>Upgrade and rent your room faster</h2>
													<p>
														Upgrading makes your ad Bold. This gets it higher in the listings and means (on average) twice as many enquiries. It also makes your ad free for everyone to contact. 
													</p>
													<p>
														Your ad will go out shortly in email alerts to people looking in your area so, by upgrading now, you'll make sure everyone can contact you straight away.
													</p>
													<p>
<a class="primary-standard" title="" href="/flatshare/get_bold_advertising.pl?M_context=">Upgrade Now</a>
													</p>
													<p>
														<a href="/flatshare/get_bold_advertising.pl?M_context=">More benefits of upgrading »</a>
													</p>
												</div>
											</div>
											<div class="block block_standard">
												<div class="block_content">
													<img src="//images2.spareroom.co.uk/img/spareroom/v4/icons/upload_photos.gif" align="right">
													<h2>Add photos</h2>
													<h4>Improve your ad with photos</h4>
													<p>
														On average adverts with photos get over 50% more views. You can upload up to 5 photos free of charge. Why not take photos of the Flat, yourself
														
															
																
															
														
														
														and the local area
													</p>
													<p><a class="secondary-standard" title="" href="/flatshare/upload-photos.pl?flatshare_id=4568312&amp;flatshare_type=offered">Add a photo now</a>													</p>
												</div>
											</div>
										</div>
									</div>
								
							
						
						
			
				
					
				
			
		</div>
		<aside>
			<!--<h4>Help topics</h4>-->
			
							
			
			
			
			
			
			
			<!-- <div class="block block_simple need_help">
	<div class="block_header">
		<h3>Need any help?</h3>
	</div>
	<div class="block_content">
		<p class="need_help_out_of_office">
	If you need any help, 
	
		our  award winning  customer services team are here from 9am until 9pm Monday - Friday and 10am until 8pm at weekends. Please call us on:
	
	
</p>
	
<p class="need_help_details">
	<strong class="need_help_tel">
		01625 666 750
	</strong>
	or <a href="/contact.pl" rel="nofollow" class="need_help_email">Contact us by email</a>
</p>

		<img src="http://images2.spareroom.co.uk/img/spareroom/v4/staff/gemma_4_small.png" class="need_help_photo">

	</div> -->
</div><div id="live_chat_button_1" class="live_chat_button"><a href="#" onclick="if (LC_API.mobile_is_detected()) { LC_API.open_mobile_window({skill:1}); } else if (LC_API.embedded_chat_supported() &amp;&amp; LC_API.embedded_chat_enabled()) { LC_API.show_full_view({skill:1}); } else { LC_Invite.windowRef = window.open('https://secure.livechatinc.com/licence/1040610/open_chat.cgi?groups=1&amp;session_id=S1450150396.d4b7838e4b#http%3A%2F%2Fwww.spareroom.co.uk%2Fflatshare%2Foffered-advert3.pl','Chat_1040610','width=530,height=520,resizable=yes,scrollbars=no'); };return false"><span class="ir live_chat_offline"></span></a></div>

		</aside>
	</div>
            </main>
</div>