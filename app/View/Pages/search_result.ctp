<?php #AuthComponent::_setTrace($this->request->data); ?>


<div class="container">

<div class="col-md-3 serach_aside" style="margin-top: 20px;">
    <aside style="width:21%">
        <?php echo $this->element('search_aside'); ?>
    </aside>
</div>

<div class="col-md-9 search_body">
    <div class="above_search_results">
        <div class="searching_options" style="padding:13px 10px 13px 22px">
            <span id="results_header" class="col-md-8">
                Showing <strong> <?php echo $offset+1;?>-<?php echo $offset+$limit;?> </strong> of <strong><?php echo $count;?></strong> results
                <?php
                    $prev_offset = $offset - $limit;
                    $next_offset = $offset + $limit;
                    $prev_link = "search/1/" . $limit . "/" . $prev_offset;
                    $next_link = "search/1/" . $limit . "/" . $next_offset;
                ?>
                <?php if($prev_offset>=0) echo $this->Html->link('Prev',$prev_link);?>
                <?php if($next_offset<=$count-$limit) echo $this->Html->link('Next',$next_link);?>
            </span>
            <span class="sort_by">
                <label for="sort_by">Sort by : </label>
                <select name="sort_by" id="sort_by">
                    <option value="">Default sort order</option>
                    <option value="created">Newest Ads</option>
                    <option value="modified">Last updated</option>
                    <option value="low_price">Price (lowest first)</option>
                    <option value="high_price">Price (highest first)</option>
                </select>

                <script type="text/javascript">
                    var sb = document.getElementById('sort_by');
                    sb.onchange = function() {
                        var o = this.options[this.selectedIndex].value;
                        document.location.href = '<?php echo $this->webroot;?>searchEngine/search/1/<?php echo $limit;?>/<?php echo $offset;?>/' + o;
                    };
                </script>
            </span>
        </div>

        <div class="searching_options_btn">
            <span class="serching_opt">
                <?php if(count($result)>0) {
                //print_r($result);
                //die;
                $i=0;
                 foreach($result as $key => $item) {
                      $parameter[$i]['lat'] = $item[$flag]['geo_loc']['lat'];
                      $parameter[$i]['lng'] = $item[$flag]['geo_loc']['lng'];
                      //$parameter[$i]['lng'] = -76;
                      $parameter[$i]['title']= $item[$flag]['title'];
                      $parameter[$i]['photo_1']=$item[$flag]['photo_1'];
                      $parameter[$i]['description']=utf8_encode($item[$flag]['description']);
                      $parameter[$i]['rent']=$item[$flag]['rent'];
                      $i++;
                 }
                 $url_data= urlencode(json_encode($parameter));
                 /*print_r($parameter);
                 $url_data= urlencode(json_encode($parameter));
                 die;*/
                ?>
                <a href="<?php echo $this->webroot;?>searchEngine/show_room_map?parameter=<?php echo $url_data;?>" id="show_map_changed" class="icon map findAflatHIDE">Show results on a map</a>
                <?php } ?>
                <a href="<?php echo $this->webroot?>searchEngine/save_search" class="icon email" rel="nofollow"><span></span>Save search for alerts</a>
                <a href="<?php echo $this->webroot;?>advanced_search" class="icon magnify"><span></span>Refine search</a>
            </p>
        </div>
    </div>

    <ul class="listing-results">
    <?php if(count($result)>0) { ?>
    <?php foreach ($result as $item) { ?>
        <li>
            <article class="listing-bold">
                <header>
                    <?php $full_ad_link = $this->webroot."searchEngine/full_ad/".$flag."/".$item[$flag]['id']; ?>
                    <a href="<?php echo $full_ad_link;?>">
                        <strong>
                            £<?php echo $item[$flag]['rent'];?>
                            <abbr title="<?php echo $item[$flag]['per_week'];?>">
                                <?php echo $item[$flag]['per_week'];?>
                            </abbr>
                        </strong>
                        <h1>
                        <?php
                        if($flag=='rent_ads') {
                            echo $item[$flag]['room_size'];
                            echo $item[$flag]['street_name'];
                            echo "(". $item[$flag]['postcode'] .")";
                        } else {
                            echo $item[$flag]['genders'];
                            echo $item[$flag]['looking_for'];
                            echo "(". $item[$flag]['area'] .")";
                        }
                        ?>
                        </h1>
                    </a>
                    <p class="details">
                        <?php
                            if(date("Y-m-d")>=$item[$flag]['available_from'])
                                echo "<em>Available Now</em>";
                            else
                                echo "<em>Available from: ". $item[$flag]['available_from'] ."</em>";

                            if($flag=='rent_ads') {
                                if($item[$flag]['bills_inc']=="Yes")
                                    echo "<em class=\"listing-price\">Bills inc.</em>";
                            }
                        ?>
                    </p>
                </header>
                <figure>
                <?php
                #AuthComponent::_setTrace($item[$flag]['photo_1']);
                if(!empty($item[$flag]['photo_1'])){                    
                    $img_url = $this->Html->url('/files/add_images/'.$item[$flag]['photo_1'], true);
                }else{
                    $img_url = $this->Html->url('/files/add_images/default_search_img.jpg', true);
                }

                $photo_count = 0;
                $video_count = 0;

                if(!empty($item[$flag]['photo_1']))
                    $photo_count ++ ;
                if(!empty($item[$flag]['photo_2']))
                    $photo_count ++ ;
                if(!empty($item[$flag]['photo_3']))
                    $photo_count ++ ;
                if(!empty($item[$flag]['photo_4']))
                    $photo_count ++ ;
                if(!empty($item[$flag]['photo_5']))
                    $photo_count ++ ;

                if(!empty($item[$flag]['video_link']))
                    $video_count ++ ;

                ?>
                    <a href="<?php echo $full_ad_link;?>">
                        <img src="<?php echo $img_url;?>" style="height: 150px;" alt="Main photo">
                        <p class="media-details">
                            <span><i class="fa fa-camera"></i> <?php echo $photo_count;?> </span>
                            <span class="no-video"><i class="fa fa-video-camera"></i> <?php echo $video_count;?> </span>
                        </p>
                    </a>
                </figure>
                <div class="listing-results-content">
                    <h2>
                        <a href="<?php echo $full_ad_link;?>" title="Advert details">
                            <?php echo $item[$flag]['title']; ?>
                        </a>
                    </h2>
                    <p class="description ">
                        <a href="<?php echo $full_ad_link;?>" title="Advert details">
                            <?php echo AppHelper::trimWords(utf8_encode($item[$flag]['description']),50)."..."; ?>
                        </a>
                    </p>
                    <footer>
                        <span class="status"> Free to Contact </span>
                        <span class="tooltip">
                            <span class="tooltip_item">
                                <a href="">
                                    <i class="fa fa-star-o"></i> Save
                                </a>
                            </span>
                            <span class="tooltip_box">
                                <small class="tooltip_text">Add this to your 'saved ads' list for quick reference in future. You'll find your saved ad lists under the 'search' tab in the main menu    </small>
                                <span class="tooltip_arrow">&nbsp;</span>
                            </span>
                        </span>
                        <a href="<?php echo $full_ad_link;?>" title="Advert details" class="more">
                        More info
                        </a>
                    </footer>
                </div>
                <?php if(date("Y-m-d",strtotime($item[$flag]['created'])) == date("Y-m-d")) { ?>
                <mark class="new-today">New Today</mark>
                <?php } ?>
            </article>
        </li>
    <?php } ?>
    <?php } else { ?>
        <li>No result found. Please <a href="search">search again.</a></li>
    <?php } ?>
    </ul>
</div>

<div class="map-overlay" style="display: none">
    <div class="map-close-btn" id="map-close-btn"><h2>(X) Close</h2></div>
    <div id="map-canvas" class="map-canvas"></div>
</div>
<script>
    $(document).ready(function(){
        var locations = new Array();
        <?php foreach($result as $key => $item) { ?>
            var lat = "<?php echo $item[$flag]['geo_loc']['lat'];?>";
            var lng = "<?php echo $item[$flag]['geo_loc']['lng'];?>";
            lat = Number(lat);
            lng = Number(lng);
            locations[<?php echo $key;?>] = [lat, lng];
        <?php } ?>
        console.log(locations);
        showPositionOnMap(locations);
        $('#map-close-btn').click(function(){
            $('.map-overlay').hide();
            showPositionOnMap(locations);
        });
        $('#show_map').click(function(e){
            e.preventDefault();
            $('.map-overlay').show();
            showPositionOnMap(locations);
        });
    });

    function showPositionOnMap(locations) {
        // avg lat lng as center. temporary. may be changed.
        var marker, i;
        var centerLat = 0, centerLng = 0;
        for (i = 0; i < locations.length; i++) {
            centerLat += Number(locations[i][0]);
            centerLng += Number(locations[i][1]);
        }
        centerLat /= locations.length;
        centerLng /= locations.length;
        console.log(centerLat);
        //returnLatLng();
        var mapOptions = {
            zoom: 8,
            center: new google.maps.LatLng( centerLat, centerLng )
        };

        var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        google.maps.event.trigger(map, 'resize');
        var image;
        //image = '../img/pin.png';

        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                map: map,
                animation: google.maps.Animation.DROP,
                position: new google.maps.LatLng( locations[i][0], locations[i][1] ),
                icon: image
            });
            //alert("lat: "+locations[i][0]+" \n lng: "+locations[i][1]);
            google.maps.event.addListener(window, 'load', (function(marker, i) {})(marker, i));

        }
    }
</script>
</div>