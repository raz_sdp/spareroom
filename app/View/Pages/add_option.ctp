<div class="container">

	<h2 style="text-align:justify;">Advertise your Room to Let</h2>
	<hr>
	<p>What are your options?</p>

<div class="col-md-6" style="background:#2980b9;color: #fff;">

<div class="col-md-12" style="background:#3498db">

</div>

<div class="block_content">
				<ul class="bulletlist points">
					<li>
						Advertise your room on the UK's busiest flatshare site.
					</li>
					<li>Inclusion in daily email alerts to room seekers.</li>
					<li>Receive email alerts listing room seekers looking in your area.</li>
					<li>All the benefits of SpareRoom's unique safety and scam protection features.</li>
					<li>Free access to SpareRoom's market-leading customer service support centre.</li>
					
				</ul>
				
				<p class="place_an_ad_now">
<a class="btn btn-primary"title="Place an ad now" href="whole_property">Place an add now</a>					
				</p>
				
			</div>


</div>

<div class="col-md-6" style="background:#8e44ad;color: #fff;">


<div class="block_content">
				<ul class="bulletlist points">
					<li>
						Advertise your room on the UK's busiest flatshare site.
					</li>
					<li>Inclusion in daily email alerts to room seekers.</li>
					<li>Receive email alerts listing room seekers looking in your area.</li>
					<li>All the benefits of SpareRoom's unique safety and scam protection features.</li>
					<li>Free access to SpareRoom's market-leading customer service support centre.</li>
					
				</ul>
				
				<p class="place_an_ad_now">
<a class="btn btn-primary" title="Place an ad now" href="room_to_rent">Place an add now</a>				
				</p>
				
			</div>


</div>


</div>



		</div>
	</div>