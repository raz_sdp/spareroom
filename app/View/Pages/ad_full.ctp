<?php
    $float_js = $this->Html->url('/floatbox/floatbox.js', true);
    $float_css = $this->Html->url('/floatbox/floatbox.css', true);
?>
<script src="<?php echo $float_js;?>"></script>
<link rel="stylesheet" href="<?php echo $float_css;?>">

<div class="container">
<i class="fa fa-arrow-left" style="color:#3480db"></i>
<a href="<?php echo $this->webroot;?>searchEngine/search/1">Back to search results</a>
<main id="spareroom" class="wrap" style="padding: 0px">

<div class="grid-12" id="mainheader">
    <div id="listing_heading">
        <p id="back_to_search_results">
            <!--<a href="/flatshare/?search_id=265832575&amp;" class="back_link">Back to search results</a>-->
	    </p>
        <h1><?php echo $result[$flag]['title'];?></h1>
    </div>
</div>
  
<div class="layoutrow" style="margin-bottom: 20px;">
    <div class="free_listing">
        <div class="block_tabbed block_simple_tabbed fullwidth_pagetabs">
            <?php echo $this->element('full_ad_menu'); ?>

            <?php echo $this->element('ad_details_panel'); ?>
            <?php echo $this->element('ad_details_mail_panel'); ?>
            <?php echo $this->element('ad_details_phone_panel'); ?>
        </div><!-- end /.fullwidth_pagetabs -->
    </div> <!-- end .free_listing -->
</div>


<div class="block block_simple report_listing asidecontent">
    <div class="block_content">
        <h5>Report this ad</h5>
        <div class="cols cols2">
            <div class="col col_first" style="float: left">
                <p>
                    We have staff moderating our ads 7 days per week, to keep quality high.
                    Please help us in our job and <a href="#" rel="nofollow">let us know</a>
                    if there is any problem with this ad, for example:
                </p>
            </div>
            <div class="col col_last" style="float: left">
                <ul class="bulletlist">
                    <li>The phone number doesn't work</li>
                    <li>The description is misleading</li>
                    <li>The ad is generic rather than for a specific available room</li>
                    <li>The advertiser is not a live in landlord</li>
                </ul>
            </div>
        </div>
    </div>
</div><!-- end /.report_listing -->
</main>

</div>