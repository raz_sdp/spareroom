<form method="post" name="search_form_home" id="search_form_home" action="searchEngine/search">
    <input type="hidden" name="search_by" value="location">
    <input type="hidden" name="advance_search" value="0">
    <input type="hidden" name="area_postcode">
    <input type="hidden" name="search_for" value="to_rent">
</form>


<section class="popular-destination" style="margin-bottom: 30px">
    <div class="container">
        <h3>Browse for ads in different places</h3>
        <div class="row" id="location_links">
            <div class="col-md-2 col-sm-4 col-xs-6">
                <ul>
                    <li><a href="#" class="blue">London</a></li>
                    <li><a href="#" class="blue">New York</a></li>
                    <li><a href="#" class="blue">Amsterdam</a></li>
                    <li><a href="#" class="blue">Paris</a></li>
                    <li><a href="#" class="">Berlin</a></li>
                    <li><a href="#" class="">Barcelona</a></li>
                    <li><a href="#" class="">Rome</a></li>
                    <li><a href="#" class="">Edinburgh</a></li>
                    <li><a href="#" class="">Vienna</a></li>
                    <li><a href="#" class="">Florence</a></li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
                <ul>
                    <li><a href="#" class="">Marrakech</a></li>
                    <li><a href="#" class="">Prague</a></li>
                    <li><a href="#" class="">Lisbon</a></li>
                    <li><a href="#" class="">Istanbul</a></li>
                    <li><a href="#" class="">Krakow</a></li>
                    <li><a href="#" class="">Madrid</a></li>
                    <li><a href="#" class="">Venice</a></li>
                    <li><a href="#" class="">Nice</a></li>
                    <li><a href="#" class="">Budapest</a></li>
                    <li><a href="#" class="">Cannes</a></li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
                <ul>
                    <li><a href="#" class="">Ibiza</a></li>
                    <li><a href="#" class="">Tuscany</a></li>
                    <li><a href="#" class="">Florida</a></li>
                    <li><a href="#" class="">Miami</a></li>
                    <li><a href="#" class="">Marseille</a></li>
                    <li><a href="#" class="">Marbella</a></li>
                    <li><a href="#" class="">Benidorm</a></li>
                    <li><a href="#" class="">Sardinia</a></li>
                    <li><a href="#" class="">Tenerife</a></li>
                    <li><a href="#" class="">Majorca</a></li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
                <ul>
                    <li><a href="#" class="">Menorca</a></li>
                    <li><a href="#" class="">Algarve</a></li>
                    <li><a href="#" class="">Dubrovnik</a></li>
                    <li><a href="#" class="">Crete</a></li>
                    <li><a href="#" class="">Corsica</a></li>
                    <li><a href="#" class="">Malaga</a></li>
                    <li><a href="#" class="">Valencia</a></li>
                    <li><a href="#" class="">Alicante</a></li>
                    <li><a href="#" class="">Sicily</a></li>
                    <li><a href="#" class="">Nerja</a></li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
                <ul>
                    <li><a href="#" class="">Kent</a></li>
                    <li><a href="#" class="">Lake District</a></li>
                    <li><a href="#" class="">Devon</a></li>
                    <li><a href="#" class="">Cumbria</a></li>
                    <li><a href="#" class="">Glasgow</a></li>
                    <li><a href="#" class="">Torquay</a></li>
                    <li><a href="#" class="">Penzance</a></li>
                    <li><a href="#" class="">England</a></li>
                    <li><a href="#" class="">Scotland</a></li>
                    <li><a href="#" class="">Wales</a></li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
                <ul>
                    <li><a href="#" class="">Cornwall</a></li>
                    <li><a href="#" class="">Newquay</a></li>
                    <li><a href="#" class="">Norfolk</a></li>
                    <li><a href="#" class="">Falmouth</a></li>
                    <li><a href="#" class="">Looe</a></li>
                </ul>
            </div>
        </div>

        <script>
            $('#location_links a').click(function(e){
                e.preventDefault();
                var search_loc = $(this).text();
                $('input[name=area_postcode]').val(search_loc);
                $('#search_form_home').submit();
            });
        </script>

    </div>
</section>
