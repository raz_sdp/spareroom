<div class="container">

<h2 style="text-align:justify;">Advertise on FindmeaRoom</h2>
<hr>
<p style="color:#222;font-weight:bold;">What do you want to advertise?</p>
	<div class="col-md-12">
		
			<p class="col-md-2">
				<?php echo $this->Html->image('board_rooms_offered.png', array('alt' => 'Rooms wanted')); ?>
			</p>
			<p class="col-md-8">
				<strong>Rooms to Rent</strong><br>
				
				Rent out one or more rooms in a property
			
			
			</p>
			<a href="#">
			<p class="col-md-2">
<a href="add_option" class="btn btn-primary">Select</a>       
			</p>
		</a>
	</div>

	<div class="col-md-12">
	
			<p class="col-md-2">
				<?php echo $this->Html->image('board_whole_properties.png', array('alt' => 'Rooms wanted')); ?>
			</p>
			<p class="col-md-8">
				<strong>Whole Property to Let</strong><br>
							
				Rent a self-contained property (with no existing flatmates) on a single contract. Includes studios and 1 bed flats
			
			</p>
				<a href="add_option">
			<p class="col-md-2">
<input type="submit" class="btn btn-primary" value="Select" name="register">        
			</p>
		</a>
	</div>
	
	<div class="col-md-12">
		
			<p class="col-md-2">
				<?php echo $this->Html->image('board_rooms_wanted.png', array('alt' => 'Rooms wanted')); ?>
			</p>
			<p class="col-md-8">
				<strong>Room Wanted</strong><br>
				Create a room wanted ad so people offering rooms can find out more about you and get in touch
			</p>
			<a href="room_wanted">
			<p class="col-md-2">
<input type="submit" class="btn btn-primary" value="Select" name="register">       
			</p>
		</a>
	</div>

            <br>
     <h4>How to edit an existing advert</h4>
         
    <p>
    
    	To see, edit, delete or upgrade your existing adverts, you must <a href="login" class="btn btn-primary">log in</a>
     
    </p>

</div>