<div class="container">
<br><br>


<form class="form-horizontal" role="form" method="post" action="users/login" name="basicform" id="basicform">


      <div class="form-group">
      <label class="control-label col-sm-2" for="email">Email:</label>
      <div class="col-sm-10">
        <input type="email" class="form-control" id="email" name="data[User][email]" placeholder="Enter email">
      </div>
    </div>



    <div class="form-group">
      <label class="control-label col-sm-2" for="pwd">Password:</label>
      <div class="col-sm-10">          
        <input type="password" class="form-control" id="password" name="data[User][password]" placeholder="Enter password">
        <a href="forget_password" style="float:right;">Forgot Your Password</a>
      </div>
    </div>

<div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <input type="submit" class="btn btn-primary" value="Login" name="login" />

      </div>
    </div>

<hr>


<div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
     <h3>Not registered? Join us FREE</h3>
      </div>
    </div>

<div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
       <a href="register" class="btn btn-primary">Register with your email</a>
      </div>
    </div>



</form>



</div>