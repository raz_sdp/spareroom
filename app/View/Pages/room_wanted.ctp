<div class="container">

  <form name="basicform" id="basicform" method="post" action="wanted_ads/add">
        
        <div id="sf1" class="frm">
          <fieldset>
            <legend>Step 1 of 5</legend>
            <h3>Get started with your room wanted advert</h3>
            <div class="form-group">
              <label class="col-lg-2 control-label" for="uname">I am / we are </label>
              <div class="col-lg-6">
                <select class="form-control" id="step1_have" name="data[WantedAd][genders]">
                    <option value="" SELECTED>Select your gender(s)...</option>
                    <option value="1 male">1 male</option>
                    <option value="1 female">1 female</option>
                    <option value="1 male 1 female">1 male 1 female</option>
                    <option value="2 males">2 males</option>
                    <option value="2 females">2 females</option>
                </select>
              </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <div class="form-group">
            <label class="col-lg-2 control-label" for="uname">Looking for </label>
                <div class="col-lg-6">
                    <select class="form-control" id="looking_for" name="data[WantedAd][looking_for]">
                        <option value="" SELECTED>Select what you are looking for...</option>
                        <option value="a single or double room">a single or double room</option>
                        <option value="a double room">a double room</option>
                        <option value="a double room (we are partners)">a double room (we are partners)</option>
                        <option value="a twin room or 2 rooms">a twin room or 2 rooms</option>
                        <option value="2 rooms">2 rooms</option>
                    </select>
                </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <div class="form-group">
                <label class="col-lg-2 control-label" for="uname">'Buddy ups' </label>
                <div class="col-lg-6">
                    <input type="checkbox" name="data[WantedAd][buddy_ups]" id="buddyup"> I/we are also interested in Buddying up
                    <br>Tick this if you might like to Buddy Up with other room seekers to find a whole flat or
                    house together and start a brand new flat/house share.
                </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <div class="form-group">
              <div class="col-lg-10 col-lg-offset-2">
                <button class="btn btn-primary open1" type="button" name="step1_complete">Next <span class="fa fa-arrow-right"></span></button> 
                <br><br>
              </div>
            </div>

          </fieldset>
        </div>


        <div id="sf2" class="frm" style="display: none;">
          <fieldset>
            <legend>Step 2 of 5</legend>
            <h3>Your search</h3>
            <div class="form-group">
              <label class="col-lg-2 control-label" for="uname">Where do you want to live? </label>
              <div class="col-lg-6">
                <select class="form-control" id="step2_area" name="data[WantedAd][area_ids]">
                    <option value="" SELECTED >Select an area...</option>
                    <option value="1">London and surrounds</option>
                    <option value="2">East Anglia</option>
                    <option value="3">East Midlands</option>
                    <option value="4">North England</option>
                    <option value="5">North West England</option>
                    <option value="6">South East England</option>
                    <option value="7">South West England</option>
                    <option value="8">West Midlands</option>
                    <option value="9">Yorkshire and Humberside</option>
                    <option value="10">Northern Ireland</option>
                    <option value="11">Scotland</option>
                    <option value="12">Wales</option>
                    <option value="13">Channel Islands / Isle of Man</option>
                </select>
              </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <div class="form-group">
              <label class="col-lg-2 control-label" for="uname">Your budget<br>(total rental amount you can afford) </label>
              <div class="col-lg-3">
                  <label>£</label>
                  <input type="number" class="form-control" name="data[WantedAd][rent]">
              </div>
              <div class="col-lg-3">
                <select class="form-control"  name="data[WantedAd][per_week]">
                    <option value="" SELECTED >per week or month?...</option>
                    <option value="per week">per week</option>
                    <option value="per month">per month</option>
                </select>
              </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <div class="form-group">
              <label class="col-lg-2 control-label" for="uname">I am available to move in from </label>
              <div class="col-lg-3">
                <input class="form-control" type="date" name="data[WantedAd][available_from]">
              </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <div class="form-group">
              <label class="col-lg-2 control-label" for="uname">Period accommodation needed for </label>
              <div class="col-lg-3">
                <select class="form-control" id="step2_transport" name="data[WantedAd][min_stay]">
                  <option value="0">No minimum</option>
                  <option value="1">1 Month</option>
                  <option value="2">2 Months</option>
                  <option value="3">3 Months</option>
                  <option value="4">4 Months</option>
                  <option value="5">5 Months</option>
                  <option value="6">6 Months</option>
                  <option value="7">7 Months</option>
                  <option value="8">8 Months</option>
                  <option value="9">9 Months</option>
                  <option value="10">10 Months</option>
                  <option value="11">11 Months</option>
                  <option value="12">1 Year</option>
                  <option value="15">1 Year 3 Months</option>
                  <option value="18">1 Year 6 Months</option>
                  <option value="21">1 Year 9 Months</option>
                  <option value="24">2 Years</option>
                  <option value="36">3 Years</option>
                </select>
                  to
              </div>
              <div class="col-lg-3">
                <select class="form-control" id="step2_transport1" name="data[WantedAd][max_stay]">
                  <option value="0">No maximum</option>
                  <option value="1">1 Month</option>
                  <option value="2">2 Months</option>
                  <option value="3">3 Months</option>
                  <option value="4">4 Months</option>
                  <option value="5">5 Months</option>
                  <option value="6">6 Months</option>
                  <option value="7">7 Months</option>
                  <option value="8">8 Months</option>
                  <option value="9">9 Months</option>
                  <option value="10">10 Months</option>
                  <option value="11">11 Months</option>
                  <option value="12">1 Year</option>
                  <option value="15">1 Year 3 Months</option>
                  <option value="18">1 Year 6 Months</option>
                  <option value="21">1 Year 9 Months</option>
                  <option value="24">2 Years</option>
                  <option value="36">3 Years</option>
                </select>
              </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>


            <div class="form-group">
              <label class="col-lg-2 control-label" for="uname">I want to stay in the accommodation </label>
              <div class="col-lg-6">
                <select class="form-control" id="step2_transport1" name="data[WantedAd][days_available]">
                  <option value="7 days a week">7 days a week</option>
                  <option value="Mon to Fri only">Mon to Fri only</option>
                  <option value="Weekends only">Weekends only</option>s
                </select>
              </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <div class="form-group">
              <label class="col-lg-2 control-label" for="uname">I would prefer these amenities </label>
              <div class="col-lg-3">
                <input type="checkbox" name="data[WantedAd][aminities][]" value="furnished"> Furnished
              </div>
              <div class="col-lg-3">
                <input type="checkbox" name="data[WantedAd][aminities][]" value="parking"> Parking
              </div>
              <div class="col-lg-3">
                <input type="checkbox" name="data[WantedAd][aminities][]" value="shared living room"> Shared living room
              </div>
              <div class="col-lg-3">
                <input type="checkbox" name="data[WantedAd][aminities][]" value="garage"> Garage
              </div>
              <div class="col-lg-3">
                <input type="checkbox" name="data[WantedAd][aminities][]" value="washing machine"> Washing machine
              </div>
              <div class="col-lg-3">
                <input type="checkbox" name="data[WantedAd][aminities][]" value="disabled access"> Disabled access
              </div>
              <div class="col-lg-3">
                <input type="checkbox" name="data[WantedAd][aminities][]" value="garden/roof terrace"> Garden/roof terrace
              </div>
              <div class="col-lg-3">
                <input type="checkbox" name="data[WantedAd][aminities][]" value="washing machine"> Washing machine
              </div>
              <div class="col-lg-3">
                <input type="checkbox" name="data[WantedAd][aminities][]" value="broadband"> Broadband
              </div>
              <div class="col-lg-2">
              </div>
              <div class="col-lg-3">
                <input type="checkbox" name="data[WantedAd][aminities][]" value="balcony/patio"> Balcony/patio
              </div>
              <div class="col-lg-3">
                <input type="checkbox" name="data[WantedAd][aminities][]" value="en suite"> En suite
              </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>


            <h3>About you</h3>
            <div class="form-group">
              <label class="col-lg-2 control-label" for="uname">Age </label>
              <div class="col-lg-6">
                <select class="form-control" id="step2_transport1" name="data[WantedAd][age]">
                    <option value="" SELECTED >Select...</option>
                    <?php
                    for($i=16; $i<=99; $i++)
                        echo "<option value=\"$i\">$i</option>";
                    ?>
                </select>
              </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <div class="form-group">
              <label class="col-lg-2 control-label" for="occupation">Occupation </label>
              <div class="col-lg-6">
                  <select class="form-control" id="occupation" name="data[WantedAd][occupation]">
                      <option value="ND" SELECTED >Not disclosed</option>
                      <option value="Student">Student</option>
                      <option value="Professional">Professional</option>
                      <option value="Other">Other</option>
                  </select>
              </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <div class="form-group">
                <label class="col-lg-2 control-label" for="dss">DSS </label>
                <div class="col-lg-6">
                    <input type="checkbox" name="data[WantedAd][dss]" value="1"> Yes, we will be using housing benefit to pay some or all our rent
                </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <div class="form-group">
                <label class="col-lg-2 control-label" for="smoker">Do you smoke</label>
                <div class="col-lg-6">
                    <select class="form-control" name="data[WantedAd][smoker]" id="step4_smoking">
                        <option value="No">No</option>
                        <option value="Yes">Yes</option>
                    </select>
                </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <div class="form-group">
              <label class="col-lg-2 control-label" for="uname">Pets </label>
              <div class="col-lg-6">
                 <select class="form-control" name="data[WantedAd][pets]" id="step4_pets">
                    <option value="No">No</option>
                    <option value="Yes">Yes</option>
                 </select>
              </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <div class="form-group">
              <label class="col-lg-2 control-label" for="uname">Sexual orientation </label>
              <div class="col-lg-6">
                  <select class="form-control" name="data[WantedAd][orientation]" id="step4_sexual_orientation">
                      <option value="Not Disclosed">Not Disclosed</option>
                      <option value="Straight">Straight</option>
                      <option value="Mixed">Mixed</option>
                      <option value="Gay/Lesbian">Gay/Lesbian</option>
                      <option value="Bi-sexual">Bi-sexual</option>
                  </select>
                  <input type="checkbox" name="data[WantedAd][is_orientation_part]" id="step4_sexual_orientation_yes">Yes, I would like my orientation to form part of my advert, search criteria and allow others to search on this field
              </div>           
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <div class="form-group">
                <label class="col-lg-2 control-label" for="uname">Language </label>
                <div class="col-lg-6">
                  <select class="form-control" name="data[WantedAd][language]" id="step4_language">
                    <option value="26">English</option>
                    <option value="27">Mixed</option>
                    <option value="4">Arabic</option>
                    <option value="11">Bengali</option>
                    <option value="17">Cantonese</option>
                    <option value="37">French</option>
                    <option value="39">German</option>
                    <option value="44">Hindi</option>
                    <option value="48">Indonesian</option>
                    <option value="50">Japanese</option>
                    <option value="62">Malay</option>
                    <option value="63">Mandarin</option>
                    <option value="73">Portuguese</option>
                    <option value="74">Punjabi</option>
                    <option value="79">Russian</option>
                    <option value="88">Spanish</option>
                    <option value="1">Afrikaans</option>
                    <option value="2">Albanian</option>
                    <option value="3">Amharic</option>
                    <option value="5">Armenian</option>
                    <option value="6">Aymara</option>
                    <option value="7">Baluchi</option>
                    <option value="8">Bambara</option>
                    <option value="9">Basque</option>
                    <option value="10">Belarussian</option>
                    <option value="12">Berber</option>
                    <option value="13">Bislama</option>
                    <option value="14">Bosnian</option>
                    <option value="15">Bulgarian</option>
                    <option value="16">Burmese</option>
                    <option value="18">Catalan</option>
                    <option value="19">Ciluba</option>
                    <option value="20">Creole</option>
                    <option value="21">Croatian</option>
                    <option value="22">Czech</option>
                    <option value="23">Danish</option>
                    <option value="24">Dari</option>
                    <option value="25">Dutch</option>
                    <option value="28">Eskimo</option>
                    <option value="29">Estonian</option>
                    <option value="30">Ewe</option>
                    <option value="31">Fang</option>
                    <option value="32">Faroese</option>
                    <option value="33">Farsi (Persian)</option>
                    <option value="34">Filipino</option>
                    <option value="35">Finnish</option>
                    <option value="36">Flemish</option>
                    <option value="38">Galician</option>
                    <option value="40">Greek</option>
                    <option value="41">Gujarati</option>
                    <option value="42">Hausa</option>
                    <option value="43">Hebrew</option>
                    <option value="45">Hungarian</option>
                    <option value="46">Ibo</option>
                    <option value="47">Icelandic</option>
                    <option value="49">Italian</option>
                    <option value="51">Kabi�</option>
                    <option value="52">Kashmiri</option>
                    <option value="53">Kirundi</option>
                    <option value="54">Kiswahili</option>
                    <option value="55">Korean</option>
                    <option value="56">Latvian</option>
                    <option value="57">Lingala</option>
                    <option value="58">Lithuanian</option>
                    <option value="59">Luxembourgish</option>
                    <option value="60">Macedonian</option>
                    <option value="61">Malagasy</option>
                    <option value="64">Mayan</option>
                    <option value="65">Motu</option>
                    <option value="66">Nepali</option>
                    <option value="67">Norwegian</option>
                    <option value="68">Noub</option>
                    <option value="69">Pashto</option>
                    <option value="70">Peul</option>
                    <option value="71">Pidgin</option>
                    <option value="72">Polish</option>
                    <option value="75">Pushtu</option>
                    <option value="76">Quechua</option>
                    <option value="77">Romanian</option>
                    <option value="78">Romansch</option>
                    <option value="80">Sango</option>
                    <option value="81">Serbian</option>
                    <option value="82">Setswana</option>
                    <option value="83">Sindhi</option>
                    <option value="84">Sinhala</option>
                    <option value="85">Slovak</option>
                    <option value="86">Slovene</option>
                    <option value="87">Somali</option>
                    <option value="89">Swahili</option>
                    <option value="90">Swedish</option>
                    <option value="91">Tamil</option>
                    <option value="92">Thai</option>
                    <option value="93">Turkish</option>
                    <option value="94">Urdu</option>
                    <option value="95">Vietnamese</option>
                    <option value="99">Welsh</option>
                    <option value="96">Xhosa</option>
                    <option value="97">Yoruba</option>
                    <option value="98">Zulu</option>
                  </select>
                </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <div class="form-group">
                <label class="col-lg-2 control-label" for="uname">Nationality </label>
                <div class="col-lg-6">
                    <select class="form-control" name="data[WantedAd][nationality]" id="step4_nationality">
                        <option value="Not disclosed" selected="">Not disclosed</option>
                        <option value="Afghan">Afghan</option>
                        <option>Albanian</option>
                        <option>Algerian</option>
                        <option>American</option>
                        <option>Andorran</option>
                        <option>Angolan</option>
                        <option>Antiguans</option>
                        <option>Argentinean</option>
                        <option>Armenian</option>
                        <option>Australian</option>
                        <option>Austrian</option>
                        <option>Azerbaijani</option>
                        <option>Bahamian</option>
                        <option>Bahraini</option>
                        <option>Bangladeshi</option>
                        <option>Barbadian</option>
                        <option>Barbudans</option>
                        <option>Batswana</option>
                        <option>Belarusian</option>
                        <option>Belgian</option>
                        <option>Belizean</option>
                        <option>Beninese</option>
                        <option>Bhutanese</option>
                        <option>Bolivian</option>
                        <option>Bosnian</option>
                        <option>Brazilian</option>
                        <option>British</option>
                        <option>Bruneian</option>
                        <option>Bulgarian</option>
                        <option>Burkinabe</option>
                        <option>Burmese</option>
                        <option>Burundian</option>
                        <option>Cambodian</option>
                        <option>Cameroonian</option>
                        <option>Canadian</option>
                        <option>Cape Verdean</option>
                        <option>Central African</option>
                        <option>Chadian</option>
                        <option>Chilean</option>
                        <option>Chinese</option>
                        <option>Colombian</option>
                        <option>Comoran</option>
                        <option>Congolese</option>
                        <option>Costa Rican</option>
                        <option>Croatian</option>
                        <option>Cuban</option>
                        <option>Cypriot</option>
                        <option>Czech</option>
                        <option>Danish</option>
                        <option>Djibouti</option>
                        <option>Dominican</option>
                        <option>Dutch</option>
                        <option>East Timorese</option>
                        <option>Ecuadorean</option>
                        <option>Egyptian</option>
                        <option>Emirian</option>
                        <option>English</option>
                        <option>Equatorial Guinean</option>
                        <option>Eritrean</option>
                        <option>Estonian</option>
                        <option>Ethiopian</option>
                        <option>Fijian</option>
                        <option>Filipino</option>
                        <option>Finnish</option>
                        <option>French</option>
                        <option>Gabonese</option>
                        <option>Gambian</option>
                        <option>Georgian</option>
                        <option>German</option>
                        <option>Ghanaian</option>
                        <option>Greek</option>
                        <option>Grenadian</option>
                        <option>Guatemalan</option>
                        <option>Guinea-Bissauan</option>
                        <option>Guinean</option>
                        <option>Guyanese</option>
                        <option>Haitian</option>
                        <option>Herzegovinian</option>
                        <option>Honduran</option>
                        <option>Hungarian</option>
                        <option>I-Kiribati</option>
                        <option>Icelander</option>
                        <option>Indian</option>
                        <option>Indonesian</option>
                        <option>Iranian</option>
                        <option>Iraqi</option>
                        <option>Irish</option>
                        <option>Israeli</option>
                        <option>Italian</option>
                        <option>Ivorian</option>
                        <option>Jamaican</option>
                        <option>Japanese</option>
                        <option>Jordanian</option>
                        <option>Kazakhstani</option>
                        <option>Kenyan</option>
                        <option>Kittian and Nevisian</option>
                        <option>Kuwaiti</option>
                        <option>Kyrgyz</option>
                        <option>Laotian</option>
                        <option>Latvian</option>
                        <option>Lebanese</option>
                        <option>Liberian</option>
                        <option>Libyan</option>
                        <option>Liechtensteiner</option>
                        <option>Lithuanian</option>
                        <option>Luxembourger</option>
                        <option>Macedonian</option>
                        <option>Malagasy</option>
                        <option>Malawian</option>
                        <option>Malaysian</option>
                        <option>Maldivan</option>
                        <option>Malian</option>
                        <option>Maltese</option>
                        <option>Marshallese</option>
                        <option>Mauritanian</option>
                        <option>Mauritian</option>
                        <option>Mexican</option>
                        <option>Micronesian</option>
                        <option>Moldovan</option>
                        <option>Monacan</option>
                        <option>Mongolian</option>
                        <option>Moroccan</option>
                        <option>Mosotho</option>
                        <option>Motswana</option>
                        <option>Mozambican</option>
                        <option>Namibian</option>
                        <option>Nauruan</option>
                        <option>Nepalese</option>
                        <option>Netherlander</option>
                        <option>New Zealander</option>
                        <option>Ni-Vanuatu</option>
                        <option>Nicaraguan</option>
                        <option>Nigerian</option>
                        <option>Nigerien</option>
                        <option>North Korean</option>
                        <option>Northern Irish</option>
                        <option>Norwegian</option>
                        <option>Omani</option>
                        <option>Pakistani</option>
                        <option>Palauan</option>
                        <option>Panamanian</option>
                        <option>Papua New Guinean</option>
                        <option>Paraguayan</option>
                        <option>Peruvian</option>
                        <option>Polish</option>
                        <option>Portuguese</option>
                        <option>Qatari</option>
                        <option>Romanian</option>
                        <option>Russian</option>
                        <option>Rwandan</option>
                        <option>Saint Lucian</option>
                        <option>Salvadoran</option>
                        <option>Samoan</option>
                        <option>San Marinese</option>
                        <option>Sao Tomean</option>
                        <option>Saudi</option>
                        <option>Scottish</option>
                        <option>Senegalese</option>
                        <option>Serbian</option>
                        <option>Seychellois</option>
                        <option>Sierra Leonean</option>
                        <option>Singaporean</option>
                        <option>Slovakian</option>
                        <option>Slovenian</option>
                        <option>Solomon Islander</option>
                        <option>Somali</option>
                        <option>South African</option>
                        <option>South Korean</option>
                        <option>Spanish</option>
                        <option>Sri Lankan</option>
                        <option>Sudanese</option>
                        <option>Surinamer</option>
                        <option>Swazi</option>
                        <option>Swedish</option>
                        <option>Swiss</option>
                        <option>Syrian</option>
                        <option>Taiwanese</option>
                        <option>Tajik</option>
                        <option>Tanzanian</option>
                        <option>Thai</option>
                        <option>Togolese</option>
                        <option>Tongan</option>
                        <option>Trinidadian or Tobagonian</option>
                        <option>Tunisian</option>
                        <option>Turkish</option>
                        <option>Tuvaluan</option>
                        <option>Ugandan</option>
                        <option>Ukrainian</option>
                        <option>Uruguayan</option>
                        <option>Uzbekistani</option>
                        <option>Venezuelan</option>
                        <option>Vietnamese</option>
                        <option>Welsh</option>
                        <option>Yemenite</option>
                        <option>Zambian</option>
                        <option>Zimbabwean</option>
                    </select>
                </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <div class="form-group">
              <label class="col-lg-2 control-label" for="uname">Interests </label>
              <div class="col-lg-6">
                <input type="text" class="form-control" name="data[WantedAd][interests]" id="step4_interests1">
              </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <div class="form-group">
                <label class="col-lg-2 control-label" for="uname">Display Name</label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" placeholder="First Name" name="data[WantedAd][display_name_first]">
                </div>
                <div class="col-sm-2">
                    <input type="text" class="form-control" placeholder="Last Name" name="data[WantedAd][display_name_last]">
                </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <div class="form-group">
              <div class="col-lg-10 col-lg-offset-2">
                <button class="btn btn-warning back1" type="button"><span class="fa fa-arrow-left"></span> Back</button>
                <button class="btn btn-primary open2" type="button">Next <span class="fa fa-arrow-right"></span></button> 
                <br><br>
              </div>
            </div>

          </fieldset>
        </div>

        <div id="sf3" class="frm" style="display: none;">
          <fieldset>
            <legend>Step 3 of 5</legend>
            <h3>Preferences For New Flatmate</h3>

            <div class="form-group">
              <label class="col-lg-2 control-label" for="uname">Smoking </label>
              <div class="col-lg-6">
                <select class="form-control" name="data[WantedAd][prefered_smoking]" id="step4_smoking2">
                      <option value="No preference">No preference</option>
                      <option value="Yes">Yes</option>
                      <option value="no">No</option>
                </select>
              </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <div class="form-group">
              <label class="col-lg-2 control-label" for="uname">Gender </label>
              <div class="col-lg-6">
                <select class="form-control" name="data[WantedAd][prefered_gender]" id="step4_gender2">
                      <option value="No preference">No preference</option>
                      <option value="Male">Male</option>
                      <option value="Female">Female</option>
                </select>
              </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <div class="form-group">
              <label class="col-lg-2 control-label" for="uname">Occupation </label>
              <div class="col-lg-6">
                    <select class="form-control" name="data[WantedAd][prefered_occupation]" id="step4_occupation2">
                      <option value="No preference">No preference</option>
                      <option value="Student">Student</option>
                      <option value="Professional">Professional</option>
                    </select>
              </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <div class="form-group">
               <label class="col-lg-2 control-label" for="uname">Pets </label>
               <div class="col-lg-6">
                <select class="form-control" name="data[WantedAd][prefered_pets]" id="step4_pets2">
                    <option value="No">No</option>
                    <option value="No preference">No preference</option>
                </select>
               </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>


            <div class="form-group">
              <label class="col-lg-2 control-label" for="uname">Minimum age </label>
              <div class="col-lg-6">
                  <select class="form-control" name="data[WantedAd][prefered_age_min]" id="step4_minimum2">
                    <option value="" selected>-</option>
                      <?php
                      for($i=16; $i<=99; $i++)
                          echo "<option value=\"$i\">$i</option>";
                      ?>
                  </select>
              </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <div class="form-group">
              <label class="col-lg-2 control-label" for="uname">Maximum age </label>
              <div class="col-lg-6">
                <select class="form-control" name="data[WantedAd][prefered_age_max]" id="step4_maximum2">
                    <option value="" selected>-</option>
                    <?php
                    for($i=16; $i<=99; $i++)
                        echo "<option value=\"$i\">$i</option>";
                    ?>
                </select>
              </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <div class="form-group">
              <label class="col-lg-2 control-label" for="uname">Orientation </label>
              <div class="col-lg-6">
                <select class="form-control" name="data[WantedAd][prefered_orientation]" id="step4_orientation2">
                      <option value="Not Disclosed">Not Disclosed</option>
                      <option value="Straight">Straight</option>
                      <option value="Mixed">Mixed</option>
                      <option value="Gay/Lesbian">Gay/Lesbian</option>
                      <option value="Bi-sexual">Bi-sexual</option>
                </select>
              </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <div class="form-group">
              <div class="col-lg-10 col-lg-offset-2">
                   <button class="btn btn-warning back2" type="button"><span class="fa fa-arrow-left"></span> Back</button>
                   <button class="btn btn-primary open3" type="button">Next <span class="fa fa-arrow-right"></span></button>
                <br><br>
              </div>
            </div>

          </fieldset>
        </div>


        <div id="sf4" class="frm" style="display: none;">
          <fieldset>
            <legend>Step 4 of 5</legend>

            <div class="form-group">
              <label class="col-lg-2 control-label" for="uname">Title<br>(short description - max 50 characters)</label>
              <div class="col-lg-6">
                <input type="text" placeholder="Title" id="title" name="data[WantedAd][title]" class="form-control" autocomplete="off">
              </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <div class="form-group">
              <label class="col-lg-2 control-label" for="uname">Description </label>
              <div class="col-lg-6 text_area">
                <textarea placeholder="" id="description" name="data[WantedAd][title]" class="form-control" autocomplete="off"style="width: 100%;height: 300px;"></textarea><br>
               <b> Tips: Give more detail about the accommodation, who you are looking for and what a potential flatmate should expect living with you. You must write at least 25 words and can write as much as you like within reason.</b>
              </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <div class="form-group">
              <label class="col-lg-2 control-label" for="uname">Telephone</label>
              <div class="col-lg-6">
                <input type="text" placeholder="Telephone" id="telephone" name="data[WantedAd][telephone]" class="form-control" autocomplete="off">
 
              </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <div class="form-group">
              <label class="col-lg-2 control-label" for="uname">Telephone</label>
              <div class="col-lg-6">
                  <b> As per your login details provided on the next step (NOTE We never reveal your email address - users email you through our messaging system which we then forward to your email, thus protecting your privacy)</b>
              </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <div class="form-group">
              <div class="col-lg-10 col-lg-offset-2">
                   <button class="btn btn-warning back3" type="button"><span class="fa fa-arrow-left"></span> Back</button>
                   <button class="btn btn-primary open4" type="button">Next <span class="fa fa-arrow-right"></span></button>
                   <br><br>
              </div>
            </div>

          </fieldset>
        </div>
        
        <div id="sf5" class="frm" style="display: none;">
          <fieldset>
             <legend>Step 5 of 5</legend>
             <div class="form-group">
              <label class="control-label col-sm-2" for="email">Email:</label>
              <div class="col-sm-10">
                <input type="email" class="form-control" id="email" name="data[User][email]" placeholder="Enter email">
              </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <div class="form-group">
              <label class="control-label col-sm-2" for="pwd">Password:</label>
              <div class="col-sm-10">
                <input type="password" class="form-control" id="password" name="data[User][simple_pwd]" placeholder="Enter password">
                <a href="/forget_password" style="float:right;">Forgot Your Password</a>
              </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <div class="form-group">
              <div class="col-lg-10 col-lg-offset-2">
                <button class="btn btn-warning back4" type="button"><span class="fa fa-arrow-left"></span> Back</button>
                <input class="btn btn-primary open6" type="submit" id="login" value="Submit">
                <img src="spinner.html" alt="" id="loader" style="display: none">
              </div>
            </div>

            <hr>

            <div class="form-group">
               <div class="col-sm-offset-2 col-sm-10">
                 <h3>Not registered? Join us FREE</h3>
               </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <div class="form-group">
               <div class="col-sm-offset-2 col-sm-10">
                   <a href="/register" class="btn btn-primary">Register with your email</a>
               </div>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <hr>
          </fieldset>
        </div>
  </form>

</div>

<script type="text/javascript">
    var i_path = "<?php echo $this->webroot;?>img/nicEditorIcons.gif";
</script>
<?php echo $this->Html->script(array('nicEdit')); ?>
<script type="text/javascript">
//<![CDATA[
    bkLib.onDomLoaded(function() { 
      nicEditors.allTextAreas(); 
      $('div.text_area>div').each(function(){$(this).width("100%");});
      $('div.text_area').removeAttr('style');
    });
//]]>
</script>

<script type="text/javascript">
  
  jQuery().ready(function() {

    // validate form on keyup and submit
    var v = jQuery("#basicform").validate({
      rules: {
        step1_postcode: {
          required: true,
          minlength: 4
        },
        step1_email: {
          required: true,
          minlength: 4,
          email: true,
          maxlength: 30
        },
       
        upass2: {
          required: true,
          minlength: 6,
          equalTo: "#upass1"
        }

      },
      errorElement: "span",
      errorClass: "help-inline-error"
    });

    $(".open1").click(function() {
      if (v.form()) {
        $(".frm").hide("fast");
        $("#sf2").show("slow");
      }
    });

    $(".open2").click(function() {
      if (v.form()) {
        $(".frm").hide("fast");
        $("#sf3").show("slow");
      }
    });



    $(".open3").click(function() {
      if (v.form()) {
        $(".frm").hide("fast");
        $("#sf4").show("slow");
      }
    });


    $(".open4").click(function() {
      if (v.form()) {
        $(".frm").hide("fast");
        $("#sf5").show("slow");
      }
    });


    $(".open5").click(function() {
      if (v.form()) {
        $(".frm").hide("fast");
        $("#sf6").show("slow");
      }
    });
    
 /*   $(".open6").click(function() {
      if (v.form()) {
        $("#loader").show();
         setTimeout(function(){
           $("#basicform").html('<h2>Thanks for your time.</h2>');
         }, 1000);
        return false;
      }
    });

*/
    
    $(".back1").click(function() {
      $(".frm").hide("fast");
      $("#sf1").show("slow");
    });

     $(".back2").click(function() {
      $(".frm").hide("fast");
      $("#sf2").show("slow");
    });

      $(".back3").click(function() {
      $(".frm").hide("fast");
      $("#sf3").show("slow");
    });

       $(".back4").click(function() {
      $(".frm").hide("fast");
      $("#sf4").show("slow");
    });

    $(".back5").click(function() {
      $(".frm").hide("fast");
      $("#sf5").show("slow");
    });

  });
</script>


		</div>
	</div>