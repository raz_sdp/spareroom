<section  class="banner-section">
    <div class="container md-section">
        <div class="row">
            <div class="col-md-8 col-sm-7">
                <div class="dream-banner">
                <form method="post" name="search_form_home" id="search_form_home" action="searchEngine/search_api">
                    <h1>Start Find Your Dream Room</h1>
                    <div class="search-part clearfix">
                        <input type="hidden" name="search_by" value="location">
                        <input type="hidden" name="advance_search" value="0">
                        <input type="search" name="area_postcode" placeholder="Area, Postcode or ad Reference" required/>
                        <input type="submit" value="Search Now" />
                    </div>
                    <div class="radio-part-outer">
                        <div class="radio-part">
                            <input id="radio1" type="radio" name="search_for" value="to_rent" checked>
                            <label for="radio1">Rooms for rent</label>
                        </div>
                        <div class="radio-part">
                            <input id="radio2" type="radio" name="search_for" value="wanted">
                            <label for="radio2">Rooms wanted</label>
                        </div>
                        <div class="radio-part">
                            <input id="radio3" type="radio" name="search_for" value="buddy_up">
                            <label for="radio3">Buddy ups (?)</label>
                        </div>
                    </div>
                </form>
                </div>
            </div>
            <div class="col-md-4 col-sm-5">
                <div class="favourite-add">
                    <h3>Our Favourite ads</h3>
                    <div class="favourite-box">
                        <h4>Advertise your room to let</h4>
                        <p>Be amazed at the response rate - rent
                            your room within days. Find out more »</p>
                        <div class="paleceadd"><a href="place_ad">Place a Free ad</a></div>

                        <div class="room-let"><span>Room To Let</span></div>
                    </div>
                    <div class="favourite-box">
                        <h4>Need a room?</h4>
                        <p>Most people with rooms to let search
                            the rooms wanted for suitable flatmates.
                            Make sure they find you!</p>
                        <div class="paleceadd"><a href="place_ad">Place a Free Room Wanted ad</a></div>

                        <div class="room-let"><span>Room Wanted</span></div>
                    </div>
                </div>

            </div>
        </div>
        <!-- end of "row" -->
    </div>
</section> <!-- end of "main_container" -->

<section class="new-room-sec slider-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="sub-heding">New rooms to rent</h2>
                <div class="slider-carousel">
                    <ul class="bxslider">
                        <?php foreach($new_ads as $key=>$item) { ?>
                        <li>
                            <a href="<?php echo $this->webroot;?>searchEngine/full_ad/rent_ads/<?php echo $item['RentAd']['id'];?>" >
                            <div class="slider-box">
                                <figure  class="slider-pic">
                                    <?php
                                    if(!empty($item['RentAd']['photo_1'])){
                                        $img_url = $this->Html->url('/files/add_images/'.$item['RentAd']['photo_1'], true);
                                    }else{
                                        $img_url = $this->Html->url('/files/add_images/default_search_img.jpg', true);
                                    }
                                    ?>
                                    <img src="<?php echo $img_url;?>" alt="" />
                                    <div class="tag">$<?php echo $item['RentAd']['rent']." ".$item['RentAd']['per_week'];?></div>
                                </figure>
                                <!--<div class="product-details">
                                    <h4><?php /*echo $item['RentAd']['area'];*/?></h4>
                                    <p><?php /*echo $item['RentAd']['description'];*/?></p>
                                </div>-->
                            </div>
                            </a>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-7">
                <div class="find-room-section">
                    <h3>LOOKING UP FINDMEAROOM ON THE GO!</h3>
                    <p>
                        Download oru top-rated app, made just for you! <br />
                        It’s free, easy and smart
                    </p>
                    <div class="appstore-link">
                        <a href="https://itunes.apple.com/en/app/apple-store/id375380948?mt=8"><img src="images/apple-store.png" alt="" target="_blank" /></a>
                        <a href="https://play.google.com/store/apps?hl=en"><img src="images/ggogle-play.png" alt="" target="_blank" /></a>
                    </div>
                    <div class="send-app-link">
                        <form method="post" action="#">
                            <input type="text" value="" class="form-control" placeholder="Enter your mobile number" required />
                            <input type="submit" value="Send App Link" class="applink-btn" style="margin-top:3px;" />

                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-5 col-md-offset-1 col-sm-5">
                <div class="mobile-pic-box">
                    <img src="images/phone-pic.png" alt="" />
                </div>
            </div>
        </div>
    </div>
</section>

<section class="popular-destination">
    <div class="container">
        <h3>Popular UK destinations</h3>
        <div class="row" id="location_links">
            <div class="col-md-2 col-sm-4 col-xs-6">
                <ul>
                    <li><a href="#" class="blue">London</a></li>
                    <li><a href="#" class="blue">New York</a></li>
                    <li><a href="#" class="blue ">Amsterdam</a></li>
                    <li><a href="#" class="blue ">Paris</a></li>
                    <li><a href="#" class="">Berlin</a></li>
                    <li><a href="#" class="">Barcelona</a></li>
                    <li><a href="#" class="">Rome</a></li>
                    <li><a href="#" class="">Edinburgh</a></li>
                    <li><a href="#" class="">Vienna</a></li>
                    <li><a href="#" class="">Florence</a></li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
                <ul>
                    <li><a href="#" class="">Marrakech</a></li>
                    <li><a href="#" class="">Prague</a></li>
                    <li><a href="#" class="">Lisbon</a></li>
                    <li><a href="#" class="">Istanbul</a></li>
                    <li><a href="#" class="">Krakow</a></li>
                    <li><a href="#" class="">Madrid</a></li>
                    <li><a href="#" class="">Venice</a></li>
                    <li><a href="#" class="">Nice</a></li>
                    <li><a href="#" class="">Budapest</a></li>
                    <li><a href="#" class="">Cannes</a></li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
                <ul>
                    <li><a href="#" class="">Ibiza</a></li>
                    <li><a href="#" class="">Tuscany</a></li>
                    <li><a href="#" class="">Florida</a></li>
                    <li><a href="#" class="">Miami</a></li>
                    <li><a href="#" class="">Marseille</a></li>
                    <li><a href="#" class="">Marbella</a></li>
                    <li><a href="#" class="">Benidorm</a></li>
                    <li><a href="#" class="">Sardinia</a></li>
                    <li><a href="#" class="">Tenerife</a></li>
                    <li><a href="#" class="">Majorca</a></li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
                <ul>
                    <li><a href="#" class="">Menorca</a></li>
                    <li><a href="#" class="">Algarve</a></li>
                    <li><a href="#" class="">Dubrovnik</a></li>
                    <li><a href="#" class="">Crete</a></li>
                    <li><a href="#" class="">Corsica</a></li>
                    <li><a href="#" class="">Malaga</a></li>
                    <li><a href="#" class="">Valencia</a></li>
                    <li><a href="#" class="">Alicante</a></li>
                    <li><a href="#" class="">Sicily</a></li>
                    <li><a href="#" class="">Nerja</a></li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
                <ul>
                    <li><a href="#" class="">Kent</a></li>
                    <li><a href="#" class="">Lake District</a></li>
                    <li><a href="#" class="">Devon</a></li>
                    <li><a href="#" class="">Cumbria</a></li>
                    <li><a href="#" class="">Glasgow</a></li>
                    <li><a href="#" class="">Torquay</a></li>
                    <li><a href="#" class="">Penzance</a></li>
                    <li><a href="#" class="">England</a></li>
                    <li><a href="#" class="">Scotland</a></li>
                    <li><a href="#" class="">Wales</a></li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
                <ul>
                    <li><a href="#" class="">Cornwall</a></li>
                    <li><a href="#" class="">Newquay</a></li>
                    <li><a href="#" class="">Norfolk</a></li>
                    <li><a href="#" class="">Falmouth</a></li>
                    <li><a href="#" class="">Looe</a></li>
                </ul>
            </div>
        </div>

        <script>
            $('#location_links a').click(function(e){
                e.preventDefault();
                var search_loc = $(this).text();
                $('input[name=area_postcode]').val(search_loc);
                $('#search_form_home').submit();
            });
        </script>

    </div>
</section>