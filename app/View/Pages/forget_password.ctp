<div class="container">
<br><br>
<form class="form-horizontal" role="form" method="post" action="#" name="basicform" id="basicform">

      <div class="form-group">
      <label class="control-label col-sm-2" for="email">Email:</label>
      <div class="col-sm-10">
        <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
      </div>
    </div>


<div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <input type="submit" class="btn btn-primary" value="Forgot Password" name="forgot_password" />
      </div>
    </div>


</form>



</div>


<script type="text/javascript">
  
  jQuery().ready(function() {

    // validate form on keyup and submit
    var v = jQuery("#basicform").validate({
      rules: {

        email: {
          required: true,
          minlength: 5,
          maxlength: 30
        }

      },
      errorElement: "span",
      errorClass: "help-inline-error",
    });

  

  });
</script>



		</div>
	</div>