<div class="wantedAds form">
<?php echo $this->Form->create('WantedAd'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Wanted Ad'); ?></legend>
	<?php
		echo $this->Form->input('user_id');
		echo $this->Form->input('genders');
		echo $this->Form->input('looking_for');
		echo $this->Form->input('buddy_ups');
		echo $this->Form->input('area_ids');
		echo $this->Form->input('budget');
		echo $this->Form->input('per_week');
		echo $this->Form->input('available_from');
		echo $this->Form->input('min_stay');
		echo $this->Form->input('max_stay');
		echo $this->Form->input('weekly_stay');
		echo $this->Form->input('amenities');
		echo $this->Form->input('age');
		echo $this->Form->input('occupation');
		echo $this->Form->input('dss');
		echo $this->Form->input('smoker');
		echo $this->Form->input('pets');
		echo $this->Form->input('orientation');
		echo $this->Form->input('is_orientation_part');
		echo $this->Form->input('language');
		echo $this->Form->input('nationality');
		echo $this->Form->input('interests');
		echo $this->Form->input('display_name');
		echo $this->Form->input('prefered_gender');
		echo $this->Form->input('prefered_age_min');
		echo $this->Form->input('prefered_age_max');
		echo $this->Form->input('prefered_occupation');
		echo $this->Form->input('prefered_smoking');
		echo $this->Form->input('prefered_pets');
		echo $this->Form->input('prefered_orientation');
		echo $this->Form->input('title');
		echo $this->Form->input('description');
		echo $this->Form->input('telephone');
		echo $this->Form->input('show_telephone');
		echo $this->Form->input('daily_mail');
		echo $this->Form->input('instant_mail');
		echo $this->Form->input('max_mail');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Wanted Ads'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
