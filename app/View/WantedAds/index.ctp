<div class="wantedAds index">
	<h2><?php echo __('Wanted Ads'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('genders'); ?></th>
			<th><?php echo $this->Paginator->sort('looking_for'); ?></th>
			<th><?php echo $this->Paginator->sort('buddy_ups'); ?></th>
			<th><?php echo $this->Paginator->sort('area_ids'); ?></th>
			<th><?php echo $this->Paginator->sort('budget'); ?></th>
			<th><?php echo $this->Paginator->sort('per_week'); ?></th>
			<th><?php echo $this->Paginator->sort('available_from'); ?></th>
			<th><?php echo $this->Paginator->sort('min_stay'); ?></th>
			<th><?php echo $this->Paginator->sort('max_stay'); ?></th>
			<th><?php echo $this->Paginator->sort('weekly_stay'); ?></th>
			<th><?php echo $this->Paginator->sort('amenities'); ?></th>
			<th><?php echo $this->Paginator->sort('age'); ?></th>
			<th><?php echo $this->Paginator->sort('occupation'); ?></th>
			<th><?php echo $this->Paginator->sort('dss'); ?></th>
			<th><?php echo $this->Paginator->sort('smoker'); ?></th>
			<th><?php echo $this->Paginator->sort('pets'); ?></th>
			<th><?php echo $this->Paginator->sort('orientation'); ?></th>
			<th><?php echo $this->Paginator->sort('is_orientation_part'); ?></th>
			<th><?php echo $this->Paginator->sort('language'); ?></th>
			<th><?php echo $this->Paginator->sort('nationality'); ?></th>
			<th><?php echo $this->Paginator->sort('interests'); ?></th>
			<th><?php echo $this->Paginator->sort('display_name'); ?></th>
			<th><?php echo $this->Paginator->sort('prefered_gender'); ?></th>
			<th><?php echo $this->Paginator->sort('prefered_age_min'); ?></th>
			<th><?php echo $this->Paginator->sort('prefered_age_max'); ?></th>
			<th><?php echo $this->Paginator->sort('prefered_occupation'); ?></th>
			<th><?php echo $this->Paginator->sort('prefered_smoking'); ?></th>
			<th><?php echo $this->Paginator->sort('prefered_pets'); ?></th>
			<th><?php echo $this->Paginator->sort('prefered_orientation'); ?></th>
			<th><?php echo $this->Paginator->sort('title'); ?></th>
			<th><?php echo $this->Paginator->sort('description'); ?></th>
			<th><?php echo $this->Paginator->sort('telephone'); ?></th>
			<th><?php echo $this->Paginator->sort('show_telephone'); ?></th>
			<th><?php echo $this->Paginator->sort('daily_mail'); ?></th>
			<th><?php echo $this->Paginator->sort('instant_mail'); ?></th>
			<th><?php echo $this->Paginator->sort('max_mail'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($wantedAds as $wantedAd): ?>
	<tr>
		<td><?php echo h($wantedAd['WantedAd']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($wantedAd['User']['id'], array('controller' => 'users', 'action' => 'view', $wantedAd['User']['id'])); ?>
		</td>
		<td><?php echo h($wantedAd['WantedAd']['genders']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['looking_for']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['buddy_ups']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['area_ids']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['budget']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['per_week']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['available_from']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['min_stay']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['max_stay']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['weekly_stay']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['amenities']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['age']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['occupation']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['dss']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['smoker']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['pets']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['orientation']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['is_orientation_part']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['language']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['nationality']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['interests']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['display_name']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['prefered_gender']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['prefered_age_min']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['prefered_age_max']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['prefered_occupation']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['prefered_smoking']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['prefered_pets']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['prefered_orientation']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['title']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['description']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['telephone']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['show_telephone']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['daily_mail']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['instant_mail']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['max_mail']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['created']); ?>&nbsp;</td>
		<td><?php echo h($wantedAd['WantedAd']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $wantedAd['WantedAd']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $wantedAd['WantedAd']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $wantedAd['WantedAd']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $wantedAd['WantedAd']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Wanted Ad'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
