<script type="text/javascript">
    //fn for from(calender)-to(calender) to show transaction records for a specified records
    $(function () {
        $("#go").on('click', function () {
            var keyword = $.trim($('#keyword').val());
            location.href = ROOT+ 'admin/wanted_ads/index/keyword:' + keyword;
        })
    });
</script>
<?php echo $this->element('admin_menu'); ?>
<div class="index col-md-10 col-sm-10">
	<div class="white">
		<div class="paging pull-right">
	        <ul class="pagination pull-right">
	            <?php
	            $this->Paginator->options(array(
	                'url' => array(
	                    'keyword' => $keyword
	                )
	            ));
	            echo $this->Paginator->prev(' < ', array('tag' => 'li', 'disabledTag' => 'li', 'escape' => false), '<a href="#"> < </a>', array('class' => 'prev disabled', 'tag' => 'li', 'disabledTag' => 'li', 'escape' => false));
	            echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a'));
	            echo $this->Paginator->next(' > ', array('tag' => 'li', 'disabledTag' => 'li', 'escape' => false), '<a href="#"> > </a>', array('class' => 'next disabled', 'tag' => 'li', 'disabledTag' => 'li', 'escape' => false));
	            ?>
	        </ul>
	        <p class="text-right">
	            <?php
	            echo $this->Paginator->counter(array(
	                'format' => __('Showing {:start} to {:end} of {:count} entries')
	            ));
	            ?>
	        </p>
	    </div>
		<h2><?php echo __('Wanted Ads'); ?></h2>
			<div class="form-horizontal">
		        <div class="form-group">
		            <label for="inputEmail3" class="col-sm-1 control-label">Keyword</label>
		            <div class="col-sm-3">
		                <?php echo $this->Form->input('titlex', array('id' => 'keyword', 'label' => false,'class'=>'form-control')); ?>
		            </div>
		            <div class="col-sm-1">
		                <?php
		                echo $this->Form->input('Go', array('id' => 'go','class'=>'btn btn-info', 'type' => 'button', 'label' => false));
		                ?>
		            </div>
		        </div>
		    </div>
		<table cellpadding="0" cellspacing="0" class="table">
		<thead>
		<tr>
				<th><?php echo $this->Paginator->sort('id'); ?></th>
				<th><?php echo $this->Paginator->sort('user_id'); ?></th>
				<th><?php echo $this->Paginator->sort('genders'); ?></th>
				<th><?php echo $this->Paginator->sort('looking_for'); ?></th>
				<th><?php echo $this->Paginator->sort('area_ids'); ?></th>
				<th><?php echo $this->Paginator->sort('display_name'); ?></th>
				<th><?php echo $this->Paginator->sort('telephone'); ?></th>
				<th><?php echo $this->Paginator->sort('title'); ?></th>
				<th><?php echo $this->Paginator->sort('created'); ?></th>
				<th class="actions"><?php echo __('Actions'); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php foreach ($wantedAds as $wantedAd): ?>
		<tr>
			<td><?php echo h($wantedAd['WantedAd']['id']); ?>&nbsp;</td>
			<td>
				<?php echo $this->Html->link($wantedAd['User']['id'], array('controller' => 'users', 'action' => 'view', $wantedAd['User']['id'])); ?>
			</td>
			<td><?php echo h($wantedAd['WantedAd']['genders']); ?>&nbsp;</td>
			<td><?php echo h($wantedAd['WantedAd']['looking_for']); ?>&nbsp;</td>
			<td><?php echo h($wantedAd['WantedAd']['area_ids']); ?>&nbsp;</td>
			<td><?php echo h($wantedAd['WantedAd']['display_name']); ?>&nbsp;</td>
			<td><?php echo h($wantedAd['WantedAd']['telephone']); ?>&nbsp;</td>
			<td><?php echo h($wantedAd['WantedAd']['title']); ?>&nbsp;</td>
			<td><?php echo h($wantedAd['WantedAd']['created']); ?>&nbsp;</td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('action' => 'view', $wantedAd['WantedAd']['id']), array('target'=>'_blank', 'class' => 'btn btn-info')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $wantedAd['WantedAd']['id']), array( 'class' => 'btn btn-info', 'confirm' => __('Are you sure you want to delete # %s?', $wantedAd['WantedAd']['id']))); ?>
			</td>
		</tr>
		<?php endforeach; ?>
		</tbody>
		</table>
	</div>
</div>
