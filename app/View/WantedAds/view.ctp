<div class="wantedAds view">
<h2><?php echo __('Wanted Ad'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($wantedAd['User']['id'], array('controller' => 'users', 'action' => 'view', $wantedAd['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Genders'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['genders']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Looking For'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['looking_for']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Buddy Ups'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['buddy_ups']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Area Ids'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['area_ids']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Budget'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['budget']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Per Week'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['per_week']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Available From'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['available_from']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Min Stay'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['min_stay']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Max Stay'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['max_stay']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Weekly Stay'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['weekly_stay']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Amenities'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['amenities']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Age'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['age']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Occupation'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['occupation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Dss'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['dss']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Smoker'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['smoker']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Pets'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['pets']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Orientation'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['orientation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Orientation Part'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['is_orientation_part']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Language'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['language']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nationality'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['nationality']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Interests'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['interests']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Display Name'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['display_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Prefered Gender'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['prefered_gender']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Prefered Age Min'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['prefered_age_min']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Prefered Age Max'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['prefered_age_max']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Prefered Occupation'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['prefered_occupation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Prefered Smoking'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['prefered_smoking']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Prefered Pets'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['prefered_pets']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Prefered Orientation'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['prefered_orientation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Telephone'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['telephone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Show Telephone'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['show_telephone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Daily Mail'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['daily_mail']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Instant Mail'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['instant_mail']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Max Mail'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['max_mail']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($wantedAd['WantedAd']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Wanted Ad'), array('action' => 'edit', $wantedAd['WantedAd']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Wanted Ad'), array('action' => 'delete', $wantedAd['WantedAd']['id']), array(), __('Are you sure you want to delete # %s?', $wantedAd['WantedAd']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Wanted Ads'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Wanted Ad'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
