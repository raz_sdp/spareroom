<!-- <div id="floatingSocialShare"><div class="top-left"><a title="share with: facebook" class="facebook pop-upper" href="https://www.facebook.com/sharer/sharer.php?u=http://hwselect.com/blog.php&amp;t=Please read blog of HW Search &amp; Selection Ltd"><i class="fa fa-facebook"></i><span class="shareCount">1</span></a><a title="share with: twitter" class="twitter pop-upper" href="https://twitter.com/home?status=http://hwselect.com/blog.php"><i class="m-top5 fa fa-twitter"></i></a><a title="share with: google-plus" class="google-plus pop-upper" href="https://plus.google.com/share?url=http://hwselect.com/blog.php"><i class="m-top5 fa fa-google-plus"></i></a><a title="share with: linkedin" class="linkedin pop-upper" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=http://hwselect.com/blog.php&amp;title=Please read blog of HW Search &amp; Selection Ltd&amp;summary=HW Search &amp; Selection Ltd&amp;source="><i class="m-top5 fa fa-linkedin"></i></a><a title="share with: youtube" class="pinterest pop-upper" href="https://www.youtube.com/watch?v=Dpf3CGpcokQ"><i class="m-top5 fa fa-youtube"></i></a></div></div> -->
<div class="container">
    <h1>Resources for Find me a Room</h1>
    <div class="col-lg-12">
        <div class="all_posts col-lg-10" >
            <?php foreach ($posts as $post) { ?>
            <div class="single_post">
                <h3 class="post_description">
                    <a href="<?php echo $this->webroot; ?>blog/view/<?php echo $post['Post']['id']; ?>">
                        <?php echo $post['Post']['title']; ?>
                    </a>
                </h3>

                <div class="post_info">
                    <div class="post_author">
                        <?php echo $post['Post']['author']; ?>
                    </div>
                    <div class="post_date">
                        <?php echo date_format(date_create($post['Post']['created']), 'd M Y');; ?>
                    </div>
                    <div class="comments">
                        <?php echo count($post['Comment']); ?>
                    </div>
                    <div style="clear:both;">
                    </div>
                </div>

                <div class="featured_img">
                    <?php
                    if (!empty($post['Post']['feature_img'])) {
                        $img_url = $this->
                            Html->
                            url('/files/blog_images/' . $post['Post']['feature_img'], true);
                    } else {
                        $img_url = $this->
                            Html->
                            url('/files/blog_images/default.jpg', true);
                    }
                    ?>
                    <img src="<?php echo $img_url; ?>" width="800px" height="450px">
                </div>
                <div class="post_description  text-justify">

                    <?php
                    $d = $post['Post']['description'];
                    $d = (strlen($d) >
                        350) ? substr($d, 0, 350) . '...' : $d;
                    echo utf8_encode($d);
                    ?>

                </div>
                <div class="read_more btn btn-primary">
                    <a href="<?php echo $this->webroot;?>blog/view/<?php echo $post['Post']['id']; ?>">
                        Read More
                    </a>
                </div>
            </div>
            <?php } ?>
        </div>
        <?php echo $this->element('blog_categories');?>
    </div>

    <p>

        <?php
        echo $this->
            Paginator->
            counter(array(
                'format' =>
                    __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
            ));
        ?>

    </p>

    <div class="paging">
        <?php
        echo $this->
            Paginator->
            prev('
< ' . __('previous'), array(), null, array('class' =>
                    'prev disabled'));
        echo $this->
            Paginator->
            numbers(array('separator' =>
                ''));
        echo $this->
            Paginator->
            next(__('next') . ' >
', array(), null, array('class' =>
                    'next disabled'));
        ?>
    </div>
</div>

<?php echo $this->element('blog_social_share');?>