<?php echo $this->element('admin_menu');?>
<div class="index col-md-10 col-sm-10">
    <div class="white">
	<?php echo $this->Form->create('Post', array('type'=>'file')); ?>
	<fieldset>
		<legend><?php echo __('Admin Add post'); ?></legend>
		<div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Title</label>
            <div class="col-sm-9">
                <?php echo $this->Form->input('title',array('label' => false,'class'=>'form-control')); ?>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Author</label>
            <div class="col-sm-9">
                <?php echo $this->Form->input('author',array('label' => false,'class'=>'form-control')); ?>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Feature Image</label>
            <div class="col-sm-9">
                <?php echo $this->Form->input('feature_img',array('label' => false,'class'=>'form-control', 'type'=>'file')); ?>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Description</label>
            <div class="col-sm-9">
                <?php echo $this->Form->input('description',array('label' => false,'class'=>'form-control')); ?>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Blog Category</label>
            <div class="col-sm-9">
                <?php echo $this->Form->input('blog_category_id',array('label' => false,'class'=>'form-control','options'=>$blog_categories)); ?>
            </div>
        </div>
	</fieldset>
<?php echo $this->Form->end(array('Submit', 'class'=>'btn submit-green s-c')); ?>
</div>
<script type="text/javascript">
    var i_path = "<?php echo $this->webroot;?>img/nicEditorIcons.gif";
</script>
<?php echo $this->Html->script(array('nicEdit')); ?>
<script type="text/javascript">
//<![CDATA[
    bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
//]]>
</script>
