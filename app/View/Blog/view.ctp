<div class="container">
<div class="col-lg-12">
<div class="col-lg-10">
<div class="full_post ">
    <h1 class="post_description"><?php echo $post['Post']['title'];?></h1>
    <div class="post_info">
        <div class="post_author"><?php echo $post['Post']['author'];?></div>
        <div class="post_date"><?php echo date_format(date_create($post['Post']['created']),'d M Y');?></div>
        <div class="comments"><?php echo count($post['Comment']);?></div>
        <div style="clear:both;"></div>
    </div>
    <?php
    if(!empty($post['Post']['feature_img'])){
        $img_url = $this->Html->url('/files/blog_images/'.$post['Post']['feature_img'], true);
    }else{
        $img_url = $this->Html->url('/files/blog_images/default.jpg', true);
    }
    ?>
    <div class="post_body">
        <?php
        if(!empty($post['Post']['feature_img'])){
            $img_url = $this->Html->url('/files/blog_images/'.$post['Post']['feature_img'], true);
        }else{
            $img_url = $this->Html->url('/files/blog_images/default.jpg', true);
        }
        ?>
        <div class="featured_img"><img src="<?php echo $img_url;?>" width="100%"></div>
        <div class="text-justify"><?php echo utf8_encode($post['Post']['description']);?></div>
    </div>
</div>
<br><hr/><br>
<div class="comment_section">    
        <?php foreach($post['Comment'] as $comment) { ?>
            <div class="col-md-2 comment-left"></div>
            <div class="col-md-10 single_comment">                
                <div class="comment_date"><?php echo date_format(date_create($comment['created']),'d M Y');?></div>
                <div class="user_name "><i><strong><?php echo $comment['name'];?></strong></i></div>
                <div class="comment"><?php echo $comment['comment'];?></div>
            </div>
            <hr>
            <div class="clearfix"></div>
        <?php } ?>
        <!-- <h3>Submit a Comment</h3>
        <div class="comment_form">
            <form action="<?php echo $this->webroot;?>blog/comment/<?php echo $post['Post']['id'];?>" method="post">
                <?php if(!empty($user)) { ?>
                    <input type="hidden" name="data[Comment][user_id]" value="<?php echo $user['User']['id'];?>">
                <?php } ?>
                <input type="hidden" >
                <div class="form-group">
                    <label for="exampleInputEmail1">Name</label>                    
                    <input type="text" class="form-control" name="data[Comment][name]" value="<?php if(!empty($user)) echo $user['User']['first_name']." ".$user['User']['last_name'];?>">
                </div>     
                <div class="form-group">
                    <label>Email</label>                    
                    <input type="email" class="form-control" name="data[Comment][email]" value="<?php if(!empty($user)) echo $user['User']['email'];?>">
                </div>   
                <div class="form-group">
                    <label>Comment</label>
                    <textarea name="data[Comment][comment]" class="form-control"></textarea>
                </div>   
                <button type="submit" class="btn btn-default">Submit</button>                                          
            </form>
        </div>   -->
</div>
</div>
<?php echo $this->element('blog_categories');?>
</div>
</div>

<?php echo $this->element('blog_social_share');?>
