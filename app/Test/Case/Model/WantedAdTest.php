<?php
App::uses('WantedAd', 'Model');

/**
 * WantedAd Test Case
 *
 */
class WantedAdTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.wanted_ad',
		'app.user'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->WantedAd = ClassRegistry::init('WantedAd');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->WantedAd);

		parent::tearDown();
	}

}
