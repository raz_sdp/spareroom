<?php
/**
 * WantedAdFixture
 *
 */
class WantedAdFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'user_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'genders' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 1, 'unsigned' => false),
		'looking_for' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'buddy_ups' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'area_ids' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'budget' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => 10, 'unsigned' => false),
		'per_week' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'available_from' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'min_stay' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => 10, 'unsigned' => false),
		'max_stay' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => 10, 'unsigned' => false),
		'weekly_stay' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 1, 'unsigned' => false),
		'amenities' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'age' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 3, 'unsigned' => false),
		'occupation' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'dss' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'smoker' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'pets' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'orientation' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'is_orientation_part' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'language' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'nationality' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'interests' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'display_name' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'prefered_gender' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 1, 'unsigned' => false),
		'prefered_age_min' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 3, 'unsigned' => false),
		'prefered_age_max' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 3, 'unsigned' => false),
		'prefered_occupation' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'prefered_smoking' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'prefered_pets' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'prefered_orientation' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'title' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'description' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'telephone' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'show_telephone' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'daily_mail' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'instant_mail' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'max_mail' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 3, 'unsigned' => false),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'user_id' => 1,
			'genders' => 1,
			'looking_for' => 'Lorem ipsum dolor sit amet',
			'buddy_ups' => 1,
			'area_ids' => 'Lorem ipsum dolor sit amet',
			'budget' => '',
			'per_week' => 1,
			'available_from' => '2015-12-03 06:51:37',
			'min_stay' => '',
			'max_stay' => '',
			'weekly_stay' => 1,
			'amenities' => 'Lorem ipsum dolor sit amet',
			'age' => 1,
			'occupation' => 'Lorem ipsum dolor sit amet',
			'dss' => 1,
			'smoker' => 1,
			'pets' => 1,
			'orientation' => 'Lorem ipsum dolor sit amet',
			'is_orientation_part' => 1,
			'language' => 'Lorem ipsum dolor sit amet',
			'nationality' => 'Lorem ipsum dolor sit amet',
			'interests' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'display_name' => 'Lorem ipsum dolor sit amet',
			'prefered_gender' => 1,
			'prefered_age_min' => 1,
			'prefered_age_max' => 1,
			'prefered_occupation' => 'Lorem ipsum dolor sit amet',
			'prefered_smoking' => 1,
			'prefered_pets' => 1,
			'prefered_orientation' => 'Lorem ipsum dolor sit amet',
			'title' => 'Lorem ipsum dolor sit amet',
			'description' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'telephone' => 'Lorem ipsum dolor ',
			'show_telephone' => 1,
			'daily_mail' => 1,
			'instant_mail' => 1,
			'max_mail' => 1,
			'created' => '2015-12-03 06:51:37',
			'modified' => '2015-12-03 06:51:37'
		),
	);

}
