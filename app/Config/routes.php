<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home_default.ctp)...
 */
	Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));
    Router::connect('/register', array('controller' => 'pages', 'action' => 'display', 'register'));
    Router::connect('/about', array('controller' => 'pages', 'action' => 'display', 'about'));
    Router::connect('/search', array('controller' => 'pages', 'action' => 'display', 'search'));
    Router::connect('/advanced_search', array('controller' => 'pages', 'action' => 'display', 'advanced_search'));
    Router::connect('/browse', array('controller' => 'pages', 'action' => 'display', 'browse'));
    Router::connect('/contact', array('controller' => 'pages', 'action' => 'display', 'contact'));
    Router::connect('/login', array('controller' => 'pages', 'action' => 'display', 'login'));
    Router::connect('/forget_password', array('controller' => 'pages', 'action' => 'display', 'forget_password'));
    Router::connect('/info', array('controller' => 'pages', 'action' => 'display', 'info'));
    Router::connect('/blog', array('controller' => 'blog', 'action' => 'index'));
    Router::connect('/place_ad', array('controller' => 'pages', 'action' => 'display', 'place_ad'));
    Router::connect('/whole_property', array('controller' => 'pages', 'action' => 'display', 'whole_property'));
    Router::connect('/add_option', array('controller' => 'pages', 'action' => 'display', 'add_option'));
    Router::connect('/policy', array('controller' => 'pages', 'action' => 'display', 'policy'));
    Router::connect('/room_wanted', array('controller' => 'pages', 'action' => 'display', 'room_wanted'));
    Router::connect('/room_to_rent', array('controller' => 'pages', 'action' => 'display', 'room_to_rent'));
    Router::connect('/my_account', array('controller' => 'users', 'action' => 'my_account', 'my_account'));
    Router::connect('/ad_full', array('controller' => 'pages', 'action' => 'display', 'ad_full'));
    Router::connect('/confirmation', array('controller' => 'pages', 'action' => 'display', 'confirmation'));
    Router::connect('/edit_ad', array('controller' => 'pages', 'action' => 'display', 'edit_ad'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */

    Router::connect('/admin', array('controller' => 'cms_users', 'action' => 'login' ,'admin'=>true));
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
